﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Support.V7.App;
using Android.Support.V7.Widget;
using Android.Views;
using Exam_App_Native.Droid.Adapter;
using Exam_App_Native.Droid.Adapter.ViewHolder;
using Exam_App_Native.Droid.General;
using Exam_App_Native.Model;

namespace Exam_App_Native.Droid.Activity
{
    [Activity(Label = "@string/teachers_activity_title",ScreenOrientation = ScreenOrientation.Portrait)]
    public class TeachersActivity : ToolBarActivity
    {
        private CancellationTokenSource _teacherToken;
        private View _progressGroup;

        public bool Loading => _progressGroup.Visibility == ViewStates.Visible;

        protected override async void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetViewWithActionBar(Resource.Layout.activity_teachers);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetDisplayShowHomeEnabled(true);

            var recyclerView = FindViewById<RecyclerView>(Resource.Id.recycler_view);
            _progressGroup = FindViewById<View>(Resource.Id.progress_group);

            var layoutManager = new LinearLayoutManager(this, LinearLayoutManager.Vertical, false);
            recyclerView.SetLayoutManager(layoutManager);
            recyclerView.AddItemDecoration(new DividerItemDecoration(this, layoutManager.Orientation));

            _progressGroup.Visibility = ViewStates.Visible;

            try
            {

                _teacherToken = new CancellationTokenSource();
                var teachers = await ExamApp.Instance.GetAllTeachers(_teacherToken);
                var recyclerAdapter =
                    new SimpleRecyclerAdapter<Teacher, TeacherViewHolder>(this, Resource.Layout.teacher_tile, teachers);
                recyclerAdapter.OnItemClicked += teacher =>
                {
                    TeachersDetailsActivity.Open(this,new TeachersDetailsActivity.ViewModel
                    {
                        Teacher = Task.FromResult(teacher)
                    });
                };
                recyclerView.SetAdapter(recyclerAdapter);
            }
            catch (Exception e)
            {
                // ignored
            }
            _progressGroup.Visibility = ViewStates.Gone;

        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            if (Loading)
            {
                _teacherToken.Cancel();
            }
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    OnBackPressed();
                    break;
            }

            return base.OnOptionsItemSelected(item);
        }

        public static void Open(Context context)
        {
            var intent = new Intent(context, typeof(TeachersActivity));

            context.StartActivity(intent);
        }
    }
}