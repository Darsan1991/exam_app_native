﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using Exam_App_Native.Droid.Adapter;
using Exam_App_Native.Droid.Adapter.ViewHolder;
using Exam_App_Native.Droid.General;
using Exam_App_Native.Model;
using Exam_App_Native.Model.ViewModel;

namespace Exam_App_Native.Droid.Activity
{
    [Activity(Label = "ReviewCreatorActivity",ScreenOrientation = ScreenOrientation.Portrait)]
    public class ReviewCreatorActivity : TaskableToolBarActivity
    {
        public const string VIEW_MODEL_KEY = "viewModel";
        public const int START_MENU_ITEM_ID = 10;

        private TextView _noOfCardsTextView;
        private TextView _disciplineTitleTextView;
        private SimpleRecyclerAdapter<IPartialSelectableItem<SubjectProgress>, PartialSelectableViewHolder<SubjectProgress>> _recyclerAdapter;
        private View _progressGroup;
        private ViewModel _viewModel;
        private IMenuItem _startMenuBtn;

        // ReSharper disable once MethodTooLong
        protected override async void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetViewWithActionBar(Resource.Layout.activity_review_creator);
            // Create your application here
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetDisplayShowHomeEnabled(true);

            _progressGroup = FindViewById(Resource.Id.progress_group);
            _disciplineTitleTextView = FindViewById<TextView>(Resource.Id.title_tv);
            _noOfCardsTextView = FindViewById<TextView>(Resource.Id.no_of_cards_tv);
            var recyclerView = FindViewById<RecyclerView>(Resource.Id.recycler_view);
            _recyclerAdapter =
                new SimpleRecyclerAdapter<IPartialSelectableItem<SubjectProgress>, PartialSelectableViewHolder<SubjectProgress>>(this
                    , Resource.Layout.partial_tile_layout);
           
            recyclerView.SetAdapter(_recyclerAdapter);
            recyclerView.SetLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.Vertical,false));
            recyclerView.AddItemDecoration(new DividerItemDecoration(this,LinearLayoutManager.Vertical));


            if (Intent == null)
                throw new Exception("This Activity Cannot be Root.");
            var viewModelKey = Intent.GetStringExtra(VIEW_MODEL_KEY);

            if(string.IsNullOrEmpty(viewModelKey))
                throw new Exception("View Model Cannot be Null");

            _viewModel = Communicator.GetValue<ViewModel>(viewModelKey);

            _disciplineTitleTextView.Text = _viewModel.Discipline.Name;
            _noOfCardsTextView.Text = 0.ToString();
            SupportActionBar.Title = _viewModel.Discipline.Name;
            _progressGroup.Visibility = ViewStates.Visible;

            _disciplineTitleTextView.Click += (sender, args) => ShowReviewNameChangeDialog();

            try
            {

                var subjectProgresses = (await _viewModel.SubjectProgressTask).ToList();
                subjectProgresses.ForEach(progress => progress.State = SelectableState.All);
                _recyclerAdapter.Items = subjectProgresses;
                _recyclerAdapter.OnCreatingViewHolder += holder =>
                {
                    holder.RightArrowClicked +=  HolderOnRightArrowClicked;
                    holder.OnStateChanged += HolderOnStateChanged;
                };
                
                UpdateSelectedCardCount();
            }
            catch (System.OperationCanceledException e)
            {

            }
            catch (ExamAppException e)
            {
                Toast.MakeText(this, e.Message, ToastLength.Short).Show();

            }
            catch (Exception e)
            {
               Toast.MakeText(this,"Something Went Wrong!",ToastLength.Short).Show();
            }

            _progressGroup.Visibility = ViewStates.Gone;
        }

        // ReSharper disable once MethodTooLong
        private void HolderOnRightArrowClicked(PartialSelectableViewHolder<SubjectProgress> partialSelectableViewHolder)
        {
            var item = partialSelectableViewHolder.Item;
            Task<IEnumerable<TopicProgress>> topicTask = null;
            if (item.Data.TopicProgresses == null)
            {
                topicTask = Task.Run(async () => _viewModel.Exam == null
                    ? await ExamApp.Instance.GetTopicProgresses(item.Data.SubjectId)
                    : await ExamApp.Instance.GetTopicProgresses(_viewModel.Exam.Id, item.Data.SubjectId));
            }
            else
            {
                topicTask = Task.FromResult(item.Data.TopicProgresses);
            }

            SimpleRecyclerAdapter<ISelectableItem<TopicProgress>, ToggleViewHolder<TopicProgress>> recyclerAdapter = null;

            SimpleRecyclerViewActivityWithCompleteMenu.Open(this, new SimpleRecyclerViewActivityWithCompleteMenu.ViewModelWithComplete
            {
                Title = item.Title.Length > 15 ? item.Title.Substring(0, 15) : item.Title,
                LayoutManager = new LinearLayoutManager(this, LinearLayoutManager.Vertical, false),
                ItemDecoration = new DividerItemDecoration(this, LinearLayoutManager.Vertical),
                RecyclerAdapter = Task.Run(async () =>
                {

                    var topicProgresses = (await topicTask).ToList();
                    topicProgresses.ForEach(progress =>
                    {
                        if (item.State == SelectableState.All)
                            progress.Selected = true;
                        else if (item.State == SelectableState.None)
                        {
                            progress.Selected = false;
                        }
                    });
                    recyclerAdapter = new SimpleRecyclerAdapter<ISelectableItem<TopicProgress>, ToggleViewHolder<TopicProgress>>(
                         this,
                        Resource.Layout.toggle_tile_layout,
                         topicProgresses);

                    return (RecyclerView.Adapter)recyclerAdapter;
                }),
                CompleteTitle = "Save",
                OnComplete = activity =>
                {
                    item.Data.TopicProgresses =
                        recyclerAdapter.Items.Select(selectableItem => selectableItem.Data).ToList();


                    _recyclerAdapter.NotifyDataSetChanged();

                    UpdateSelectedCardCount();
                },
                QueryTextChange = (sender, args) =>
                {
                    recyclerAdapter.Filter = selectableItem =>
                        selectableItem.Title.ToLower().Contains(args.NewText.ToLower());
                }
            });
        }

        private void HolderOnStateChanged(PartialSelectableViewHolder<SubjectProgress> partialSelectableViewHolder)
        {
            UpdateSelectedCardCount();
        }

        private void UpdateSelectedCardCount()
        {
            var selectedCount = GetSelectedCardCount();
            _noOfCardsTextView.Text = selectedCount.ToString();
        }

        // ReSharper disable once TooManyDeclarations
        private int GetSelectedCardCount()
        {
            return _recyclerAdapter.Items.AllIntoOne<IPartialSelectableItem<SubjectProgress>, int>((item, count) =>
            {
                var subjectProgress = item.Data;

                int val;

                switch (subjectProgress.State)
                {
                    case SelectableState.All:
                        val = subjectProgress.Total;
                        break;
                    case SelectableState.Partial:
                        val = subjectProgress.TopicProgresses?
                                  .Where(progress => progress.Selected)
                                  .Select(progress => progress.Total).Sum() ?? 0;
                        break;
                    case SelectableState.None:
                        val = 0;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }

                count += val;
                return count;
            });
        }

        // ReSharper disable once TooManyDeclarations
        private void ShowReviewNameChangeDialog()
        {
            var builder = new AlertDialog.Builder(this);
            var view = LayoutInflater.Inflate(Resource.Layout.review_name_change_popup, null);
            var editText = view.FindViewById<EditText>(Resource.Id.review_name_et);
            editText.Text = _disciplineTitleTextView.Text;
            var dialog = builder.SetView(view)
                .SetPositiveButton("Ok", async (sender, args) =>
                {
                    if (string.IsNullOrEmpty(editText.Text))
                    {
                        Toast.MakeText(this, "Review name cannot be empty", ToastLength.Long).Show();
                        return;
                    }

                    _disciplineTitleTextView.Text = editText.Text;
                })
                .SetNegativeButton("Cancel", (sender, args) => { })
                // ReSharper disable once TooManyChainedReferences
                .Create();
            dialog.Show();
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            _startMenuBtn = menu.Add(Menu.None,START_MENU_ITEM_ID,Menu.None,GetString(Resource.String.start_text));
            _startMenuBtn.SetShowAsAction(ShowAsAction.Always);
            return base.OnCreateOptionsMenu(menu);
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                   OnBackPressed();
                    return true;

                case START_MENU_ITEM_ID:
                    CheckAndStartTheReview();
                    return true;
            }
            return base.OnOptionsItemSelected(item);
        }

        // ReSharper disable once MethodTooLong
        private  void CheckAndStartTheReview()
        {
            if (GetSelectedCardCount() == 0)
            {
                Toast.MakeText(this, "Please Select Some Cards.", ToastLength.Long).Show();
                return;
            }

            var selectableItems = _recyclerAdapter.Items.ToList();
            var subjects = selectableItems
                .Where(item => item.State == SelectableState.All).ToList();

            var topics = selectableItems.Where(item => item.State == SelectableState.Partial)
                .Select(item => item.Data.TopicProgresses.Where(progress => progress.Selected))
                .AllIntoOne<IEnumerable<TopicProgress>, List<TopicProgress>>((enumerable, list) =>
                {
                    if(list==null)
                        list = new List<TopicProgress>();
                    list.AddRange(enumerable);
                    return list;
                }) ?? new List<TopicProgress>();


            var viewModel = new QuizActivity.ViewModel
            {
                ReviewSetStartUpInfos = Task.Run(async ()=>
                {
                    
                    var result = await ExamApp.Instance
                        .CreateNewReviewSet(subjects.Select(item => item.Data.SubjectId)
                            , topics.Select(progresses => progresses.TopicId),_disciplineTitleTextView.Text);
                    return (IEnumerable<ReviewSetStartUpInfo>)new[] {result};
                })

            };

            QuizActivity.Open(this,viewModel);
            MyTask?.Complete();
        }

        public static void Open(Context context, ViewModel viewModel,MyTask task = null)
        {
            var intent = CreateIntent(context,typeof(ReviewCreatorActivity),task);
            if (viewModel != null)
            {
                var viewModelKey = Communicator.StoreValue(viewModel);
                intent.PutExtra(VIEW_MODEL_KEY, viewModelKey);
            }
            context.StartActivity(intent);
        }

        public class ViewModel
        {
            public Discipline Discipline { get; set; }
            public Task<IEnumerable<SubjectProgress>> SubjectProgressTask { get; set; }
            public CancellationTokenSource TokenSource { get; set; }
            public Exam Exam { get; set; }
        }
    }


   
}