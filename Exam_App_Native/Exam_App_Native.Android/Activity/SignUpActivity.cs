﻿using System;
//using System.ComponentModel.DataAnnotations;
//using System.ComponentModel.DataAnnotations;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Graphics.Drawables;
using Android.OS;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;

namespace Exam_App_Native.Droid.Activity
{
    [Activity(Theme = "@style/MainTheme.NoActionBar",MainLauncher = false,ScreenOrientation = ScreenOrientation.Portrait)]
    public class SignUpActivity : AppCompatActivity
    {
        private EditText _nameEditText;
        private EditText _emailEditText;
        private EditText _passwordEditText;
        private View _progressGroup;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_signup);
            Window.AddFlags(WindowManagerFlags.DrawsSystemBarBackgrounds);
            _nameEditText = FindViewById<EditText>(Resource.Id.name_et);
            _emailEditText = FindViewById<EditText>(Resource.Id.email_et);
            _passwordEditText = FindViewById<EditText>(Resource.Id.password_et);
            _progressGroup = FindViewById(Resource.Id.progress_group);

            FindViewById(Resource.Id.sign_up_btn).Click += (sender, args) => { ValidateAndSignUp(); };
        }

        private async void ValidateAndSignUp()
        {
            string error = null;
//            var addressAttribute = new EmailAddressAttribute();

            if (string.IsNullOrEmpty(_nameEditText.Text) 
                || string.IsNullOrEmpty(_emailEditText.Text) 
                ||string.IsNullOrEmpty(_passwordEditText.Text))
            {
                error = "Some of the Fields are Empty!.";
            }
            else if (!Exam_App_Native.Utils.IsValidEmail(_emailEditText.Text))
            {
                error = "Email is not valid!";
            }
            else if (_passwordEditText.Text.Length < 8)
            {
                error = "Password Length Must be at least 8";
            }

            if (error != null)
            {
                ShowToast(error);
                return;
            }
            ShowProgressOverlay(0.3f);
            try
            {
                //TODO:Try to SignUp
                await Auth.Instance.Regiser(_nameEditText.Text, _emailEditText.Text, _passwordEditText.Text);
                ShowToast("Successfully SignUp!");
                //Have to set the Auth Current User here
                ConfirmationCodeActivity.Open(this);
                Finish();

            }
            catch (NotConfirmedException e)
            {
                ShowToast("This Email Already Registed!.Please Confirm the Code");
                ConfirmationCodeActivity.Open(this);
            }
            catch (Exception e)
            {
                Toast.MakeText(this,e.Message,ToastLength.Long).Show();
            }
            HideProgressOverlay();
        }

        private void ShowToast(string message)
        {
            Toast.MakeText(this, message, ToastLength.Long).Show();
        }

        private void ShowProgressOverlay(float alpha = 1f)
        {
            var color = ((ColorDrawable)_progressGroup.Background).Color;
            color.A = (byte) (alpha*255);
            _progressGroup.SetBackgroundColor(color);
            _progressGroup.Visibility = ViewStates.Visible;
        }

        private void HideProgressOverlay() => _progressGroup.Visibility = ViewStates.Gone;

        public static void Open(Context context)
        {
            var intent = new Intent(context,typeof(SignUpActivity));
            context.StartActivity(intent);
        }
    }
}