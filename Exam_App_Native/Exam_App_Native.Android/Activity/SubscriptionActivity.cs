﻿using System;
using System.Collections.Generic;
using System.Linq;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Graphics;
using Android.OS;
using Android.Support.V7.App;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using Exam_App_Native.Droid.Adapter;
using Exam_App_Native.Droid.Adapter.ViewHolder;
using Exam_App_Native.Droid.Views;

namespace Exam_App_Native.Droid.Activity
{
    [Activity(Label = "@string/subscription_plan_activity_title", MainLauncher = false,ScreenOrientation = ScreenOrientation.Portrait)]
    public partial class SubscriptionActivity : AppCompatActivity
    {
        protected override async void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

           _recyclerData = new List<SubscriptionCardViewHolder.ViewModel>
            {
                new SubscriptionCardViewHolder.ViewModel
                {
                    Content = GetString(Resource.String.no_of_flash_cards_in_each_discipline),
                    LeftCardTitle = "20",
                    RightCardTitle = GetString(Resource.String.unlimited_title).ToUpper(),
                },
                new SubscriptionCardViewHolder.ViewModel
                {
                    Content = GetString(Resource.String.no_of_exams),
                    LeftCardTitle = "1",
                    RightCardTitle = GetString(Resource.String.unlimited_title).ToUpper(),
                },
                new SubscriptionCardViewHolder.ViewModel
                {
                    Content = GetString(Resource.String.no_of_flashcards_in_each_exam).ToUpper(),
                    LeftCardTitle = "100",
                    RightCardTitle = GetString(Resource.String.unlimited_title).ToUpper(),
                },
                new SubscriptionCardViewHolder.ViewModel
                {
                    Content = GetString(Resource.String.price),
                    LeftCardTitle = GetString(Resource.String.free_title).ToUpper(),
                    RightCardTitle = "$19.99",
                },
            };




            SetContentView(Resource.Layout.activity_subscription);
            var toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetDisplayShowHomeEnabled(true);

            var recyclerView = FindViewById<RecyclerView>(Resource.Id.recycler_view);
            var linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.Vertical, false);
            recyclerView.SetLayoutManager(linearLayoutManager);
            recyclerView.AddItemDecoration(new OffsetRecyclerItemDecoration(new Rect(0, 0, 0, 30)));

            var progressGroup = FindViewById(Resource.Id.progress_group);
            progressGroup.Visibility = ViewStates.Visible;
            try
            {
                var plan = await ExamApp.Instance.GetMonthlyPaymentPlanPrice();
                var models = _recyclerData.ToList();
                var viewModel = models[3];
                viewModel.RightCardTitle = plan;
                models[3] = viewModel;

                var simpleRecyclerAdapter =
                    new SimpleRecyclerAdapter<SubscriptionCardViewHolder.ViewModel, SubscriptionCardViewHolder>(this,
                        Resource.Layout.activity_subscription_card, models);

                recyclerView.SetAdapter(simpleRecyclerAdapter);
            }
            catch (Exception e)
            {
                Toast.MakeText(this,"Something went wrong!",ToastLength.Short).Show();
                Finish();
                return;
            }
            progressGroup.Visibility = ViewStates.Gone;


            FindViewById(Resource.Id.img_btn).Click += (sender, args) =>
            {
                PaymentActivity.Open(this);
            };
            Window.AddFlags(WindowManagerFlags.DrawsSystemBarBackgrounds);
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    OnBackPressed();
                    break;
            }

            return base.OnOptionsItemSelected(item);
        }

        public static void Open(Context context)
        {
            context.StartActivity(new Intent(context, typeof(SubscriptionActivity)));
        }
    }

    //Subscription Recycler View Data
    public partial class SubscriptionActivity
    {
        private IEnumerable<SubscriptionCardViewHolder.ViewModel> _recyclerData;

    }
}