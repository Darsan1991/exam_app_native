﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Android.Animation;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Views;
using Android.Widget;
using Exam_App_Native.Droid.Fragment;
using Exam_App_Native.Droid.General;
using Exam_App_Native.Model;
using Java.Lang;
using Exception = System.Exception;

namespace Exam_App_Native.Droid.Activity
{
    [Activity(Label = "",ScreenOrientation = ScreenOrientation.Portrait)]
    public class QuizActivity : ToolBarActivity
    {
        public const string VIEW_MODEL_KEY = "viewModel";
        private const int STOP_MENU_BUTTON_ID = 10;

        private View _quizContainer;
        private View _resultContainer;

        private QuizFragment _quizFragment;
        private ResultFragment _resultFragment;

        private IMenuItem _stopBtn;
        private ViewModel _viewModel;
        private View _progressGroup;
        private List<ReviewSetStartUpInfo> _startUpInfos;
        private ReviewCardInfo _currentReviewCardInfo;

        private int _currentReviewIndex = 0;
        private int _currentQuizIndex = 0;

        protected override async void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetViewWithActionBar(Resource.Layout.activity_quiz);
            SupportActionBar?.SetTitle(Resource.String.quiz_activity_title);
            _quizContainer = FindViewById(Resource.Id.quiz_container);
            _resultContainer = FindViewById(Resource.Id.result_container);
            _progressGroup = FindViewById(Resource.Id.progress_group);

            _quizFragment = new QuizFragment();
            _resultFragment = new ResultFragment();

            SupportFragmentManager.BeginTransaction()
                .Add(Resource.Id.quiz_container, _quizFragment,"quiz").Commit();
            SupportFragmentManager.BeginTransaction()
                .Add(Resource.Id.result_container, _resultFragment,"result").Commit();
//
            
            _quizFragment.OnAnswered +=QuizFragmentOnAnswered;
            _resultFragment.OnClickNextOrComplete +=ResultFragmentOnClickNextOrComplete;

            _resultContainer.Visibility = ViewStates.Gone;

            if(Intent==null)
                throw new Exception("This cannot be root activity.");

            var modelKey = Intent.GetStringExtra(VIEW_MODEL_KEY);
            if (string.IsNullOrEmpty(modelKey))
            {
                throw new NullPointerException("View Model Should be specify");
            }

            _viewModel = Communicator.GetValue<ViewModel>(modelKey);

            _progressGroup.Visibility = ViewStates.Visible;
            try
            {
                _startUpInfos = (await _viewModel.ReviewSetStartUpInfos).ToList();

                if (_startUpInfos.Count == 0)
                {
                    Toast.MakeText(this, "No Review Sets found!", ToastLength.Short).Show();
                    Finish();
                    return;
                }

                var startUpInfo = _startUpInfos[_currentReviewIndex];
                _quizFragment.TotalQuizCount = _startUpInfos.Sum(info => info.CardCount);
                _currentReviewCardInfo = new ReviewCardInfo
                {
                    QuizCard = startUpInfo.FirstCard,
                    ReviewId = startUpInfo.ReviewId,
                    ReviewSetId = startUpInfo.ReviewSetId,

                };
                HideResulAndShowNext(_currentReviewCardInfo.QuizCard,_currentQuizIndex);
                _progressGroup.Visibility = ViewStates.Gone;
            }
            catch (Exception e)
            {
                Toast.MakeText(this,"Something Went Wrong!",ToastLength.Short).Show();
                Finish();
            }

        }

        private void ResultFragmentOnClickNextOrComplete()
        {
            if (_currentReviewCardInfo.QuizCard == null)
            {
                if(_currentReviewIndex == _startUpInfos.Count-1)
                CompleteTheQuiz();
                else
                {
                    _currentQuizIndex++;
                    var startUpInfo = _startUpInfos[++_currentReviewIndex];
                    _currentReviewCardInfo = new ReviewCardInfo
                    {
                        QuizCard = startUpInfo.FirstCard,
                        ReviewId = startUpInfo.ReviewId,
                        ReviewSetId = startUpInfo.ReviewSetId,

                    };
                    _quizFragment.TotalQuizCount = startUpInfo.CardCount;
                    HideResulAndShowNext(_currentReviewCardInfo.QuizCard,_currentQuizIndex);
                }
            }
            else
            {
                _currentQuizIndex++;
                HideResulAndShowNext(_currentReviewCardInfo.QuizCard,_currentQuizIndex);
            }
        }

        public override void OnBackPressed()
        {
//            base.OnBackPressed();
        }

        private void CompleteTheQuiz()
        {
            Finish();
            MainActivity.Instance.SetCurrentTab(MainActivity.TabButton.Reviews);
            ((ReviewsFragment)MainActivity.Instance.GetTab(MainActivity.TabButton.Reviews))
                .LoadOrUpdateReviews(_viewModel.ReviewSetId ??_startUpInfos.First().ReviewSetId);
        }

        private  void QuizFragmentOnAnswered(bool answer,float totalSec)
        {
            var task = Task.Run(async () =>
            {
                var info = await ExamApp.Instance
                    .EvaluateCard(_currentReviewCardInfo.ReviewId
                        , _currentReviewCardInfo.QuizCard.Id, answer,(int)totalSec, _currentReviewCardInfo.HitReviewId,
                        _currentReviewCardInfo.MistakeReviewId);
                info.CurrentCard = _currentReviewCardInfo.QuizCard;
          
                info.IsReviewFinished = info.IsReviewFinished || info.NextCard == null;
                info.NextCard = info.IsReviewFinished ? null : info.NextCard;

                _currentReviewCardInfo.QuizCard = info.NextCard;
                _currentReviewCardInfo.HitReviewId = info.NewHitsReviewId;
                _currentReviewCardInfo.MistakeReviewId = info.NewMistakeReviewId;
                return info;
            });
            ShowResultWithAnimation(task);
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            base.OnCreateOptionsMenu(menu);
            _stopBtn = menu.Add(Menu.None, STOP_MENU_BUTTON_ID, Menu.None, GetString(Resource.String.stop_text));
            _stopBtn.SetShowAsAction(ShowAsAction.Always);
            return true;
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case STOP_MENU_BUTTON_ID:
                    CompleteTheQuiz();
                    break;
            }

            return base.OnOptionsItemSelected(item);
        }

        private void ShowResultWithAnimation(Task<CardEvalResultInfo> result)
        {
            _quizContainer.Enabled = false;
            _resultContainer.Enabled = false;

            var loadAnimator = AnimatorInflater.LoadAnimator(this,Resource.Animator.card_flip_right_out);
            loadAnimator.SetTarget(_quizContainer);
            loadAnimator.AnimationEnd += (sender, args) => _quizContainer.Visibility = ViewStates.Gone;
            loadAnimator.Start();

            _resultContainer.Visibility = ViewStates.Visible;
            _resultContainer.Alpha = 0f;
            
            _resultFragment.SetResult(result,_currentReviewIndex == _startUpInfos.Count-1);

            var resultAniamtor = AnimatorInflater.LoadAnimator(this,Resource.Animator.card_flip_right_in);
            resultAniamtor.SetTarget(_resultContainer);
            resultAniamtor.Start();
        }

        private void HideResulAndShowNext(QuizCard card,int index)
        {
            _resultContainer.Visibility = ViewStates.Gone;
            _quizContainer.RotationY = 0f;
            _quizContainer.Visibility = ViewStates.Visible;
            _quizContainer.Alpha = 1f;
            
            _quizFragment.StartOrGoToNextQuiz(Task.FromResult(card),index);
            
            

        }

        public static Intent Open(Context context, ViewModel viewModel)
        {
            
            var intent = new Intent(context,typeof(QuizActivity));
            if (viewModel != null)
            {
                var key = Communicator.StoreValue(viewModel);
                intent.PutExtra(VIEW_MODEL_KEY, key);
            }
            context.StartActivity(intent);
            return intent;
        }


        public class ViewModel
        {
            public Task<IEnumerable<ReviewSetStartUpInfo>> ReviewSetStartUpInfos { get; set; }
            public int? ReviewSetId { get; set; }
        }


        private struct ReviewCardInfo
        {
            public QuizCard QuizCard { get; set; }
            public int ReviewId { get; set; }
            public int ReviewSetId { get; set; }
            public int? HitReviewId { get; set; }
            public int? MistakeReviewId { get; set; }
        }
        
    }
}