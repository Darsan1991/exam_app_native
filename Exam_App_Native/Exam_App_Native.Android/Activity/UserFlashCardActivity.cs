﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Views;
using Android.Widget;
using Exam_App_Native.Droid.General;
using Exam_App_Native.Model;
using Exam_App_Native.Model.ViewModel;

namespace Exam_App_Native.Droid.Activity
{
    [Activity(Label = "User Flash Card",ScreenOrientation = ScreenOrientation.Portrait)]
    public partial class UserFlashCardActivity : ToolBarActivity
    {
        public static readonly ViewModel DEFAULT_VIEW_MODEL = new ViewModel
        {
            MyCard = null
        };

        public const string VIEW_MODEL_KEY = "viewModel";

        private const int MENU_ITEM_ID = 10;

        private IMenuItem _menuItemBtn;

        private TileRadioGroup _tileRadioGroup;

        private ViewModel _viewModel;
        private View _progressGroup;
        private CancellationTokenSource _tokenSource;
        private TextView _disciplineTextView;
        private TextView _subjectTextView;
        private TextView _topicTextView;
        private TextView _questionEditText;
        private TextView _commentEditText;
        private MyCard _card;

        private MyCard ViewCard
        {
            get
            {
                var card = _card.Clone();
                card.Answer = _tileRadioGroup.Selected.Value;
                card.Content = _questionEditText.Text;
                card.Comment = _commentEditText.Text;
                return card;
            }
        }


        public bool Loading => _progressGroup.Visibility == ViewStates.Visible;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetViewWithActionBar(Resource.Layout.activity_user_flash_card);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetDisplayShowHomeEnabled(true);

            _progressGroup = FindViewById(Resource.Id.progress_group);
            _disciplineTextView = FindViewById<TextView>(Resource.Id.discipline_drop_text);
            _subjectTextView = FindViewById<TextView>(Resource.Id.subject_drop_text);
            _topicTextView = FindViewById<TextView>(Resource.Id.topic_drop_text);
            _questionEditText = FindViewById<TextView>(Resource.Id.question_et);
            _commentEditText = FindViewById<TextView>(Resource.Id.comment_et);

            _disciplineTextView.Click += (sender, args) => { OpenSelectionForDisciplines(_card.Discipline); };
            _subjectTextView.Click += (sender, args) =>
            {
                if (_card.Discipline == null)
                {
                    ShowToast("Select the Discipline First!", ToastLength.Short);
                    return;
                }

                OpenSelectionForSubjects(_card.Discipline, _card.Subject);
            };
            _topicTextView.Click += (sender, args) =>
            {
                if (_card.Subject == null)
                {
                    ShowToast("Select the Discipline,Subject first!", ToastLength.Short);
                    return;
                }

                OpenSelectionForTopics(_card.Subject, _card.Topic);
            };

            _tileRadioGroup = new TileRadioGroup
            {
                RadioButtons = new[]
                {
                    new TileRadioButton
                    {
                        Background = FindViewById(Resource.Id.true_group),
                        RadioButton = FindViewById<RadioButton>(Resource.Id.true_radio_btn),
                        Value = true
                    },
                    new TileRadioButton
                    {
                        Background = FindViewById(Resource.Id.false_group),
                        RadioButton = FindViewById<RadioButton>(Resource.Id.false_radio_btn),
                        Value = false
                    },
                }
            };


            if (Intent == null)
            {
                throw new Exception("This Activity Cannot be root");
            }

            var modelKey = Intent.GetStringExtra(VIEW_MODEL_KEY);

            _viewModel = (string.IsNullOrEmpty(modelKey) ? null : Communicator.GetValue<ViewModel>(modelKey))
                         ?? DEFAULT_VIEW_MODEL;

            _card = _viewModel.MyCard ?? new MyCard();
            UpdateCard(_card);
        }

        private void OpenSelectionForDisciplines(Discipline selected)
        {
            var tokenSource = new CancellationTokenSource();

            // ReSharper disable once ConvertToLocalFunction
            Func<string, Task<IEnumerable<IItem>>> createTask = s =>
            {
                return Task.Run(async () =>
                {
                    //TODO:Will Change to search
                    return (await (string.IsNullOrEmpty(s)
                        ? ExamApp.Instance.GetAllDiscipline(tokenSource)
                        : ExamApp.Instance.GetAllDiscipline(tokenSource))).Select(discipline =>
                        (IItem) new Item {Title = discipline.Name, Data = discipline});
                }, tokenSource.Token);
            };

            RadioSelectableActivity.OpenActivity(this, new RadioSelectableActivity.ViewModel
            {
                Title = GetString(Resource.String.disciplines_text),
                CompleteTitle = GetString(Resource.String.select_text),
                Selected = selected,
                ItemsTask = createTask(null),
                QueryTextChange = (sender, args) =>
                {
                    if (string.IsNullOrEmpty(args.NewText))
                    {
                        ((RadioSelectableActivity) sender).Update(createTask(null), tokenSource, selected);
                    }
                },
                OnComplete = item =>
                {
                    var myCard = ViewCard.Clone();
                    myCard.Discipline = (Discipline) item.Data;
                    myCard.Subject = null;
                    myCard.Topic = null;
                    UpdateCard(myCard);
                    //Update the Card
                }
            });
        }

        private void OpenSelectionForSubjects(Discipline discipline, Subject selected)
        {
            var tokenSource = new CancellationTokenSource();

            // ReSharper disable once ConvertToLocalFunction
            Func<string, Task<IEnumerable<IItem>>> createTask = s =>
            {
                return Task.Run(async () =>
                {
                    //TODO:Will Change to search
                    return (await (string.IsNullOrEmpty(s)
                        ? ExamApp.Instance.GetSubjectsForDiscipline(discipline.Id, tokenSource)
                        : ExamApp.Instance.GetSubjectsForDiscipline(discipline.Id, tokenSource))).Select(subject =>
                        (IItem) new Item {Title = subject.Name, Data = subject});
                }, tokenSource.Token);
            };

            RadioSelectableActivity.OpenActivity(this, new RadioSelectableActivity.ViewModel
            {
                Title = GetString(Resource.String.subjects_text),
                CompleteTitle = GetString(Resource.String.select_text),
                Selected = selected,
                ItemsTask = createTask(null),
                QueryTextChange = (sender, args) =>
                {
                    if (string.IsNullOrEmpty(args.NewText))
                    {
                        ((RadioSelectableActivity) sender).Update(createTask(null), tokenSource, selected);
                    }
                },
                OnComplete = item =>
                {
                    //Update the card
                    var myCard = ViewCard.Clone();
                    myCard.Subject = ((Subject) item.Data);
                    myCard.Topic = null;
                    UpdateCard(myCard);
                }
            });
        }

        private void ShowToast(string message, ToastLength length = ToastLength.Long)
        {
            Toast.MakeText(this, message, length).Show();
        }

        private void OpenSelectionForTopics(Subject subject, Topic selected)
        {
            var tokenSource = new CancellationTokenSource();

            // ReSharper disable once ConvertToLocalFunction
            Func<string, Task<IEnumerable<IItem>>> createTask = s =>
            {
                return Task.Run(async () =>
                {
                    //TODO:Will Change to search
                    return (await (string.IsNullOrEmpty(s)
                        ? ExamApp.Instance.GetTopicsForSubject(subject.Id, tokenSource)
                        : ExamApp.Instance.GetTopicsForSubject(subject.Id, tokenSource))).Select(topic =>
                        (IItem) new Item {Title = topic.Name, Data = topic});
                }, tokenSource.Token);
            };

            RadioSelectableActivity.OpenActivity(this, new RadioSelectableActivity.ViewModel
            {
                Title = GetString(Resource.String.topics_text),
                CompleteTitle = GetString(Resource.String.select_text),
                Selected = selected,
                ItemsTask = createTask(null),
                QueryTextChange = (sender, args) =>
                {
                    if (string.IsNullOrEmpty(args.NewText))
                    {
                        ((RadioSelectableActivity) sender).Update(createTask(null), tokenSource, selected);
                    }
                },
                OnComplete = item =>
                {
                    //Update the card
                    var myCard = ViewCard.Clone();
                    myCard.Topic = ((Topic) item.Data);
                    UpdateCard(myCard);
                }
            });
        }

        private void UpdateCard(MyCard card)
        {
            _card = card;
            _card.Answer = _card.Answer ?? false;
            _tileRadioGroup.Selected =
                _tileRadioGroup.RadioButtons.First(button => button.Value == _card.Answer);
            _disciplineTextView.Text = _card.Discipline?.Name ?? "Select";
            _subjectTextView.Text = _card.Subject?.Name ?? "Select";
            _topicTextView.Text = _card.Topic?.Name ?? "Select";
            _questionEditText.Text = _card.Content;
            _commentEditText.Text = _card.Comment;
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            base.OnCreateOptionsMenu(menu);
            _menuItemBtn = menu.Add(Menu.None, MENU_ITEM_ID, Menu.None, "Save");
            _menuItemBtn.SetShowAsAction(ShowAsAction.Always);

            return true;
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    OnBackPressed();
                    break;

                case MENU_ITEM_ID:

                    CheckAndSave();
                    break;
            }

            return base.OnOptionsItemSelected(item);
        }

        private void CheckAndSave()
        {
            var card = ViewCard;

            string error = null;

            if (card.Discipline == null || card.Subject == null
                                        || card.Topic == null
                                        || card.Answer == null
                                        || string.IsNullOrEmpty(card.Content)
                                        || string.IsNullOrEmpty(card.Comment))
            {
                error = "Some Of the Fields are Empty!";
            }

            if (error != null)
            {
                ShowToast(error, ToastLength.Short);
                return;
            }

            _tokenSource = new CancellationTokenSource();
            var user = Auth.Instance.CurrentUser;
            _viewModel.OnComplete?.Invoke(this, new CompleteEventArgs
            {
                Task = (card.Id == null ? user.CreateNewCard(card) : user.UpdateCard(card))
            });

            Finish();
        }

        private void CancelSave()
        {
            if (Loading)
            {
                _tokenSource.Cancel();
                Toast.MakeText(this, "Save Canceled!", ToastLength.Short).Show();
            }
        }

        public static void Open(Context context, ViewModel viewModel)
        {
            var intent = new Intent(context, typeof(UserFlashCardActivity));
            if (viewModel != null)
            {
                var key = Communicator.StoreValue(viewModel);
                intent.PutExtra(VIEW_MODEL_KEY, key);
            }

            context.StartActivity(intent);
        }
    }


    public partial class UserFlashCardActivity
    {
        public class ViewModel
        {
            public MyCard MyCard { get; set; }
            public EventHandler<CompleteEventArgs> OnComplete { get; set; }
        }

        public class CompleteEventArgs
        {
            public Task Task { get; set; }
            public CancellationTokenSource TokenSource { get; set; }
        }
    }


    public partial class UserFlashCardActivity
    {
        private class TileRadioGroup
        {
            public event EventHandler<TileRadioButton> OnSelectionChanged;
            private readonly List<TileRadioButton> _radioButtons = new List<TileRadioButton>();
            private TileRadioButton _selected;

            public IEnumerable<TileRadioButton> RadioButtons
            {
                get => _radioButtons;
                set
                {
                    if (value == null)
                    {
                        throw new Exception("Null Value not Accepted Or Inplemented");
                    }

                    _radioButtons.Clear();
                    _radioButtons.AddRange(value);
                    _radioButtons.ForEach(button => button.Clicked += RadioButtonClicked);
                }
            }

            private void RadioButtonClicked(object sender, bool b)
            {
                Selected = (TileRadioButton) sender;
            }

            public TileRadioButton Selected
            {
                get => _selected;
                set
                {
                    if (_selected == value)
                    {
                        return;
                    }

                    var radioButton = _radioButtons.FirstOrDefault(button => button.Selected);
                    if (radioButton != null)
                    {
                        radioButton.Selected = false;
                    }

                    value.Selected = true;
                    _selected = value;
                    OnSelectionChanged?.Invoke(this, _selected);
                }
            }
        }

        private class TileRadioButton
        {
            private View _background;
            private bool _selected;
            public event EventHandler<bool> Clicked;

            public View Background
            {
                get => _background;
                set
                {
                    if (_background == value)
                    {
                        return;
                    }

                    if (_background != null)
                    {
                        _background.Click -= BackgroundOnClick;
                    }

                    _background = value;

                    if (_background != null)
                    {
                        _background.Click += BackgroundOnClick;
                    }


                    _background = value;
                }
            }

            private void BackgroundOnClick(object sender, EventArgs eventArgs)
            {
                Clicked?.Invoke(this, Value);
            }

            public bool Selected
            {
                get => _selected;
                set
                {
                    this.RadioButton.Checked = value;
                    _selected = value;
                }
            }

            public RadioButton RadioButton { get; set; }
            public bool Value { get; set; }
        }
    }
}