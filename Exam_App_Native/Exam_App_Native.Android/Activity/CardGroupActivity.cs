﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Graphics;
using Android.OS;
using Android.Support.V7.App;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using Exam_App_Native.Droid.Adapter;
using Exam_App_Native.Droid.General;
using Exam_App_Native.Droid.Views;
using Exam_App_Native.Model.ViewModel;
using SearchView = Android.Support.V7.Widget.SearchView;

namespace Exam_App_Native.Droid.Activity
{
    [Activity(Label = "CardGroupActivity", MainLauncher = false,ScreenOrientation = ScreenOrientation.Portrait)]
    public class CardGroupActivity : AppCompatActivity
    {
        private const string TITLE_KEY = "title";
        private const string SUB_TITLE_KEY = "subtitle";
        private const string CARDS_KEY = "cards";


        protected override async void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_card_group);
            var toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            Window.AddFlags(WindowManagerFlags.DrawsSystemBarBackgrounds);

            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetDisplayShowHomeEnabled(true);

            var recyclerView = FindViewById<RecyclerView>(Resource.Id.recycler_view);
            var recyclerAdapter = new CardRecyclerAdapter<object>(this);
            var gridLayoutManager = new GridLayoutManager(this,2);
            recyclerView.SetLayoutManager(gridLayoutManager);
            recyclerView.AddItemDecoration(new OffsetRecyclerItemDecoration(new Rect(30,30,30,30)));
            recyclerView.SetAdapter(recyclerAdapter);

            var progressBar = FindViewById<Android.Widget.ProgressBar>(Resource.Id.progress_bar);

            

            if (Intent != null)
            {
                var title = Intent.GetStringExtra(TITLE_KEY);
                var subTitle = Intent.GetStringExtra(SUB_TITLE_KEY);
                var cardKey = Intent.GetStringExtra(CARDS_KEY);

                if (!string.IsNullOrEmpty(title))
                {
                    SupportActionBar.Title = title;
                }

                if (!string.IsNullOrEmpty(subTitle))
                {
                    SupportActionBar.Subtitle = subTitle;
                }

                if (!string.IsNullOrEmpty(cardKey))
                {
                    var task = Communicator.GetValue<Task<IEnumerable<ICard<object>>>>(cardKey);
                    progressBar.Visibility = ViewStates.Visible;
                    try
                    {
                        await task;
                        recyclerAdapter.Items = task.Result;
                    }
                    catch (Exception e)
                    {
                        Toast.MakeText(this,"Please check your internet connection",ToastLength.Short).Show();
                        Finish();
                    }
                    progressBar.Visibility = ViewStates.Gone;

                }
            }

        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    OnBackPressed();
                    break;
            }

            return base.OnOptionsItemSelected(item);
        }


        public static void OpenActivity(Context context, string title, string subtitle = null, Task<IEnumerable<ICard<object>>> cards = null)
        {
            var intent = new Intent(context, typeof(CardGroupActivity));
            intent.PutExtra(TITLE_KEY, title);
            intent.PutExtra(SUB_TITLE_KEY, subtitle);
            if (cards != null)
            {
                var key = Communicator.StoreValue(cards);
                intent.PutExtra(CARDS_KEY, key);
            }


            context.StartActivity(intent);
        }

        public class ViewModel
        {
            public  EventHandler<SearchView.QueryTextSubmitEventArgs> QueryTextSubmit { get; set; }
            public  EventHandler<SearchView.QueryTextChangeEventArgs> QueryTextChange { get; set; }

            public string Title { get; set; }
            public string SubTitle { get; set; }
            public RecyclerView.LayoutManager LayoutManager { get; set; }
            public RecyclerView.ItemDecoration ItemDecoration { get; set; }
            public RecyclerView.Adapter RecyclerAdapter { get; set; }
        }
    }
}