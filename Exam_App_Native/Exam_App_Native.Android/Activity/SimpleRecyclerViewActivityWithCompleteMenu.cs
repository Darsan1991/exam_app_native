﻿using System;
using Android.App;
using Android.Content;
using Android.Views;
using Exam_App_Native.Droid.General;

namespace Exam_App_Native.Droid.Activity
{
    [Activity]
    public class SimpleRecyclerViewActivityWithCompleteMenu:SimpleRecyclerViewActivity
    {
        private const int COMPLETE_MENU_BUTTON_ID = 10;


        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            var menuItem = menu.Add(Menu.None, COMPLETE_MENU_BUTTON_ID, Menu.None, ((ViewModelWithComplete) viewModel).CompleteTitle);
            menuItem.SetShowAsAction(ShowAsAction.Always);
            base.OnCreateOptionsMenu(menu);
            return true;
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case COMPLETE_MENU_BUTTON_ID:
                    ((ViewModelWithComplete)viewModel).OnComplete?.Invoke(this);
                    Finish();
                    return true;
            }
            return base.OnOptionsItemSelected(item);
        }

        public class ViewModelWithComplete : SimpleRecyclerViewActivity.ViewModel
        {
            public string CompleteTitle { get; set; }
            public Action<SimpleRecyclerViewActivityWithCompleteMenu> OnComplete { get; set; }
        }

        public new static void Open(Context context, ViewModel viewModel)
        {
            throw new Exception("You have to Use Other Overload method!");
        }

        public static void Open(Context context, ViewModelWithComplete viewModel)
        {
            var intent = new Intent(context, typeof(SimpleRecyclerViewActivityWithCompleteMenu));
            if (viewModel != null)
            {
                var storeValue = Communicator.StoreValue(viewModel, VIEW_MODEL_KEY);
                intent.PutExtra(VIEW_MODEL_KEY, storeValue);
            }
            context.StartActivity(intent);
        }
    }
}