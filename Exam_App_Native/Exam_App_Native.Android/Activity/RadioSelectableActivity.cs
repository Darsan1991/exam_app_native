﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Graphics;
using Android.OS;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using Exam_App_Native.Droid.Adapter;
using Exam_App_Native.Droid.Adapter.ViewHolder;
using Exam_App_Native.Droid.Adapter.ViewHolder.General;
using Exam_App_Native.Droid.General;
using Exam_App_Native.Droid.Views;
using Exam_App_Native.Model.ViewModel;
using SearchView = Android.Support.V7.Widget.SearchView;

namespace Exam_App_Native.Droid.Activity
{
    [Activity(Label = "Radio Selection", MainLauncher = false, Theme = "@style/MainTheme",ScreenOrientation = ScreenOrientation.Portrait)]
    public class RadioSelectableActivity : ToolBarActivity
    {
        public const string COMPLETE_TITLE_KEY = "complete";

        public IItem SelectedItem
        {
            get => _selectedItem;
            private set
            {
                menuButton?.SetEnabled(value != null);
                _selectedItem = value;
            }
        }

        public IEnumerable<IItem> Tiles => _recyclerAdapter.Items;

        private IMenuItem menuButton;
        private const int MENU_BUTTON_ID = 10;

        protected const string VIEW_MODEL_KEY = "tiles";
        private const string LISTENER_KEY = "listener";

        private IItem _selectedItem;
        private string menuButtonTitle;
        private SelectableRecyclerAdapter<IItem, RadioSelectableHolderViewHolder> _recyclerAdapter;

        private Task<IEnumerable<IItem>> _activeTask;
        private CancellationTokenSource _activeTokenSource;
        private View _progressGroup;
        private ViewModel _viewModel;

        protected override async void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetViewWithActionBar(Resource.Layout.activity_radio_selectable);
            SetUpTitles();
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetDisplayShowHomeEnabled(true);
            SupportActionBar.SetDisplayShowTitleEnabled(true);

            var searchBar = FindViewById<SearchView>(Resource.Id.search_bar);
            var recyclerView = FindViewById<RecyclerView>(Resource.Id.recycler_view);
            _recyclerAdapter =
                new SelectableRecyclerAdapter<IItem, RadioSelectableHolderViewHolder>(this,
                    Resource.Layout.radio_tile_layout);
            var layoutManager = new LinearLayoutManager(this, LinearLayoutManager.Vertical, false);
            recyclerView.SetLayoutManager(layoutManager);
//            recyclerView.AddItemDecoration(new OffsetRecyclerItemDecoration(new Rect(30,30,30,30)));
            recyclerView.SetAdapter(_recyclerAdapter);

            _recyclerAdapter.OnSelectionChanged += tile => SelectedItem = tile;
            

            _progressGroup = FindViewById(Resource.Id.progress_group);


            if (Intent == null)
            {
                throw new InvalidOperationException("This Activity Cannot be Root!");
            }

            var key = Intent.GetStringExtra(VIEW_MODEL_KEY);
            _viewModel = Communicator.GetValue<ViewModel>(key);


            SupportActionBar.Title = _viewModel.Title;
            SupportActionBar.Subtitle = _viewModel.SubTitle;

            menuButtonTitle = _viewModel.CompleteTitle ?? "Start";
            menuButton?.SetTitle(menuButtonTitle);

            searchBar.QueryTextChange += (sender, args) => _viewModel.QueryTextChange?.Invoke(this, args);
            searchBar.QueryTextSubmit += (sender, args) => _viewModel.QueryTextSubmit?.Invoke(this, args);


            Update(_viewModel.ItemsTask, new CancellationTokenSource(), _viewModel.Selected);
        }

        public async void Update(Task<IEnumerable<IItem>> task, CancellationTokenSource tokenSource, object selected,
            Action<IEnumerable<IItem>> onComplete = null)
        {
            if (_activeTask != null && (!_activeTask.IsCompleted && !_activeTask.IsFaulted && !_activeTask.IsCanceled))
            {
                _activeTokenSource?.Cancel();
                _activeTokenSource = null;
            }

            _activeTokenSource = tokenSource;
            _activeTask = task;

            _progressGroup.Visibility = ViewStates.Visible;
            try
            {
                var result = (await task).ToList();
                _recyclerAdapter.Items = result;
                var selectedItem = _recyclerAdapter.Items.FirstOrDefault(item => Equals(selected, item.Data));
                _recyclerAdapter.Selected = selectedItem;
                onComplete?.Invoke(result);
            }
            catch (Exception e)
            {
            }

            _progressGroup.Visibility = ViewStates.Gone;
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            base.OnCreateOptionsMenu(menu);

            menuButton = menu.Add(Menu.None, MENU_BUTTON_ID, Menu.First, menuButtonTitle);
//            menuButton.SetVisible(true);
            menuButton.SetShowAsAction(ShowAsAction.Always);
            menuButton.SetEnabled(false);
            return true;
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
//            _completedEventHandler?.Invoke(SelectedItem);
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    OnBackPressed();
                    break;

                case MENU_BUTTON_ID:
                    _viewModel.OnComplete?.Invoke(SelectedItem);
                    Finish();
                    break;
            }

            return base.OnOptionsItemSelected(item);
        }


        public static void OpenActivity(Context context, ViewModel viewModel)
        {
            var intent = new Intent(context, typeof(RadioSelectableActivity));

            if (viewModel != null)
            {
                var key = Communicator.StoreValue(viewModel);
                intent.PutExtra(VIEW_MODEL_KEY, key);
            }

            context.StartActivity(intent);
        }


        public class ViewModel
        {
            public EventHandler<SearchView.QueryTextSubmitEventArgs> QueryTextSubmit { get; set; }
            public EventHandler<SearchView.QueryTextChangeEventArgs> QueryTextChange { get; set; }

            public string Title { get; set; }
            public string SubTitle { get; set; }
            public string CompleteTitle { get; set; }
            public Action<IItem> OnComplete { get; set; }
            public Task<IEnumerable<IItem>> ItemsTask { get; set; }
            public object Selected { get; set; }
        }
    }
}