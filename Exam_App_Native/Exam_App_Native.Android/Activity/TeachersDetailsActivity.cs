﻿using System;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using Exam_App_Native.Droid.Adapter;
using Exam_App_Native.Droid.Adapter.ViewHolder;
using Exam_App_Native.Droid.General;
using Exam_App_Native.Model;
using Exam_App_Native.Model.ViewModel;
using Java.Lang;
using Org.Apache.Http.Client.Params;
using Exception = System.Exception;

namespace Exam_App_Native.Droid.Activity
{
    [Activity(Label = "Teachers",ScreenOrientation = ScreenOrientation.Portrait)]
    public class TeachersDetailsActivity : ToolBarActivity
    {
        public const string VIEW_MODEL_KEY = "viewModel";


        private ViewModel _viewModel;


        protected override async void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetViewWithActionBar(Resource.Layout.activity_teacher_details);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetDisplayShowHomeEnabled(true);


            var progressGroup = FindViewById<View>(Resource.Id.progress_group);
            var progressBarForImage = FindViewById<View>(Resource.Id.progress_bar_for_image);
            var recyclerView = FindViewById<RecyclerView>(Resource.Id.recycler_view);
            var teacherImageView = FindViewById<ImageView>(Resource.Id.image_view);

            progressGroup.Visibility = ViewStates.Visible;

            if (Intent == null)
                throw new Exception("This Cannot be Root Activity");

            var viewModelStoreKey = Intent.GetStringExtra(VIEW_MODEL_KEY);

            if (string.IsNullOrEmpty(viewModelStoreKey))
            {
                throw new NullPointerException("View Model Key Should be set!");
            }

            _viewModel = Communicator.GetValue<ViewModel>(viewModelStoreKey);

            try
            {
               
                var teacher = await _viewModel.Teacher;
                SupportActionBar.Title = teacher.Name;

                progressGroup.Visibility = ViewStates.Gone;

                recyclerView.SetLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.Vertical, false));
                var simpleRecyclerAdapter = new SimpleRecyclerAdapter<IItem<object>, TeacherDetailsCardViewHolder>(this,
                    Resource.Layout.teacher_detals_card_tile, new[]
                    {
                        //Summary
                        new Item<object>
                        {
                            Title = GetString(Resource.String.summary_text),
                            SubTitle = teacher.Summary + (string.IsNullOrEmpty(teacher.SocialMedia)
                                           ? ""
                                           : "\nContact: " + teacher.SocialMedia)
                        },
                        //Education
                        new Item<object>
                        {
                            Title = GetString(Resource.String.education_text),
                            SubTitle = ToBulletLines(
                                teacher.Educations.Select(education =>
                                        education.Year + " - " + education.Description + " - " + education.Institution)
                                    .ToArray())
                        },
                        //Experience
                        new Item<object>
                        {
                            Title = GetString(Resource.String.experience_text),
                            SubTitle = ToBulletLines(
                                teacher.Experiences.Select(experience =>
                                        experience.Year + " - " + experience.Description + " - " + experience.Institution)
                                    .ToArray())
                        },
                    });

                simpleRecyclerAdapter.OnBind += (holder, item) =>
                {
                    var margin = item.Title != GetString(Resource.String.summary_text) ? 40:10;
                    holder.TextMarginLeft = margin;
                };

                recyclerView.SetAdapter(simpleRecyclerAdapter);
               
                progressBarForImage.Visibility = ViewStates.Visible;
                var bitmap = await RemoteFileManager.Instance.DownloadImageAsync(teacher.ImageUrl,"photo_"+teacher.Name);
                teacherImageView.SetImageBitmap(bitmap);
                progressBarForImage.Visibility = ViewStates.Gone;

            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                
            }
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    OnBackPressed();
                    break;
            }
            return base.OnOptionsItemSelected(item);
        }

        private static string ToBulletLines(params string[] lines)
        {
            var str = "";
            for (var i = 0; i < lines.Length; i++)
            {
                str += "\u2022" + lines[i] + ((i < lines.Length - 1) ? "\n" : "");
            }

            return str;
        }


        public static void Open(Context context,ViewModel viewModel)
        {
            var intent = new Intent(context, typeof(TeachersDetailsActivity));
            if(viewModel==null)
                throw new NullPointerException("viewModel Cannot be null");
            var storeValue = Communicator.StoreValue(viewModel);
            intent.PutExtra(VIEW_MODEL_KEY, storeValue);
            context.StartActivity(intent);
        }


        public class ViewModel
        {
            public Task<Teacher> Teacher { get; set; }
        }
    }
}