﻿using System;
using System.Collections.Generic;
using Android.Animation;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Graphics.Drawables;
using Android.OS;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using Xamarin.Facebook;
using Xamarin.Facebook.Login.Widget;
using Object = Java.Lang.Object;

namespace Exam_App_Native.Droid.Activity
{
    [Activity(Theme = "@style/MainTheme.NoActionBar", MainLauncher = false,ScreenOrientation = ScreenOrientation.Portrait)]
    public class LoginActivity : AppCompatActivity, IFacebookCallback
    {
        private TextView _emailEditText;
        private TextView _passwordEditText;
        private View _correctPasswordIndicatorImageView;
        private View _progressGroupView;

        private ValueAnimator _passwordIndicatorAnimator;
        private bool _isPasswordValid;
        private ICallbackManager _callbackManager;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_login);
            Window.AddFlags(WindowManagerFlags.DrawsSystemBarBackgrounds);
            _emailEditText = FindViewById<TextView>(Resource.Id.email_tv);
            _passwordEditText = FindViewById<TextView>(Resource.Id.password_tv);
            _correctPasswordIndicatorImageView = FindViewById(Resource.Id.correct_password_img);
            _progressGroupView = FindViewById(Resource.Id.progress_group);

            _passwordEditText.TextChanged += (sender, args) => ValidatePassword();

            _correctPasswordIndicatorImageView.ScaleX = 0;
            _correctPasswordIndicatorImageView.ScaleY = 0;


            FindViewById(Resource.Id.sign_up_btn).Click += (sender, args) =>
            {
                SignUpActivity.Open(this);
//                Finish();
            };

            FindViewById(Resource.Id.login_btn).Click += (sender, args) => { ValidateAndLogIn(); };

            FindViewById(Resource.Id.forgot_your_password_btn).Click += (sender, args) =>
            {
                ResetPasswordActivity.Open(this);
            };


            var loginButton = FindViewById<LoginButton>(Resource.Id.fblogin);

            loginButton.SetReadPermissions(new List<string> {"public_profile", "email"});
            _callbackManager = CallbackManagerFactory.Create();
            loginButton.RegisterCallback(_callbackManager, this);

            FindViewById(Resource.Id.facebook_btn).Click += (sender, args) =>
            {
                _progressGroupView.Visibility = ViewStates.Visible;
                loginButton.PerformClick();
            };
        }

        protected override async void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);
            _callbackManager.OnActivityResult(requestCode, (int) resultCode, data);

            if (AccessToken.CurrentAccessToken!=null)
            {
                try
                {
                    var tokenString = AccessToken.CurrentAccessToken.Token;
                    await Auth.Instance.LogIn(tokenString);
                    _progressGroupView.Visibility = ViewStates.Gone;
                    OnLogInCompleted();
                }
                catch (Exception e)
                {
                  
                }
            }
            else
            {
                _progressGroupView.Visibility = ViewStates.Gone;
            }
            Console.WriteLine($"Current Access Token:{AccessToken.CurrentAccessToken}");
        }

        private void ValidatePassword()
        {
            var currentValidation = (_passwordEditText.Text?.Length >= 6);
            if (_isPasswordValid != currentValidation)
            {
                if (_passwordIndicatorAnimator?.IsRunning ?? false)
                {
                    _passwordIndicatorAnimator.Cancel();
                }

                _passwordIndicatorAnimator = ValueAnimator.OfFloat(_correctPasswordIndicatorImageView.ScaleX,
                    currentValidation ? 1 : 0);
                _passwordIndicatorAnimator.SetDuration(100);
                _passwordIndicatorAnimator.Update += (sender, args) =>
                {
                    var animatedValue = (float) args.Animation.AnimatedValue;
                    _correctPasswordIndicatorImageView.ScaleX = animatedValue;
                    _correctPasswordIndicatorImageView.ScaleY = animatedValue;
                };
                _passwordIndicatorAnimator.Start();
                _isPasswordValid = currentValidation;
            }
        }

        private async void ValidateAndLogIn()
        {
            string error = null;

            if (!Exam_App_Native.Utils.IsValidEmail(_emailEditText.Text))
            {
                error = "Email Not Valid!";
            }
            else if (_passwordEditText.Text?.Length < 6)
            {
                error = "Password must have atleast 6 characters.";
            }

            if (error != null)
            {
                ShowToast(error);
                return;
            }

            ShowProgressOverlay(0.3f);

            try
            {
                //TODO:Setup to Login
                var user = await Auth.Instance.LogIn(_emailEditText.Text, _passwordEditText.Text, true);
                OnLogInCompleted();
                //                MainActivity.Open(this);
                ShowToast("Successy fully Login", ToastLength.Short);
            }
            catch (NotConfirmedException e)
            {
                ShowToast("The User is already signup but not confirmed");
                ConfirmationCodeActivity.Open(this);
            }
            catch (Exception e)
            {
                ShowToast(e.Message);
            }

            HideProgressOverlay();
            //_progressGroupView.Visibility = ViewStates.Gone;
        }

        private void OnLogInCompleted()
        {
            var intent = new Intent(this, typeof(MainActivity));
            intent.AddFlags(ActivityFlags.ClearTask | ActivityFlags.NewTask);
            StartActivity(intent);
        }

        private void ShowToast(string message, ToastLength length = ToastLength.Long) =>
            Toast.MakeText(this, message, ToastLength.Long).Show();

        private void ShowProgressOverlay(float alpha = 1f)
        {
            var color = ((ColorDrawable) _progressGroupView.Background).Color;
            color.A = (byte) (alpha * 255);
            _progressGroupView.SetBackgroundColor(color);
            _progressGroupView.Visibility = ViewStates.Visible;
        }

        private void HideProgressOverlay() => _progressGroupView.Visibility = ViewStates.Gone;

        public void OnCancel()
        {
        }

        public void OnError(FacebookException error)
        {
            
            ShowToast("Unknown Error in Facebook login!.");
        }

        public void OnSuccess(Object result)
        {
        }
    }
}