﻿using System;
//using System.ComponentModel.DataAnnotations;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Graphics.Drawables;
using Android.OS;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using Exam_App_Native.Model;

namespace Exam_App_Native.Droid.Activity
{
    [Activity(Label = "",Theme = "@style/MainTheme.NoActionBar",MainLauncher = false,ScreenOrientation = ScreenOrientation.Portrait)]
    public class ResetPasswordActivity : AppCompatActivity
    {


        private TextView _emailEditText;
        private View _progressGroup;


        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_reset_password);
            Window.AddFlags(WindowManagerFlags.DrawsSystemBarBackgrounds);
            _emailEditText = FindViewById<TextView>(Resource.Id.email_tv);
            _progressGroup = FindViewById(Resource.Id.progress_group);
            FindViewById(Resource.Id.reset_password_btn).Click += (sender, args) => { ValidateAndResetPassword(); };


        }

        private async void ValidateAndResetPassword()
        {
//            var emailAddressAttribute = new EmailAddressAttribute();
            string error = null;
            if (!Exam_App_Native.Utils.IsValidEmail(_emailEditText.Text??""))
            {
                error = "Email is not valid!";
            }

            if (error != null)
            {
                ShowToast(error);
                return;
            }
            ShowProgressOverlay(0.3f);
            try
            {
                await User.ResetPassword(_emailEditText.Text);
                ShowToast("Check You Email for new password!");
                var intent = new Intent(this,typeof(LoginActivity));
                intent.SetFlags(ActivityFlags.ClearTask | ActivityFlags.NewTask);
                StartActivity(intent);
            }
            catch (Exception e)
            {
                ShowToast(e.Message);
            }
            HideProgressOverlay();
        }

        private void ShowToast(string message) => 
            Toast.MakeText(this, message, ToastLength.Long).Show();

        private void ShowProgressOverlay(float alpha = 1f)
        {
            var color = ((ColorDrawable)_progressGroup.Background).Color;
            color.A = (byte) (alpha * 255);
            _progressGroup.SetBackgroundColor(color);
            _progressGroup.Visibility = ViewStates.Visible;
        }

        private void HideProgressOverlay()
        {
            _progressGroup.Visibility = ViewStates.Gone;
        }

        public static void Open(Context context)
        {
            var intent = new Intent(context,typeof(ResetPasswordActivity));

            context.StartActivity(intent);
        }
    }
}