﻿using System;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Graphics.Drawables;
using Android.OS;
using Android.Views;
using Android.Widget;
using Exam_App_Native.Droid.Views;
using Exam_App_Native.Model;
using AlertDialog = Android.App.AlertDialog;

namespace Exam_App_Native.Droid.Activity
{
    [Activity(Label = "@string/account_activity_title",ScreenOrientation = ScreenOrientation.Portrait)]
    public class AccountActivity : ToolBarActivity
    {
        private const int LOGOUT_BTN_ID = 10;

        private View _progressGroup;
        private Auth _auth = Auth.Instance;
        private ToggleSwitchView _mixTileToggle;
        private ToggleSwitchView _skipToggle;
        private TextView _nameTextView;
        private TextView _emailTextView;
        private TextView _dateTextView;
        private View _subscriptionGroup;
        private Button _advancedBtn;
        private ImageView _cancelBtn;


        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetViewWithActionBar(Resource.Layout.activity_account);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetDisplayShowHomeEnabled(true);

            SupportActionBar.Title = GetString(Resource.String.account_activity_title);

            _progressGroup = FindViewById(Resource.Id.progress_group);
             _mixTileToggle = new ToggleSwitchView(FindViewById<ImageView>(Resource.Id.mix_tiles_toggle_iv));
            _skipToggle = new ToggleSwitchView(FindViewById<ImageView>(Resource.Id.skip_toggle_iv));
            _nameTextView = FindViewById<TextView>(Resource.Id.name_tv);
             _emailTextView = FindViewById<TextView>(Resource.Id.email_tv);
             _dateTextView = FindViewById<TextView>(Resource.Id.date_tv);
            _subscriptionGroup = FindViewById(Resource.Id.subcription_details_group);
            _advancedBtn = FindViewById<Button>(Resource.Id.advanced_btn);
            _cancelBtn = FindViewById<ImageView>(Resource.Id.cancel_btn);


            _nameTextView.Text = _auth.CurrentUser.Name;
            _emailTextView.Text = _auth.CurrentUser.Email;

            UpdateUserUI(_auth.CurrentUser);

            _mixTileToggle.ValueChanged +=async (sender, args) =>
            {
                ShowProgressOverlay(0.3f);
                try
                {
                    await _auth.CurrentUser.UpdateSettings(new UserSettings
                    {
                        MixCards = _mixTileToggle.Checked,
                        SkipFeedback = _auth.CurrentUser.UserSettings.SkipFeedback
                    });
//                    UpdateUserUI(_auth.CurrentUser);
                }
                catch (Exception e)
                {
                    
                }
                HideProgressOverlay();
            };

            _skipToggle.ValueChanged +=async (sender, args) =>
            {
                ShowProgressOverlay(0.3f);
                try
                {
                    await _auth.CurrentUser.UpdateSettings(new UserSettings
                    {
                        MixCards = _auth.CurrentUser.UserSettings.MixCards,
                        SkipFeedback = _skipToggle.Checked
                    });
//                    UpdateUserUI(_auth.CurrentUser);
                }
                catch (Exception e)
                {
                    
                }

                HideProgressOverlay();
            };

            _nameTextView.Click += (sender, args) => { ShowUsernameChangeDialog(); };

            _cancelBtn.Click += async (sender, args) =>
            {
                ShowProgressOverlay(0.3f);
                try
                {
                    //Cancel the Subscription
                    await _auth.CurrentUser.UnSubscribe();
                }
                catch (Exception e)
                {
                       ShowToast("Something went wrong!");
                }

                HideProgressOverlay();
            };

            FindViewById<Button>(Resource.Id.reset_password_btn).Click += (sender, args) =>
            {
                ResetPasswordActivity.Open(this);
            };

            _advancedBtn.Click += (sender, args) =>
            {
                SubscriptionActivity.Open(this);
            };
        }

        protected override void OnResume()
        {
            base.OnResume();
            _auth.CurrentUser.OnUpdateData += UpdateUserUI;
            UpdateUserUI(_auth.CurrentUser);
        }

        protected override void OnPause()
        {
            base.OnPause();
            if(_auth.CurrentUser!=null)
            _auth.CurrentUser.OnUpdateData -= UpdateUserUI;

        }


        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            var menuItem = menu.Add(Menu.None, LOGOUT_BTN_ID, Menu.None, GetString(Resource.String.logout_text));
            menuItem.SetShowAsAction(ShowAsAction.Always);
            return base.OnCreateOptionsMenu(menu);
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    OnBackPressed();
                    break;

                case LOGOUT_BTN_ID:
                    AndroiExamApp.Instance.LogOut();
                    var intent = new Intent(this, typeof(LoginActivity));
                    intent.AddFlags(ActivityFlags.ClearTask | ActivityFlags.NewTask);
                    StartActivity(intent);

                    break;
            }

            return base.OnOptionsItemSelected(item);
        }

        private void UpdateUserUI(User user)
        {
            _nameTextView.Text = user.Name;
            _emailTextView.Text = user.Email;
            if(_mixTileToggle.Checked!=user.UserSettings.MixCards)
            _mixTileToggle.Checked = user.UserSettings.MixCards;
            if (_skipToggle.Checked != user.UserSettings.SkipFeedback)
                _skipToggle.Checked = user.UserSettings.SkipFeedback;

            _subscriptionGroup.Visibility =
                user.Free ? ViewStates.Gone : ViewStates.Visible;
            _cancelBtn.Visibility = user.Subscribed ? ViewStates.Visible : ViewStates.Gone;
            _advancedBtn.Visibility = string.IsNullOrEmpty(user.ExpirationDate) ? ViewStates.Visible : ViewStates.Gone;
            _dateTextView.Text = user.ExpirationDate;
        }

        private void ShowUsernameChangeDialog()
        {
            var builder = new AlertDialog.Builder(this);

            var view = LayoutInflater.Inflate(Resource.Layout.user_name_change_popup, null);
            var editText = view.FindViewById<EditText>(Resource.Id.username_et);
            var dialog = builder.SetView(view)
                .SetPositiveButton("Ok", async (sender, args) =>
                {
                    if (string.IsNullOrEmpty(editText.Text))
                    {
                        Toast.MakeText(this, "Username cannot be empty!", ToastLength.Long).Show();
                        return;
                    }

                    ShowProgressOverlay(0.3f);
                    try
                    {
                        await _auth.CurrentUser.ChangeUserName(editText.Text);
//                        UpdateUserUI(_auth.CurrentUser);
                    }
                    catch (Exception e)
                    {
                        ShowToast("Somthing wen Wrong!");
                    }
                    
                    HideProgressOverlay();
                })
                .SetNegativeButton("Cancel", (sender, args) => { }).Create();
            dialog.Show();
        }

        public static void Open(Context context)
        {
            context.StartActivity(new Intent(context, typeof(AccountActivity)));
        }

        public void ShowToast(string message, ToastLength length = ToastLength.Short)
        {
            Toast.MakeText(this,message,length).Show();
        }

        private void ShowProgressOverlay(float alpha = 1f)
        {
            var color = ((ColorDrawable) _progressGroup.Background).Color;
            color.A = (byte) (alpha * 255);
            _progressGroup.SetBackgroundColor(color);
            _progressGroup.Visibility = ViewStates.Visible;
        }

        private void HideProgressOverlay() => _progressGroup.Visibility = ViewStates.Gone;
    }
}