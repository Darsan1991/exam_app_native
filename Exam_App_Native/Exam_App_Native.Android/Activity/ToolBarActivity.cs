﻿using System;
using Android.Content;
using Android.OS;
using Android.Support.V7.App;
using Android.Views;

namespace Exam_App_Native.Droid.Activity
{

    public class TaskableToolBarActivity : ToolBarActivity
    {
        protected MyTask MyTask { get; private set; }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            if (Intent!=null)
            {
                if (Intent.HasExtra(TaskIntent.TASK_ID_KEY))
                {
                    var id = Intent.GetIntExtra(TaskIntent.TASK_ID_KEY,-1);
                    MyTask = MyTask.GetTaskById(id);
                    MyTask.AddActivity(this);
                }
            }
        }


        protected override void OnDestroy()
        {
            base.OnDestroy();
            MyTask?.RemoveActivity(this);
        }

        public static Intent CreateIntent(Context context, Type type,MyTask task=null)
        {
            var intent = task!=null?new TaskIntent(context,type,task.Id):(!(context is TaskableToolBarActivity barActivity)
                ? new Intent(context, type)
                : new TaskIntent(context, type, barActivity.MyTask.Id));
            return intent;
        }
    }
    
    public class ToolBarActivity : AppCompatActivity
    {
        protected const string TITLE_KEY = "title";
        protected const string SUB_TITLE_KEY = "subtitle";


        protected void SetViewWithActionBar(int id,int? toolBarId=null)
        {
            SetContentView(id);
            var toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(toolBarId??Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            Window.AddFlags(WindowManagerFlags.DrawsSystemBarBackgrounds);
        }

        protected void SetUpTitles()
        {
            if (Intent != null)
            {
                var title = Intent.GetStringExtra(TITLE_KEY);
                var subTitle = Intent.GetStringExtra(SUB_TITLE_KEY);
           

                if (!string.IsNullOrEmpty(title))
                {
                    SupportActionBar.Title = title;
                }

                if (!string.IsNullOrEmpty(subTitle))
                {
                    SupportActionBar.Subtitle = subTitle;
                }
            }
        }

 
    }
}