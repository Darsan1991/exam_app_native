﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Exam_App_Native.Droid.Activity
{
    [Activity(Theme = "@style/MainTheme.NoActionBar", MainLauncher = true,ScreenOrientation = ScreenOrientation.Portrait)]
    public class SplashActivity : ToolBarActivity
    {
        protected override async void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            try
            {
                await Auth.Instance.TryAutoLogIn();
                MainActivity.Open(this);
                Finish();
            }
            catch (Exception e)
            {
                var intent = new Intent(this,typeof(LoginActivity));
                StartActivity(intent);
                Finish();
            }
        }
    }
}