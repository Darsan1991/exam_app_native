﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using Exam_App_Native.Droid.General;
using Java.Lang;
using Exception = System.Exception;
using SearchView = Android.Support.V7.Widget.SearchView;

namespace Exam_App_Native.Droid.Activity
{
    [Activity(ScreenOrientation = ScreenOrientation.Portrait)]
    public class SimpleRecyclerViewActivity : TaskableToolBarActivity
    {
        protected const string VIEW_MODEL_KEY = "viewModel";

        protected ViewModel viewModel;
        protected View progressGroup;

        public bool Loading => progressGroup.Visibility == ViewStates.Visible;

        private Task _activeTask;
        private CancellationTokenSource _actuveTokenSource;
        private Timer _searchBarTimer;

        protected override async void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetViewWithActionBar(Resource.Layout.activity_simple_recycler_view);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetDisplayShowHomeEnabled(true);
            var recyclerView = FindViewById<RecyclerView>(Resource.Id.recycler_view);
            var searchView = FindViewById<SearchView>(Resource.Id.search_bar);
            progressGroup = FindViewById(Resource.Id.progress_group);

            if (Intent == null)
            {
                throw new NullPointerException("The Simple ReyclerView Activity Cannot be Root");
            }

            var viewModelKey = Intent.GetStringExtra(VIEW_MODEL_KEY);
            if (string.IsNullOrEmpty(viewModelKey))
            {
                throw new Exception("This activity should be invoke with ViewModel");
            }

            viewModel = Communicator.GetValue<ViewModel>(viewModelKey);
            viewModel.Instance = this;
            if(viewModel.LayoutManager == null)
                throw new NullPointerException("Layout manager cannot be null");

            SupportActionBar.Title = viewModel.Title;
            SupportActionBar.Subtitle = viewModel.SubTitle;

            searchView.QueryTextChange += (sender, args) =>
            {
                if (_searchBarTimer == null)
                {
                    _searchBarTimer = new System.Threading.Timer(
                        state =>RunOnUiThread(() =>
                        {
                            var eventArgs = new SearchView.QueryTextChangeEventArgs(args.Handled,searchView.Query);
                            viewModel.QueryTextChange?.Invoke(this, eventArgs);
                        })
                        , null
                        , 200
                        , Timeout.Infinite);
                    
                }
                else
                {
                    _searchBarTimer.Change(200, Timeout.Infinite);
                }
            };
            searchView.QueryTextSubmit += (sender, args) => { viewModel.QueryTextSubmit?.Invoke(this, args); };
            searchView.Click += (sender, args) =>
            {
                searchView.Focusable = true;
                searchView.Iconified = false;
            };

            recyclerView.SetLayoutManager(viewModel.LayoutManager);
           
            if(viewModel.ItemDecoration!=null)
                recyclerView.AddItemDecoration(viewModel.ItemDecoration);

            progressGroup.Visibility = ViewStates.Visible;
            _actuveTokenSource = viewModel.TokenSource;
            try
            {
                var adapter = await viewModel.RecyclerAdapter;
                recyclerView.SetAdapter(adapter);
            }
            catch (Exception e)
            {
                Toast.MakeText(this,e.Message,ToastLength.Short).Show();

            }

            progressGroup.Visibility = ViewStates.Gone;

        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    OnBackPressed();
                    break;
            }
            return base.OnOptionsItemSelected(item);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            if (Loading)
            {
                _actuveTokenSource?.Cancel();
            }
        }

        public async void Update(Task<object> task,CancellationTokenSource tokenSource)
        {
            if (!viewModel.RecyclerAdapter?.IsCompleted??true)
            {
                tokenSource?.Cancel();
                return;
            }

            if (_activeTask!=null && (!_activeTask.IsCompleted && !_activeTask.IsCanceled && !_activeTask.IsFaulted))
            {
                _actuveTokenSource?.Cancel();
            }

            _activeTask = task;
            _actuveTokenSource = tokenSource;

            progressGroup.Visibility = ViewStates.Visible;
            try
            {
                var obj = await task;
                viewModel.OnUpdatated?.Invoke(this,obj);
            }
            catch (Exception)
            {
                // ignored
            }

            progressGroup.Visibility = ViewStates.Gone;
        }


        public static void Open(Context context, ViewModel viewModel,MyTask task =null)
        {
            var intent = CreateIntent(context,typeof(SimpleRecyclerViewActivity),task);
            if (viewModel != null)
            {
                var storeValue = Communicator.StoreValue(viewModel,VIEW_MODEL_KEY);
                intent.PutExtra(VIEW_MODEL_KEY, storeValue);
            }
            context.StartActivity(intent);
        }

        public class ViewModel
        {
            public EventHandler<SearchView.QueryTextSubmitEventArgs> QueryTextSubmit { get; set; }
            public EventHandler<SearchView.QueryTextChangeEventArgs> QueryTextChange { get; set; }
            public EventHandler<object> OnUpdatated { get; set; }
            public string Title { get; set; }
            public string SubTitle { get; set; }
            public RecyclerView.LayoutManager LayoutManager { get; set; }
            public RecyclerView.ItemDecoration ItemDecoration { get; set; }
            public Task<RecyclerView.Adapter> RecyclerAdapter { get; set; }
            public CancellationTokenSource TokenSource { get; set; }

            //TODO:Try to solve this different way
            public SimpleRecyclerViewActivity Instance { get; set; }
        }
    }
}