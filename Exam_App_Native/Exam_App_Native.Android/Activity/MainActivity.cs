﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Support.Design.Widget;
using Android.Support.V4.App;
using Android.Support.V4.View;
using Android.Support.V4.Widget;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using Exam_App_Native.Droid.Activity;
using Exam_App_Native.Droid.Fragment;
using Exam_App_Native.Droid.Fragment.Base;
using Exam_App_Native.Droid.Views;
using Exam_App_Native.Model;
using Exception = System.Exception;
using FragmentManager = Android.Support.V4.App.FragmentManager;
using Toolbar = Android.Support.V7.Widget.Toolbar;

namespace Exam_App_Native.Droid
{
    [Activity(Label = "Exam_App_Native",Theme = "@style/MainTheme.NoActionBar",MainLauncher = false,ScreenOrientation = ScreenOrientation.Portrait)]
    public partial class MainActivity : AppCompatActivity
    {
        public static MainActivity Instance { get; private set; }

        private ViewPager _viewPager;
        private MainFragmentPageAdapter _fragmentPageAdapter;
        private TabBarFragment _tabBarFragment;

        private Toolbar _toolbar;
        private DrawerLayout _drawerLayout;
        private NavigationView _navView;
        private TextView _nameTextView;
        private TextView _emailTextView;


        public int SelectedIndex => _viewPager.CurrentItem;

        public Android.Support.V4.App.Fragment Selected => GetTab(GetTabButtonForPosition(SelectedIndex));


        protected override void OnCreate(Bundle savedInstanceState)
        {
            Instance = this;
            //            AppCompatDelegate.CompatVectorFromResourcesEnabled = true;
            base.OnCreate(savedInstanceState);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);
            _toolbar = FindViewById<Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(_toolbar);

            Console.WriteLine("Support Bar:" + SupportActionBar);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            _drawerLayout = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
            _navView = FindViewById<NavigationView>(Resource.Id.nav_view);

            _navView.ItemIconTintList = null;

            _nameTextView = _navView.GetHeaderView(0).FindViewById<TextView>(Resource.Id.name_tv);
            _emailTextView = _navView.GetHeaderView(0).FindViewById<TextView>(Resource.Id.email_tv);

            UpdateUserUI(Auth.Instance.CurrentUser);
            Auth.Instance.CurrentUser.OnUpdateData += UpdateUserUI;

            var toggle = new Android.Support.V7.App.ActionBarDrawerToggle(
                this, _drawerLayout, _toolbar, Resource.String.navigation_drawer_open,
                Resource.String.navigation_drawer_close);

            _drawerLayout.AddDrawerListener(toggle);
            toggle.SyncState();
            _navView.NavigationItemSelected += OnNavigationItemSelected;

            _tabBarFragment = (TabBarFragment)FragmentManager.FindFragmentById(Resource.Id.tab_bar);

            _tabBarFragment.TabBarItems = new List<TabBarItem>()
            {
                new TabBarItem(GetString(Resource.String.exams_text),Resource.Mipmap.exams_icon,0),
                new TabBarItem(GetString(Resource.String.discipline_text),Resource.Mipmap.diciplines_icon,1),
                new TabBarItem(GetString(Resource.String.my_cards_text),Resource.Mipmap.my_cards_icon,2),
                new TabBarItem(GetString(Resource.String.reviews_text),Resource.Mipmap.reviews_icon,3),
                new TabBarItem(GetString(Resource.String.statistics_text),Resource.Mipmap.statistics_icon,4)
            };

            _tabBarFragment.OnTabBarItemSelected +=
                item =>
                {
                    //                    Console.WriteLine("Current Item:"+viewPager.CurrentItem);

                    _viewPager.SetCurrentItem(_tabBarFragment.GetPosition(item), true);
                    //                    Console.WriteLine("Current Item after set:"+viewPager.CurrentItem);
                };

            _tabBarFragment.SelectedPosition = 0;

            _viewPager = FindViewById<ViewPagerAdvanced>(Resource.Id.viewPager);
            ((ViewPagerAdvanced)_viewPager).Interaction = false;
            _fragmentPageAdapter = new MainFragmentPageAdapter(SupportFragmentManager, _viewPager);
            _viewPager.OffscreenPageLimit = 5;
            _viewPager.Adapter = _fragmentPageAdapter;
            _viewPager.AddOnPageChangeListener(this);
            Window.AddFlags(WindowManagerFlags.DrawsSystemBarBackgrounds);
            OnPageSelected(0);
        }

        private void UpdateUserUI(User user)
        {
            _nameTextView.Text = user.Name;
            _emailTextView.Text = user.Email;
        }

        private void OnNavigationItemSelected(object sender, NavigationView.NavigationItemSelectedEventArgs e)
        {
            _drawerLayout.CloseDrawer(GravityCompat.Start);
            switch (e.MenuItem.ItemId)
            {
                case Resource.Id.nav_account:
                    AccountActivity.Open(this);
                    break;

                case Resource.Id.nav_teachers:
                    TeachersActivity.Open(this);
                    break;

                case Resource.Id.nav_plans:
                    SubscriptionActivity.Open(this);
                    break;

                case Resource.Id.nav_logout:
                    AndroiExamApp.Instance.LogOut();
                    var intent = new Intent(this, typeof(LoginActivity));
                    intent.AddFlags(ActivityFlags.ClearTask | ActivityFlags.NewTask);
                    StartActivity(intent);
                    break;
            }
        }

        public void SetCurrentTab(TabButton btn,bool animate=true)
        {
            var i = GetPostionForTabButton(btn);
            _viewPager.SetCurrentItem(i,animate);
        }

        public void SetCurrentTab(int position, bool animate = true)
        {
            _viewPager.SetCurrentItem(position, animate);
        }

        public int GetPostionForTabButton(TabButton btn)
        {
            switch (btn)
            {
                case TabButton.Exam:
                    return 0;
                case TabButton.Diciline:
                    return 1;
                case TabButton.MyCard:
                    return 2;
                case TabButton.Reviews:
                    return 3;
                case TabButton.Statics:
                    return 4;
                default:
                    throw new ArgumentOutOfRangeException(nameof(btn), btn, null);
            }
        }

        public TabButton GetTabButtonForPosition(int position)
        {
            switch (position)
            {
                case 0:
                    return TabButton.Exam;

                case 1:
                    return TabButton.Diciline;

                case 2:
                    return TabButton.MyCard;

                case 3:
                    return TabButton.Reviews;

                case 4:
                    return TabButton.Statics;
            }

            return TabButton.Exam;
        }

        public Android.Support.V4.App.Fragment GetTab(TabButton tabButton) => 
            _fragmentPageAdapter.GetItem(GetPostionForTabButton(tabButton));

        public static void Open(Context context)
        {
            var intent = new Intent(context,typeof(MainActivity));
            context.StartActivity(intent);
        }

        public enum TabButton
        {
            Exam,Diciline,MyCard,Reviews,Statics
        }
    }

    public partial class MainActivity : ViewPager.IOnPageChangeListener
    {
        public void OnPageScrollStateChanged(int state)
        {
            
        }

        public void OnPageScrolled(int position, float positionOffset, int positionOffsetPixels)
        {
        }

        public void OnPageSelected(int position)
        {
            _tabBarFragment.SelectedPosition = position;
            SupportActionBar.Title = _fragmentPageAdapter.GetPageTitle(position);
        }
    }



    //Main Fragment Page Adapter
    public partial class MainActivity     {
        private class MainFragmentPageAdapter : FragmentPagerAdapter,ViewPager.IOnPageChangeListener
        {
            private const float SHOW_NORMALIZED = 0.7f;
            private const float HIDE_NORMALIZED = 0.6f;

            public ViewPager viewPager
            {
                get => _viewPager;
                private set
                {
                    if (_viewPager!=value)
                    {
                        _viewPager?.RemoveOnPageChangeListener(this);
                        _viewPager = value;
                        _viewPager?.AddOnPageChangeListener(this);
                    }

                }
            }


            private int CurrentScrollState
            {
                get => currentScrollState;
                set
                {
                    if (currentScrollState==value)
                    {
                        return;
                    }

                    if (value != ViewPager.ScrollStateDragging)
                        animateState = ViewPagerAnimate.NONE;

                    else
                    {
                        animateState = currentScrollState == ViewPager.ScrollStateIdle
                            ? ViewPagerAnimate.VIA_SELECT
                            : ViewPagerAnimate.VIA_DRAG;
                    }

                    currentScrollState = value;

                }
            }

            private ViewPagerAnimate animateState = ViewPagerAnimate.NONE;
            private readonly List<NameCompactPageFragment> fragmentList = new List<NameCompactPageFragment>();
            private ViewPager _viewPager;
            private int currentScrollState;

            public MainFragmentPageAdapter(FragmentManager fm,ViewPager pager) : base(fm)
            {
                fragmentList.Add(new ExamsFragment());
                fragmentList.Add(new DisciplinesFragment());
                fragmentList.Add(new MyCardsFragment());
                fragmentList.Add(new ReviewsFragment());
                fragmentList.Add(new StatisticsFragment());
                viewPager = pager;
            }

            public override Android.Support.V4.App.Fragment GetItem(int position) => 
                fragmentList[position];

            public override int Count =>fragmentList.Count;

            public void OnPageScrollStateChanged(int state)
            {
                if (viewPager.Adapter!=this)
                {
                    throw new Exception("Wrong Pager");
                }

                currentScrollState = state;
                Console.WriteLine("OnPageScrollStateChanged:"+state);
                
            }

            public new string GetPageTitle(int position)
            {
                return fragmentList[position].Title;
            }

            public void OnPageScrolled(int position, float positionOffset, int positionOffsetPixels)
            {

                var basePage = fragmentList[position];

                if (positionOffset > SHOW_NORMALIZED 
                    &&(currentScrollState==ViewPager.ScrollStateDragging || animateState == ViewPagerAnimate.VIA_DRAG|| viewPager.CurrentItem == position+1))
                {
                    var pageFragment = fragmentList[position+1];
                    if (!pageFragment.showing)
                    {
                        pageFragment.OnPageAppear();
                    }

                   
                }
                if (basePage.showing && positionOffset > HIDE_NORMALIZED)
                {
                    basePage.OnPageDisAppear();
                }

                if (positionOffset< 1 - HIDE_NORMALIZED )
                {

                    if (position < fragmentList.Count - 1 && fragmentList[position+1].showing)
                    {
                        fragmentList[position+1].OnPageDisAppear();
                    }

                 
                }

                if (positionOffset < 1 - SHOW_NORMALIZED && !basePage.showing 
                    && (currentScrollState == ViewPager.ScrollStateDragging || animateState == ViewPagerAnimate.VIA_DRAG || viewPager.CurrentItem == position))
                {
                    basePage.OnPageAppear();
                }
            }

            public void OnPageSelected(int position)
            {
                
            }

            public enum ViewPagerAnimate
            {
                NONE,VIA_DRAG,VIA_SELECT
            }

          
        }
    }
}