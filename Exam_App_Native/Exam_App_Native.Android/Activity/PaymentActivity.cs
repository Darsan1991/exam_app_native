﻿using System;
using System.Collections.Generic;
using System.Linq;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.OS;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using Com.Stripe.Android;
using Com.Stripe.Android.Model;
using Exam_App_Native.Droid.Adapter;
using Exam_App_Native.Droid.Adapter.ViewHolder;
using Exam_App_Native.Droid.Views;
using Exam_App_Native.Model;
using Exam_App_Native.Model.ViewModel;
using Java.Lang;
using Exception = System.Exception;
using Math = System.Math;

namespace Exam_App_Native.Droid.Activity
{
    [Activity(Label = "@string/payment_activity_title",ScreenOrientation = ScreenOrientation.Portrait)]
    public partial class PaymentActivity:ToolBarActivity
    {
        private const string PUBLISH_STRIPE_KEY = "pk_test_mJf4KS1A3f0694WniMD5kbX5";

        private ToggleSwitchView _autoRenewalToggle;
        private EditText _cardHolderNameEditText;
        private EditText _cardHolderNoEditText;
        private EditText _cardExpireDateEditText;
        private EditText _bankSlipCpfEditText;
        private EditText _creditCardCpfEditText;
        private EditText _bankSlipCodeEditText;
        private View _progressGroup;
        private EditText _cardCcvEditText;
        private SelectableRecyclerAdapter<IItem, RadioSelectableHolderViewHolder> _adapter;

        private Auth Auth => Auth.Instance;
        private PaymentPlan SelectedPlan => _adapter.Selected?.Data as PaymentPlan;

        protected override async void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetViewWithActionBar(Resource.Layout.activity_payment);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetDisplayShowHomeEnabled(true);

            var recyclerView = FindViewById<RecyclerView>(Resource.Id.recycler_view);
            _progressGroup = FindViewById(Resource.Id.progress_group);

            _autoRenewalToggle = new ToggleSwitchView(FindViewById<ImageView>(Resource.Id.auto_renewal_toggle_iv));
            _cardHolderNameEditText = FindViewById<EditText>(Resource.Id.card_holder_et);
            _cardHolderNoEditText = FindViewById<EditText>(Resource.Id.card_number_et);
            _cardExpireDateEditText = FindViewById<EditText>(Resource.Id.expire_date_et);
            _cardCcvEditText = FindViewById<EditText>(Resource.Id.ccv_et);
            _creditCardCpfEditText = FindViewById<EditText>(Resource.Id.credit_card_cpf_et);

            _bankSlipCpfEditText = FindViewById<EditText>(Resource.Id.bank_slip_cpf_et);
            _bankSlipCodeEditText = FindViewById<EditText>(Resource.Id.bank_slip_code_et);

            FindViewById(Resource.Id.generate_bank_slip_btn).Click += async (sender, args) =>
            {
                ValidateAndGenerateBankSlip();

            };

            FindViewById(Resource.Id.check_out_btn).Click += (sender, args) =>
            {
                ValidateAndPayThroughCard();
            };

            FindViewById(Resource.Id.send_email_btn).Click +=async (sender, args) => { ValidateAndSendMail(); };

            FindViewById(Resource.Id.copy_code_btn).Click += (sender, args) => { ValidateAndCopyCode(); };

            ShowProgressOverlay();
            try
            {
                var plans = await ExamApp.Instance.GetPaymentPlans();

                _adapter = new SelectableRecyclerAdapter<IItem, RadioSelectableHolderViewHolder>(this, Resource.Layout.payment_options_radio_tile, plans.Select(
                    plan => new Item
                    {
                        Title = plan.Name,
                        SubTitle = plan.Amount,
                        Data = plan
                    }));
                recyclerView.SetLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.Vertical, false));
                _adapter.OnCreatingViewHolder += holder =>
                {
                    var height = recyclerView.MeasuredHeight;
                    var parameters = holder.ItemView.LayoutParameters;
                    parameters.Height =  Math.Max(Math.Min(height / 4, Utils.Utils.DpToPx(45)),Utils.Utils.DpToPx(40));
                    holder.ItemView.LayoutParameters = parameters;
                };
                recyclerView.SetAdapter(_adapter);

                _adapter.Selected = _adapter.Items.First();
            }
            catch (Exception e)
            {
                Toast.MakeText(this,"Some thing went wrong!",ToastLength.Short).Show();
                Finish();
                return;
            }

            HideProgressOverlay();

            var segmentGroup = new SegmentGroup
            {
                Segments = new []
                {
                    new SegmentCtrl
                    {
                        Button = FindViewById<Button>(Resource.Id.credit_card_segment_btn),
                        View = FindViewById(Resource.Id.credit_card_group),
                    },
                    new SegmentCtrl
                    {
                        Button = FindViewById<Button>(Resource.Id.bank_slips_segment_btn),
                        View = FindViewById(Resource.Id.bank_slip_group)
                    }
                }
            };
            segmentGroup.Segments.ForEach(ctrl => ctrl.Selected = false);
            segmentGroup.Selected = segmentGroup.Segments.First();
        }

        private void ValidateAndCopyCode()
        {
            string error = null;
            if (string.IsNullOrWhiteSpace(_bankSlipCodeEditText.Text))
            {
                error = "Please Generate BankSlip Code before Copy!.";
            }

            if (error != null)
            {
                ShowToast(error);
                return;
            }

            var clipboardManager = (ClipboardManager)GetSystemService(ClipboardService);

            var data = ClipData.NewPlainText("text",_bankSlipCodeEditText.Text);
            clipboardManager.PrimaryClip = data;
            ShowToast("Copied to Clipboard!");
        }

        private async void ValidateAndSendMail()
        {
            string error = null;

            if (string.IsNullOrWhiteSpace(_bankSlipCodeEditText.Text))
            {
                error = "Please Generate BankSlipCode before email";
            }

            if (error != null)
            {
                ShowToast(error);
                return;
            }
            ShowProgressOverlay(0.3f);
            try
            {
                var messageAndResult = await Auth.CurrentUser.SendBankSlipCodeToEmail();
                ShowToast(messageAndResult.Message);
            }
            catch (Exception e)
            {
                ShowToast("Something went wrong!");
            }

            HideProgressOverlay();
        }

        private async void ValidateAndGenerateBankSlip()
        {

            string error = null;

            if (string.IsNullOrWhiteSpace(_bankSlipCpfEditText.Text))
            {
                error = "Some of the Field are Empty!";
            }

            if (error != null)
            {
                ShowToast(error);
                return;
            }

            ShowProgressOverlay(0.3f);

            try
            {
                var bankSlip = await  Auth.CurrentUser.GenerateBankSlip(SelectedPlan.Id, _bankSlipCpfEditText.Text);
                _bankSlipCodeEditText.Text = bankSlip.Code;
            }
            catch (Exception e)
            {
                ShowToast("Something went wrong!");
            }

            HideProgressOverlay();
        }

        private void ValidateAndPayThroughCard()
        {
            var splits = _cardExpireDateEditText.Text.Split('/');

            string error = null;
            int year = 0;
            int month = 0;

            if (Auth.Instance.CurrentUser.Subscribed)
            {
                error = "Already Subscribed.Please UnSubscribed Current Before Order again!";
            }
            else if(string.IsNullOrWhiteSpace(_cardHolderNameEditText.Text) || string.IsNullOrWhiteSpace(_cardHolderNoEditText.Text) || string.IsNullOrWhiteSpace(_cardCcvEditText.Text) || string.IsNullOrWhiteSpace(_cardExpireDateEditText.Text) || string.IsNullOrWhiteSpace(_creditCardCpfEditText.Text))
            {
                error = "Some of the field are Empty!";
            }
            else if(_cardHolderNoEditText.Text.Length != 13 && _cardHolderNoEditText.Text.Length != 16)
            {
                error = "The Card no is Wrong";
            }
            else if (splits.Length != 2)
            {
                error = "Invalid Date Format";
            }
            else if (!int.TryParse(splits[0], out  month))
            {
                error = "The Month Cannot be Detected";
            }
            else if (!int.TryParse(splits[1], out  year))
            {
                error = "The Year Cannot be Detected";
            }

            if (error != null)
            {
                ShowToast(error);
                return;
            }

            var stripe = new Stripe(this,PUBLISH_STRIPE_KEY);
            ShowProgressOverlay(0.3f);
            stripe.CreateToken(new Com.Stripe.Android.Model.Card(_cardHolderNoEditText.Text,new Integer(month),new Integer(year),_cardCcvEditText.Text),new TokenCallback(
                async token =>
                {

                    try
                    {
                        await Auth.Instance.CurrentUser.Subscribe(((PaymentPlan) _adapter.Selected.Data).Id, token.Id,
                            _creditCardCpfEditText.Text, _autoRenewalToggle.Checked);
                        Finish();
                    }
                    catch (Exception e)
                    {
                        ShowToast(e.Message);
                    }

                    HideProgressOverlay();
                }, (e) =>
                {
                    HideProgressOverlay();
                    ShowToast(e.Message);
                }));
        }

        private void ShowToast(string message, ToastLength length = ToastLength.Short)
        {
            Toast.MakeText(this,message,length).Show();
        }

        private class TokenCallback:Java.Lang.Object,ITokenCallback
        {
            private readonly Action<Token> _onSuccess;
            private readonly Action<Exception> _onFailure;

            public void OnError(Java.Lang.Exception p0)
            {
                _onFailure?.Invoke(new Exception(p0.Message));
            }

            public void OnSuccess(Token p0)
            {
                _onSuccess?.Invoke(p0);
            }

            public TokenCallback(Action<Token> onSuccess,Action<Exception> onFailure=null)
            {
                _onSuccess = onSuccess;
                _onFailure = onFailure;
            }
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    OnBackPressed();
                    return true;
            }
            return base.OnOptionsItemSelected(item);
        }

        private void ShowProgressOverlay(float alpha = 1f)
        {
            var color = ((ColorDrawable)_progressGroup.Background).Color;
            color.A = (byte)(alpha * 255);
            _progressGroup.SetBackgroundColor(color);
            _progressGroup.Visibility = ViewStates.Visible;
        }

        private void HideProgressOverlay()
        {
            _progressGroup.Visibility = ViewStates.Gone;
        }

        public static void Open(Context context)
        {
            var intent = new Intent(context,typeof(PaymentActivity));
            context.StartActivity(intent);
        }

    }

    public partial class PaymentActivity
    {
        public class SegmentCtrl
        {
            private Button _button;
            private bool _selected;
            public event Action<SegmentCtrl> Clicked;

            public Button Button
            {
                get => _button;
                set
                {
                    if (_button==value)
                    {
                        return;
                    }

                    if (_button!=null)
                    {
                        _button.Click -= ButtonOnClick;
                    }

                    _button = value;

                    if (_button!=null)
                    {
                        _button.Click += ButtonOnClick;
                    }
                }
            }


            public View View { get; set; }

            public bool Selected
            {
                get => _selected;
                set
                {
                    View.Visibility = value ? ViewStates.Visible : ViewStates.Gone;

                    if (value)
                    {
                        Button.Background = new ColorDrawable(Color.ParseColor("#3374b9"));
                        Button.SetTextColor(Color.White);
                    }
                    else
                    {
                        Button.SetBackgroundResource(Resource.Drawable.payment_segment_button_border);
                        Button.SetTextColor(Color.ParseColor("#3374b9"));
                    }

                    _selected = value;
                }
            }

            private void ButtonOnClick(object sender, EventArgs eventArgs)
            {
                Clicked?.Invoke(this);
            }

        }

        public class SegmentGroup
        {
            private readonly List<SegmentCtrl> _segments = new List<SegmentCtrl>();
            private SegmentCtrl _selected;

            public IEnumerable<SegmentCtrl> Segments
            {
                get => _segments;
                set
                {
                    _segments.ForEach(ctrl => ctrl.Clicked -= CtrlOnClicked);
                    _segments.Clear();
                    _segments.AddRange(value);
                    _segments.ForEach(ctrl => ctrl.Clicked += CtrlOnClicked);
                }
            }


            public SegmentCtrl Selected
            {
                get => _selected;
                set
                {
                    if (_selected == value)
                    {
                        return;
                    }
                    if (_selected!=null)
                    {
                        _selected.Selected = false;
                    }
                    _selected = value;

                    if (_selected != null)
                    {
                        _selected.Selected = true;
                    }
                }
            }

            private void CtrlOnClicked(SegmentCtrl segmentCtrl)
            {
                Selected = segmentCtrl;
            }
        }
    }
}