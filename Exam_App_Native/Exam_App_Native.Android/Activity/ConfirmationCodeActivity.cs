﻿using System;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Graphics.Drawables;
using Android.OS;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;

namespace Exam_App_Native.Droid.Activity
{
    [Activity(Theme = "@style/MainTheme.NoActionBar",MainLauncher = false,ScreenOrientation = ScreenOrientation.Portrait)]
    public class ConfirmationCodeActivity : AppCompatActivity
    {
        private const string EMAIL_KEY = "email";

        private EditText _codeEditText;
        private View _progressGroup;
//        private string _email;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_confirm_code);
            _codeEditText = FindViewById<EditText>(Resource.Id.confirmation_code_et);
            _progressGroup = FindViewById(Resource.Id.progress_group);



            FindViewById(Resource.Id.confirm_btn).Click += (sender, args) => { ValidateAndConfirmCode(); };
            FindViewById(Resource.Id.resend_btn).Click += async (sender, args) =>
            {
                ShowProgressOverlay(0.3f);
                try
                {
                    await Auth.Instance.CurrentUser.ResendToken();

                }
                catch (Exception e)
                {
                    Toast.MakeText(this,e.Message,ToastLength.Short).Show();
                }
                HideProgressOverlay();
               
            };

//            if (Intent!=null)
//            {
//                _email = Intent.GetStringExtra(EMAIL_KEY);
//            }
        }

        private async void ValidateAndConfirmCode()
        {
            string error = null;
            if (string.IsNullOrEmpty(_codeEditText.Text))
            {
                error = "Code Cannot be empty";
                
            }

            if (error != null)
            {
                ShowToast(error);
                return;
            }
            ShowProgressOverlay(0.3f);
            try
            {
                await Auth.Instance.CurrentUser.ConfirmEmail(_codeEditText.Text);
                ShowToast("Confirmed Successfully");

                var intent = new Intent(this,typeof(MainActivity));
                intent.AddFlags(ActivityFlags.ClearTask | ActivityFlags.NewTask);
                StartActivity(intent);
            }
            catch (Exception e)
            {
                ShowToast(e.Message);
            }
            HideProgressOverlay();
        }


        private void ShowToast(string message,ToastLength length=ToastLength.Long)
        {
            Toast.MakeText(this,message,length).Show();
        }

        private void ShowProgressOverlay(float alpha = 1f)
        {
            var color = ((ColorDrawable)_progressGroup.Background).Color;
            color.A = (byte) (alpha * 255);
            _progressGroup.SetBackgroundColor(color);
            _progressGroup.Visibility = ViewStates.Visible;
        }

        private void HideProgressOverlay() => _progressGroup.Visibility = ViewStates.Gone;

        public static void Open(Context context)
        {
            var intent = new Intent(context,typeof(ConfirmationCodeActivity));
            context.StartActivity(intent);
        }

    }
}