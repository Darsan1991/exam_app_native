﻿using System;
using System.Collections.Generic;
using System.Linq;
using Android.App;
using Android.Content;
using Android.Preferences;
using Android.Runtime;
using Xamarin.Facebook;
using Xamarin.Facebook.Login;

namespace Exam_App_Native.Droid
{
    [Application]
    public class AndroiExamApp : Application
    {
        public const float CARD_WIDTH_IN_DP = 150;
        public static AndroiExamApp Instance { get; private set; }

        protected AndroiExamApp(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
        {
            
            Instance = this;
        }

        public override void OnCreate()
        {
            base.OnCreate();
            var defaultSharedPreferences = PreferenceManager.GetDefaultSharedPreferences(this);
            ExamApp.Instance.SetUserPrefs(new UserPrefs(defaultSharedPreferences));
        }

        public void LogOut()
        {
            if (AccessToken.CurrentAccessToken!=null)
            {
               LoginManager.Instance.LogOut();
            }
            Auth.Instance.LogOut();
        }
    }

    public class MyTask
    {

        private static readonly List<MyTask> _taskList = new List<MyTask>();

        public event Action<MyTask> OnComplete; 
        public int Id { get;}

        private readonly List<Android.App.Activity> _activityList = new List<Android.App.Activity>();

        public MyTask(int id)
        {
            Id = id;
        }

        public void Complete()
        {
            _activityList.ForEach(activity => activity.Finish());
            OnComplete?.Invoke(this);
        }

        public void AddActivity(Android.App.Activity activity)
        {
            _activityList.Add(activity);
        }

        public void RemoveActivity(Android.App.Activity activity)
        {
            _activityList.Remove(activity);
        }

        public static int GetUniqueId() =>_taskList.Count>0? 
            _taskList.Select(task => task.Id).Max():1;

        public static MyTask Create()
        {
            var task = new MyTask(GetUniqueId());
            _taskList.Add(task);
            task.OnComplete += myTask => _taskList.Remove(myTask);
            return task;
        }

        public static MyTask GetTaskById(int id) => 
            _taskList.Find(task => task.Id == id);

    }

    public sealed class TaskIntent : Intent
    {


        public const string TASK_ID_KEY = "task";

        public TaskIntent(Context packageContext, Type type,int taskId) : base(packageContext, type)
        {
            PutExtra(TASK_ID_KEY, taskId);
        }

    }

    public class UserPrefs : IUserPrefs
    {
        private readonly ISharedPreferences _sharedPreferences;

        public UserPrefs(ISharedPreferences sharedPreferences)
        {
            _sharedPreferences = sharedPreferences;
        }

        public void SetString(string key, string value)
        {
            var sharedPreferencesEditor = _sharedPreferences.Edit();
            sharedPreferencesEditor.PutString(key, value);
            sharedPreferencesEditor.Apply();
            sharedPreferencesEditor.Commit();
            
        }

        public string GetString(string key, string defValue = "") => _sharedPreferences.GetString(key, defValue);

        public void SetInt(string key, int value)
        {
            var sharedPreferencesEditor = _sharedPreferences.Edit();
            sharedPreferencesEditor.PutInt(key, value);
            sharedPreferencesEditor.Apply();
           
            sharedPreferencesEditor.Commit();

        }

        public int GetInt(string key, int defValue = 0) => _sharedPreferences.GetInt(key, defValue);

        public void SetFloat(string key, float value)
        {
            var sharedPreferencesEditor = _sharedPreferences.Edit();
            sharedPreferencesEditor.PutFloat(key, value);
            sharedPreferencesEditor.Apply();
            sharedPreferencesEditor.Commit();

        }

        public void DeleteKey(string key)
        {
            var sharedPreferencesEditor = _sharedPreferences.Edit();
            sharedPreferencesEditor.Remove(key);
            sharedPreferencesEditor.Apply();
            sharedPreferencesEditor.Commit();
        }

        public float GetFloat(string key, float defValue = 0) => _sharedPreferences.GetFloat(key, defValue);
    }


}