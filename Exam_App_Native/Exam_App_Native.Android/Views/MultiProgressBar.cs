﻿using System;
using System.Collections.Generic;
using System.Linq;
using Android.Animation;
using Android.Content;
using Android.Graphics;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Java.Lang;
using Exception = System.Exception;
using Math = System.Math;

namespace Exam_App_Native.Droid.Views
{
    public class MultiProgressBar:View
    {
        public IEnumerable<ProgressBar> ProgressBars
        {
            get => _progressBars;
            set
            {
                if(_animator!=null && _animator.IsRunning)
                    throw new Exception("Cannot change bars when animating");
                _progressBars.Clear();
                _progressBars.AddRange(value);

                while (_progresses.Count<_progressBars.Count)
                {
                    var paint = new Paint();
                    paint.SetStyle(Paint.Style.Fill);
                    _progresses.Add(new Progress
                    {
                       Paint = paint
                    });
                }

                while (_progressBars.Count>_progresses.Count)
                {
                    _progresses.RemoveAt(_progresses.Count-1);
                }

                for (var i = 0; i < _progresses.Count; i++)
                {
                    _progresses[i].CurrentValue = 0f;
                    _progresses[i].TargetValue = 0f;
                    _progresses[i].StartedValue = 0f;
                    _progresses[i].Paint.Color = _progressBars[i].Color;
                }

            }
        }

        public Color BackGroudColor
        {
            get => _backgroundPaint.Color;
            set => _backgroundPaint.Color = value;
        }

        public float TotalProgress { get; set; } = 1f;

        private readonly List<Progress> _progresses = new List<Progress>();

        private readonly List<ProgressBar> _progressBars = new List<ProgressBar>();

        private readonly Paint _backgroundPaint = new Paint
        {
            Color = Color.Gray
        };

        private ValueAnimator _animator;

        protected MultiProgressBar(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
        {
            Inilize();
        }

        public MultiProgressBar(Context context) : base(context)
        {
            Inilize();
        }

        public MultiProgressBar(Context context, IAttributeSet attrs) : base(context, attrs)
        {
            Inilize();
        }

        public MultiProgressBar(Context context, IAttributeSet attrs, int defStyleAttr) : base(context, attrs, defStyleAttr)
        {
            Inilize();
        }

        public MultiProgressBar(Context context, IAttributeSet attrs, int defStyleAttr, int defStyleRes) : base(context, attrs, defStyleAttr, defStyleRes)
        {
            Inilize();
        }

        private void Inilize()
        {
            _backgroundPaint.SetStyle(Paint.Style.Fill);
        }

        protected override void OnDraw(Canvas canvas)
        {
            base.OnDraw(canvas);

            canvas.DrawRect(new Rect(0,0,Width,Height),_backgroundPaint);

            var lastX = 0;
            foreach (var progress in _progresses)
            {
                var width = (int)Math.Round(Width*progress.CurrentValue/TotalProgress);
                canvas.DrawRect(new Rect(lastX,0, lastX + width, Height),progress.Paint);
                lastX += width;
            }
        }

        public void SetValues(IEnumerable<float> values, bool animate = true)
        {
            if (values==null)
            {
                throw new NullPointerException();
            }
            var list = values.ToList();
            if (list.Count != _progresses.Count)
            {
                throw new Exception("Value count not match with bars");
            }

            for (var i = 0; i < list.Count; i++)
            {
                _progresses[i].TargetValue = list[i];
            }

            if (animate)
            {
                _animator?.Cancel();
                _animator = ValueAnimator.OfFloat(0,1f);
                _animator.SetDuration(500);
                _animator.Update += (sender, args) =>
                {
                    _progresses.ForEach(progress =>
                        {
                            progress.CurrentValue =
                                (progress.TargetValue - progress.StartedValue)*((float)args.Animation.AnimatedValue) + progress.StartedValue;
                        });
                    Invalidate();
                };

                _animator.AnimationStart += (sender, args) =>
                {
                    _progresses.ForEach(progress => progress.StartedValue = progress.CurrentValue);
                };

                _animator.AnimationEnd += (sender, args) =>
                {
                    _progresses.ForEach(progress => progress.StartedValue = progress.CurrentValue);
                };

                _animator.AnimationCancel += (sender, args) =>
                {
                    _progresses.ForEach(progress => progress.StartedValue = progress.CurrentValue);
                };
                _animator.Start();
            }
            else
            {
                _progresses.ForEach(progress =>
                {
                    progress.StartedValue = progress.TargetValue;
                    progress.CurrentValue = progress.TargetValue;
                });
            }
            Invalidate();
            
        }

        public class ProgressBar
        {
            public Color Color { get; set; }
          
        }

        private class Progress
        {
            public Paint Paint { get; set; }
            public float TargetValue { get; set; }
            public float CurrentValue { get; set; }
            public float StartedValue { get; set; }
        }

    }
}