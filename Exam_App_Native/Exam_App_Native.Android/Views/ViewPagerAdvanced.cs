﻿using System;
using Android.Content;
using Android.Runtime;
using Android.Support.V4.View;
using Android.Util;
using Android.Views;

namespace Exam_App_Native.Droid.Views
{
    public class ViewPagerAdvanced:ViewPager
    {
        public bool Interaction { get; set; } = true;

        public ViewPagerAdvanced(Context context) : base(context)
        {
        }

        public ViewPagerAdvanced(Context context, IAttributeSet attrs) : base(context, attrs)
        {
        }

        public override bool OnTouchEvent(MotionEvent e)
        {
            if(Interaction)
            return base.OnTouchEvent(e);

            return false;
        }

        public override bool OnInterceptTouchEvent(MotionEvent ev)
        {
            if (Interaction)
            {
            return base.OnInterceptTouchEvent(ev);

            }

            return false;
        }

        public override bool OnInterceptHoverEvent(MotionEvent e)
        {
            if (Interaction)
            {
                return base.OnInterceptHoverEvent(e);
            }

            return false;
        }
    }
}