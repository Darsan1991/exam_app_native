﻿using System;
using System.Collections.Generic;
using Android.Content;
using Android.Graphics;
using Android.Runtime;
using Android.Util;
using Android.Widget;
using Exam_App_Native.Model.ViewModel;

namespace Exam_App_Native.Droid.Views
{
    public class PartialSelectableButton:Button
    {
        public event EventHandler<SelectableState> OnStateChanged; 

        private readonly Paint _paint = new Paint
        {
            StrokeWidth = Utils.Utils.DpToPx(5)
        };


        public SelectableState State
        {
            get => _state;
            set
            {
                CurrentStateProperties = StateVsProperties.ContainsKey(value)
                    ? StateVsProperties[value]
                    : new StateProperties();
                _state = value;
//                Invalidate();
            }
        }

        public StateProperties CurrentStateProperties
        {
            get => _currentStateProperties;
            set
            {
                Text = value.Text;
                _paint.Color = value.Color;
                _currentStateProperties = value;
                Invalidate();
            }
        }

        public Dictionary<SelectableState, StateProperties> StateVsProperties { get; set; } =
            new Dictionary<SelectableState, StateProperties>
            {
                {SelectableState.None,new StateProperties{Text = "None",Color = Color.DarkGray}},
                {SelectableState.All,new StateProperties{Text = "All",Color = Color.ParseColor("#FF4081")}},
                {SelectableState.Partial,new StateProperties{Text = "Part",Color = Color.Blue}},
            };


        private SelectableState _state;
        private StateProperties _currentStateProperties;


        protected PartialSelectableButton(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
        {
            Init();
        }

        private void Init()
        {
            Click += (sender, args) =>
            {
                if (State == SelectableState.None)
                    State = SelectableState.All;
                else
                {
                    State = SelectableState.None;
                }

                OnStateChanged?.Invoke(this, State);
            };
        }

        public PartialSelectableButton(Context context) : base(context)
        {
            Init();

        }

        public PartialSelectableButton(Context context, IAttributeSet attrs) : base(context, attrs)
        {
            Init();

        }

        public PartialSelectableButton(Context context, IAttributeSet attrs, int defStyleAttr) : base(context, attrs, defStyleAttr)
        {
            Init();

        }

        public PartialSelectableButton(Context context, IAttributeSet attrs, int defStyleAttr, int defStyleRes) : base(context, attrs, defStyleAttr, defStyleRes)
        {
            Init();

        }

        protected override void OnDraw(Canvas canvas)
        {
            base.OnDraw(canvas);
            canvas.DrawLine(0,Height,Width,Height,_paint);
        }


        public struct StateProperties
        {
            public Color Color { get; set; }
            public string Text { get; set; }
        }
    }
}