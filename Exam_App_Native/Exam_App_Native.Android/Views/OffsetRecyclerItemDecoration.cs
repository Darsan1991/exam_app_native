﻿using System;
using Android.Graphics;
using Android.Support.V7.Widget;
using Android.Views;

namespace Exam_App_Native.Droid.Views
{
    public class OffsetRecyclerItemDecoration : RecyclerView.ItemDecoration
    {
        public Rect offset { get; set; } = new Rect(0, 0, 0, 0);

        public OffsetRecyclerItemDecoration(Rect offset)
        {
            this.offset = offset;
        }

        public OffsetRecyclerItemDecoration()
        {
        }

        public override void GetItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state)
        {
            outRect.Set(offset);
        }

        public enum Direction
        {
            VERTICAL,
            HORIZONTAL
        }
    }

    public class GridSpacingItemDecoration : RecyclerView.ItemDecoration
    {
        private readonly int _spanCount;
        private readonly int _spacing;
        private readonly bool _includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, bool includeEdge)
        {
            this._spanCount = spanCount;
            this._spacing = spacing;
            this._includeEdge = includeEdge;
        }


        public override void GetItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state)
        {
            var position = parent.GetChildAdapterPosition(view); // item position
            var column = position % _spanCount; // item column

            if (_includeEdge)
            {
                outRect.Left =
                    _spacing - column * _spacing / _spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.Right = (column + 1) * _spacing / _spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < _spanCount)
                {
                    // top edge
                    outRect.Top = _spacing;
                }

                outRect.Bottom = _spacing; // item bottom
            }
            else
            {
                outRect.Left = column * _spacing / _spanCount; // column * ((1f / spanCount) * spacing)
                outRect.Right =
                    _spacing - (column + 1) * _spacing /
                    _spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= _spanCount)
                {
                    outRect.Top = _spacing; // item top
                }
            }
        }
    }
}