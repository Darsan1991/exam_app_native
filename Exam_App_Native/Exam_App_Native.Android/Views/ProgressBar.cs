﻿using System;
using Android.Animation;
using Android.Content;
using Android.Graphics;
using Android.Runtime;
using Android.Util;
using Android.Views;

namespace Exam_App_Native.Droid.Views
{
    public class ProgressBar:View
    {
        private readonly Paint _backgroundPaint = new Paint
        {
            StrokeWidth = Utils.Utils.DpToPx(8),
            Color = Color.ParseColor("#FFEBEEEF")
        };

        private readonly Paint _progressPaint = new Paint
        {
            StrokeWidth = Utils.Utils.DpToPx(8),
            Color = Color.ParseColor("#478ecb"),
            
        };

        private readonly Paint _textPaint = new Paint
        {
            TextAlign = Paint.Align.Center,
           
           TextSize = Utils.Utils.DpToPx(18),
            Color =  Color.ParseColor("#478ecb")
        };

        private  float _currentActiveProgress;
        private ValueAnimator _animator;

        public Color BackgroundColor
        {
            get => _backgroundPaint.Color;
            set
            {
                _backgroundPaint.Color = value;
                Invalidate();
            }
        }

        public Color ProgressColor
        {
            get => _progressPaint.Color;
            set
            {
                _progressPaint.Color = value;
                Invalidate();
            }
        }

        public Color TextColor
        {
            get => _textPaint.Color;
            set
            {
                _textPaint.Color = value;
                Invalidate();
            }
        }

        public float TextSize
        {
            get => _textPaint.TextSize;
            set
            {
                _textPaint.TextSize = value;
                Invalidate();
            }
        }

        public float StokeSize
        {
            get => _backgroundPaint.StrokeWidth;
            set
            {
                _progressPaint.StrokeWidth = value;
                _backgroundPaint.StrokeWidth = value;
                Invalidate();
            }
        }

        public float TotalProgress { get; set; } = 1f;

        private float CurrentActiveProgress
        {
            get => _currentActiveProgress;
            set
            {
                _currentActiveProgress = value;
                Invalidate();
            }
        }

        public float CurrentProgress { get; private set; }


        protected ProgressBar(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
        {
            Inilize();
        }

        public ProgressBar(Context context) : base(context)
        {
            Inilize();
        }

        public ProgressBar(Context context, IAttributeSet attrs) : base(context, attrs)
        {
            Inilize();
        }

        public ProgressBar(Context context, IAttributeSet attrs, int defStyleAttr) : base(context, attrs, defStyleAttr)
        {
            Inilize();
        }

        public ProgressBar(Context context, IAttributeSet attrs, int defStyleAttr, int defStyleRes) : base(context, attrs, defStyleAttr, defStyleRes)
        {
            Inilize();
        }

        private void Inilize()
        {
            _backgroundPaint.SetStyle(Paint.Style.Stroke);
            _progressPaint.SetStyle(Paint.Style.Stroke);
            _textPaint.SetStyle(Paint.Style.Fill);
            _textPaint.SetTypeface(Typeface.DefaultBold);
            CurrentActiveProgress = 0.7f;
        }

        protected override void OnDraw(Canvas canvas)
        {
            base.OnDraw(canvas);
            var radius = Math.Min(Width,Height)/2f - StokeSize/2;
                        canvas.DrawCircle(Width/2f,Height/2f,radius,_backgroundPaint);

            canvas.DrawArc(Width / 2f - radius, Height / 2f - radius, Width / 2f + radius, Height / 2f + radius, 0, CurrentActiveProgress*360/TotalProgress, false, _progressPaint);
            var bounds = new Rect();
            _textPaint.GetTextBounds("H", 0, 1, bounds);
            // ReSharper disable once PossibleLossOfFraction
            canvas.DrawText(Math.Round(_currentActiveProgress*100/TotalProgress)+"%",Width/2f,Height/2f + bounds.Height()/2 ,_textPaint);
        }

        public void SetProgress(float progress, bool animate = true)
        {
            _animator?.Cancel();
            CurrentProgress = progress;

            if (animate)
            {
                _animator = ValueAnimator.OfFloat(CurrentActiveProgress / TotalProgress, progress / TotalProgress);
                _animator.SetDuration(500);
                _animator.Update += (sender, args) =>
                    {
                        CurrentActiveProgress = (float) args.Animation.AnimatedValue * TotalProgress;
                    };
                _animator.Start();
            }
            else
            {
                CurrentActiveProgress = progress;
            }

        }
    }
}