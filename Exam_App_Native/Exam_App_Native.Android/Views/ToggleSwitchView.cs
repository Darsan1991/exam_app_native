﻿using System;
using Android.Widget;
using Exam_App_Native.Model.ViewModel;

namespace Exam_App_Native.Droid.Views
{
    public class ToggleSwitchView
    {
        private const int CHECKED_IMAGE_ID = Resource.Mipmap.check_box_all;
        private const int UNCHECKED_IMAGE_ID = Resource.Mipmap.check_box_none;

        public event EventHandler<bool> ValueChanged;

        private readonly ImageView _imageView;
        private bool _enabled = true;
        private bool _checked;

        public bool Enabled
        {
            get => _enabled;
            set
            {
                if (_enabled==value)
                {
                    return;
                }

                if(value)
                _imageView.Click += ImageViewOnClick;
                else
                {
                    _imageView.Click -= ImageViewOnClick;
                }
                _enabled = value;
            }
        }

        public bool Checked
        {
            get => _checked;
            set
            {
                if (_checked==value)
                {
                    return;
                }

                _imageView.SetImageResource(value?CHECKED_IMAGE_ID : UNCHECKED_IMAGE_ID);

                _checked = value;
                ValueChanged?.Invoke(this,Checked);
            }
        }

        public ToggleSwitchView(ImageView imageView)
        {
            _imageView = imageView;
            if(Enabled)
            _imageView.Click += ImageViewOnClick;
            _imageView.SetImageResource(Checked? CHECKED_IMAGE_ID : UNCHECKED_IMAGE_ID);

        }

        private void ImageViewOnClick(object sender, EventArgs eventArgs)
        {
            Checked = !Checked;
        }
    }


    public class PartialToggleSwitchView
    {
        private const int CHECKED_IMAGE_ID = Resource.Mipmap.check_box_all;
        private const int UNCHECKED_IMAGE_ID = Resource.Mipmap.check_box_none;
        private const int PARTIAL_IMAGE_ID = Resource.Mipmap.check_box_partial;

        public event EventHandler<SelectableState> ValueChanged;

        private readonly ImageView _imageView;
        private bool _enabled = true;
        private SelectableState _currentState;

        public bool Enabled
        {
            get => _enabled;
            set
            {
                if (_enabled == value)
                {
                    return;
                }

                if (value)
                    _imageView.Click += ImageViewOnClick;
                else
                {
                    _imageView.Click -= ImageViewOnClick;
                }
                _enabled = value;
            }
        }

        public SelectableState CurrentState
        {
            get => _currentState;
            set
            {
                if (_currentState == value)
                {
                    return;
                }

                SetImageForState(value);
                _currentState = value;
                ValueChanged?.Invoke(this, CurrentState);
            }
        }

        public PartialToggleSwitchView(ImageView imageView)
        {
            _imageView = imageView;
            if (Enabled)
                _imageView.Click += ImageViewOnClick;
            SetImageForState(CurrentState);
        }

        private void ImageViewOnClick(object sender, EventArgs eventArgs)
        {
            CurrentState = CurrentState == SelectableState.None? SelectableState.All: SelectableState.None;
        }

        private void SetImageForState(SelectableState state)
        {
            switch (state)
            {
                case SelectableState.None:
                    _imageView.SetImageResource(UNCHECKED_IMAGE_ID);
                    break;
                case SelectableState.Partial:
                    _imageView.SetImageResource(PARTIAL_IMAGE_ID);
                    break;
                case SelectableState.All:
                    _imageView.SetImageResource(CHECKED_IMAGE_ID);

                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(state), state, null);
            }
        }

    }
}