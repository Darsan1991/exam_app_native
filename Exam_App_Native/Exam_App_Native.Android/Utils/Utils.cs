﻿using System;
using System.Globalization;
using Android.Content;
using Android.Content.Res;
using Android.Graphics;

namespace Exam_App_Native.Droid.Utils
{
    public static class Utils
    {
        public static int GetSelectableResource(Context context)
        {
            var attrs = new int[] { Resource.Attribute.selectableItemBackground };
            var typedArray = context.ObtainStyledAttributes(attrs);
            var backgroundResource = typedArray.GetResourceId(0, 0);
            typedArray.Recycle();
            return backgroundResource;
        }

        public static int DpToPx(int dp)
        {
            return (int)(dp * Resources.System.DisplayMetrics.Density);
        }

        public static int PxToDp(int px)
        {
            return (int)(px / Resources.System.DisplayMetrics.Density);
        }

        public static Bitmap GetRoundCornerImage(Bitmap bitmap)
        {
            var width = bitmap.Width;
            var height = bitmap.Height;

            var result = Bitmap.CreateBitmap(width,height,Bitmap.Config.Argb8888);
            var canvas = new Canvas(result);
            canvas.DrawARGB(0,0,0,0);

            var paint = new Paint {AntiAlias = true,Color = Color.ParseColor("#000000")};

            var rect = new Rect(0,0,width,height);
            var rectF = new RectF(rect);

            canvas.DrawRoundRect(rectF,rect.Width()/2f,rect.Height()/2f,paint);
            paint.SetXfermode(new PorterDuffXfermode(PorterDuff.Mode.SrcIn));
            canvas.DrawBitmap(bitmap,rect,rect,paint);

            return result;
        }

       
    }
}