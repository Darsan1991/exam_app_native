﻿using System;
using System.Collections.Generic;
using System.Linq;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Exam_App_Native.Model;
using MikePhil.Charting.Charts;
using MikePhil.Charting.Components;
using MikePhil.Charting.Data;
using MikePhil.Charting.Formatter;
using MikePhil.Charting.Interfaces.Datasets;

namespace Exam_App_Native.Droid.Fragment
{
    public class ExamPerformanceFragment : Android.Support.V4.App.Fragment
    {
        private static readonly Color[] COLORS = new[]
        {
            Color.Red, Color.Blue, Color.Brown, Color.DarkGreen, Color.Purple, Color.DarkOrange, Color.LightBlue,
            Color.Violet, Color.SteelBlue, Color.SeaGreen,Color.Green,Color.BlueViolet
        };

        public ViewModel MViewModel
        {
            get => _mViewModel;
            set
            {
                _radarData.ClearValues();
                _webAxises.Clear();

                var group = value.ExamPerformance.PerformanceResults.GroupBy(result => result.Month).ToList();
                var axises = group.SelectMany(results => results.Select(result => result.DisciplineName.ToLower())).Distinct();

                _webAxises.AddRange(axises);
                
                
                
                group.ForEach((index,results) =>
                {
                    var list = results.ToList();
                    var entries = new RadarEntry[list.Count];

                    foreach (var result in results)
                    {
                        var i = _webAxises.IndexOf(result.DisciplineName.ToLower());
                       entries[i] = new RadarEntry(result.Score);
                    }

                    for (var i = 0; i < entries.Length; i++)
                    {
                        if (entries[i] == null)
                        {
                            entries[i] = new RadarEntry(0);
                        }
                    }

                    _radarData.AddDataSet(new RadarDataSet(entries.ToList(), results.Key)
                    {
                        LineWidth = 2,
                        ValueTextSize = 0,
                        Color = COLORS[index]
                    });
                   
                });

                if (IsViewCreated)
                {
                    _titleTextView.Text = value.ExamPerformance.JobTitle;
                    _subTitleTextView.Text =
                        $"{value.ExamPerformance.Institution}/{value.ExamPerformance.YearAndCompany}";
                    _radarChart.NotifyDataSetChanged();
                }

                _mViewModel = value;
            }
        }

        private readonly RadarData _radarData = new RadarData();
        private ViewModel _mViewModel;
        private readonly List<string> _webAxises = new List<string>();
        private RadarChart _radarChart;
        private TextView _titleTextView;
        private TextView _subTitleTextView;

        public bool IsViewCreated { get; private set; }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var view = inflater.Inflate(Resource.Layout.fragment_exam_performance, container, false);
            _titleTextView = view.FindViewById<TextView>(Resource.Id.title_tv);
            _subTitleTextView = view.FindViewById<TextView>(Resource.Id.sub_title_tv);
            var mapContainer = (ViewGroup)view.FindViewById(Resource.Id.map_container);

            _titleTextView.Text = MViewModel?.ExamPerformance.JobTitle;
            _subTitleTextView.Text =
                $"{MViewModel?.ExamPerformance.Institution}/{MViewModel?.ExamPerformance.YearAndCompany}";

            _radarChart = new RadarChart(Context);
            _radarChart.SetNoDataText("");

            mapContainer.AddView(_radarChart);
            var parameters = _radarChart.LayoutParameters;
            parameters.Height = ViewGroup.LayoutParams.MatchParent;
            parameters.Width = ViewGroup.LayoutParams.MatchParent;

            _radarChart.Description = null;
            _radarChart.ContentDescription = null;
            _radarChart.WebColor = Color.Black;
//            _radarChart.XAxis.TextSize = 5;
            
            _radarChart.YAxis.AxisMinimum = 0;
            _radarChart.YAxis.AxisMaximum = 100;
            _radarChart.YAxis.SetLabelCount(3,true);
            _radarChart.WebAlpha = (int) (0.3f * 255);
            _radarChart.DragDecelerationEnabled = false;
            _radarChart.RotationEnabled = false;

            _radarChart.TextAlignment = TextAlignment.Center;

            _radarChart.Data = _radarData;

            IsViewCreated = true;
            return view;
        }


        public override void OnDestroyView()
        {
            IsViewCreated = false;
            base.OnDestroyView();
        }

        public class ViewModel
        {
            public ExamPerformance ExamPerformance { get; set; }
        }



    }

    
}