﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Android.Content;
using Android.OS;
using Android.Support.V4.App;
using Android.Support.V4.View;
using Android.Views;
using Android.Widget;
using Exam_App_Native.Droid.Activity;
using Exam_App_Native.Droid.Fragment.Base;
using Exam_App_Native.Droid.Views;
using Exam_App_Native.Model;
using Exam_App_Native.Model.ViewModel;
using MikePhil.Charting.Charts;

namespace Exam_App_Native.Droid.Fragment
{
    public partial class StatisticsFragment : NameCompactPageFragment
    {
        public override string Title => AppContext.GetString(Resource.String.statistics_text);

        private Context AppContext => AndroiExamApp.Instance;

        private ViewPager _disciplineViewPager;
        private View _progressGroup;
        private ViewPager _examViewPager;


        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var view = inflater.Inflate(Resource.Layout.fragment_statistics, container, false);
            

            _disciplineViewPager = view.FindViewById<ViewPager>(Resource.Id.disciplineViewPager);
            _examViewPager = view.FindViewById<ViewPager>(Resource.Id.examViewPager);
            _progressGroup = view.FindViewById(Resource.Id.progress_group);
//            //SetUp Radar chart
//            var radarChart = new RadarChart(Context);
//
//            var layoutParams = radarChart.LayoutParameters;
//            layoutParams.Height = ViewGroup.LayoutParams.MatchParent;
//            layoutParams.Width = ViewGroup.LayoutParams.MatchParent;

            var toggleButton = new ToggleSwitchView(view.FindViewById<ImageView>(Resource.Id.show_trend_line_toggle_iv));
            toggleButton.ValueChanged += (sender, args) =>
            {
                if (_disciplineViewPager.Adapter is DisciplinesPageAdapter adapter)
                {
                    adapter.ShowTrendLine = toggleButton.Checked;
                }
            };

           
            return view;
        }


        public override async void OnPageAppear()
        {
            base.OnPageAppear();
            _progressGroup.Visibility = ViewStates.Visible;
            try
            {
                await  LoadDisciplineTimeSeries();
                await LoadExamTimeSeries();
            }
            catch (Exception e)
            {
                
            }
            _progressGroup.Visibility = ViewStates.Gone;

        }

        

        private async Task LoadDisciplineTimeSeries()
        {
            try
            {
                var disciplineTimeSeries = await Auth.Instance.CurrentUser.GetDisciplineTimeSeries();

                Activity.RunOnUiThread(() =>
                {
                    _disciplineViewPager.Adapter = new DisciplinesPageAdapter(ChildFragmentManager)
                    {
                        Items = disciplineTimeSeries.Select((v) => new LineChartFragment.ViewModel
                            {
                                KnownCardValues = v.KnownCards.ToDictionary(value =>
                                    DisciplineTimeSeries.TimeAndValue.GetMonth(value.Time), value => value.Value),
                                TrendLineValues = v.TrendSeries.ToDictionary(value => DisciplineTimeSeries.TimeAndValue.GetMonth(value.Time), value => value.Value),
                                Title = v.DisciplineName
                            }
                        )
                    };
                });
                
            }
            catch (Exception e)
            {
                // ignored
            }

        }

        private async Task LoadExamTimeSeries()
        {
            try
            {
                var examTimeSeries = await Auth.Instance.CurrentUser.GetExamPerformances();

                Activity.RunOnUiThread(() =>
                {
                    _examViewPager.Adapter = new ExamPerformancePageAdapter(ChildFragmentManager)
                    {
                        Items = examTimeSeries.Select((v) => new ExamPerformanceFragment.ViewModel
                            {
                                ExamPerformance = v
                            }
                        )
                    };
                });

            }
            catch (Exception e)
            {
                // ignored
            }

        }


    }




    //Disciplines View Pager Adapter
    public partial class StatisticsFragment
    {
        public class DisciplinesPageAdapter : FragmentPagerAdapter
        {
            public bool ShowTrendLine
            {
                get => _showTrendLine;
                set
                {
                    _lineChartFragmentList.ForEach(fragment => fragment.ShowTrendLine = value);
                    _showTrendLine = value;
                }
            }

            public IEnumerable<LineChartFragment.ViewModel> Items
            {
                get => _itemList;
                set
                {
                    _itemList.Clear();
                    _itemList.AddRange(value);

                    while (_lineChartFragmentList.Count < _itemList.Count)
                    {
                        _lineChartFragmentList.Add(new LineChartFragment {ShowTrendLine = ShowTrendLine});
                    }

                    while (_itemList.Count < _lineChartFragmentList.Count)
                    {
                        _lineChartFragmentList.RemoveAt(_lineChartFragmentList.Count - 1);
                    }

                    for (var i = 0; i < _lineChartFragmentList.Count; i++)
                    {
                        _lineChartFragmentList[i].MViewModel = _itemList[i];
                    }

                }
            }

            private readonly List<LineChartFragment.ViewModel> _itemList = new List<LineChartFragment.ViewModel>();

            private readonly List<LineChartFragment> _lineChartFragmentList = new List<LineChartFragment>();
            private bool _showTrendLine;

            public DisciplinesPageAdapter(FragmentManager fm) : base(fm)
            {
            }

            public override int Count => _itemList.Count;

            public override Android.Support.V4.App.Fragment GetItem(int position)
            {
                return _lineChartFragmentList[position];
            }
        }



    }

    //Disciplines View Pager Adapter
    public partial class StatisticsFragment
    {
        public class ExamPerformancePageAdapter : FragmentPagerAdapter
        {
            public IEnumerable<ExamPerformanceFragment.ViewModel> Items
            {
                get => _itemList;
                set
                {
                    _itemList.Clear();
                    _itemList.AddRange(value);

                    while (_examPerformanceFragmentList.Count < _itemList.Count)
                    {
                        _examPerformanceFragmentList.Add(new ExamPerformanceFragment());
                    }

                    while (_itemList.Count < _examPerformanceFragmentList.Count)
                    {
                        _examPerformanceFragmentList.RemoveAt(_examPerformanceFragmentList.Count - 1);
                    }

                    for (var i = 0; i < _examPerformanceFragmentList.Count; i++)
                    {
                        _examPerformanceFragmentList[i].MViewModel = _itemList[i];
                    }

                }
            }

            private readonly List<ExamPerformanceFragment.ViewModel> _itemList = new List<ExamPerformanceFragment.ViewModel>();

            private readonly List<ExamPerformanceFragment> _examPerformanceFragmentList = new List<ExamPerformanceFragment>();

            public ExamPerformancePageAdapter(FragmentManager fm) : base(fm)
            {
            }

            public override int Count => _itemList.Count;

            public override Android.Support.V4.App.Fragment GetItem(int position)
            {
                return _examPerformanceFragmentList[position];
            }
        }
    }
}