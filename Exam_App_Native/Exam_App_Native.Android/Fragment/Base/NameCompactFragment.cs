﻿using System;

namespace Exam_App_Native.Droid.Fragment.Base
{
    public abstract class NameCompactFragment:Android.Support.V4.App.Fragment
    {
        public abstract string Title { get; }
    }

    public interface IPage
    {
        bool showing { get;  }
        void OnPageAppear();
        void OnPageDisAppear();
    }


    public abstract class NameCompactPageFragment :NameCompactFragment,IPage 
    {
        public bool showing { get; private set; }

        public virtual void OnPageAppear()
        {
            showing = true;
            Console.WriteLine(nameof(OnPageAppear)+ this.Class.Name);
        }

        public virtual void OnPageDisAppear()
        {
            showing = false;
            Console.WriteLine(nameof(OnPageDisAppear) + (this.Class.Name));
        }
    }
}