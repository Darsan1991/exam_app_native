﻿using System;
using System.Threading.Tasks;
using Android.OS;
using Android.Views;
using Android.Widget;
using Exam_App_Native.Model;

namespace Exam_App_Native.Droid.Fragment
{
    public class QuizFragment : Android.Support.V4.App.Fragment
    {
        public event Action<bool,float> OnAnswered; 

        public bool IsViewCreated { get; private set; }

        public int TotalQuizCount
        {
            get => _totalQuizCount;
            set
            {
                _totalQuizCount = value;
                if (IsViewCreated)
                {
                    _progressBar.Max = value;
                }
            }
        }


        public QuizCard QuizCard
        {
            get => _quizCard;
            set
            {
                _quizCard = value;
                if (IsViewCreated)
                {
                    _titleTextView.Text = _quizCard.Topic;
                    _questionTextView.Text = _quizCard.Content;
                    _teacherTextView.Text = _quizCard.Teacher;
                    _subTitleTextView.Text = _quizCard.Topic;
                }
            }
        }

        private View _progressGroup;
        private TextView _titleTextView;
        private TextView _subTitleTextView;
        private TextView _questionTextView;
        private TextView _teacherTextView;
        private ProgressBar _progressBar;
        private TextView _completeIndicatorTextView;
        private  QuizCard _quizCard;
        private Task<QuizCard> _quizCardTask;
        private int _currentQuizIndex = -1;
        private int _totalQuizCount;
        private DateTime _quizStartTime;


        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var view = inflater.Inflate(Resource.Layout.fragment_quiz, container,false);

            IsViewCreated = true;

            _progressGroup = view.FindViewById(Resource.Id.progress_group);
            _titleTextView = view.FindViewById<TextView>(Resource.Id.title_tv);
            _subTitleTextView = view.FindViewById<TextView>(Resource.Id.subtitle_tv);
            _questionTextView = view.FindViewById<TextView>(Resource.Id.question_tv);
            _teacherTextView = view.FindViewById<TextView>(Resource.Id.teacher_name_tv);
            _progressBar = view.FindViewById<ProgressBar>(Resource.Id.horizontal_progress_bar);
            _completeIndicatorTextView = view.FindViewById<TextView>(Resource.Id.completed_indicator_tv);

            view.FindViewById(Resource.Id.correct_btn).Click += (sender, args) => OnAnswered?.Invoke(true,DateTime.Now.Subtract(_quizStartTime).Seconds);
            view.FindViewById(Resource.Id.wrong_btn).Click += (sender, args) => OnAnswered?.Invoke(false, DateTime.Now.Subtract(_quizStartTime).Seconds);

            _progressBar.Max = TotalQuizCount;
            UpdateProgress();

            _progressGroup.Visibility = (_quizCardTask != null && (!_quizCardTask.IsCompleted && !_quizCardTask.IsCanceled && !_quizCardTask.IsFaulted))
                ? ViewStates.Visible
                : ViewStates.Gone;


            if (_quizCardTask != null && _quizCardTask.IsCompleted)
            {
                QuizCard = _quizCardTask.Result;
            }


            return view;
        }

        private void UpdateProgress()
        {
            _completeIndicatorTextView.Text = $"{_currentQuizIndex} {Context.GetString(Resource.String.quiz_activity_of)} {TotalQuizCount}";
            _progressBar.Progress = _currentQuizIndex;
        }

        public void StartOrGoToNextQuiz(Task<QuizCard> task,int quizIndex)
        {
            _quizCardTask = task;
            _currentQuizIndex = quizIndex;

            if (IsViewCreated)
            {
                UpdateProgress();
            }

            SetQuiz(task);

        }

        private async void SetQuiz(Task<QuizCard> task)
        {
            if (IsViewCreated)
                _progressGroup.Visibility = ViewStates.Visible;
            try
            {
                QuizCard = await task;
                _quizStartTime = DateTime.Now;
            }
            catch (Exception e)
            {
                // ignored
            }

            if (IsViewCreated)
                _progressGroup.Visibility = ViewStates.Gone;

        }

    }
}