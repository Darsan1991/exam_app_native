﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Support.V7.Widget;
using Android.Views;
using Exam_App_Native.Droid.Activity;
using Exam_App_Native.Droid.Adapter;
using Exam_App_Native.Droid.Adapter.ViewHolder.General;
using Exam_App_Native.Droid.Fragment.Base;
using Exam_App_Native.Droid.Views;
using Exam_App_Native.Model;
using Exam_App_Native.Model.ViewModel;

namespace Exam_App_Native.Droid.Fragment
{
    public class DisciplinesFragment: NameCompactPageFragment
    {
        private Context AppContext => AndroiExamApp.Instance;

        private View _progressGroup;
        private CancellationTokenSource _tokenSource;
        private CardGroupRecyclerAdapter<DisciplineGroup, Discipline> _recyclerAdapter;
        private View _searchProgressGroup;
        private SearchView _searchView;
        private View _layout;
        private Timer _searchBarTimer;
        public override string Title => AppContext.GetString(Resource.String.discipline_text);
        public bool Loading => _progressGroup?.Visibility == ViewStates.Visible;

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            _layout = inflater.Inflate(Resource.Layout.fragment_desciplines,container,false);
            var recyclerView = _layout.FindViewById<RecyclerView>(Resource.Id.recycler_view);
            _progressGroup = _layout.FindViewById(Resource.Id.progress_group);
            _searchProgressGroup = _layout.FindViewById(Resource.Id.search_progress_group);
            _progressGroup.SetZ(Utils.Utils.DpToPx(5));
            var linearLayoutManager = new LinearLayoutManager(Context, LinearLayoutManager.Vertical, false);
            recyclerView.SetLayoutManager(linearLayoutManager);


            _searchView = _layout.FindViewById<SearchView>(Resource.Id.search_bar);


            _searchView.Click += (sender, args) =>
            {
                _searchView.Focusable = true;
                _searchView.Iconified = false;
            };

            _searchView.QueryTextChange += (sender, args) =>
            {
                if (_searchBarTimer == null)
                {

                    _searchBarTimer = new System.Threading.Timer(
                        state => Activity.RunOnUiThread(() => LoadOrUpdate(_searchView.Query, true))
                        , null
                        , 200
                        , Timeout.Infinite);


                }
                else
                {
                    _searchBarTimer.Change(200, Timeout.Infinite);
                }

            };

            _recyclerAdapter =
                new CardGroupRecyclerAdapter<DisciplineGroup, Discipline>(Context);
               
            recyclerView.SetAdapter(_recyclerAdapter);
            recyclerView.AddItemDecoration(new OffsetRecyclerItemDecoration(new Rect(0, 0, 0, 30)));
            _recyclerAdapter.OnCreatingViewHolder += OnCreatingViewHolder;
          

            return _layout;
        }


        public override void OnPageAppear()
        {
            base.OnPageAppear();
            LoadOrUpdate(_searchView?.Query);
        }

        public override void OnPageDisAppear()
        {
            base.OnPageDisAppear();
            if(Loading)
            CancelLoading();
        }

        private async void LoadOrUpdate(string filter=null,bool searchLoad=false)
        {
            if (_searchProgressGroup.Visibility == ViewStates.Visible || _progressGroup.Visibility == ViewStates.Visible)
                _tokenSource?.Cancel();

            var progressGroup = searchLoad ? _searchProgressGroup : _progressGroup;

            progressGroup.Visibility = ViewStates.Visible;
            _tokenSource = new CancellationTokenSource();
            try
            {
                var examGroups = await ExamApp.Instance.GetDesciplineGroups(_tokenSource);
                _recyclerAdapter.Items = examGroups.Select(group =>
                new ItemGroup<DisciplineGroup, ICard<Discipline>>
                {
                    Title = group.Name,
                    Items = group.Disciplines.Select(discipline => new Card<Discipline>
                    {
                        Title = discipline.Name,
                        Footer = $"{discipline.CardCount} {GetString(Resource.String.cards_text).ToLower()}",
                        Data = discipline
                    })
                });
            }
            catch (Exception e)
            {
                
            }

            progressGroup.Visibility = ViewStates.Gone;

        }

        public void CancelLoading()
        {
            _tokenSource.Cancel();
        }

        private void OnCreatingViewHolder(CardGroupTileViewHolder<DisciplineGroup, Discipline> holder)
        {
//            holder.InteractableDetails = false;
            holder.IsRecyclable = false;
            holder.OnDetailsClicked += OnHolderTileDetailsClicked;
            holder.OnChildItemClicked +=HolderOnChildItemClicked;
        }

        // ReSharper disable once MethodTooLong
        private void OnHolderTileDetailsClicked(CardGroupTileViewHolder<DisciplineGroup, Discipline> holder)
        {
            var cardWidth = Utils.Utils.DpToPx((int)AndroiExamApp.CARD_WIDTH_IN_DP);
            var layoutWidth = _layout.MeasuredWidth;

            var spacing = (layoutWidth - 2 * cardWidth) / 2;

            // ReSharper disable once ConvertToLocalFunction
            Func<string, CancellationTokenSource, Task<IEnumerable<ICard<Discipline>>>> cardsTaskCreator =
                (query, tokenSource) =>
                {
                    tokenSource = tokenSource ?? new CancellationTokenSource();
                    return Task.Run(async () =>
                    {

                        var disciplines = await (string.IsNullOrEmpty(query) || query.Length < 3 ? ExamApp.Instance.GetDesciplinesFromGroup(holder.Item.Title,tokenSource)
                                : ExamApp.Instance.SearchDisciplines(query, tokenSource));

                        return disciplines.Select(discipline => (ICard<Discipline>)new Card<Discipline>
                        {
                            Title = discipline.Name,
                            Footer = $"{discipline.CardCount} {GetString(Resource.String.cards_text).ToLower()}",
                            Data = discipline
                        });

                    }, tokenSource.Token);
                };
            CardRecyclerAdapter<Discipline> recyclerAdapter = null;

            SimpleRecyclerViewActivity.ViewModel viewModel = new SimpleRecyclerViewActivity.ViewModel();
             viewModel   = new SimpleRecyclerViewActivity.ViewModel
            {

                Title = holder.Item.Title,
                LayoutManager = new GridLayoutManager(Context, 2) { },
                ItemDecoration = new OffsetRecyclerItemDecoration(new Rect(spacing / 2, spacing / 2, spacing / 2, spacing / 2)),
                RecyclerAdapter = Task.Run(async () =>
                {
                    var cards = await cardsTaskCreator("", null);
                    recyclerAdapter = new CardRecyclerAdapter<Discipline>(Context,cards);
                    recyclerAdapter.OnItemClicked += c =>
                    {
                        OpenReviewCreatorActivity(c.Data);
                        // ReSharper disable once AccessToModifiedClosure
//                        viewModel.Instance.Finish();
                    };
                    return (RecyclerView.Adapter)recyclerAdapter;
                }),
                QueryTextChange = (sender, args) =>
                {
                    var tokenSource = new CancellationTokenSource();
                    ((SimpleRecyclerViewActivity)sender).Update(
                        Task.Run(async () => (await cardsTaskCreator(args.NewText, tokenSource)) as object,
                            tokenSource.Token), tokenSource);
                },
                OnUpdatated = (sender, objects) =>
                {
                    if (objects is IEnumerable<ICard<Discipline>> items)
                    {
                        recyclerAdapter.Items = items;
                    }
                }
            };
            SimpleRecyclerViewActivity.Open(Context, viewModel, MyTask.Create());
        }

        private void HolderOnChildItemClicked(CardGroupTileViewHolder<DisciplineGroup, Discipline> cardGroupTileViewHolder, ICard<Discipline> card)
        {
            OpenReviewCreatorActivity(card.Data);
        }

        private void OpenReviewCreatorActivity(Discipline discipline)
        {
            ReviewCreatorActivity.Open(Context, new ReviewCreatorActivity.ViewModel
            {
                Discipline = discipline,
                SubjectProgressTask = ExamApp.Instance.GetSubjectProgressesForDiciplines(discipline.Id)
            }, MyTask.Create());
        }
    }
}