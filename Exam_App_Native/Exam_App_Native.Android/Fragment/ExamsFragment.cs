﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.Widget;
using Android.Util;
using Android.Views;
using Android.Widget;
using Exam_App_Native.Droid.Activity;
using Exam_App_Native.Droid.Adapter;
using Exam_App_Native.Droid.Adapter.ViewHolder;
using Exam_App_Native.Droid.Adapter.ViewHolder.General;
using Exam_App_Native.Droid.Fragment.Base;
using Exam_App_Native.Droid.Views;
using Exam_App_Native.Model;
using Exam_App_Native.Model.ViewModel;
using SearchView = Android.Support.V7.Widget.SearchView;
using Timer = System.Timers.Timer;

namespace Exam_App_Native.Droid.Fragment
{
    public class ExamsFragment : NameCompactPageFragment
    {
        private Context AppContext => AndroiExamApp.Instance;

        private CancellationTokenSource _tokenSource;
        private CardGroupRecyclerAdapter<ExamGroup, Exam> _recyclerAdapter;
        private View _progressGroup;
        private View _layout;
        private View _searchProgressGroup;
        private SearchView _searchView;
        private System.Threading.Timer _searchBarTimer;

        public override string Title => AppContext.GetString(Resource.String.exams_text);
        public bool Loading => _progressGroup.Visibility == ViewStates.Visible;
        

        public ExamsFragment()
        {
            Console.WriteLine(nameof(ExamsFragment));
        }

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Console.WriteLine(nameof(ExamsFragment) + nameof(OnCreate));
         
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
//            Console.WriteLine(nameof(ExamsFragment)+nameof(OnCreateView)+" "+s);
            _layout = inflater.Inflate(Resource.Layout.fragment_exams, container, false);
            var recyclerView = _layout.FindViewById<RecyclerView>(Resource.Id.recycler_view);
            _progressGroup = _layout.FindViewById(Resource.Id.progress_group);
            _progressGroup.SetZ(Utils.Utils.DpToPx(5));
            _searchProgressGroup = _layout.FindViewById(Resource.Id.search_progress_group);
            _searchView = _layout.FindViewById<Android.Support.V7.Widget.SearchView>(Resource.Id.search_bar);

            _searchView.QueryTextChange += (sender, args) =>
            {
                if (_searchBarTimer == null)
                {
                    
                    _searchBarTimer = new System.Threading.Timer(
                        state => Activity.RunOnUiThread(()=>LoadOrUpdate(_searchView.Query,true))
                        ,null
                        ,200
                        ,Timeout.Infinite);

                }
                else
                {
                    _searchBarTimer.Change(200, Timeout.Infinite);
                }
                
            };

            _searchView.Click += (sender, args) =>
            {
                _searchView.Focusable = true;
                _searchView.Iconified = false;
            };


            var linearLayoutManager = new LinearLayoutManager(Context, LinearLayoutManager.Vertical, false);
            recyclerView.SetLayoutManager(linearLayoutManager);

            _recyclerAdapter = new CardGroupRecyclerAdapter<ExamGroup, Exam>(Context);
            recyclerView.SetAdapter(_recyclerAdapter);
            recyclerView.AddItemDecoration(new OffsetRecyclerItemDecoration(new Rect(0, 0, 0, 60)));
            _recyclerAdapter.OnCreatingViewHolder += OnCreatingViewHolder;

            return _layout;
        }

        public override void OnPageAppear()
        {
            base.OnPageAppear();
            LoadOrUpdate(_searchView?.Query);
          
        }

        public override void OnPageDisAppear()
        {
            base.OnPageDisAppear();
            if(Loading)
            CancleLoadOrUpdate();
        }

        public  async void  LoadOrUpdate(string filter=null,bool searchLoad = false)
        {
            if (_searchProgressGroup.Visibility == ViewStates.Visible ||
                _progressGroup.Visibility == ViewStates.Visible)
            {
               _tokenSource?.Cancel();
            }

            var progressGroup = searchLoad ? _searchProgressGroup : _progressGroup;

            progressGroup.Visibility = ViewStates.Visible;
            _tokenSource = new CancellationTokenSource();
            try
            {
                var examGroups = await (string.IsNullOrEmpty(filter) || filter.Length < 3
                    ? ExamApp.Instance.GetExamGroups(_tokenSource)
                    : ExamApp.Instance.SearchExamGroups(filter, _tokenSource));
                Console.WriteLine("Setting Items to Recycler view!");
                _recyclerAdapter.Items = examGroups.Select(group => new ItemGroup<ExamGroup, ICard<Exam>>
                {
                    Data = group,
                    Items = group.Exams.Select(exam => new Card<Exam>
                    {
                        Title = exam.JobTitle,
                        SubTitle = exam.Institution,
                        Footer = $"{exam.Company} - {exam.Year}",
                        Data = exam
                    }),
                    Title = group.Name
                });
            }
            catch (TaskCanceledException e)
            {

            }
            catch (Exception e)
            {
                
            }

            progressGroup.Visibility = ViewStates.Gone;
        }

        public void CancleLoadOrUpdate()
        {
            _tokenSource.Cancel();
        }

        private void OnCreatingViewHolder(CardGroupTileViewHolder<ExamGroup, Exam> holder)
        {
            holder.OnDetailsClicked += OnHolderTileDetailsClicked;
            holder.OnChildItemClicked += OnHolderTileChildClicked;
            holder.IsRecyclable = false;
        }

        private void OnHolderTileChildClicked(CardGroupTileViewHolder<ExamGroup, Exam> cardGroupTileViewHolder, ICard<Exam> card)
        {
            OpenDisciplineSelectionActivity(card.Data,task:MyTask.Create());
        }

        private void OpenDisciplineSelectionActivity(Exam exam,Context context = null,MyTask task = null)
        {
            var cardWidth = Utils.Utils.DpToPx((int) AndroiExamApp.CARD_WIDTH_IN_DP);
            var layoutWidth = _layout.MeasuredWidth;

            var spacing = (layoutWidth - 2*cardWidth)/2;


            CardRecyclerAdapter<Discipline> recyclerAdapter = null;

            // ReSharper disable once ConvertToLocalFunction
            Func<string, CancellationTokenSource, Task<IEnumerable<ICard<Discipline>>>> cardsTaskCreator =
                (query, tokenSource) =>
                {
                    tokenSource = tokenSource ?? new CancellationTokenSource();
                    return Task.Run(async () =>
                    {
                        
                        var disciplines = await (string.IsNullOrEmpty(query) || query.Length<3
                            ? Task.FromResult(exam.Disciplines)
                            : ExamApp.Instance.SearchDisciplines(query, tokenSource));
                        return disciplines.Select(discipline => (ICard<Discipline>) new Card<Discipline>
                        {
                            Data = discipline,
                            Title = discipline.Name,
                            Footer = $"{discipline.CardCount} {GetString(Resource.String.cards_text).ToLower()}"
                        });

                    }, tokenSource.Token);
                };
            System.Threading.Timer searchBarTimer = null;
            string queryTxt = "";
            var viewModel = new SimpleRecyclerViewActivity.ViewModel(); 
              viewModel = new SimpleRecyclerViewActivity.ViewModel
            {

                Title = exam.JobTitle,
                SubTitle = exam.Institution,

                LayoutManager = new GridLayoutManager(Context, 2)
                ,ItemDecoration = //new GridSpacingItemDecoration(2,50,true)
                    new OffsetRecyclerItemDecoration(new Rect(spacing/2, spacing/2, spacing/2, spacing/2))
                ,RecyclerAdapter = Task.Run(async () =>
                {
                    var cards = await cardsTaskCreator("",null);
                    recyclerAdapter = new CardRecyclerAdapter<Discipline>(Context, cards);
                    recyclerAdapter.OnItemClicked += c =>
                    {
                        // ReSharper disable once AccessToModifiedClosure
                        OpenReviewSetCreator(c.Data,exam.Id,viewModel.Instance);
                        // ReSharper disable once AccessToModifiedClosure
//                        viewModel.Instance.Finish();
                    };
                    return (RecyclerView.Adapter)recyclerAdapter;
                })
                ,QueryTextChange = (sender, args) =>
                {
                    var tokenSource = new CancellationTokenSource();
                    ((SimpleRecyclerViewActivity)sender).Update(
                        Task.Run(async () => await cardsTaskCreator(queryTxt, tokenSource) as object,
                            tokenSource.Token), tokenSource);
                    //                    if(!string.IsNullOrEmpty(args.NewText) && args.NewText?.Length<3)
                    //                        return;

                },
                OnUpdatated = (sender, objects) =>
                {
                    if (objects is IEnumerable<ICard<Discipline>> cards)
                    {
                        recyclerAdapter.Items = cards;
                    }
                }
            };
            SimpleRecyclerViewActivity.Open(context ?? Context, viewModel, task);
        }

        private void OpenReviewSetCreator(Discipline discipline,int examId,Context context = null)
        {
            ReviewCreatorActivity.Open( context ?? Context,new ReviewCreatorActivity.ViewModel
            {
                Discipline = discipline,
                SubjectProgressTask = ExamApp.Instance.GetSubjectProgressesForDiciplines(examId,discipline.Id)
            });
        }

        private void OnHolderTileDetailsClicked(CardGroupTileViewHolder<ExamGroup, Exam> holder)
        {
            var cardWidth = Utils.Utils.DpToPx((int)AndroiExamApp.CARD_WIDTH_IN_DP);
            var layoutWidth = _layout.MeasuredWidth;

            var spacing = (layoutWidth - 2 * cardWidth) / 2;

            // ReSharper disable once ConvertToLocalFunction
            Func<string, CancellationTokenSource, Task<IEnumerable<ICard<Exam>>>> cardsTaskCreator =
                (query, tokenSource) =>
                {
                    tokenSource = tokenSource ?? new CancellationTokenSource();
                    return Task.Run(async () =>
                    {

                        var exams = await (string.IsNullOrEmpty(query) || query.Length<3?  ExamApp.Instance.GetExamsFromGroup(holder.Item.Data.Id) 
                                : ExamApp.Instance.SearchExams(query,null));
                           
                        return exams.Select(exam => (ICard<Exam>)new Card<Exam>
                        {
                            Title = exam.JobTitle,
                            SubTitle = exam.Institution,
                            Footer = $"{exam.Company} - {exam.Year}",
                            Data = exam
                        });

                    }, tokenSource.Token);
                };
            CardRecyclerAdapter<Exam> recyclerAdapter = null;
            SimpleRecyclerViewActivity.ViewModel viewModel = new SimpleRecyclerViewActivity.ViewModel();
            viewModel = new SimpleRecyclerViewActivity.ViewModel
            {

                Title = holder.Item.Title,
                LayoutManager = new GridLayoutManager(Context, 2){},
                ItemDecoration = new OffsetRecyclerItemDecoration(new Rect(spacing/2, spacing/2, spacing/2, spacing/2)),
                RecyclerAdapter = Task.Run(async () =>
                {
                    var cards = await cardsTaskCreator("",null);
                    recyclerAdapter = new CardRecyclerAdapter<Exam>(Context, cards);
                    recyclerAdapter.OnItemClicked += c =>
                    {
                        // ReSharper disable once AccessToModifiedClosure
                        OpenDisciplineSelectionActivity(c.Data,viewModel.Instance);
                    };
                    return (RecyclerView.Adapter)recyclerAdapter;
                }),
                QueryTextChange = (sender, args) =>
                {
                    //                    if (!string.IsNullOrEmpty(args.NewText) && args.NewText?.Length < 3)
                    //                        return;
                    var tokenSource = new CancellationTokenSource();
                    ((SimpleRecyclerViewActivity)sender).Update(
                        Task.Run(async () => (await cardsTaskCreator(args.NewText, tokenSource)) as object,
                            tokenSource.Token), tokenSource);




                },
                OnUpdatated = (sender, objects) =>
                {
                    if (objects is IEnumerable<ICard<Exam>> examCards)
                    {
                        recyclerAdapter.Items = examCards;
                    }
                }
            };
            SimpleRecyclerViewActivity.Open(Context, viewModel,MyTask.Create());
        }

        
    }
}