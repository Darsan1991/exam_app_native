﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.OS;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using Exam_App_Native.Droid.Activity;
using Exam_App_Native.Droid.Adapter;
using Exam_App_Native.Droid.Adapter.ViewHolder;
using Exam_App_Native.Droid.Fragment.Base;
using Exam_App_Native.Droid.Views;
using Exam_App_Native.Model;
using ProgressBar = Exam_App_Native.Droid.Views.ProgressBar;

namespace Exam_App_Native.Droid.Fragment
{
    public class ReviewsFragment : NameCompactPageFragment
    {
        private Context AppContext => AndroiExamApp.Instance;
        public override string Title => AppContext.GetString(Resource.String.reviews_text);
        public bool Loading => _progressGroup?.Visibility == ViewStates.Visible;

        private View _progressGroup;
        private View _emptyNotifactionGroup;
        private CancellationTokenSource _tokenSource;
        private SelectableRecyclerAdapter<ProgressBarViewHolder<ReviewSet>.IProgressItem, ProgressBarViewHolder<ReviewSet>> _recyclerAdapter;
        private ReviewSet _selectedItem;

        private ProgressBar _progressBar;
        private TextView _badgetTextView;
        private TextView _topicTextView;
        private Button _scheduledBtn;
        private View _badgeGroup;
        private View _headerGroup;
        private RecyclerView _recyclerView;

        private readonly List<CalenderAndQuestionText> _calenderAndQuestionTexts = new List<CalenderAndQuestionText>();


        private ReviewSet SelectedItem
        {
            get => _selectedItem;
            set
            {
                _selectedItem = value;

                if (value == null)
                    return;

                var schedules = value.Schedules.ToList();
               
                for (var i = 0; i < _calenderAndQuestionTexts.Count; i++)
                {
                    if (schedules.Count>i)
                    {
                        var text = _calenderAndQuestionTexts[i];

                        text.CalenderTextView.Text = schedules[i].Period.ToString();
                        text.QuestionTextView.Text = schedules[i].CardCount.ToString();
                        text.CalenderTextView.Visibility = ViewStates.Visible;
                        text.QuestionTextView.Visibility = ViewStates.Visible;
                    }
                    else
                    {
                        var text = _calenderAndQuestionTexts[i];
                        text.CalenderTextView.Visibility = ViewStates.Gone;
                        text.QuestionTextView.Visibility = ViewStates.Gone;
                    }
                }


                _topicTextView.Text = value.Name;
                _badgetTextView.Text = value.Badge.ToString();
                _scheduledBtn.Enabled = value.Badge > 0;
                _badgeGroup.Visibility = value.Badge > 0 ? ViewStates.Visible : ViewStates.Gone;
                _progressBar.SetProgress(value.Score/100f);
            }
        }


        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var view = inflater.Inflate(Resource.Layout.fragment_reviews, container, false);
             _progressBar = view.FindViewById<ProgressBar>(Resource.Id.progress_bar);
            _badgetTextView = view.FindViewById<TextView>(Resource.Id.badge_tv);
            _topicTextView = view.FindViewById<TextView>(Resource.Id.topic_tv);

            _calenderAndQuestionTexts.Add(new CalenderAndQuestionText
            {
                CalenderTextView = view.FindViewById<TextView>(Resource.Id.calender_1_tv),
                QuestionTextView = view.FindViewById<TextView>(Resource.Id.question_1_tv)
            });

            _calenderAndQuestionTexts.Add(new CalenderAndQuestionText
            {
                CalenderTextView = view.FindViewById<TextView>(Resource.Id.calender_2_tv),
                QuestionTextView = view.FindViewById<TextView>(Resource.Id.question_2_tv)
            });

            _calenderAndQuestionTexts.Add(new CalenderAndQuestionText
            {
                CalenderTextView = view.FindViewById<TextView>(Resource.Id.calender_3_tv),
                QuestionTextView = view.FindViewById<TextView>(Resource.Id.question_3_tv)
            });

            _calenderAndQuestionTexts.Add(new CalenderAndQuestionText
            {
                CalenderTextView = view.FindViewById<TextView>(Resource.Id.calender_4_tv),
                QuestionTextView = view.FindViewById<TextView>(Resource.Id.question_4_tv)
            });

            _calenderAndQuestionTexts.Add(new CalenderAndQuestionText
            {
                CalenderTextView = view.FindViewById<TextView>(Resource.Id.calender_5_tv),
                QuestionTextView = view.FindViewById<TextView>(Resource.Id.question_5_tv)
            });

            _calenderAndQuestionTexts.Add(new CalenderAndQuestionText
            {
                CalenderTextView = view.FindViewById<TextView>(Resource.Id.calender_6_tv),
                QuestionTextView = view.FindViewById<TextView>(Resource.Id.question_6_tv)
            });

            _calenderAndQuestionTexts.Add(new CalenderAndQuestionText
            {
                CalenderTextView = view.FindViewById<TextView>(Resource.Id.calender_7_tv),
                QuestionTextView = view.FindViewById<TextView>(Resource.Id.question_7_tv)
            });


            _badgeGroup = view.FindViewById(Resource.Id.badge_group);
            _scheduledBtn = view.FindViewById<Button>(Resource.Id.scheduled_btn);

            _recyclerView = view.FindViewById<RecyclerView>(Resource.Id.recycler_view);
            _headerGroup = view.FindViewById(Resource.Id.header_group);
            _progressGroup = view.FindViewById(Resource.Id.progress_group);
            _emptyNotifactionGroup = view.FindViewById(Resource.Id.empty_notifaction_group);
            view.FindViewById(Resource.Id.delete_btn).Click += (sender, args) =>
            {
                if (SelectedItem == null)
                {
                    ShowToast(_recyclerAdapter.Items.Any() ? "Please Select the Item First" : "You don't any reviews");
                    return;
                }

                if (SelectedItem!=null)
                ShowAlertAndDeleteItem(SelectedItem);
                else
                {
                    ShowToast("Select the Review Before Delete");
                }
            };

            view.FindViewById(Resource.Id.all_btn).Click += (sender, args) =>
            {
                if (SelectedItem == null)
                {
                    ShowToast(_recyclerAdapter.Items.Any() ? "Please Select the Item First" : "You don't any reviews");
                    return;
                }

                var viewModel = new QuizActivity.ViewModel
                {
                    ReviewSetStartUpInfos = Task.Run(async ()=>
                    {
                       var startup = await ExamApp.Instance.RestartAllReviewSet(SelectedItem.Id);
                        return (IEnumerable<ReviewSetStartUpInfo>)new[] {startup};
                    })

                };
                QuizActivity.Open(Context, viewModel);
            };
            
            _scheduledBtn.Click += (sender, args) =>
            {
                if (SelectedItem == null)
                {
                    ShowToast(_recyclerAdapter.Items.Any() ? "Please Select the Item First" : "You don't any reviews");
                    return;
                }

                var viewModel = new QuizActivity.ViewModel
                {
                    ReviewSetStartUpInfos = ExamApp.Instance.RetakeReviewSet(SelectedItem.Id)

                };
                QuizActivity.Open(Context, viewModel);
            };

            var layoutManager = new LinearLayoutManager(Context,LinearLayoutManager.Vertical,false);
            _recyclerView.SetLayoutManager(layoutManager);
            _recyclerView.AddItemDecoration(new DividerItemDecoration(Context,layoutManager.Orientation));
            _recyclerAdapter = new SelectableRecyclerAdapter<ProgressBarViewHolder<ReviewSet>.IProgressItem,ProgressBarViewHolder<ReviewSet> >(Context,Resource.Layout.review_progress_tile);

            //Set the Progress Bars
            _recyclerAdapter.OnCreatingViewHolder += holder =>
            {
                holder.ProgressBars = new[]
                {
                    new MultiProgressBar.ProgressBar
                    {
                        Color = Color.ParseColor("#7ad46d")
                    },
                    new MultiProgressBar.ProgressBar
                    {
                        Color = Color.ParseColor("#ed634c")
                    },
                };

                
            };

            _recyclerAdapter.OnSelectionChanged += item =>
            {
                if (item != null)
                    SelectedItem = item.Data;
            
            };
            _recyclerView.SetAdapter(_recyclerAdapter);
            return view;
        }

        private void ShowToast(string message)
        {
            Toast.MakeText(Context, message, ToastLength.Short)
                .Show();
        }

        private void ShowAlertAndDeleteItem(ReviewSet reviewSet)
        {
            var builder = new AlertDialog.Builder(Context);
            var alertDialog = builder.SetMessage(GetString(Resource.String.delete_message_text)).SetPositiveButton(GetString(Resource.String.yes_text),async (sender, args) =>
            {
                ShowProgressOverlay(0.3f);
                try
                {
                    _tokenSource = _tokenSource ?? new CancellationTokenSource();
                    await Auth.Instance.CurrentUser.DeleteReviewSet(reviewSet.Id, _tokenSource);
                    _recyclerAdapter.RemoveItem(_recyclerAdapter.Items.First(item => Equals(item.Data, reviewSet)));
                    SelectedItem = null;
                    UpdateVisibility();
                }
                catch (ExamAppException e)
                {
                    Toast.MakeText(Context, e.Message, ToastLength.Short).Show();
                }
                catch (System.OperationCanceledException e)
                {

                }
                catch (Exception e)
                {
                    Toast.MakeText(Context, "Unknown Error Occurs!", ToastLength.Short).Show();
                }

                HideProgressOverlay();
            }).SetNegativeButton(GetString(Resource.String.no_text),(sender, args) => {}).Create();
            alertDialog.Show();
        }



        public async void LoadOrUpdateReviews(int? selReviewId=null)
        {
            if (Loading)
            {
               return;
            }
            ShowProgressOverlay();
            _progressGroup.Visibility = ViewStates.Visible;
            try
            {
                _tokenSource = new CancellationTokenSource();
                var reviewSets = (await Auth.Instance.CurrentUser.GetReviewSets()).ToList();
               


                _recyclerAdapter.Items = reviewSets.Select(set => new ProgressBarViewHolder<ReviewSet>.ProgressItem
                {
                    Data = set,
                    Title = set.Name,
                    Values = new []
                    {
                        (float)set.Resolved.Hits/set.Resolved.Total
                        ,(float)set.Resolved.Mistakes/set.Resolved.Total
                    }
                });

                var recyclerAdapterItems = _recyclerAdapter.Items.ToList();
                if (recyclerAdapterItems.Any())
                {
                    _recyclerAdapter.Selected = selReviewId != null ? 
                        recyclerAdapterItems.Find((item) => item.Data.Id == selReviewId) 
                        : recyclerAdapterItems.First();
                    if(selReviewId!=null)
                    _recyclerView.GetLayoutManager().ScrollToPosition(recyclerAdapterItems.FindIndex((item) => item.Data.Id == selReviewId));

                }


                UpdateVisibility();
            }
            catch (Exception e)
            {
                
            }

            HideProgressOverlay();

        }

        private void UpdateVisibility()
        {
            _headerGroup.Visibility = _recyclerAdapter.Items.Any()? ViewStates.Visible : ViewStates.Gone;
            _emptyNotifactionGroup.Visibility = _recyclerAdapter.Items.Any() ? ViewStates.Gone : ViewStates.Visible;

        }

        private void CancelLoadOrUpdateReview()
        {
            if(Loading)
                _tokenSource.Cancel();
        }

        private void ShowProgressOverlay(float alpha=1f)
        {
            var color = ((ColorDrawable)_progressGroup.Background).Color;
            color.A = (byte) (alpha * 255);
            _progressGroup.SetBackgroundColor(color);
            _progressGroup.Visibility = ViewStates.Visible;
        }

        private void HideProgressOverlay() =>
            _progressGroup.Visibility = ViewStates.Gone;

        public override void OnPageAppear()
        {
            base.OnPageAppear();
            LoadOrUpdateReviews();
        }

        public override void OnPageDisAppear()
        {
            base.OnPageDisAppear();
            CancelLoadOrUpdateReview();
        }

        private struct CalenderAndQuestionText
        {
            public TextView CalenderTextView { get; set; }
            public TextView QuestionTextView { get; set; }
        }
    }
}