﻿using System;
using System.Threading.Tasks;
using Android.Content.Res;
using Android.Graphics;
using Android.OS;
using Android.Views;
using Android.Widget;
using Exam_App_Native.Model;

namespace Exam_App_Native.Droid.Fragment
{
    public class ResultFragment : Android.Support.V4.App.Fragment
    {
        public event Action OnClickNextOrComplete; 

        private static readonly Color CORRECT_COLOR = Color.ParseColor("#7bd46e");
        private static readonly Color WRONG_COLOR = Color.ParseColor("#ef433a");

        public bool IsViewCreated { get; private set; }

        private ActionButton CurrentActionButton
        {
            get => _currentActionButton;
            set
            {
                if (IsViewCreated)
                {
                    UpdateButtonUserIntForAction(value,AnsweredCorrectly);
                }
                _currentActionButton = value;
            }
        }

        private bool AnsweredCorrectly
        {
            get => _answeredCorrectly;

            set
            {
                if (IsViewCreated)
                {
                    UpdateButtonUserIntForAction(CurrentActionButton, value);
                    UpdateImage(value);
                }
                _answeredCorrectly = value;
            }
        }

        public CardEvalResultInfo CardEvalResultInfo => _task.IsCompleted ? _task.Result : null;

        private View _progressGroup;

        private Task<CardEvalResultInfo> _task;
        private TextView _titleTextView;
        private TextView _subTitleTextView;
        private TextView _questionTextView;
        private TextView _teacherTextView;
        private TextView _resultTextView;
        private Button _nextBtn;
        private ImageView _resultImageView;
        private  ActionButton _currentActionButton;
        private bool _answeredCorrectly;


        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            base.OnCreateView(inflater, container, savedInstanceState);
            var view = inflater.Inflate(Resource.Layout.fragment_result,container,false);
            _progressGroup = view.FindViewById(Resource.Id.progress_group);

            _titleTextView = view.FindViewById<TextView>(Resource.Id.title_tv);
            _subTitleTextView = view.FindViewById<TextView>(Resource.Id.subtitle_tv);
            _questionTextView = view.FindViewById<TextView>(Resource.Id.question_tv);
            _teacherTextView = view.FindViewById<TextView>(Resource.Id.teacher_name_tv);
            _resultTextView = view.FindViewById<TextView>(Resource.Id.result_tv);
            _resultImageView = view.FindViewById<ImageView>(Resource.Id.result_img_view);
            _nextBtn = view.FindViewById<Button>(Resource.Id.next_btn);

            _progressGroup.Visibility = (_task != null && (!_task.IsCompleted && !_task.IsCanceled && !_task.IsFaulted))
                ? ViewStates.Visible
                : ViewStates.Gone;

         
            if (_task != null && _task.IsCompleted)
            {
                UpdateTheUiForResult(_task.Result);
            }

            _nextBtn.Click += (sender, args) => OnClickNextOrComplete?.Invoke();

            IsViewCreated = true;
            return view;
        }

        public async void SetResult(Task<CardEvalResultInfo> task,bool lastReview=false)
        {
            if (IsViewCreated)
                _progressGroup.Visibility = ViewStates.Visible;
            _task = task;
            try
            {
                var resultInfo = await task;
                UpdateTheUiForResult(resultInfo);
            }
            catch (Exception e)
            {
                Toast.MakeText(Context,"Something went wrong!",ToastLength.Long).Show();
                
            }

            if (IsViewCreated)
                _progressGroup.Visibility = ViewStates.Gone;
        }

        private void UpdateTheUiForResult(CardEvalResultInfo info)
        {
            _titleTextView.Text = info.CurrentCard.Subject;
            _subTitleTextView.Text = info.CurrentCard.Topic;
            _teacherTextView.Text = info.CurrentCard.Teacher;

            _questionTextView.Text = info.Comment;
            _resultTextView.Text = info.Feedback;
            AnsweredCorrectly = info.AnswredCorrectly;
            CurrentActionButton = info.IsReviewFinished ? ActionButton.Complete : ActionButton.Next;
        }

        private void UpdateButtonUserIntForAction(ActionButton actionButton,bool answredCorrectly)
        {
            _nextBtn.Text = actionButton != ActionButton.Complete ?  GetString(Resource.String.next_text).ToUpper(): GetString(Resource.String.quiz_activity_completed).ToUpper();
            _nextBtn.BackgroundTintList = ColorStateList.ValueOf(answredCorrectly?  CORRECT_COLOR: WRONG_COLOR);
        }

        private void UpdateImage(bool answeredCorrectly)
        {
            _resultImageView.SetImageResource(answeredCorrectly?
                Resource.Mipmap.right_img:Resource.Mipmap.wrong_img);
        }

        private enum ActionButton
        {
            Next,Complete
        }
    }
}