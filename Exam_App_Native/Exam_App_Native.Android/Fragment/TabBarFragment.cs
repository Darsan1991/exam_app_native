﻿using System;
using System.Collections.Generic;
using System.Linq;
using Android.Content;
using Android.Content.Res;
using Android.Graphics;
using Android.OS;
using Android.Views;
using Android.Widget;
using Orientation = Android.Widget.Orientation;

namespace Exam_App_Native.Droid.Fragment
{
    public class TabBarFragment : Android.App.Fragment
    {
        public event Action<TabBarItem> OnTabBarItemSelected;


        #region Poperties
        public IEnumerable<TabBarItem> TabBarItems
        {
            get { return tabBarItemHolders.Select((arg) => arg.tabBarItem); }
            set{
                if(tabBarItemHolders.Count>0)
                {
                    throw new NotImplementedException("Changing Tab Bar not implemented yet!");
 //                   tabBarItems.ForEach((obj) => ;);
                }



                tabBarItemHolders.Clear();
                if(value!=null)
                {
                    foreach(var item in value)
                    {
                        var holder = new TabBarItemHolder(Context, item);
                            tabContainer.AddView(holder.view);
                        holder.OnClicked += (obj) => OnTabBarItemSelected?.Invoke(obj.tabBarItem);
                        tabBarItemHolders.Add(holder);
                    }
                }
            }
        }

        public TabBarItem SelectedItem { get { return tabBarItemHolders[selectedPosition].tabBarItem; } set{
                if (value == (selectedPosition >= 0 ? tabBarItemHolders[selectedPosition].tabBarItem : null))
                    return;
                if (value == null)
                    throw new NotImplementedException("Not implemented to hadle null");
                GetPosition(value);
            }}
        public int SelectedPosition{ get { return selectedPosition; } set { 
                if(selectedPosition!=value)
                {
                    if (selectedPosition >= 0)
                        tabBarItemHolders[selectedPosition].Selected = false;

                    selectedPosition = value;
                    tabBarItemHolders[selectedPosition].Selected = true;
                    
                }
            } }
#endregion

        private readonly List<TabBarItemHolder> tabBarItemHolders = new List<TabBarItemHolder>();
        private int selectedPosition = -1;
        private View m_Layout;
        private ViewGroup tabContainer;

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            m_Layout = inflater.Inflate(Resource.Layout.fragment_home_tab_bar, container, false);
            tabContainer = m_Layout.FindViewById<LinearLayout>(Resource.Id.tab_container);
    
            //tabContainer.AddView(new TabBarItemHolder(Context, new TabBarItem("Exams", Resource.Drawable.abc_ic_star_black_48dp,0)).view);
            //tabContainer.AddView(new TabBarItemHolder(Context, new TabBarItem("Disciplines", Resource.Drawable.abc_ic_star_black_48dp,1)).view);
            //tabContainer.AddView(new TabBarItemHolder(Context, new TabBarItem("My Cards", Resource.Drawable.abc_ic_star_black_48dp,2)).view);
            //tabContainer.AddView(new TabBarItemHolder(Context, new TabBarItem("Reviews", Resource.Drawable.abc_ic_star_black_48dp,3)).view);
            //tabContainer.AddView(new TabBarItemHolder(Context, new TabBarItem("Statistics", Resource.Drawable.abc_ic_star_black_48dp,4)).view);

            return m_Layout;
        }


        public int GetPosition(TabBarItem tabBarItem) => 
        tabBarItemHolders.FindIndex((obj) => obj.tabBarItem == tabBarItem);


        private class TabBarItemHolder
        {
            public event Action<TabBarItemHolder> OnClicked;

            public bool Selected
            {
                get => selected;
                set
                {
                    selected = value;
                    view.Alpha = selected ? 1 : 0.5f;
                }
            }

            public TabBarItem tabBarItem { get; }

            public readonly View view;
            private bool selected;

            public TabBarItemHolder(Context context,TabBarItem tabBarItem)
            {
                this.tabBarItem = tabBarItem;
                view = new LinearLayout(context)
                {
                    Orientation = Orientation.Vertical,
                    LayoutParameters = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MatchParent, 1),
//                    Background = new ColorDrawable(Color.Blue)
                };
                view.SetPadding(Utils.Utils.DpToPx(3), Utils.Utils.DpToPx(3), Utils.Utils.DpToPx(3), Utils.Utils.DpToPx(3));

                var imageView = new ImageView(context)
                {
                    LayoutParameters =
                        new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MatchParent, 0, 2)
                        {
                            Gravity = GravityFlags.Center
                        }
//                    ,Background = new ColorDrawable(Color.Coral),
                };
                imageView.SetImageDrawable(context.GetDrawable(tabBarItem.ImageId));
//                var filter = new PorterDuffColorFilter(Color.ParseColor("#82878f"),PorterDuff.Mode.Multiply);
//                imageView.SetColorFilter(filter);
                imageView.SetPadding(Utils.Utils.DpToPx(3), Utils.Utils.DpToPx(5), Utils.Utils.DpToPx(3), Utils.Utils.DpToPx(3));
                imageView.SetScaleType(ImageView.ScaleType.CenterInside);
                ((LinearLayout) view).AddView(imageView);

                var textView = new TextView(context)
                {
                    LayoutParameters =
                        new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MatchParent, 0, 1)
                        {
                            Gravity = GravityFlags.Center
                        },
                    Text = tabBarItem.Name,
                    TextSize = 10,
                    Gravity = GravityFlags.Center
                };
                ((LinearLayout) view).AddView(textView);
                view.Click += (sender, args) => { OnClicked?.Invoke(this);};
                Selected = false;
            }
        }
    }


     public class TabBarItem
    {
        public string Name { get; }
        public int ImageId { get; }
        public int Position { get; }

        public TabBarItem(string name, int imageId, int position)
        {
            Name = name;
            ImageId = imageId;
            Position = position;
        }
    }
    
}