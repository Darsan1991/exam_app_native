﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Android.Content;
using Android.OS;
using Android.Service.QuickSettings;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using Exam_App_Native.Droid.Activity;
using Exam_App_Native.Droid.Adapter;
using Exam_App_Native.Droid.Adapter.ViewHolder;
using Exam_App_Native.Droid.Adapter.ViewHolder.General;
using Exam_App_Native.Droid.Fragment.Base;
using Exam_App_Native.Model;
using Exam_App_Native.Model.ViewModel;
using SearchView = Android.Support.V7.Widget.SearchView;

namespace Exam_App_Native.Droid.Fragment
{
    public class MyCardsFragment : NameCompactPageFragment
    {
        private Context AppContext => AndroiExamApp.Instance;

        public const int NEW_MENU_BTN_ID = 10;

        private readonly Auth _auth = Auth.Instance;


        private View _progressGroup;
        private CancellationTokenSource _tokenSource;
        private Task<IEnumerable<MyDiscipline>> _task;
        private SimpleRecyclerAdapter<IItem<MyDiscipline>, TileRecyclerViewHolder<MyDiscipline>> _recyclerAdapter;
        private SearchView _searchView;
        private IMenuItem _newButton;
        private Timer _searchBarTimer;
        public override string Title => AppContext.GetString(Resource.String.my_cards_text);

        public bool Loading
        {
            get => _progressGroup?.Visibility == ViewStates.Visible;
            set
            {
                if (_progressGroup != null)
                {
                    _progressGroup.Visibility = value ? ViewStates.Visible : ViewStates.Gone;
                }

                _newButton?.SetEnabled(!value);
            }
        }

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            HasOptionsMenu = true;
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var layout = inflater.Inflate(Resource.Layout.fragment_my_cards, container, false);
            var recyclerView = layout.FindViewById<RecyclerView>(Resource.Id.recycler_view);
            _progressGroup = layout.FindViewById(Resource.Id.progress_group);
            _searchView = layout.FindViewById<Android.Support.V7.Widget.SearchView>(Resource.Id.search_bar);

            _searchView.QueryTextChange += (sender, args) =>
            {
                if (_searchBarTimer == null)
                {

                    _searchBarTimer = new System.Threading.Timer(
                        state => Activity.RunOnUiThread(() => LoadOrUpdate(_searchView.Query))
                        , null
                        , 200
                        , Timeout.Infinite);


                }
                else
                {
                    _searchBarTimer.Change(200, Timeout.Infinite);
                }

            };


            _searchView.Click += (sender, args) =>
            {
                _searchView.Focusable = true;
                _searchView.Iconified = false;
            };

            var layoutManager = new LinearLayoutManager(Context);
            recyclerView.SetLayoutManager(layoutManager);
            recyclerView.AddItemDecoration(new DividerItemDecoration(Context, layoutManager.Orientation));
            _recyclerAdapter =
                new SimpleRecyclerAdapter<IItem<MyDiscipline>, TileRecyclerViewHolder<MyDiscipline>>(Context,
                    Resource.Layout.simple_title_details_tile);
            
            recyclerView.SetAdapter(_recyclerAdapter);
            _recyclerAdapter.OnItemClicked += tile => { OpenMyCards(tile.Data); };
            return layout;
        }

        public override void OnResume()
        {
            base.OnResume();
            if (Activity is MainActivity main)
            {
                if (main.Selected == this)
                {
                    LoadOrUpdate();
                }
            }
        }


        private void OpenMyCards(MyDiscipline discipline)
        {
            SimpleRecyclerAdapter<MyCard, FlashCardViewHolder> recyclerAdapter = null;
            var viewModel = new SimpleRecyclerViewActivity.ViewModel
            {
                LayoutManager = new LinearLayoutManager(Context, LinearLayoutManager.Vertical, false),
                ItemDecoration = new DividerItemDecoration(Context, LinearLayoutManager.Vertical),
                Title = discipline.Name + " Cards"
            };

            // ReSharper disable once ConvertToLocalFunction
            Func<string, MyDiscipline, CancellationTokenSource, Task<IEnumerable<MyCard>>> loadCards =
                (query, myDiscipline, token) =>
                {
                    return Task.Run(async () =>
                    {
                        var cards = (string.IsNullOrEmpty(query) || query.Length < 3)
                            ? (await _auth.CurrentUser.GetMyCardsForDiscipline(discipline.Id, token)).ToList()
                            : (await _auth.CurrentUser.SearchMyCards(query,
                                new Discipline {Name = discipline.Name, Id = discipline.Id}, token)).ToList();
                        return cards.Select(card =>
                        {
                            if (myDiscipline != null)
                                card.Discipline = new Discipline
                                {
                                    Name = myDiscipline.Name,
                                    Id = myDiscipline.Id
                                };
                            return card;
                        });
                    }, token.Token);
                };

            //Setup Recycler Adapter
            viewModel.RecyclerAdapter = Task.Run(async () =>
            {
                Console.WriteLine($"Auth:{_auth.CurrentUser}");
                var cards = await loadCards(null, discipline, new CancellationTokenSource());
                recyclerAdapter = new SimpleRecyclerAdapter<MyCard, FlashCardViewHolder>(Context,
                    Resource.Layout.flash_card_tile, cards);
                recyclerAdapter.OnItemClicked += card =>
                {
                    UserFlashCardActivity.Open(Context, new UserFlashCardActivity.ViewModel
                    {
                        MyCard = card,
                        OnComplete = (sender, args) =>
                        {
                            try
                            {
                                viewModel.Instance.Update(Task.Run(async () =>
                                {
                                    await args.Task;
                                    var myCards = await loadCards(null, discipline, new CancellationTokenSource());
                                    return (object) myCards;
                                }), null);
                            }
                            catch (Exception e)
                            {
                                // ignored
                            }
                        }
                    });
                };
                return (RecyclerView.Adapter) recyclerAdapter;
            });

            //Update For Query Text Changed
            viewModel.QueryTextChange = (sender, args) =>
            {
                var tokenSource = new CancellationTokenSource();
                ((SimpleRecyclerViewActivity) sender).Update(Task.Run(async () =>
                {
                    var myCards = await loadCards(args.NewText, discipline, tokenSource);
                    return (object) myCards;
                }), tokenSource);
            };

            viewModel.OnUpdatated = (sender, o) =>
            {
                if (o is IEnumerable<MyCard> cards)
                {
                    recyclerAdapter.Items = cards;
                }
            };

            SimpleRecyclerViewActivity.Open(Context, viewModel);
        }

        public override void OnCreateOptionsMenu(IMenu menu, MenuInflater inflater)
        {
            base.OnCreateOptionsMenu(menu, inflater);
            _newButton = menu.Add(Menu.None, NEW_MENU_BTN_ID, Menu.None, GetString(Resource.String.new_text));
            _newButton.SetShowAsAction(ShowAsAction.Always);
        }


        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case NEW_MENU_BTN_ID:
                    UserFlashCardActivity.Open(Context, new UserFlashCardActivity.ViewModel
                    {
                        OnComplete = async (sender, args) =>
                        {
                            await args.Task;
                            LoadOrUpdate(_searchView.Query);
                        }
                    });
                    break;
            }

            return base.OnOptionsItemSelected(item);
        }


        public override void OnPageAppear()
        {
            base.OnPageAppear();
            var query = _searchView?.Query;

            LoadOrUpdate(query?.Length > 2 ? query : null);
        }

        public override void OnPageDisAppear()
        {
            base.OnPageDisAppear();
            CancelLoadOrUpdate();
        }

        private async void LoadOrUpdate(string filter = null)
        {
            if (_task != null && (!_task.IsCompleted && !_task.IsCanceled && !_task.IsFaulted))
            {
                CancelLoadOrUpdate();
            }

            Loading = true;


            try
            {
                _tokenSource = new CancellationTokenSource();
                _task = string.IsNullOrEmpty(filter) || filter.Length<3
                    ? _auth.CurrentUser.GetMyCardsDisciplines(_tokenSource)
                    : _auth.CurrentUser.SearchMyCardsDisciplines(filter, _tokenSource);
                var disciplines = await _task;
                _recyclerAdapter.Items = disciplines.Select(discipline => new Item<MyDiscipline>
                {
                    Title = discipline.Name,
                    SubTitle = discipline.Subjects.AllIntoOne<MyDiscipline.Subject, string>((subject, s) =>
                        s = (s == null ? "" : (s + ",")) + subject.Name),
                    Data = discipline
                });
            }

            catch (Exception e)
            {
//                Toast.MakeText(Context,"Something went wrong with the connection",ToastLength.Short).Show();
            }

            Loading = false;
        }

        private void CancelLoadOrUpdate()
        {
            if (Loading)
            {
                _tokenSource.Cancel();
            }
        }
    }
}