﻿using System.Collections.Generic;
using System.Linq;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Views;
using Android.Widget;
using MikePhil.Charting.Charts;
using MikePhil.Charting.Components;
using MikePhil.Charting.Data;
using MikePhil.Charting.Formatter;

namespace Exam_App_Native.Droid.Fragment
{
    public class LineChartFragment : Android.Support.V4.App.Fragment
    {

        private Context AppContext => AndroiExamApp.Instance;

        public bool ShowTrendLine
        {
            get => _showTrendLine;
            set
            {
                if(value == _showTrendLine)
                    return;

                if (_trendLineDataSet != null)
                {
                    if (value && !_lineData.Contains(_trendLineDataSet))
                        _lineData.AddDataSet(_trendLineDataSet);
                    else if(_lineData.Contains(_trendLineDataSet))
                        _lineData.RemoveDataSet(_trendLineDataSet);

                    if (IsViewCreated)
                    {
                        _lineChart.Invalidate();
//                        _lineChart.NotifyDataSetChanged();
                    }

                }

                _showTrendLine = value;
            }
        }

        public bool IsViewCreated { get; private set; }

        public ViewModel MViewModel
        {
            get => _mViewModel;
            set
            {

                _knownCardLineDataSet = new LineDataSet(new List<Entry>(value.KnownCardValues.Select(pair => new Entry(pair.Key,pair.Value))),$"{AppContext.GetString(Resource.String.no_of_cards_you_know)}(%)");
                _knownCardLineDataSet.SetMode(LineDataSet.Mode.CubicBezier);
                _knownCardLineDataSet.Color = Color.ParseColor("#5078d7");
                _knownCardLineDataSet.ValueTextSize = 0;

                _trendLineDataSet = new LineDataSet(new List<Entry>(value.TrendLineValues.Select(pair => new Entry(pair.Key, pair.Value))), "Trend Line");
                _trendLineDataSet.EnableDashedLine(10f,10f,0f);
                _trendLineDataSet.Color = Color.ParseColor("#5078d7");
                _trendLineDataSet.ValueTextSize = 0;


                _lineData.ClearValues();
                _lineData.AddDataSet(_knownCardLineDataSet);
                
                if(ShowTrendLine)
                    _lineData.AddDataSet(_trendLineDataSet);

                if (IsViewCreated)
                {
                    _lineChart.NotifyDataSetChanged();
                    _titleTextView.Text = value.Title;
                }

                _mViewModel = value;
            }
        }

        private readonly LineData _lineData = new LineData();
        private LineChart _lineChart;
        private ViewModel _mViewModel;
        private LineDataSet _knownCardLineDataSet;
        private LineDataSet _trendLineDataSet;
        private bool _showTrendLine;
        private TextView _titleTextView;


        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var view = inflater.Inflate(Resource.Layout.fragment_line_chart, container, false);

            _titleTextView = view.FindViewById<TextView>(Resource.Id.title_tv);

            _titleTextView.Text = MViewModel?.Title;

            _lineChart = new LineChart(Context);
            view.FindViewById<ViewGroup>(Resource.Id.map_container).AddView(_lineChart);
            var parameters = _lineChart.LayoutParameters;
            parameters.Height = ViewGroup.LayoutParams.MatchParent;
            parameters.Width = ViewGroup.LayoutParams.MatchParent;

            _lineChart.LayoutParameters = parameters;

            _lineChart.SetDrawGridBackground(false);
            _lineChart.HighlightPerTapEnabled = false;
            _lineChart.HighlightPerDragEnabled = false;
            _lineChart.Description = null;
            _lineChart.AxisRight.Enabled = false;


            _lineChart.XAxis.AxisMinimum = 0;
            _lineChart.XAxis.AxisMaximum = 11;
            _lineChart.XAxis.SetLabelCount(11, true);
            _lineChart.XAxis.ValueFormatter = new MonthAxisValueFormatter(AppContext,0);
           
            _lineChart.Data = _lineData;

            IsViewCreated = true;
            return view;
        }

        public override void OnDestroyView()
        {
            IsViewCreated = false;
            base.OnDestroyView();
        }

        public class ViewModel
        {
            public Dictionary<int,float> KnownCardValues { get; set; }
            public Dictionary<int,float> TrendLineValues { get; set; }
            public string Title { get; set; }
        }

        public class MonthAxisValueFormatter : DefaultAxisValueFormatter
        {
            private readonly Context _context;

            public MonthAxisValueFormatter(Context context,int digits) : base(digits)
            {
                _context = context;
            }

            public override string GetFormattedValue(float value, AxisBase axis)
            {
                var month = (int)value;
                switch (month)
                {
                    case 0:
                        return _context.GetString(Resource.String.jan);
                    case 1:
                        return _context.GetString(Resource.String.feb);
                    case 2:
                        return _context.GetString(Resource.String.mar);
                    case 3:
                        return _context.GetString(Resource.String.api);
                    case 4:
                        return _context.GetString(Resource.String.may);
                    case 5:
                        return _context.GetString(Resource.String.jun);
                    case 6:
                        return _context.GetString(Resource.String.jul);
                    case 7:
                        return _context.GetString(Resource.String.aug);
                    case 8:
                        return _context.GetString(Resource.String.sep);
                    case 9:
                        return _context.GetString(Resource.String.oct); 
                    case 10:
                        return _context.GetString(Resource.String.nov);
                    case 11:
                        return _context.GetString(Resource.String.dec);
                }

                return "";
                //                return base.GetFormattedValue(value, axis);
            }
        }
    }
}