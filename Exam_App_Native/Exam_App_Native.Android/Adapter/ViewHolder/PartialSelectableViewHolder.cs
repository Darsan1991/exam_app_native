﻿using System;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using Exam_App_Native.Droid.Views;
using Exam_App_Native.Model;
using Exam_App_Native.Model.ViewModel;

namespace Exam_App_Native.Droid.Adapter.ViewHolder
{
    public class PartialSelectableViewHolder<T> : RecyclerView.ViewHolder, IPartialSelectableHolder<IPartialSelectableItem<T>>
    {
        public event Action<PartialSelectableViewHolder<T>> OnStateChanged;
        public event Action<PartialSelectableViewHolder<T>> RightArrowClicked;

        public IPartialSelectableItem<T> Item
        {
            get => _item;
            set
            {

                _titleTextView.Text = value.Title;
                _detailsTextView.Text = value.SubTitle;
                _item = value;
                State = value.State;
            }
        }

        private readonly PartialToggleSwitchView _partialButton;
        private readonly TextView _titleTextView;
        private readonly TextView _detailsTextView;
        private IPartialSelectableItem<T> _item;
        private View _rightArrowGroup;

        public SelectableState State
        {
            get => _partialButton.CurrentState ;
            set
            {
                if (_partialButton.CurrentState == value && Item.State == value)
                {
                    return;
                }

                _partialButton.CurrentState = value;

                Item.State = value;



                OnStateChanged?.Invoke(this);
            }
        }

        public PartialSelectableViewHolder(View itemView) : base(itemView)
        {
            _partialButton = new PartialToggleSwitchView(itemView.FindViewById<ImageView>(Resource.Id.toggle_iv));
            _titleTextView = itemView.FindViewById<TextView>(Resource.Id.title_tv);
            _detailsTextView = itemView.FindViewById<TextView>(Resource.Id.details_tv);
            _rightArrowGroup = itemView.FindViewById(Resource.Id.right_arrow_group);
            itemView.Click += (sender, args) =>
                {
                    State = State == SelectableState.None ? SelectableState.All : SelectableState.None;
                };
            _rightArrowGroup.Click += (sender, args) => RightArrowClicked?.Invoke(this);
            _partialButton.ValueChanged += (sender, args) => State = args;
        }

    }
}