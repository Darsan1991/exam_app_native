﻿using System;
using Android.Content;
using Android.Graphics;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using Exam_App_Native.Droid.Views;
using Exam_App_Native.Model;
using Exam_App_Native.Model.ViewModel;

namespace Exam_App_Native.Droid.Adapter.ViewHolder.General
{
    public class CardGroupTileViewHolder<J,T> : RecyclerView.ViewHolder, IHolder<IItemGroup<J,ICard<T>>>
    {
        public event Action<CardGroupTileViewHolder<J, T>> OnDetailsClicked;
        public event Action<CardGroupTileViewHolder<J,T>,ICard<T>> OnChildItemClicked;


        public IItemGroup<J,ICard<T>> Item
        {
            get => item;
            set
            {
                item = value;
                titleTextView.Text = item.Title;
                recyclerAdapter.Items = item;
            }
        }

        public bool InteractableDetails
        {
            get => interactableDetails;
            set
            {
                if (interactableDetails == value)
                {
                    return;
                }

                interactableDetails = value;

                if (interactableDetails)
                {
                    titleGroup.Click += OnTitleGroupOnClick;
                    titleGroup.SetBackgroundResource(Utils.Utils.GetSelectableResource(context));
                }
                else
                {
                    titleGroup.Click -= OnTitleGroupOnClick;
                    titleGroup.Background = null;
                }
            }
        }



        private readonly TextView titleTextView;
        private readonly CardRecyclerAdapter<T> recyclerAdapter;
        private readonly View titleGroup;
        private readonly Context context;

        private IItemGroup<J, ICard<T>> item;
        private bool interactableDetails;
 

        public CardGroupTileViewHolder(View itemView) : base(itemView)
        {
            context = itemView.Context;
            titleTextView = itemView.FindViewById<TextView>(Resource.Id.title_tv);
            var recyclerView = itemView.FindViewById<RecyclerView>(Resource.Id.recycler_view);
            titleGroup= itemView.FindViewById(Resource.Id.title_group);
            var layoutManager = new LinearLayoutManager(itemView.Context,LinearLayoutManager.Horizontal,false);
            recyclerView.SetLayoutManager(layoutManager);
            recyclerAdapter = new CardRecyclerAdapter<T>(itemView.Context);
            recyclerAdapter.OnItemClicked += OnRecyclerAdapterOnOnItemClicked;
            recyclerView.AddItemDecoration(new OffsetRecyclerItemDecoration(new Rect(0,0,30,0)));
            recyclerView.SetAdapter(recyclerAdapter);

            InteractableDetails = true;
           
        }

        void OnRecyclerAdapterOnOnItemClicked(ICard<T> card) => OnChildItemClicked?.Invoke(this, card);
        void OnTitleGroupOnClick(object sender, EventArgs args) => OnDetailsClicked?.Invoke(this);

    }

   
}