﻿using System;
using Android.Runtime;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using Exam_App_Native.Model;
using Exam_App_Native.Model.ViewModel;

namespace Exam_App_Native.Droid.Adapter.ViewHolder.General
{
    public class TileRecyclerViewHolder<T> : RecyclerView.ViewHolder, IHolder<IItem<T>>
    {
        public IItem<T> Item
        {
            get => item;
            set
            {
                item = value;
                titleTV.Text = item.Title;

                subTitleTV.Text = item.SubTitle;
                subTitleTV.Visibility = string.IsNullOrEmpty(subTitleTV.Text) ? ViewStates.Gone : ViewStates.Visible;
            }
        }

        private readonly TextView titleTV, subTitleTV;
        private IItem<T> item;

        public TileRecyclerViewHolder(View itemView) : base(itemView)
        {
            titleTV = itemView.FindViewById<TextView>(Resource.Id.title_tv);
            subTitleTV = itemView.FindViewById<TextView>(Resource.Id.sub_title_tv);
        }
    }


}