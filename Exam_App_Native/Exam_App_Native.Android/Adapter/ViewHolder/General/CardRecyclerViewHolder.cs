﻿using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using Exam_App_Native.Model;
using Exam_App_Native.Model.ViewModel;

namespace Exam_App_Native.Droid.Adapter.ViewHolder.General
{
    public class CardRecyclerViewHolder<T>:RecyclerView.ViewHolder,IHolder<ICard<T>>
    {
        public ICard<T> Item
        {
            get => card;
            set
            {
                card = value;
                titleTV.Text = card.Title;
                
                subTitleTV.Text = card.SubTitle;
                subTitleTV.Visibility = string.IsNullOrEmpty(subTitleTV.Text) ? ViewStates.Gone : ViewStates.Visible;
                footerTV.Text = card.Footer;
            }
        }

        private readonly TextView titleTV,subTitleTV,footerTV;
        private ICard<T> card;

        public CardRecyclerViewHolder(View itemView) : base(itemView)
        {
            titleTV = itemView.FindViewById<TextView>(Resource.Id.title_tv);
            subTitleTV = itemView.FindViewById<TextView>(Resource.Id.sub_title_tv);
            footerTV = itemView.FindViewById<TextView>(Resource.Id.footer_tv);
        }
    }
}