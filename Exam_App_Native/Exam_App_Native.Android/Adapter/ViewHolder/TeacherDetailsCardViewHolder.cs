﻿using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using Exam_App_Native.Model;
using Exam_App_Native.Model.ViewModel;

namespace Exam_App_Native.Droid.Adapter.ViewHolder
{
    public class TeacherDetailsCardViewHolder:RecyclerView.ViewHolder,IHolder<IItem<object>>
    {
        public IItem<object> Item
        {
            get => _item;
            set
            {
                _titleTV.Text = value?.Title;
                _contentTV.Text = value?.SubTitle;
                _item = value;
            }
        }

        public int TextMarginLeft
        {
            get => ((ViewGroup.MarginLayoutParams)_contentTV.LayoutParameters).LeftMargin;
            set
            {
              var p =  (ViewGroup.MarginLayoutParams)_contentTV.LayoutParameters;
                  p.LeftMargin = value;
                _contentTV.LayoutParameters = p;
            }
        }

        private readonly TextView _titleTV;
        private readonly TextView _contentTV;
        private IItem<object> _item;

        public TeacherDetailsCardViewHolder(View itemView) : base(itemView)
        {
            _titleTV = itemView.FindViewById<TextView>(Resource.Id.title_tv);
            _contentTV = itemView.FindViewById<TextView>(Resource.Id.content_tv);
        }


    }
}