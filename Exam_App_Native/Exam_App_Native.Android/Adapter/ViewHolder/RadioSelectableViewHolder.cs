﻿using System;
using Android.Graphics;
using Android.Runtime;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using Exam_App_Native.Model;
using Exam_App_Native.Model.ViewModel;

namespace Exam_App_Native.Droid.Adapter.ViewHolder
{
    public class RadioSelectableHolderViewHolder:RecyclerView.ViewHolder,ISelectableHolder<IItem>
    {

        private IItem _item;

        public IItem Item
        {
            get => _item;
            set
            {
                _textView.Text = value?.Title;

                if (_subTitleTextView != null)
                    _subTitleTextView.Text = value?.SubTitle;

                _item = value;
            }
        }

        public bool SelectedItem
        {
            get => _radioButton.Checked;
            set
            {
                var color = Color.Gray;
                color.A = 40;
                ItemView.SetBackgroundColor(value?color:Color.White);
                _radioButton.Checked = value;
                _radioButton.Invalidate();
            }
        }

        private readonly TextView _textView,_subTitleTextView;
        private readonly RadioButton _radioButton;

        public RadioSelectableHolderViewHolder(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
        {
        }

        public RadioSelectableHolderViewHolder(View itemView) : base(itemView)
        {
            _textView = itemView.FindViewById<TextView>(Resource.Id.title_tv);
            _subTitleTextView = itemView.FindViewById(Resource.Id.sub_title_tv) as TextView;
            _radioButton = itemView.FindViewById<RadioButton>(Resource.Id.radio_btn);
        }        
    }
}