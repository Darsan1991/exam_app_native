﻿using System;
using System.Collections.Generic;
using System.Linq;
using Android.Graphics;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using Exam_App_Native.Droid.Views;
using Exam_App_Native.Model;
using Exam_App_Native.Model.ViewModel;

namespace Exam_App_Native.Droid.Adapter.ViewHolder
{
    public class ProgressBarViewHolder<T> : RecyclerView.ViewHolder, ISelectableHolder<ProgressBarViewHolder<T>.IProgressItem>
    {
        public IProgressItem Item
        {
            get => _item;
            set
            {
                if (value == null)
                {
                    throw new Exception("Value Cannot be null");
                }

                if (_multiProgressBar.ProgressBars.Count()!=value.Values.Count())
                {
                    throw new Exception("Values and Progress bar count does't match");
                }

                _titleTV.Text = value.Title;
                _multiProgressBar.SetValues(value.Values.Select(f => 0f),false);
                _multiProgressBar.SetValues(value.Values);

                _item = value;
            }
        }

        public IEnumerable<MultiProgressBar.ProgressBar> ProgressBars
        {
            get => _multiProgressBar.ProgressBars;
            set => _multiProgressBar.ProgressBars = value;
        }

        public bool SelectedItem
        {
            get => _selected;
            set
            {
                var color = Color.ParseColor("#478ecb");
                color.A = 40;

                var clearColor = Color.White;
                clearColor.A = 0;

                ItemView.SetBackgroundColor(value ? color : clearColor);
                _selected = value;
            }
        }

        private readonly MultiProgressBar _multiProgressBar;
        private readonly TextView _titleTV;
        private bool _selected;
        private IProgressItem _item;

        public ProgressBarViewHolder(View itemView) : base(itemView)
        {
            _multiProgressBar = itemView.FindViewById<MultiProgressBar>(Resource.Id.progress_bar);
            _multiProgressBar.BackGroudColor = Color.ParseColor("#d7d8d8");
            _titleTV = itemView.FindViewById<TextView>(Resource.Id.title_tv);
        }

        public interface IProgressItem
        {
            string Title { get; set; }
            IEnumerable<float> Values { get; set; }
            T Data { get; set; }
        }

        public class ProgressItem:IProgressItem
        {
            public string Title { get; set; }
            public IEnumerable<float> Values { get; set; }
            public T Data { get; set; }
        }

    }
}