﻿using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using Exam_App_Native.Model;

namespace Exam_App_Native.Droid.Adapter.ViewHolder
{
    public class SubscriptionCardViewHolder:RecyclerView.ViewHolder,IHolder<SubscriptionCardViewHolder.ViewModel>
    {
        public ViewModel Item
        {
            get => _item;
            set
            {
                _leftCardTitleTv.Text = value.LeftCardTitle;
                _rightCardTitleTv.Text = value.RightCardTitle;
                _contextTv.Text = value.Content;
                _item = value;
            }
        }

        private readonly TextView _leftCardTitleTv, _rightCardTitleTv, _contextTv;
        private ViewModel _item;

        public SubscriptionCardViewHolder(View itemView) : base(itemView)
        {
            _leftCardTitleTv = itemView.FindViewById<TextView>(Resource.Id.left_card_title);
            _rightCardTitleTv = itemView.FindViewById<TextView>(Resource.Id.right_card_title);
            _contextTv = itemView.FindViewById<TextView>(Resource.Id.context_tv);
        }


        public struct ViewModel
        {
            public string Content { get; set; }
            public string LeftCardTitle { get; set; }
            public string RightCardTitle { get; set; }
        }

       
    }
}