﻿using Android.Graphics;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using Exam_App_Native.Model;
using Java.Lang;

namespace Exam_App_Native.Droid.Adapter.ViewHolder
{
    public class FlashCardViewHolder:RecyclerView.ViewHolder,IHolder<MyCard>
    {
        private static readonly Color TRUE_COLOR = Color.ParseColor("#7fbeec");
        private static readonly Color FALSE_COLOR = Color.Red;

        private readonly TextView _titleTextView;
        private readonly TextView _answerTextView;
        private readonly TextView _topicTextView;
        private MyCard _item;

        public MyCard Item
        {
            get => _item;
            set
            {
                if(value == null)
                    throw new NullPointerException("Item Cannot be null For Now!");

                _titleTextView.Text = value.Content;
                _topicTextView.Text = value.Topic.Name;
                _answerTextView.SetTextColor((value.Answer??false)?TRUE_COLOR:FALSE_COLOR);
                _answerTextView.Text = (value.Answer??false) ? "True" : "False";
                _item = value;
            }
        }


        public FlashCardViewHolder(View itemView) : base(itemView)
        {
            _titleTextView = itemView.FindViewById<TextView>(Resource.Id.content_tv);
            _answerTextView = itemView.FindViewById<TextView>(Resource.Id.answer_tv);
            _topicTextView = itemView.FindViewById<TextView>(Resource.Id.topic_tv);
        }


    }
}