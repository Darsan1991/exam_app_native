﻿using System;
using Android.Graphics;
using Android.Runtime;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using Exam_App_Native.Droid.General;
using Exam_App_Native.Model;

namespace Exam_App_Native.Droid.Adapter.ViewHolder
{
    public class TeacherViewHolder:RecyclerView.ViewHolder,IHolder<Teacher>
    {
        private Teacher _item;
        private readonly ImageView _imageView;
        private readonly TextView _nameTV;
        private readonly TextView _subjectTV;
        private readonly TextView _jobTV;
        private readonly ProgressBar _progressBar;


        public Teacher Item
        {
            get => _item;
            set
            {
                if (_item==value || value==null)
                {
                    return;
                }

                _nameTV.Text = value.Name;
                _subjectTV.Text = value.Disciplines;
                _jobTV.Text = value.JobTitle;
                LoadAndSetImage(value.ImageUrl,value.Name);

                          
                _item = value;
            }
        }

        private void OnCompleteLoad(string s, Bitmap bitmap)
        {
            if (s.ToLower() == Item?.ImageUrl)
            {
                LoadingImage = false;
                _imageView.SetImageBitmap(Utils.Utils.GetRoundCornerImage(bitmap));
            }
        }

        private async void LoadAndSetImage(string url,string suggName)
        {
            LoadingImage = true;
            var bitmap = await RemoteFileManager.Instance.DownloadImageAsync(url, $"teacher{suggName}");
            LoadingImage = false;
            var width = Math.Min(bitmap.Width, bitmap.Height);
            
            //Crop the image
            bitmap = Bitmap.CreateBitmap(bitmap,(bitmap.Width - width)/2,(bitmap.Height - width)/2,width,width);

            _imageView.SetImageBitmap(Utils.Utils.GetRoundCornerImage(bitmap));
        }

        public bool LoadingImage
        {
            get => _progressBar.Visibility == ViewStates.Visible;
            private set =>
                _progressBar.Visibility = value ? ViewStates.Visible : ViewStates.Gone;
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            if (Item!=null && LoadingImage)
            {
               RemoteFileManager.Instance.RemoveImageCallback(OnCompleteLoad,Item.ImageUrl);
            }
        }

        public TeacherViewHolder(View itemView) : base(itemView)
        {
            _imageView = itemView.FindViewById<ImageView>(Resource.Id.image_view);
            _nameTV = itemView.FindViewById<TextView>(Resource.Id.name_tv);
            _subjectTV = itemView.FindViewById<TextView>(Resource.Id.subject_tv);
            _jobTV = itemView.FindViewById<TextView>(Resource.Id.job_tv);
            _progressBar = itemView.FindViewById<ProgressBar>(Resource.Id.progress_bar);
        }
    }
}