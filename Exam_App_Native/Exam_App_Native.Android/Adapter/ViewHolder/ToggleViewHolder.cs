﻿using System;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using Exam_App_Native.Droid.Views;
using Exam_App_Native.Model;
using Exam_App_Native.Model.ViewModel;

namespace Exam_App_Native.Droid.Adapter.ViewHolder
{
    public class ToggleViewHolder<T>:RecyclerView.ViewHolder,ISelectableHolder<ISelectableItem<T>>
    {
        public event Action<ToggleViewHolder<T>> OnSelectionChanged;

        public ISelectableItem<T> Item
        {
            get => _item;
            set
            {
               
                _titleTextView.Text = value.Title;
                _detailsTextView.Text = value.SubTitle;
                _item = value;
                SelectedItem = value.Selected;
            }
        }

        private readonly ToggleSwitchView _toggleButton;
        private readonly TextView _titleTextView;
        private readonly TextView _detailsTextView;
        private ISelectableItem<T> _item;

        public bool SelectedItem
        {
            get => _toggleButton.Checked && Item.Selected;
            set
            {
                if (_toggleButton.Checked==value && Item.Selected ==value)
                {
                    return;
                }

                _toggleButton.Checked = value;

                Item.Selected = value;

                

                OnSelectionChanged?.Invoke(this);
            }
        }

        public ToggleViewHolder(View itemView) : base(itemView)
        {
            _toggleButton = new ToggleSwitchView(itemView.FindViewById<ImageView>(Resource.Id.toggle_iv));
            _titleTextView = itemView.FindViewById<TextView>(Resource.Id.title_tv);
            _detailsTextView = itemView.FindViewById<TextView>(Resource.Id.details_tv);

            _toggleButton.ValueChanged += (sender, args) =>
            {
                SelectedItem = args;
            };

            itemView.Click += (sender, args) => { SelectedItem = !_toggleButton.Checked; };
        }

    }
}