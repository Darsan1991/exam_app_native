﻿using System;
using System.Collections.Generic;
using Android.Content;
using Android.Support.V7.Widget;
using Exam_App_Native.Model;

namespace Exam_App_Native.Droid.Adapter
{
    public class SelectableRecyclerAdapter<T, TH> : SimpleRecyclerAdapter<T, TH> where TH : RecyclerView.ViewHolder,ISelectableHolder<T>
    {
        public event Action<T> OnSelectionChanged; 

        public T Selected
        {
            get
            {
                if (_selected==null)
                {
                    return _selected;
                }

                return activeItemList.Contains(_selected) ? _selected : default(T);
            }
            set
            {
                if (Equals(_selected, value))
                {
                    return;
                }
                if (_selected!=null)
                {
                    _itemVsSelectedDict[_selected] = false;
                    var holders = GetHoldersFor(_selected);
                    foreach (var holder in holders)
                    {
                        holder.SelectedItem = false;
                    }
                }
                _selected = value;
                if (_selected != null)
                {
                    _itemVsSelectedDict[_selected] = true;
                    var holders = GetHoldersFor(_selected);
                    foreach (var holder in holders)
                    {
                        holder.SelectedItem = true;
                    }
                }

                OnSelectionChanged?.Invoke(Selected);
            }
        }

        public override IEnumerable<T> Items
        {
            get => base.Items;
            set
            {
                foreach (var item in value)
                {
                    if (!_itemVsSelectedDict.ContainsKey(item))
                    {
                        _itemVsSelectedDict.Add(item, false);
                    }
                }
                base.Items = value;
                //TODO: Try to change later
                Selected = _selected;

            } }

        private T _selected;


        private readonly Dictionary<T,bool> _itemVsSelectedDict = new Dictionary<T, bool>();

        public SelectableRecyclerAdapter(Context context, int itemLayoutResourceId, IEnumerable<T> tiles = null) : base(context, itemLayoutResourceId, tiles)
        {
            OnItemClicked += obj => { Selected = obj; };
        }

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            base.OnBindViewHolder(holder, position);
            
            ((ISelectableHolder<T>) holder).SelectedItem = _itemVsSelectedDict.ContainsKey(activeItemList[position]) && _itemVsSelectedDict[activeItemList[position]];
        }

        protected override void ApplyFilter(Predicate<T> value)
        {
            var selected = Selected;
            base.ApplyFilter(value);
            if (!Equals(selected, Selected))
            {
                OnSelectionChanged?.Invoke(Selected);
            }
        }
    }
}