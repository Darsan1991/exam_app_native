﻿using System;
using System.Collections.Generic;
using System.Linq;
using Android.Content;
using Android.Support.V7.Widget;
using Android.Views;
using Exam_App_Native.Model;
using Exam_App_Native.Model.Events;

namespace Exam_App_Native.Droid.Adapter
{
    public class SimpleRecyclerAdapter<T, TH> : RecyclerView.Adapter, IChildClickEventProvider<T>
        where TH : RecyclerView.ViewHolder, IHolder<T>
    {
        public event Action<TH> OnCreatingViewHolder;
        public event Action<T> OnItemClicked;
        public event Action<TH,T> OnBind;

        public virtual IEnumerable<T> Items
        {
            get => itemList;
            set
            {
                itemList.Clear();
                if (value != null)
                {
                    itemList.AddRange(value);
                }

                ApplyFilter(Filter);
            }
        }


        public virtual Predicate<T> Filter
        {
           private get { return _filter; }
            set
            {
                _filter = value;
                ApplyFilter(value);

            }
        }



        private readonly LayoutInflater _inflater;

        protected readonly int itemLayoutResourceId;
        protected readonly List<T> itemList = new List<T>();
        protected readonly List<T> activeItemList = new List<T>();

        protected readonly List<TH> holders=new List<TH>();

        private Predicate<T> _filter;
//        private readonly Action<H, T> onBindToHolder;

        public SimpleRecyclerAdapter(Context context, int itemLayoutResourceId, IEnumerable<T> tiles = null)
        {
            _inflater = LayoutInflater.From(context);
            this.itemLayoutResourceId = itemLayoutResourceId;
//            this.onBindToHolder = onBindToHolder;
            if (tiles != null)
            {
                Items = tiles.ToList();
            }
        }

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            ((IHolder<T>) holder).Item = activeItemList[position];
            OnBind?.Invoke((TH) holder,activeItemList[position]);
        }
//            onBindToHolder((H) holder, _activeItemList[position]);

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            var view = _inflater.Inflate(itemLayoutResourceId, parent, false);

            var holder = Activator.CreateInstance(typeof(TH), view);
            var viewHolder = (TH) holder;
            holders.Add(viewHolder);
            view.Click += (sender, args) => { OnItemClicked?.Invoke(viewHolder.Item); };
            OnCreatingViewHolder?.Invoke(viewHolder);
            return viewHolder;
        }

        public override int ItemCount => activeItemList.Count;

        protected virtual void ApplyFilter(Predicate<T> value)
        {
            activeItemList.Clear();
            activeItemList.AddRange(_filter == null ? itemList : itemList.FindAll(value));
            NotifyDataSetChanged();
           
        }


        public void RemoveItem(T item)
        {
            itemList.Remove(item);
            var index = activeItemList.FindIndex(obj => Equals(item,obj));
            if (index != -1)
            {
                activeItemList.RemoveAt(index);
                NotifyItemRemoved(index);
            }
        }
       

        public IEnumerable<TH> GetHoldersFor(T t)
        {
            return holders.Where(h => Equals(h.Item, t));
        }
    }
}