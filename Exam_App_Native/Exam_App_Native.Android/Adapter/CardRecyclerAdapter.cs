﻿using System;
using System.Collections.Generic;
using Android.Content;
using Exam_App_Native.Droid.Adapter;
using Exam_App_Native.Droid.Adapter.ViewHolder;
using Exam_App_Native.Droid.Adapter.ViewHolder.General;
using Exam_App_Native.Model.ViewModel;

namespace Exam_App_Native.Droid.Adapter
{
    public class CardRecyclerAdapter<T>:SimpleRecyclerAdapter<ICard<T>, CardRecyclerViewHolder<T>>
    {
        public CardRecyclerAdapter(Context context, IEnumerable<ICard<T>> tiles =null) 
            : base(context, Resource.Layout.card, tiles)
        {
            
        }
    }

    public class CardGroupRecyclerAdapter<J,T> : SimpleRecyclerAdapter<IItemGroup<J, ICard<T>>,
        CardGroupTileViewHolder<J,T>>
    {
        public CardGroupRecyclerAdapter(Context context, IEnumerable<IItemGroup<J, ICard<T>>> tiles = null) : base(context, Resource.Layout.card_group_tile, tiles)
        {

        }
    }
}