﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Android.Graphics;

namespace Exam_App_Native.Droid.General
{
    public class RemoteFileManager
    {
        public static readonly RemoteFileManager Instance = new RemoteFileManager();

        private readonly List<string> _pendingLoadList = new List<string>();
        private readonly Dictionary<string,List<Action<string,Bitmap>>> _imageCallbacks = new Dictionary<string, List<Action<string, Bitmap>>>();
        private readonly Dictionary<string,string> _imageUrlVsLocalUrl = new Dictionary<string, string>();

        public bool DownloadImage(string url, string suggestedName = null, Action<string, Bitmap> onComplete=null)
        {
            var urlLowercased = url.ToLower();

            if (_imageUrlVsLocalUrl.ContainsKey(urlLowercased))
            {
                onComplete?.Invoke(url,
                    BitmapFactory.DecodeFile(_imageUrlVsLocalUrl[urlLowercased]));
                return true;
            }

            if (onComplete != null)
            {
                AddImageCallback(onComplete, urlLowercased);
            }
            if (!_pendingLoadList.Contains(urlLowercased))
            {
                _pendingLoadList.Add(urlLowercased);
                
                //TODO Get Data by calling Modle class
                var bytes = new byte[100];
                var docPath = System.Environment.GetFolderPath(Environment.SpecialFolder.Personal);
                var fileName = (suggestedName ?? "img_" + DateTime.Today.TimeOfDay.TotalMilliseconds) + ".png";
                var localPath = System.IO.Path.Combine(docPath,fileName);

                var fs = new FileStream(localPath,FileMode.OpenOrCreate);
                var task = fs.WriteAsync(bytes,0,bytes.Length);

                Action onWritted = () =>
                {
                    fs.Close();
                    var bitmap = BitmapFactory.DecodeFile(localPath);
                    if (_imageCallbacks.ContainsKey(urlLowercased))
                    {
                        _imageCallbacks[urlLowercased].ForEach(action => { action?.Invoke(url, bitmap); });
                        _imageCallbacks.Clear();
                    }
                };

                if (task.IsCompleted)
                {
                    onWritted();
                }
                else
                {
                    task.ContinueWith(t => { onWritted(); });
                }

            }

            return false;
        }

        public void AddImageCallback(Action<string, Bitmap> onComplete, string url)
        {
            var urlLowercased = url.ToLower();
            if (!_imageCallbacks.ContainsKey(urlLowercased))
            {
                _imageCallbacks.Add(urlLowercased, new List<Action<string, Bitmap>>());
            }

            _imageCallbacks[urlLowercased].Add(onComplete);
        }

        public void RemoveImageCallback(Action<string, Bitmap> onComplete, string url)
        {
            var urlLowercased = url.ToLower();
            if (_imageCallbacks.ContainsKey(urlLowercased))
            {
                _imageCallbacks[urlLowercased].Remove(onComplete);
            }
        }

        public async Task<Bitmap> DownloadImageAsync(string url, string suggestedName = null,CancellationTokenSource cancellationTokenSource=null)
        {
            var urlLowercased = url.ToLower();

            if (_imageUrlVsLocalUrl.ContainsKey(urlLowercased))
            {
               
                   return BitmapFactory.DecodeFile(_imageUrlVsLocalUrl[urlLowercased]);
               
            }

                var bytes = await ExamApp.Instance.GetFile(url);
            var localPath = "";
            if (_imageUrlVsLocalUrl.ContainsKey(urlLowercased))
                localPath = _imageUrlVsLocalUrl[urlLowercased];
            else
            {
                var docPath = System.Environment.GetFolderPath(Environment.SpecialFolder.Personal);
                var fileName = (suggestedName ?? "img_" + DateTime.Today.TimeOfDay.TotalMilliseconds) + ".png";
                 localPath = System.IO.Path.Combine(docPath, fileName);
                var fs = new FileStream(localPath, FileMode.OpenOrCreate);
                if(cancellationTokenSource!=null)
                 await fs.WriteAsync(bytes, 0, bytes.Length,cancellationTokenSource.Token);
                else
                {
                    await fs.WriteAsync(bytes, 0, bytes.Length);
                }
                fs.Close();
                if (_imageUrlVsLocalUrl.ContainsKey(urlLowercased))
                {
                    _imageUrlVsLocalUrl[urlLowercased] = localPath;
                }
                else
                {
                    _imageUrlVsLocalUrl.Add(urlLowercased,localPath);
                }
            }



            return BitmapFactory.DecodeFile(localPath);

        }
    }
}