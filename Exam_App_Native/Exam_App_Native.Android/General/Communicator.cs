﻿using System;
using System.Collections.Generic;
using Java.Lang;
using Exception = System.Exception;

namespace Exam_App_Native.Droid.General
{
    public class Communicator
    {
        public static Communicator instance { get; } = new Communicator();

       private readonly Dictionary<string,object> keyValDictionary = new Dictionary<string, object>();

        public static bool ContainsKey(string key) => 
            instance.keyValDictionary.ContainsKey(key);

        /// <summary>
        /// Get Unique Key with prefix and suffix
        /// </summary>
        /// <param name="prefix">Prefix</param>
        /// <param name="suffix">Suffix</param>
        /// <returns>Unique Key</returns>
        public static string GetUniqueKey( string prefix=null, string suffix = null)
        {
            var next = 0;

            do
            {
                next = new Random().Next(10000000);
            } while (instance.keyValDictionary.ContainsKey(prefix+next+suffix));

            return prefix + next + suffix;
        }

        /// <summary>
        /// Store Value will store the value in the dictionary. you can get the value via key.
        /// if you leave key as null it will create unique key
        /// </summary>
        /// <param name="val">Any object want to save</param>
        /// <param name="key">Key - set null to create new unique key</param>
        /// <returns>The key of the value</returns>
        public static string StoreValue(object val, string key = null)
        {
            key = string.IsNullOrEmpty(key)?GetUniqueKey():key;
            if (ContainsKey(key))
            {
                throw new Exception("The Key is already used - "+key);
            }
            instance.keyValDictionary.Add(key,val);
            return key;
        }

        /// <summary>
        /// Get Value for key.If key not there it will return null.
        /// key and value will be removed after Get
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static object GetValue(string key)
        {
            if (instance.keyValDictionary.ContainsKey(key))
            {
                var val = instance.keyValDictionary[key];
                instance.keyValDictionary.Remove(key);
                return val;
            }

            return key;
        }

        /// <summary>
        /// Get Value with type for key.If key not there it will return null.
        /// key and value will be removed after Get
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static T GetValue<T>(string key)
        {
            if (instance.keyValDictionary.ContainsKey(key))
            {
                var val = (T) instance.keyValDictionary[key];
                instance.keyValDictionary.Remove(key);
                return val;
            }
            return default(T);
        }
    }
}