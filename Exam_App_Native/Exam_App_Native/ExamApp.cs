﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Exam_App_Native.Model;
using Newtonsoft.Json;

namespace Exam_App_Native
{
    public class ExamApp
    {
        public const string BASE_URL = "https://brein.com.br/api/v1";

        public static readonly ExamApp Instance = new ExamApp();

        private readonly Auth _auth = Auth.Instance;


        public IUserPrefs UserPrefs { get; private set; }

        private ExamApp()
        {
        }


        public void SetUserPrefs(IUserPrefs userPrefs)
        {
            this.UserPrefs = userPrefs;
        }

        public async Task<IEnumerable<ExamGroup>> GetExamGroups(CancellationTokenSource tokenSource = null)
        {
            if (_auth.CurrentUser == null)
            {
                throw new Exception("You have to Login Before access this content");
            }

            var cookieContainer = new CookieContainer();
            _auth.CurrentUser.Cookies.ForEach(cookie => cookieContainer.Add(cookie));

            tokenSource = tokenSource ?? new CancellationTokenSource();

            using (var handler = new HttpClientHandler {CookieContainer = cookieContainer})
            {
                using (var client = new HttpClient(handler))
                {
                    var response = await client.GetAsync(BASE_URL + "/examgroups", tokenSource.Token);
                    response.EnsureSuccessStatusCode();
                    var resultJson = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<IEnumerable<ExamGroup>>(resultJson);
                }
            }
        }

        public async Task<IEnumerable<ExamGroup>> SearchExamGroups(string query,CancellationTokenSource tokenSource = null)
        {
            if (_auth.CurrentUser == null)
            {
                throw new Exception("You have to Login Before access this content");
            }

            var cookieContainer = new CookieContainer();
            _auth.CurrentUser.Cookies.ForEach(cookie => cookieContainer.Add(cookie));

            tokenSource = tokenSource ?? new CancellationTokenSource();

            using (var handler = new HttpClientHandler { CookieContainer = cookieContainer })
            {
                using (var client = new HttpClient(handler))
                {
                    var response = await client.GetAsync(BASE_URL + $"/search?q={query}&t=exam", tokenSource.Token);
                    response.EnsureSuccessStatusCode();
                    var resultJson = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<IEnumerable<ExamGroup>>(resultJson);
                }
            }
        }

        public async Task<IEnumerable<Exam>> SearchExams(string query, CancellationTokenSource tokenSource = null)
        {
            if (_auth.CurrentUser == null)
            {
                throw new Exception("You have to Login Before access this content");
            }

            var cookieContainer = new CookieContainer();
            _auth.CurrentUser.Cookies.ForEach(cookie => cookieContainer.Add(cookie));

            tokenSource = tokenSource ?? new CancellationTokenSource();

            using (var handler = new HttpClientHandler { CookieContainer = cookieContainer })
            {
                using (var client = new HttpClient(handler))
                {
                    var response = await client.GetAsync(BASE_URL + $"/search?q={query}&t=exam", tokenSource.Token);
                    response.EnsureSuccessStatusCode();
                    var resultJson = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<IEnumerable<Exam>>(resultJson);
                }
            }
        }

        public async Task<IEnumerable<Subject>> SearchSubjects(int disciplineId,string query,
            CancellationTokenSource tokenSource = null)
        {
            if (_auth.CurrentUser == null)
            {
                throw new Exception("You have to Login Before access this content");
            }

            var cookieContainer = new CookieContainer();
            _auth.CurrentUser.Cookies.ForEach(cookie => cookieContainer.Add(cookie));

            tokenSource = tokenSource ?? new CancellationTokenSource();

            using (var handler = new HttpClientHandler { CookieContainer = cookieContainer })
            {
                using (var client = new HttpClient(handler))
                {
                    var response = await client.GetAsync(BASE_URL + $"/search?q={query}&t=subject&id={disciplineId}", tokenSource.Token);
                    response.EnsureSuccessStatusCode();
                    var resultJson = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<IEnumerable<Subject>>(resultJson);
                }
            }
        }

        public async Task<IEnumerable<Topic>> SearchTopics(int subjectId,string query,
            CancellationTokenSource tokenSource = null)
        {
            if (_auth.CurrentUser == null)
            {
                throw new Exception("You have to Login Before access this content");
            }

            var cookieContainer = new CookieContainer();
            _auth.CurrentUser.Cookies.ForEach(cookie => cookieContainer.Add(cookie));

            tokenSource = tokenSource ?? new CancellationTokenSource();

            using (var handler = new HttpClientHandler { CookieContainer = cookieContainer })
            {
                using (var client = new HttpClient(handler))
                {
                    var response = await client.GetAsync(BASE_URL + $"/search?q={query}&t=topic&id={subjectId}", tokenSource.Token);
                    response.EnsureSuccessStatusCode();
                    var resultJson = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<IEnumerable<Topic>>(resultJson);
                }
            }
        }

        public async Task<IEnumerable<Discipline>> SearchDisciplines(string query,
            CancellationTokenSource tokenSource = null)
        {
            if (_auth.CurrentUser == null)
            {
                throw new Exception("You have to Login Before access this content");
            }

            var cookieContainer = new CookieContainer();
            _auth.CurrentUser.Cookies.ForEach(cookie => cookieContainer.Add(cookie));

            tokenSource = tokenSource ?? new CancellationTokenSource();

            using (var handler = new HttpClientHandler { CookieContainer = cookieContainer })
            {
                using (var client = new HttpClient(handler))
                {
                    var response = await client.GetAsync(BASE_URL + $"/search?q={query}&t=discipline", tokenSource.Token);
                    response.EnsureSuccessStatusCode();
                    var resultJson = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<DisciplineSearchResult>(resultJson).Disciplines;
                }
            }
        }

        public async Task<IEnumerable<Discipline>> GetAllDiscipline(CancellationTokenSource tokenSource = null)
        {
            if (_auth.CurrentUser == null)
            {
                throw new Exception("You have to Login Before access this content");
            }

            var cookieContainer = new CookieContainer();
            _auth.CurrentUser.Cookies.ForEach(cookie => cookieContainer.Add(cookie));

            tokenSource = tokenSource ?? new CancellationTokenSource();

            using (var handler = new HttpClientHandler { CookieContainer = cookieContainer })
            {
                using (var client = new HttpClient(handler))
                {
                    var response = await client.GetAsync(BASE_URL + $"/disciplines", tokenSource.Token);
                    response.EnsureSuccessStatusCode();
                    var resultJson = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<IEnumerable<Discipline>>(resultJson);
                }
            }

        }

        public async Task<IEnumerable<Subject>> GetSubjectsForDiscipline(int disciplineId,CancellationTokenSource tokenSource = null)
        {
            if (_auth.CurrentUser == null)
            {
                throw new Exception("You have to Login Before access this content");
            }

            var cookieContainer = new CookieContainer();
            _auth.CurrentUser.Cookies.ForEach(cookie => cookieContainer.Add(cookie));

            tokenSource = tokenSource ?? new CancellationTokenSource();

            using (var handler = new HttpClientHandler { CookieContainer = cookieContainer })
            {
                using (var client = new HttpClient(handler))
                {
                    var response = await client.GetAsync(BASE_URL + $"/disciplines/{disciplineId}/subjects", tokenSource.Token);
                    response.EnsureSuccessStatusCode();
                    var resultJson = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<IEnumerable<Subject>>(resultJson);
                }
            }
        }


        public async Task<string> GetMonthlyPaymentPlanPrice(CancellationTokenSource tokenSource=null)
        {
            if (_auth.CurrentUser == null)
            {
                throw new Exception("You have to Login Before access this content");
            }

            var cookieContainer = new CookieContainer();
            _auth.CurrentUser.Cookies.ForEach(cookie => cookieContainer.Add(cookie));

            tokenSource = tokenSource ?? new CancellationTokenSource();

            using (var handler = new HttpClientHandler { CookieContainer = cookieContainer })
            {
                using (var client = new HttpClient(handler))
                {
                    var response = await client.GetAsync(BASE_URL + $"/payment/monthlyplan", tokenSource.Token);
                    response.EnsureSuccessStatusCode();
                    var resultJson = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<Dictionary<string,string>>(resultJson)["monthly_plan"];
                }
            }
        }

        public async Task<IEnumerable<Topic>> GetTopicsForSubject(int subjectId, CancellationTokenSource tokenSource = null)
        {
            if (_auth.CurrentUser == null)
            {
                throw new Exception("You have to Login Before access this content");
            }

            var cookieContainer = new CookieContainer();
            _auth.CurrentUser.Cookies.ForEach(cookie => cookieContainer.Add(cookie));

            tokenSource = tokenSource ?? new CancellationTokenSource();

            using (var handler = new HttpClientHandler { CookieContainer = cookieContainer })
            {
                using (var client = new HttpClient(handler))
                {
                    var response = await client.GetAsync(BASE_URL + $"/subjects/{subjectId}/topics", tokenSource.Token);
                    response.EnsureSuccessStatusCode();
                    var resultJson = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<IEnumerable<Topic>>(resultJson);
                }
            }
        }

        public async Task<IEnumerable<DisciplineGroup>> GetDesciplineGroups(CancellationTokenSource tokenSource = null)
        {
            if (_auth.CurrentUser == null)
            {
                throw new Exception("You have to Login Before access this content");
            }

            var cookieContainer = new CookieContainer();
            _auth.CurrentUser.Cookies.ForEach(cookie => cookieContainer.Add(cookie));

            tokenSource = tokenSource ?? new CancellationTokenSource();

            using (var handler = new HttpClientHandler {CookieContainer = cookieContainer})
            {
                using (var client = new HttpClient(handler))
                {
                    var response = await client.GetAsync(BASE_URL + "/disciplinegroups", tokenSource.Token);
                    response.EnsureSuccessStatusCode();
                    var resultJson = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<IEnumerable<DisciplineGroup>>(resultJson);
                }
            }
        }


        public async Task<IEnumerable<PaymentPlan>> GetPaymentPlans(CancellationTokenSource tokenSource = null)
        {
            if (_auth.CurrentUser == null)
            {
                throw new Exception("You have to Login Before access this content");
            }

            var cookieContainer = new CookieContainer();
            _auth.CurrentUser.Cookies.ForEach(cookie => cookieContainer.Add(cookie));

            tokenSource = tokenSource ?? new CancellationTokenSource();

            using (var handler = new HttpClientHandler { CookieContainer = cookieContainer })
            {
                using (var client = new HttpClient(handler))
                {
                    var response = await client.GetAsync(BASE_URL + "/payment/plans", tokenSource.Token);
                    response.EnsureSuccessStatusCode();
                    var resultJson = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<IEnumerable<PaymentPlan>>(resultJson);
                }
            }
        }


        public async Task<IEnumerable<DisciplineGroup>> SearchDisciplineGroups(string query,CancellationTokenSource tokenSource = null)
        {
            if (_auth.CurrentUser == null)
            {
                throw new Exception("You have to Login Before access this content");
            }

            var cookieContainer = new CookieContainer();
            _auth.CurrentUser.Cookies.ForEach(cookie => cookieContainer.Add(cookie));

            tokenSource = tokenSource ?? new CancellationTokenSource();

            using (var handler = new HttpClientHandler { CookieContainer = cookieContainer })
            {
                using (var client = new HttpClient(handler))
                {
                    var response = await client.GetAsync(BASE_URL + $"/search?q={query}&t=discipline&output=scope", tokenSource.Token);
                    response.EnsureSuccessStatusCode();
                    var resultJson = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<IEnumerable<DisciplineGroup>>(resultJson);
                }
            }
        }



        public async Task<IEnumerable<Discipline>> GetDesciplinesFromGroup(string groupName,
            CancellationTokenSource tokenSource = null)
        {
            if (_auth.CurrentUser == null)
            {
                throw new Exception("You have to Login Before access this content");
            }

            var cookieContainer = new CookieContainer();
            _auth.CurrentUser.Cookies.ForEach(cookie => cookieContainer.Add(cookie));

            tokenSource = tokenSource ?? new CancellationTokenSource();

            using (var handler = new HttpClientHandler {CookieContainer = cookieContainer})
            {
                using (var client = new HttpClient(handler))
                {
                    var response = await client.GetAsync($"{BASE_URL}/disciplinegroups/{groupName}", tokenSource.Token);
                    response.EnsureSuccessStatusCode();
                    var resultJson = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<DisciplineGroup>(resultJson).Disciplines;
                }
            }
        }


        public async Task<IEnumerable<Exam>> GetExamsFromGroup(int groupId, CancellationTokenSource tokenSource = null)
        {
            if (_auth.CurrentUser == null)
            {
                throw new Exception("You have to Login Before access this content");
            }

            var cookieContainer = new CookieContainer();
            _auth.CurrentUser.Cookies.ForEach(cookie => cookieContainer.Add(cookie));

            tokenSource = tokenSource ?? new CancellationTokenSource();

            using (var handler = new HttpClientHandler {CookieContainer = cookieContainer})
            {
                using (var client = new HttpClient(handler))
                {
                    var response = await client.GetAsync($"{BASE_URL}/examgroups/{groupId}", tokenSource.Token);
                    response.EnsureSuccessStatusCode();
                    var resultJson = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<IEnumerable<Exam>>(resultJson);
                }
            }
        }

        public async Task<IEnumerable<Teacher>> GetAllTeachers(CancellationTokenSource tokenSource = null)
        {
            var cookieContainer = new CookieContainer();
            _auth.CurrentUser.Cookies.ForEach(cookie => cookieContainer.Add(cookie));
            tokenSource = tokenSource ?? new CancellationTokenSource();
            using (var handler = new HttpClientHandler {CookieContainer = cookieContainer})
            {
                using (var client = new HttpClient(handler))
                {
                    var responseMessage = await client.GetAsync(BASE_URL + "/teachers", tokenSource.Token);
                    responseMessage.EnsureSuccessStatusCode();
                    var resultJson = await responseMessage.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<IEnumerable<Teacher>>(resultJson);
                }
            }
        }

        public async Task<IEnumerable<TopicProgress>> GetTopicProgressForSubject(int disciplineId, int subjectId,CancellationTokenSource tokenSource=null)
        {
            var cookieContainer = new CookieContainer();
            _auth.CurrentUser.Cookies.ForEach(cookie => cookieContainer.Add(cookie));

            tokenSource = tokenSource ?? new CancellationTokenSource();

            using (var handler = new HttpClientHandler { CookieContainer = cookieContainer })
            {
                using (var client = new HttpClient(handler))
                {
                    var responseMessage = await client.GetAsync(
                        BASE_URL + $"/users/{_auth.CurrentUser.Id}/disciplines/{disciplineId}/subjects/{subjectId}/topics",
                        tokenSource.Token);
                    if (responseMessage.StatusCode != HttpStatusCode.NotFound)
                        responseMessage.EnsureSuccessStatusCode();


                    var resultJson = await responseMessage.Content.ReadAsStringAsync();
                    var messageAndResult = JsonConvert.DeserializeObject<MessageAndResult>(resultJson);

                    if (messageAndResult?.Result?.ToLower() == "error")
                        throw new ExamAppException(messageAndResult.Message);

                    return JsonConvert.DeserializeObject<IEnumerable<TopicProgress>>(resultJson);
                }
            }
        }

        


        public async Task<IEnumerable<SubjectProgress>> GetSubjectProgressesForDiciplines(int examId, int disciplineId,
            CancellationTokenSource tokenSource = null)
        {
            var cookieContainer = new CookieContainer();
            _auth.CurrentUser.Cookies.ForEach(cookie => cookieContainer.Add(cookie));

            tokenSource = tokenSource ?? new CancellationTokenSource();

            using (var handler = new HttpClientHandler {CookieContainer = cookieContainer})
            {
                using (var client = new HttpClient(handler))
                {
                    var responseMessage = await client.GetAsync(
                        BASE_URL + $"/users/{_auth.CurrentUser.Id}/exams/{examId}/disciplines/{disciplineId}/subjects",
                        tokenSource.Token);
                    if (responseMessage.StatusCode != HttpStatusCode.NotFound)
                        responseMessage.EnsureSuccessStatusCode();


                    var resultJson = await responseMessage.Content.ReadAsStringAsync();

                    MessageAndResult messageAndResult = null;

                    try
                    {
                        messageAndResult = JsonConvert.DeserializeObject<MessageAndResult>(resultJson);
                    }
                    catch (Exception)
                    {
                        // ignored
                    }

                    if (messageAndResult?.Result?.ToLower() == "error")
                        throw new ExamAppException(messageAndResult.Message);

                    return JsonConvert.DeserializeObject<IEnumerable<SubjectProgress>>(resultJson);
                }
            }
        }

        public async Task<IEnumerable<SubjectProgress>> GetSubjectProgressesForDiciplines(int disciplineId,
            CancellationTokenSource tokenSource = null)
        {
            var cookieContainer = new CookieContainer();
            _auth.CurrentUser.Cookies.ForEach(cookie => cookieContainer.Add(cookie));

            tokenSource = tokenSource ?? new CancellationTokenSource();

            using (var handler = new HttpClientHandler {CookieContainer = cookieContainer})
            {
                using (var client = new HttpClient(handler))
                {
                    var responseMessage = await client.GetAsync(
                        BASE_URL + $"/users/{_auth.CurrentUser.Id}/disciplines/{disciplineId}/subjects",
                        tokenSource.Token);
                    if (responseMessage.StatusCode != HttpStatusCode.NotFound)
                        responseMessage.EnsureSuccessStatusCode();

                    var resultJson = await responseMessage.Content.ReadAsStringAsync();
                    MessageAndResult messageAndResult = null;
                    try
                    {
                        messageAndResult = JsonConvert.DeserializeObject<MessageAndResult>(resultJson);
                    }
                    catch (Exception e)
                    {
                    }

                    if (messageAndResult?.Result?.ToLower() == "error")
                        throw new ExamAppException(messageAndResult.Message);

                    

                    return JsonConvert.DeserializeObject<IEnumerable<SubjectProgress>>(resultJson);
                }
            }
        }

        public async Task<IEnumerable<TopicProgress>> GetTopicProgresses(int examId,
            int subjectId, CancellationTokenSource tokenSource = null)
        {
            var cookieContainer = new CookieContainer();
            _auth.CurrentUser.Cookies.ForEach(cookie => cookieContainer.Add(cookie));

            tokenSource = tokenSource ?? new CancellationTokenSource();

            using (var handler = new HttpClientHandler { CookieContainer = cookieContainer })
            {
                using (var client = new HttpClient(handler))
                {
                    var responseMessage = await client.GetAsync(
                        BASE_URL + $"/users/{_auth.CurrentUser.Id}/exams/{examId}/subjects/{subjectId}/topics",
                        tokenSource.Token);
                    if (responseMessage.StatusCode != HttpStatusCode.NotFound)
                        responseMessage.EnsureSuccessStatusCode();


                    var resultJson = await responseMessage.Content.ReadAsStringAsync();
                    var messageAndResult = JsonConvert.DeserializeObject<MessageAndResult>(resultJson);

                    if (messageAndResult?.Result?.ToLower() == "error")
                        throw new ExamAppException(messageAndResult.Message);

                    return JsonConvert.DeserializeObject<IEnumerable<TopicProgress>>(resultJson);
                }
            }
        }

        public async Task<IEnumerable<TopicProgress>> GetTopicProgresses(int subjectId,
            CancellationTokenSource tokenSource = null)
        {
            var cookieContainer = new CookieContainer();
            _auth.CurrentUser.Cookies.ForEach(cookie => cookieContainer.Add(cookie));
            tokenSource = tokenSource ?? new CancellationTokenSource();
            using (var handler = new HttpClientHandler {CookieContainer = cookieContainer})
            {
                using (var client = new HttpClient(handler))
                {
                    var responseMessage = await client.GetAsync(
                        BASE_URL + $"/users/{_auth.CurrentUser.Id}/subjects/{subjectId}/topics",
                        tokenSource.Token);
                    responseMessage.EnsureSuccessStatusCode();
                    var resultJson = await responseMessage.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<IEnumerable<TopicProgress>>(resultJson);
                }
            }
        }

        public async Task<ReviewSetStartUpInfo> CreateNewReviewSet(IEnumerable<int> subjects, IEnumerable<int> topics,string disciplineName="Review",CancellationTokenSource tokenSource=null)
            {
            if (_auth.CurrentUser == null)
                throw new Exception("You Have to Login to access this content!");

            var cookieContainer = new CookieContainer();
            _auth.CurrentUser.Cookies.ForEach(cookie => cookieContainer.Add(cookie));
            tokenSource = tokenSource ?? new CancellationTokenSource();
            using (var handler = new HttpClientHandler { CookieContainer = cookieContainer })
            {
                using (var client = new HttpClient(handler))
                {
                    var jsonBody = $"{{\"topic_id\":[{topics?.AllIntoOne<int,string>((i, s) => string.IsNullOrEmpty(s)?(i.ToString()):(s+","+i))??""}],\"subject_id\":[{subjects?.AllIntoOne<int, string>((i, s) => string.IsNullOrEmpty(s) ? (i.ToString()) : (s+"," + i))??""}]}}";
                    var content = new StringContent(jsonBody,Encoding.UTF8,"application/json");

                    var random = new Random();
                    HttpResponseMessage response = null;

                    response = await client.PostAsync($"{BASE_URL}/users/{_auth.CurrentUser.Id}/reviewsets/{disciplineName}", content, tokenSource.Token);

                    while (response.StatusCode == HttpStatusCode.Conflict)
                    {
                        var next = random.Next(0, 100000);
                        response = await client.PostAsync($"{BASE_URL}/users/{_auth.CurrentUser.Id}/reviewsets/{disciplineName}{next}",content, tokenSource.Token);
                    }

                    response.EnsureSuccessStatusCode();

                    var resultJson = await response.Content.ReadAsStringAsync();

                   
                    return JsonConvert.DeserializeObject<ReviewSetStartUpInfo>(resultJson);
                }
            }
        }

        public async Task<IEnumerable<ReviewSetStartUpInfo>> RetakeReviewSet(int reviewSetId, CancellationTokenSource tokenSource = null)
        {
            if (_auth.CurrentUser == null)
                throw new Exception("You Have to Login to access this content!");

            var cookieContainer = new CookieContainer();
            _auth.CurrentUser.Cookies.ForEach(cookie => cookieContainer.Add(cookie));
            tokenSource = tokenSource ?? new CancellationTokenSource();
            using (var handler = new HttpClientHandler { CookieContainer = cookieContainer })
            {
                using (var client = new HttpClient(handler))
                {
                      var  response = await client.PostAsync($"{BASE_URL}/users/{_auth.CurrentUser.Id}/reviewsets/{reviewSetId}/retakereview",null, tokenSource.Token);
                    
                    response.EnsureSuccessStatusCode();

                    var resultJson = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<ReviewSetGroupStartUpInfo>(resultJson).ReviewSetStartUpInfos;
                }
            }
        }

        public async Task<ReviewSetStartUpInfo> RestartAllReviewSet(int reviewSetId,
            CancellationTokenSource tokenSource = null)
        {
            if (_auth.CurrentUser == null)
                throw new Exception("You Have to Login to access this content!");

            var cookieContainer = new CookieContainer();
            _auth.CurrentUser.Cookies.ForEach(cookie => cookieContainer.Add(cookie));
            tokenSource = tokenSource ?? new CancellationTokenSource();
            using (var handler = new HttpClientHandler { CookieContainer = cookieContainer })
            {
                using (var client = new HttpClient(handler))
                {
                    var response = await client.GetAsync($"{BASE_URL}/users/{_auth.CurrentUser.Id}/restartreviewset/{reviewSetId}", tokenSource.Token);

                    response.EnsureSuccessStatusCode();

                    var resultJson = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<ReviewSetStartUpInfo>(resultJson);
                }
            }
        }



        public async Task<CardEvalResultInfo> EvaluateCard(int reviewId, int cardId, bool answer, int totalSec,
            int? hitReview = null, int? mistakeReview = null, CancellationTokenSource tokenSource = null)
        {
            if (_auth.CurrentUser == null)
                throw new Exception("You Have to Login to access this content!");

            var cookieContainer = new CookieContainer();
            _auth.CurrentUser.Cookies.ForEach(cookie => cookieContainer.Add(cookie));
            tokenSource = tokenSource ?? new CancellationTokenSource();
            using (var handler = new HttpClientHandler { CookieContainer = cookieContainer })
            {
                using (var client = new HttpClient(handler))
                {
                    var jsonBody = $"{{\"answer\":{(answer?"true":"false")},\"totaltimesec\":{totalSec}{(hitReview!=null?$",\"new_hits_review_id\":{hitReview}":"")}{(mistakeReview != null ? $",\"new_mistakes_review_id\":{mistakeReview}" : "")}}}";
                    var content = new StringContent(jsonBody,Encoding.UTF8,"application/json");

                     var response = await client.PostAsync($"{BASE_URL}/users/{_auth.CurrentUser.Id}/reviews/{reviewId}/evalcard/{cardId}", content, tokenSource.Token);


                    response.EnsureSuccessStatusCode();

                    var resultJson = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<CardEvalResultInfo>(resultJson);
                }
            }
        }

        public async Task<byte[]> GetFile(string url, CancellationTokenSource tokenSource = null)
        {
            if (_auth.CurrentUser == null)
                throw new Exception("You Have to Login to access this content!");

            var cookieContainer = new CookieContainer();
            _auth.CurrentUser.Cookies.ForEach(cookie => cookieContainer.Add(cookie));
            tokenSource = tokenSource ?? new CancellationTokenSource();
            using (var handler = new HttpClientHandler {CookieContainer = cookieContainer})
            {
                using (var client = new HttpClient(handler))
                {
                    var response = await client.GetAsync(url, tokenSource.Token);
                    response.EnsureSuccessStatusCode();
                    using (var stream = await response.Content.ReadAsStreamAsync())
                    {
                        using (var memoryStream = new MemoryStream())
                        {
                            await stream.CopyToAsync(memoryStream);
                            return memoryStream.ToArray();
                        }
                    }
                }
            }
        }
    }
}