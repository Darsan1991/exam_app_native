﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;

namespace Exam_App_Native
{
    public class Utils
    {
        public static bool IsValidEmail(string text)=>
         Regex.IsMatch(text, @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");

        public static DateTime ParseToDate(string date)
        {
            return DateTime.ParseExact(date, "dd/MM/yy", CultureInfo.InvariantCulture);
        }
    }

    
}