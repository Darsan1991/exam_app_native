﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.Serialization;
using System.Threading;
using System.Threading.Tasks;
using Exam_App_Native.Model;
using Newtonsoft.Json;

namespace Exam_App_Native
{
    public partial class Auth
    {
        private const string EMAIL_KEY = "email";
        private const string PASSWORD_KEY = "password";
        private const string ACCESS_TOKEN_KEY = "accesstoken";
        private const string LOGIN_METHOD_KEY = "login";

        private const string FACEBOOK_LOGIN_METHOD = "facebook";
        private const string EMAIL_LOGIN_METHOD = "email";

        public static readonly Auth Instance = new Auth();
        private ExamApp ExamApp => ExamApp.Instance;

        public User CurrentUser { get; private set; }

        public async Task<User> TryAutoLogIn(CancellationTokenSource tokenSource=null)
        {
            if(ExamApp.UserPrefs==null)
                throw new Exception("Presistance is null");


            var loginMethod = ExamApp.UserPrefs.GetString(LOGIN_METHOD_KEY);

            if (string.IsNullOrEmpty(loginMethod))
                throw new Exception("You Didn't login Before");

            if (loginMethod == FACEBOOK_LOGIN_METHOD)
            {
                var tokenKey = ExamApp.UserPrefs.GetString(ACCESS_TOKEN_KEY);

                return await LogIn(tokenKey);

            }

            var email = ExamApp.UserPrefs.GetString(EMAIL_KEY);
            var password = ExamApp.UserPrefs.GetString(PASSWORD_KEY);
            return await LogIn(email, password, true, tokenSource);
        }



        public  async Task<User> LogIn(string email, string password, bool rememberMe,CancellationTokenSource tokenSource=null)
        {
            var cookieContainer = new CookieContainer();
            tokenSource = tokenSource ?? new CancellationTokenSource();
            using (var handler = new HttpClientHandler { CookieContainer = cookieContainer })
            {
                using (var client = new HttpClient(handler: handler))
                {
                    var content = new FormUrlEncodedContent(new[]
                    {
                        new KeyValuePair<string, string>("email", email),
                        new KeyValuePair<string, string>("password", password),
                        new KeyValuePair<string, string>("remember_me", rememberMe ? "true" : "false"),
                    });

                    var response = await client.PostAsync(ExamApp.BASE_URL + "/login", content,tokenSource.Token);
                    //                    response.EnsureSuccessStatusCode();

                    var responseJson = await response.Content.ReadAsStringAsync();
                    var result = JsonConvert.DeserializeObject<LogInResult>(responseJson);

                    if (result.Result == "success")
                    {
                        var responseUser = await client.GetAsync(result.Location,tokenSource.Token);
                        responseUser.EnsureSuccessStatusCode();

                        var user = JsonConvert.DeserializeObject<User>(await responseUser.Content.ReadAsStringAsync());
                        user.Cookies = cookieContainer.GetCookies(new Uri(ExamApp.BASE_URL + "/login")).Cast<Cookie>();
                        
                        user.Confirmed = true;
                        CurrentUser = user;
                        ExamApp.UserPrefs?.SetString(LOGIN_METHOD_KEY,EMAIL_LOGIN_METHOD);
                        ExamApp.UserPrefs?.SetString(EMAIL_KEY,email);
                        ExamApp.UserPrefs?.SetString(PASSWORD_KEY,password);
                        return user;
                    }

                    if (result.Message.ToLower() == "Please, confirm your email before loging in.")
                        throw new NotConfirmedException("Please, confirm your email before loging in");


                    throw new Exception(JsonConvert.DeserializeObject<MessageAndResult>(responseJson).Message);
                }
            }
        }

        public async Task<User> LogIn(string accessToken, CancellationTokenSource tokenSource = null)
        {
            var cookieContainer = new CookieContainer();
            tokenSource = tokenSource ?? new CancellationTokenSource();
            using (var handler = new HttpClientHandler { CookieContainer = cookieContainer })
            {
                using (var client = new HttpClient(handler: handler))
                {
                    var content = new FormUrlEncodedContent(new[]
                    {
                        new KeyValuePair<string, string>("accesstoken", accessToken),
                        
                    });

                    var response = await client.PostAsync(ExamApp.BASE_URL + "/accesstoken", content, tokenSource.Token);
                    response.EnsureSuccessStatusCode();

                    var responseJson = await response.Content.ReadAsStringAsync();

                    CurrentUser = JsonConvert.DeserializeObject<User>(responseJson);

                    //TODO:Remove after Api Bug Fixed
                    CurrentUser.UserSettings = CurrentUser.UserSettings ?? new UserSettings();

                    CurrentUser.Cookies = cookieContainer.GetCookies(new Uri(ExamApp.BASE_URL + "/accesstoken")).Cast<Cookie>();
                    ExamApp.UserPrefs?.SetString(LOGIN_METHOD_KEY, FACEBOOK_LOGIN_METHOD);
                    ExamApp.UserPrefs?.SetString(ACCESS_TOKEN_KEY, accessToken);

                    return CurrentUser;
                }
            }
        }



        public void LogOut()
        {
            CurrentUser = null;
            ExamApp.UserPrefs?.DeleteKey(LOGIN_METHOD_KEY);
            
        }

        public  async Task<User> Regiser(string name, string email, string password,CancellationTokenSource tokenSource=null)
        {
            tokenSource = tokenSource ?? new CancellationTokenSource();
            using (var client = new HttpClient())
            {
                var content = new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string, string>("email", email),
                    new KeyValuePair<string, string>("password", password),
                    new KeyValuePair<string, string>("name", name),
                });

                var response = await client.PostAsync(ExamApp.BASE_URL + "/signup", content,tokenSource.Token);
//                if(response.StatusCode != HttpStatusCode.BadRequest)
//                response.EnsureSuccessStatusCode();
                var responseJson = await response.Content.ReadAsStringAsync();
                var signUpResult = JsonConvert.DeserializeObject<SignUpResult>(responseJson);
                if (signUpResult.Result == "success")
                {
                    CurrentUser = JsonConvert.DeserializeObject<User>(responseJson);
                    ExamApp.UserPrefs?.SetString(EMAIL_KEY,email);
                    ExamApp.UserPrefs?.SetString(PASSWORD_KEY,password);
                    return CurrentUser;
                }

                var result = JsonConvert.DeserializeObject<MessageAndResult>(responseJson);
                if (string.Equals(result.Message, "You did not confirm your email. Get the code was sent to your email an type here.", StringComparison.CurrentCultureIgnoreCase))
                {
                    throw new NotConfirmedException("User not Confirmed");
                }

                throw new Exception(result.Message);
            }
        }

        public  async Task<bool> CheckUser(string email,CancellationTokenSource tokenSource=null)
        {
            tokenSource = tokenSource ?? new CancellationTokenSource();
            using (var client = new HttpClient())
            {
                var response = await client.GetAsync($"{ExamApp.BASE_URL}/users/{email}/check",tokenSource.Token);
                response.EnsureSuccessStatusCode();
                var resultJson = await response.Content.ReadAsStringAsync();

                var result = JsonConvert.DeserializeObject<MessageAndResult>(resultJson);

                return result.Result == "success";
            }
        }

    }

    public partial class Auth
    {
        private class LogInResult : MessageAndResult
        {
            [JsonProperty("user_id")]
            public int Id { get; set; }
            public string Location { get; set; }
        }

        private class SignUpResult
        {
            public string Email { get; set; }
            public string Name { get; set; }
            [JsonProperty("expiration_date")] public string ExpirationDate { get; set; }
            public UserSettings Settings { get; set; }
            public string Url { get; set; }
            public int Id { get; set; }
            public string Result { get; set; }
        }
    }

    //Exception
    public class NotConfirmedException : Exception
    {
        public NotConfirmedException()
        {
        }

        protected NotConfirmedException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public NotConfirmedException(string message) : base(message)
        {
        }

        public NotConfirmedException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }

    public class ExamAppException : Exception
    {
        public ExamAppException()
        {
        }

        protected ExamAppException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public ExamAppException(string message) : base(message)
        {
        }

        public ExamAppException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }

}