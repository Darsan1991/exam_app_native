﻿using System;

namespace Exam_App_Native.Model.Events
{
    public interface IClickEventProvider
    {
        event Action<IClickEventProvider> OnClicked;
    }
}