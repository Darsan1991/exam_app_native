﻿using System;

namespace Exam_App_Native.Model.Events
{
    public interface IChildClickEventProvider<out T>
    {
        event Action<T> OnItemClicked ;
    }
}