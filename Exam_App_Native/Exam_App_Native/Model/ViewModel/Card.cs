﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Exam_App_Native.Model.ViewModel
{
    public class Card : ICard
    {
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public string Footer { get; set; }
        public object Data { get; set; }
    }

    public class Card<T>:ICard<T>
    {
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public string Footer { get; set; }
        public T Data { get; set; }
    }


    public interface ICard:IItem
    {
        string Footer { get; set; }
    }

    public interface ICard<T>:IItem<T>
    {
        string Footer { get; set; }
       
    }
}