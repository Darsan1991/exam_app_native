﻿namespace Exam_App_Native.Model.ViewModel
{
    public interface IItem
    {
        string Title { get; set; }
        string SubTitle { get; set; }
        object Data { get; set; }
    }

    public interface IItem<T>
    {
        string Title { get; set; }
        string SubTitle { get; set; }
        T Data { get; set; }
    }

    public interface ISelectableItem<T> : IItem<T>
    {
        bool Selected { get; set; }
    }

    public interface IPartialSelectable
    {
        SelectableState State { get; set; }
    }

    public interface IPartialSelectableItem<T> : IItem<T>, IPartialSelectable
    {

    }

    public enum SelectableState
    {
        All, Partial, None
    }

    public class Item : IItem
    {
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public object Data { get; set; }
    }

    public class Item<T> : IItem<T>
    {
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public T Data { get; set; }
    }

    public class SelectableItem<T> : ISelectableItem<T>
    {
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public T Data { get; set; }
        public bool Selected { get; set; }
    }
}