﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using Exam_App_Native.Model.Events;

namespace Exam_App_Native.Model.ViewModel{
    public interface IItemGroup<J,T>:IEnumerable<T>
    {
//        event EventHandler<T> OnChildItemClicked; 
//        event EventHandler<IItemGroup<J,T>> OnClicked; 

        IEnumerable<T> Items { get; set; }
        string Title { get; set; }
        J Data { get; set; }
//        IClickEventProvider ClickEventProvider { get; set; }
//        IChildClickEventProvider<T> ChildClickEventProvider { get; set; }
    }

    public class ItemGroup<J,T> : IItemGroup<J, T>
    {
//        public event EventHandler<T> OnChildItemClicked;
//        public event EventHandler<IItemGroup<J, T>> OnClicked;

        public string Title { get; set; }
        public J Data { get; set; }

        public IEnumerable<T> Items
        {
            get => (IEnumerable<T>) items;
            set
            {
                items.Clear();
                if (value!=null)
                {
                    items.AddRange(value);
                }
            }
        }

//        public IClickEventProvider ClickEventProvider
//        {
//            get => clickEventProvider;
//            set
//            {
//                if (clickEventProvider!=null)
//                {
//                    clickEventProvider.OnClicked -= ClickEventProviderOnOnClicked;
//                }
//                clickEventProvider = value;
//                if (clickEventProvider!=null)
//                {
//                    clickEventProvider.OnClicked += ClickEventProviderOnOnClicked;
//                }
//            }
//        }
//
//        public IChildClickEventProvider<T> ChildClickEventProvider
//        {
//            get => childClickEventProvider;
//            set
//            {
//                if (childClickEventProvider!=null)
//                {
//                    childClickEventProvider.OnItemClicked -= ChildClickEventProviderOnOnClicked;
//                }
//
//                childClickEventProvider = value;
//
//                if (childClickEventProvider != null)
//                {
//                    childClickEventProvider.OnItemClicked += ChildClickEventProviderOnOnClicked;
//                }
//            }
//        }

        private readonly List<T> items = new List<T>();
//        private IClickEventProvider clickEventProvider;
//        private IChildClickEventProvider<T> childClickEventProvider;

        public IEnumerator<T> GetEnumerator() => items.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

//
//        private void ClickEventProviderOnOnClicked(IClickEventProvider clickEventProvider) =>
//            OnClicked?.Invoke(this, this);
//
//
//        private void ChildClickEventProviderOnOnClicked(T obj) =>
//            OnChildItemClicked?.Invoke(this,obj);
    }
}