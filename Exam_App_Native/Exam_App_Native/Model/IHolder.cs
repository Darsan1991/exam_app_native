﻿using Exam_App_Native.Model.ViewModel;

namespace Exam_App_Native.Model
{ 
    public interface IHolder<T>
    {
        T Item { get; set; }
    }

    public interface ISelectableHolder<T>:IHolder<T>
    {
        bool SelectedItem { get; set; }
    }

    public interface IPartialSelectableHolder<T> : IHolder<T>
    {
        SelectableState State { get; set; }
    }
    
}