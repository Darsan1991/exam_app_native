﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Runtime.InteropServices;
using Exam_App_Native.Model.ViewModel;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Exam_App_Native.Model
{
    public class DisciplineGroup
    {
        public IEnumerable<Discipline> Disciplines { get; set; }
        [JsonProperty("group_name")] public string Name { get; set; }
    }

    public class Discipline
    {
        [JsonProperty("discipline_id")] public int Id { get; set; }
        [JsonProperty("discipline_name")] public string Name { get; set; }
        [JsonProperty("ncards")] public int CardCount { get; set; }
        public string Url { get; set; }


        public override bool Equals(object obj)
        {
            if (obj is Discipline discipline)
            {
                return discipline.Id == Id;
            }
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return Id;
        }
    }

    public class MyDiscipline
    {
        [JsonProperty("discipline_id")] public int Id { get; set; }
        [JsonProperty("discipline_name")] public string Name { get; set; }
        public IEnumerable<Subject> Subjects { get; set; }
        [JsonProperty("subjects_alias")] public string SubjectAlias { get; set; }
        public string Url { get; set; }

        public class Subject
        {
            [JsonProperty("subject_id")] public string Id { get; set; }
            [JsonProperty("subject_name")] public string Name { get; set; }
        }
    }

    public class MyCard
    {
        [JsonProperty("card_id")] public int? Id { get; set; }
        public string Content { get; set; }
        [JsonProperty("teacher_name")]
        public string Teacher { get; set; }
        [JsonProperty("topics")]
        public Topic Topic { get; set; }
        [JsonProperty("subjects")]
        public Subject Subject { get; set; }
        public string Comment { get; set; }
        public bool? Answer { get; set; }
        [JsonIgnore]
        public Discipline Discipline { get; set; }

        public MyCard Clone() => (MyCard) MemberwiseClone();
    }

    public class QuizCard
    {
        [JsonProperty("card_id")]
        public int Id { get; set; }
        public string Content { get; set; }
        [JsonProperty("teacher_name")]
        public string Teacher { get; set; }
        [JsonProperty("subjects")]
        public string Subject { get; set; }
        [JsonProperty("topics")]
        public string Topic { get; set; }
        public bool? Answer { get; set; }
    }

    public class ExamGroup
    {
        [JsonProperty("group_name")] public string Name { get; set; }
        public IEnumerable<Exam> Exams { get; set; }
        [JsonProperty("examgroup_id")] public int Id { get; set; }
    }

    public class Exam
    {
        [JsonProperty("exam_id")] public int Id { get; set; }
        public string Company { get; set; }
        public string Institution { get; set; }
        public string JobTitle { get; set; }
        public string Url { get; set; }
        public string Year { get; set; }
        public IEnumerable<Discipline> Disciplines { get; set; }
    }

    public class MyCardSearchResult
    {

    }

    public class SearchResult<T>
    {
        [JsonProperty("objects")]
        public IEnumerable<T> Results { get; set; }
    }

    public class ExamsSearchResult
    {
        [JsonProperty("objects")]
        public IEnumerable<Exam> Exams { get; set; }
    }

    public class DisciplineSearchResult
    {
        [JsonProperty("objects")]
        public IEnumerable<Discipline> Disciplines { get; set; }
    }

    public class ReviewSetStartUpInfo
    {
        [JsonProperty("first_card")]
        public QuizCard FirstCard { get; set; }
        [JsonProperty("ncards")]
        public int CardCount { get; set; }
        [JsonProperty("review_id")]
        public int ReviewId { get; set; }
        [JsonProperty("reviewset_id")]
        public int ReviewSetId { get; set; }
    }


    public class BankSlip
    {
        public string Code { get; set; }
    }
   

    public class ReviewSetGroupStartUpInfo
    {
        [JsonProperty("reviews")]
        public IEnumerable<ReviewSetStartUpInfo> ReviewSetStartUpInfos { get; set; }
    }

    public class CardEvalResultInfo
    {
        public string Comment { get; set; }
        public string Feedback { get; set; }
        [JsonProperty("new_hits_review_id")]
        public int? NewHitsReviewId { get; set; }
        [JsonProperty("new_mistakes_review_id")]
        public int? NewMistakeReviewId { get; set; }
        [JsonProperty("next_card")]
        public QuizCard NextCard { get; set; }

        [JsonProperty("eval_result")]
        public string Result { get; set; }
        [JsonProperty("review_finished")]
        public bool IsReviewFinished { get; set; }
        [JsonProperty("review_id")]
        public int ReviewId { get; set; }

        [JsonIgnore]
        public bool AnswredCorrectly => !string.IsNullOrEmpty(Result) && int.Parse(Result) > 0;
        [JsonIgnore] public QuizCard CurrentCard { get; set; }
    }

    public class DisciplineTimeSeries
    {
        [JsonProperty("discipline_id")]
        public int DisciplineId { get; set; }
        [JsonProperty("discipline_name")]
        public string DisciplineName { get; set; }
        [JsonProperty("known_cards")]
        public IEnumerable<TimeAndValue> KnownCards { get; set; }
        [JsonProperty("trendline")]
        public IEnumerable<TimeAndValue> TrendSeries { get; set; }

        public class TimeAndValue
        {
            public string Time{ get; set; }
            public float Value { get; set; }

            public static int GetMonth(string time)
            {
                var strings = time.Split('/');
                if (strings.Length !=2)
                {
                    throw new InvalidDataException("Time format is not correct!");
                }

                return int.Parse(strings[1]);
            }
        }
    }

    public class ExamPerformance
    {
        [JsonProperty("exam_id")]
        public int ExamId { get; set; }
        public string Institution { get; set; }
        public string JobTitle { get; set; }
        [JsonProperty("year_company")]
        public string YearAndCompany { get; set; }
        [JsonProperty("values")]
        public IEnumerable<IEnumerable<PerformanceResult>> Values { get; set; }
        [JsonIgnore]
        public IEnumerable<PerformanceResult> PerformanceResults
        {
            get { return Values.SelectMany(results => results); }
            set { throw new NotImplementedException(); }
        }


        public class PerformanceResult
        {
            [JsonProperty("discipline_name")]
            public string DisciplineName { get; set; }
            public string Month { get; set; }
            public float Score { get; set; }
        }
    }


    public class PaymentPlan
    {
        [JsonProperty("amount_text")]
        public string Amount { get; set; }
        public string Name { get; set; }
        [JsonProperty("period_length")]
        public int Period { get; set; }
        [JsonProperty("plan_id")]
        public string Id { get; set; }
        public float Value { get; set; }
    }


    public class DisciplineProgress
    {
        [JsonProperty("discipline_id")] public int DesciplineId { get; set; }
        [JsonProperty("discipline_name")] public string DisciplineName { get; set; }
        [JsonProperty("subjects_alias")] public string SubjectAlias { get; set; }
        public string Url { get; set; }
        public IEnumerable<Subject> Subjects { get; set; }
        [JsonIgnore] public IEnumerable<SubjectProgress> SubjectProgresses { get; set; }


        public class Subject
        {
            [JsonProperty("subject_id")] public int Id { get; set; }
            [JsonProperty("subject_name")] public string Name { get; set; }
        }
    }

    public class Subject
    {
        [JsonProperty("subject_id")] public int Id { get; set; }
        [JsonProperty("subject_name")] public string Name { get; set; }


        public override bool Equals(object obj)
        {
            if (obj is Subject subject)
            {
                return subject.Id == Id;
            }
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return Id;
        }
    }

    public class Topic
    {
        [JsonProperty("topic_id")]
        public int Id { get; set; }
        [JsonProperty("topic_name")]
        public string Name { get; set; }

        public override bool Equals(object obj)
        {
            if (obj is Topic topic)
            {
                return topic.Id == Id;
            }
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return Id;
        }
    }

    public class SubjectProgress:IPartialSelectableItem<SubjectProgress>
    {
        [JsonProperty("subject_id")] public int SubjectId { get; set; }
        [JsonProperty("subject_name")] public string SubjectName { get; set; }
        public int Resolved { get; set; }
        public int Total { get; set; }
        [JsonIgnore] public IEnumerable<TopicProgress> TopicProgresses { get; set; }


        [JsonIgnore] public string Title
        {
            get => SubjectName; set=>throw new NotImplementedException();}
        [JsonIgnore] public string SubTitle
        {
            get => $"{Resolved}/{Total}"; set=> throw new NotImplementedException();
        }
        [JsonIgnore] public SubjectProgress Data
        {
            get => this;
            set => throw new NotImplementedException();
        }
        [JsonIgnore] public bool Selected { get; set; }

        [JsonIgnore]
        public SelectableState State
        {
            get
            {
                if (TopicProgresses?.All(progress => progress.Selected) ?? false)
                    return SelectableState.All;

                if (TopicProgresses?.Any(progress => progress.Selected) ?? false)
                    return SelectableState.Partial;

                return (Selected && (TopicProgresses==null|| !TopicProgresses.Any()))?SelectableState.All:SelectableState.None;
            }
            set 
            {
                if (value == SelectableState.Partial)
                {
                    return;
                }

                Selected = value != SelectableState.None;

                TopicProgresses?.ForEach(progress => progress.Selected = value == SelectableState.All);
            }
        }
    }

    public class TopicProgress:ISelectableItem<TopicProgress>
    {
        [JsonProperty("topic_id")] public int TopicId { get; set; }
        [JsonProperty("topic_name")] public string TopicName { get; set; }
        public int Resolved { get; set; }
        public int Total { get; set; }

        public string Title
        {
            get => TopicName; set=>throw new NotImplementedException();}
        public string SubTitle
        {
            get => $"{Resolved}/{Total}"; set=>throw new NotImplementedException();}
        public TopicProgress Data
        {
            get => this; set=>throw new NotImplementedException();}
        public bool Selected { get; set; }
    }

    public class ReviewSet
    {
        [JsonProperty("reviewset_id")]
        public int Id { get; set; }
        public int Badge { get; set; }
        public string Name { get; set; }
        [JsonIgnore]
        public float Score
        {
            get
            {
                if (string.IsNullOrEmpty(ScoreString))
                {
                    return 0;
                }

                var valueString = ScoreString.Substring(0,ScoreString.Length-1);
                float.TryParse(valueString, out var val);
                return val;
            }
            set => throw new NotImplementedException();
        }
        [JsonProperty("score")]
        public string ScoreString { get; set; }
        public string Url { get; set; }
        [JsonProperty("schedule")]
        public IEnumerable<Schedule> Schedules { get; set; }
        public Result Resolved { get; set; }

        public override bool Equals(object obj)
        {
            var reviewSet = obj as ReviewSet;
            return reviewSet?.Id == Id;
        }

        public override int GetHashCode()
        {
            return Id;
        }

        public class Result
        {
            public int Hits { get; set; }
            public int Mistakes { get; set; }
            public int Total { get; set; }
            public int UnReviewed { get; set; }
        }


        public class Schedule
        {
            [JsonProperty("ncards")]
            public int CardCount { get; set; }
            [JsonProperty("period")]
            public int Period { get; set; }
        }

        public class Review
        {
            [JsonIgnore]
            public DateTime Date
            {
                get => Utils.ParseToDate(DateString);
                set => throw new NotImplementedException();
            }

            [JsonIgnore]
            public DateTime DueDate
            {
                get => Utils.ParseToDate(DueDateString);
                set => throw new NotImplementedException();
            }

            [JsonProperty("review_id")] public int Id { get; set; }
            public string Url { get; set; }
            [JsonProperty("date")] public string DateString { get; set; }
            [JsonProperty("due_date")] public string DueDateString { get; set; }
        }
    }
}