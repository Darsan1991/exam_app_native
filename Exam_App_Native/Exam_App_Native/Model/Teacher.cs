﻿using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Exam_App_Native.Model
{
    public class Teacher
    {
        [JsonProperty("teacher_id")]
        public int Id { get; set; }
        public string Name { get; set; }
        [JsonProperty("photo")]
        public string ImageUrl { get; set; }
        [JsonProperty("current_job")]
        public string JobTitle { get; set; }
        [JsonProperty("disciplines")]
        public string Disciplines { get; set; }
        public string Url { get; set; }
        public string SocialMedia { get; set; }
        public string Summary { get; set; }
        [JsonProperty("education")]
        public IEnumerable<TeacherEducation> Educations { get; set; }
        [JsonProperty("experience")]
        public IEnumerable<TeacherExperience> Experiences { get; set; }
    }

    public class TeacherEducation
    {
        public string Description { get; set; }
        public string Institution { get; set; }
        public string Year { get; set; }
    }

    public class TeacherExperience
    {
        public string Description { get; set; }
        public string Institution { get; set; }
        public string Year { get; set; }
    }
}