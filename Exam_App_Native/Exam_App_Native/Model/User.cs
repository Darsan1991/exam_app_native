﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Cache;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Exam_App_Native.Model
{
    public partial class User
    {
        public event Action<User> OnUpdateData;

        [JsonProperty("user_id")] public int Id { get; set; }

        public string Email { get; set; }
        public bool Subscribed { get; set; }
        public bool Free { get; set; }
        public string Name { get; set; }
        [JsonProperty("settings")] public UserSettings UserSettings { get; set; }

        public string Url { get; set; }
        [JsonProperty("expiration_date")] public string ExpirationDate { get; set; }
        [JsonIgnore] public IEnumerable<Cookie> Cookies { get; set; }
        [JsonIgnore] public bool Confirmed { get; set; }


        protected void UpdateData(User user)
        {
            if (user.Id != Id)
                throw new Exception("User id should be same");

            Email = user.Email;
            ExpirationDate = user.ExpirationDate;
            Name = user.Name;
            UserSettings = user.UserSettings;
            Url = user.Url;
            Free = user.Free;
            Subscribed = user.Subscribed;

            OnUpdateData?.Invoke(this);
        }
    }


    public partial class User
    {
        public static async Task<bool> ResetPassword(string email)
        {
            var cookieContainer = new CookieContainer();
           
            using (var handler = new HttpClientHandler {CookieContainer = cookieContainer})
            {
                using (var client = new HttpClient(
                    handler
                ))
                {
                    var content = new FormUrlEncodedContent(new[]
                    {
                        new KeyValuePair<string, string>("email", email),
                    });

                    var response = await client.PostAsync($"{ExamApp.BASE_URL}/users/resetpassword", content);
                    response.EnsureSuccessStatusCode();

                    var resultJson = await response.Content.ReadAsStringAsync();
                    var messageAndResult = JsonConvert.DeserializeObject<MessageAndResult>(resultJson);

                    return messageAndResult.Result == "success";
                }
            }
        }
    }

    //Object Functions
    public partial class User
    {
        public async Task<bool> ConfirmEmail(string code)
        {
            using (var client = new HttpClient())
            {
                var content = new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string, string>("email", Email),
                    new KeyValuePair<string, string>("code", code),
                });

                var response = await client.PostAsync(ExamApp.BASE_URL + "/" + "users/confirm", content);
                response.EnsureSuccessStatusCode();

                var resultJson = await response.Content.ReadAsStringAsync();

                var result = JsonConvert.DeserializeObject<MessageAndResult>(resultJson);
                if (result.Result == "success")
                {
                    return true;
                }

                throw new Exception(result.Message);
            }
        }

        public async Task<MessageAndResult> ResendToken()
        {
            using (var client = new HttpClient())
            {
                var response = await client.GetAsync(ExamApp.BASE_URL + $"/users/{Email}/resendtoken");
                response.EnsureSuccessStatusCode();
                var resultJson = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<MessageAndResult>(resultJson);
            }
        }

        public async Task<User> UpdateSettings(UserSettings settings)
        {
            var cookieContainer = new CookieContainer();
            Cookies.ForEach(cookie => cookieContainer.Add(cookie));

            using (var handler = new HttpClientHandler {CookieContainer = cookieContainer})
            {
                using (var client = new HttpClient(handler))
                {
                    var content = new FormUrlEncodedContent(new[]
                    {
                        new KeyValuePair<string, string>("mix_cards", settings.MixCards ? "true" : "false"),
                        new KeyValuePair<string, string>("skip_feedback", settings.SkipFeedback ? "true" : "false"),
                    });

                    var response = await client.PutAsync($"{ExamApp.BASE_URL}/users/{Id}/settings", content);
                    response.EnsureSuccessStatusCode();
                    var resultJson = await response.Content.ReadAsStringAsync();
                    UpdateData(JsonConvert.DeserializeObject<User>(resultJson));
                    return this;
                }
            }
        }


        public async Task<User> ReloadUser(CancellationTokenSource tokenSource = null)
        {
            var cookieContainer = new CookieContainer();
            Cookies.ForEach(cookie => cookieContainer.Add(cookie));
            using (var handler = new HttpClientHandler {CookieContainer = cookieContainer})
            {
                using (var client = new HttpClient(handler))
                {
                    tokenSource = tokenSource ?? new CancellationTokenSource();

                    var response = await client.GetAsync($"{ExamApp.BASE_URL}/users/{Email}",tokenSource.Token);
                    response.EnsureSuccessStatusCode();

                    var resultJson = await response.Content.ReadAsStringAsync();

                    var result = JsonConvert.DeserializeObject<MessageAndResult>(resultJson);
                    if (result.Result == "error")
                    {
                        throw new Exception(result.Message);
                    }

                    var user = JsonConvert.DeserializeObject<User>(resultJson);
                    UpdateData(user);
                    return this;
                }
            }
        }


        public async Task<User> Subscribe(string planId, string stripeToken, string cpf, bool renewal,CancellationTokenSource tokenSource=null)
        {
            var cookieContainer = new CookieContainer();
            Cookies.ForEach(cookie => cookieContainer.Add(cookie));
            using (var handler = new HttpClientHandler { CookieContainer = cookieContainer })
            {
                using (var client = new HttpClient(handler))
                {
                    tokenSource = tokenSource ?? new CancellationTokenSource();

                    var content = new FormUrlEncodedContent(new[]
                    {
                        new KeyValuePair<string, string>("plan_id", planId),
                        new KeyValuePair<string, string>("user_id", Id.ToString()),
                        new KeyValuePair<string, string>("stripe_token", stripeToken),
                        new KeyValuePair<string, string>("cpf", cpf),
                        new KeyValuePair<string, string>("renewal", renewal.ToString()),
                    });

                    var response = await client.PostAsync($"{ExamApp.BASE_URL}/payment/creditcard",content, tokenSource.Token);
                    response.EnsureSuccessStatusCode();
                    var resultJson = await  response.Content.ReadAsStringAsync();
                    await ReloadUser(tokenSource);
                    return this;
                }
            }
        }

        public async Task<BankSlip> GenerateBankSlip(string planId, string cpf, CancellationTokenSource tokenSource = null)
        {
            var cookieContainer = new CookieContainer();
            Cookies.ForEach(cookie => cookieContainer.Add(cookie));
            using (var handler = new HttpClientHandler { CookieContainer = cookieContainer })
            {
                using (var client = new HttpClient(handler))
                {
                    tokenSource = tokenSource ?? new CancellationTokenSource();

                    var content = new FormUrlEncodedContent(new []
                    {
                        new KeyValuePair<string, string>("user_id",Id.ToString()),
                        new KeyValuePair<string, string>("plan_id",planId),
                        new KeyValuePair<string, string>("cpf",cpf),
                    });

                    var response = await client.PostAsync($"{ExamApp.BASE_URL}/payment/bankslip", content, tokenSource.Token);
                    response.EnsureSuccessStatusCode();
                    var resultJson = await response.Content.ReadAsStringAsync();
                    
                    return JsonConvert.DeserializeObject<BankSlip>(resultJson);
                }
            }
        }

        public async Task<MessageAndResult> SendBankSlipCodeToEmail(CancellationTokenSource tokenSource=null)
        {
            var cookieContainer = new CookieContainer();
            Cookies.ForEach(cookie => cookieContainer.Add(cookie));
            using (var handler = new HttpClientHandler { CookieContainer = cookieContainer })
            {
                using (var client = new HttpClient(handler))
                {
                    tokenSource = tokenSource ?? new CancellationTokenSource();

                    var content = new FormUrlEncodedContent(new List<KeyValuePair<string, string>>());

                    var response = await client.PostAsync($"{ExamApp.BASE_URL}/payment/bankslip/sendemail", content, tokenSource.Token);
                    response.EnsureSuccessStatusCode();

                    var json = await response.Content.ReadAsStringAsync();


                    return JsonConvert.DeserializeObject<MessageAndResult>(json);
                }
            }
        }

        public async Task<User> UnSubscribe(CancellationTokenSource tokenSource = null)
        {
            var cookieContainer = new CookieContainer();
            Cookies.ForEach(cookie => cookieContainer.Add(cookie));
            using (var handler = new HttpClientHandler { CookieContainer = cookieContainer })
            {
                using (var client = new HttpClient(handler))
                {
                    tokenSource = tokenSource ?? new CancellationTokenSource();

                    var content = new FormUrlEncodedContent(new List<KeyValuePair<string, string>>());

                    var response = await client.PostAsync($"{ExamApp.BASE_URL}/payment/unsubscribe", content, tokenSource.Token);
                    response.EnsureSuccessStatusCode();

                    await ReloadUser(tokenSource);

                    return this;
                }
            }
        }


        public async Task<User> ChangeUserName(string name)
        {
            var cookieContainer = new CookieContainer();
            Cookies.ForEach(cookie => cookieContainer.Add(cookie));

            using (var handler = new HttpClientHandler {CookieContainer = cookieContainer})
            {
                using (var client = new HttpClient(handler))
                {
                    var content = new FormUrlEncodedContent(new[]
                    {
                        new KeyValuePair<string, string>("name", name),
                    });

                    var response = await client.PutAsync($"{ExamApp.BASE_URL}/users/{Id}", content);
                    response.EnsureSuccessStatusCode();
                    var resultJson = await response.Content.ReadAsStringAsync();
                    UpdateData(JsonConvert.DeserializeObject<User>(resultJson));
                    return this;
                }
            }
        }


        public async Task<IEnumerable<MyDiscipline>> GetMyCardsDisciplines(CancellationTokenSource tokenSource = null)
        {
            var cookieContainer = new CookieContainer();
            Cookies.ForEach(cookie => cookieContainer.Add(cookie));
            tokenSource = tokenSource ?? new CancellationTokenSource();
            using (var handler = new HttpClientHandler {CookieContainer = cookieContainer})
            {
                using (var client = new HttpClient(handler))
                {
                    var response = await client.GetAsync($"{ExamApp.BASE_URL}/users/{Id}/cards/disciplines",
                        tokenSource.Token);
                    response.EnsureSuccessStatusCode();

                    var resultJson = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<IEnumerable<MyDiscipline>>(resultJson);
                }
            }
        }


        public async Task<IEnumerable<DisciplineTimeSeries>> GetDisciplineTimeSeries(
            CancellationTokenSource tokenSource = null)
        {
            var cookieContainer = new CookieContainer();
            Cookies.ForEach(cookie => cookieContainer.Add(cookie));
            tokenSource = tokenSource ?? new CancellationTokenSource();
            using (var handler = new HttpClientHandler { CookieContainer = cookieContainer })
            {
                using (var client = new HttpClient(handler))
                {
                    var response = await client.GetAsync($"{ExamApp.BASE_URL}/users/2/statistics/timeseries/disciplines",
                        tokenSource.Token);
                    response.EnsureSuccessStatusCode();

                    var resultJson = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<IEnumerable<DisciplineTimeSeries>>(resultJson);
                }
            }
        }

        public async Task<IEnumerable<ExamPerformance>> GetExamPerformances(
            CancellationTokenSource tokenSource = null)
        {
            var cookieContainer = new CookieContainer();
            Cookies.ForEach(cookie => cookieContainer.Add(cookie));
            tokenSource = tokenSource ?? new CancellationTokenSource();
            using (var handler = new HttpClientHandler { CookieContainer = cookieContainer })
            {
                using (var client = new HttpClient(handler))
                {
                    var response = await client.GetAsync($"{ExamApp.BASE_URL}/users/2/statistics/timeseries/exams",
                        tokenSource.Token);
                    response.EnsureSuccessStatusCode();

                    var resultJson = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<IEnumerable<ExamPerformance>>(resultJson);
                }
            }
        }

        public async Task<IEnumerable<MyDiscipline>> SearchMyCardsDisciplines(string query,
            CancellationTokenSource tokenSource = null)
        {
            var cookieContainer = new CookieContainer();
            Cookies.ForEach(cookie => cookieContainer.Add(cookie));
            tokenSource = tokenSource ?? new CancellationTokenSource();
            using (var handler = new HttpClientHandler {CookieContainer = cookieContainer})
            {
                using (var client = new HttpClient(handler))
                {
                    var response = await client.GetAsync($"{ExamApp.BASE_URL}/search?q={query}&t=card&user_id={Id}",
                        tokenSource.Token);
                    response.EnsureSuccessStatusCode();

                    var resultJson = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<SearchResult<MyDiscipline>>(resultJson).Results;
                }
            }
        }

        public async Task<IEnumerable<MyCard>> SearchMyCards(string query, CancellationTokenSource tokenSource = null)
        {
            var cookieContainer = new CookieContainer();
            Cookies.ForEach(cookie => cookieContainer.Add(cookie));
            tokenSource = tokenSource ?? new CancellationTokenSource();
            using (var handler = new HttpClientHandler {CookieContainer = cookieContainer})
            {
                using (var client = new HttpClient(handler))
                {
                    var response = await client.GetAsync($"{ExamApp.BASE_URL}/search?q={query}&t=card&user_id={Id}",
                        tokenSource.Token);
                    response.EnsureSuccessStatusCode();

                    var resultJson = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<SearchResult<MyCard>>(resultJson).Results;
                }
            }
        }

        public async Task<IEnumerable<MyCard>> SearchMyCards(string query,Discipline discipline, CancellationTokenSource tokenSource = null)
        {
            var cookieContainer = new CookieContainer();
            Cookies.ForEach(cookie => cookieContainer.Add(cookie));
            tokenSource = tokenSource ?? new CancellationTokenSource();
            using (var handler = new HttpClientHandler { CookieContainer = cookieContainer })
            {
                using (var client = new HttpClient(handler))
                {
                    var response = await client.GetAsync($"{ExamApp.BASE_URL}/search?q={query}&t=card&user_id={Id}&discipline={discipline.Name}",
                        tokenSource.Token);
                    response.EnsureSuccessStatusCode();

                    var resultJson = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<IEnumerable<MyCard>>(resultJson);
                }
            }
        }


        public async Task<IEnumerable<MyCard>> GetMyCardsForDiscipline(int disciplineId,
            CancellationTokenSource tokenSource = null)
        {
            var cookieContainer = new CookieContainer();
            Cookies.ForEach(cookie => cookieContainer.Add(cookie));
            tokenSource = tokenSource ?? new CancellationTokenSource();
            using (var handler = new HttpClientHandler {CookieContainer = cookieContainer})
            {
                using (var client = new HttpClient(handler))
                {
                    var response =
                        await client.GetAsync($"{ExamApp.BASE_URL}/users/{Id}/cards/disciplines/{disciplineId}",
                            tokenSource.Token);
                    response.EnsureSuccessStatusCode();

                    var resultJson = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<IEnumerable<MyCard>>(resultJson);
                }
            }
        }

        public async Task<IEnumerable<ReviewSet>> GetReviewSets(CancellationTokenSource tokenSource = null)
        {
            var cookieContainer = new CookieContainer();
            Cookies.ForEach(cookie => cookieContainer.Add(cookie));
            tokenSource = tokenSource ?? new CancellationTokenSource();
            using (var handler = new HttpClientHandler {CookieContainer = cookieContainer})
            {
                using (var client = new HttpClient(handler))
                {
                    var response =
                        await client.GetAsync($"{ExamApp.BASE_URL}/users/{Id}/reviewsets", tokenSource.Token);
                    response.EnsureSuccessStatusCode();

                    var resultJson = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<IEnumerable<ReviewSet>>(resultJson);
                }
            }
        }

        public async Task DeleteReviewSet(int reviewId, CancellationTokenSource tokenSource)
        {
            var cookieContainer = new CookieContainer();
            Cookies.ForEach(cookie => cookieContainer.Add(cookie));
            tokenSource = tokenSource ?? new CancellationTokenSource();
            using (var handler = new HttpClientHandler {CookieContainer = cookieContainer})
            {
                using (var client = new HttpClient(handler))
                {
                    var response = await client.GetAsync($"{ExamApp.BASE_URL}/delete/users/{Id}/reviewsets/{reviewId}",
                        tokenSource.Token);
//                    response.EnsureSuccessStatusCode();

                    var resultJson = await response.Content.ReadAsStringAsync();
                    var messageAndResult = JsonConvert.DeserializeObject<MessageAndResult>(resultJson);
                    if (messageAndResult.Result != "successful")
                        throw new ExamAppException(messageAndResult.Message);

//                    return messageAndResult.Message;
                }
            }
        }

        public async Task CreateNewCard(MyCard card, CancellationTokenSource tokenSource = null)
        {
            var cookieContainer = new CookieContainer();
            Cookies.ForEach(cookie => cookieContainer.Add(cookie));
            tokenSource = tokenSource ?? new CancellationTokenSource();
            using (var handler = new HttpClientHandler {CookieContainer = cookieContainer})
            {
                using (var client = new HttpClient(handler))
                {
                    var jsonBody = JsonConvert.SerializeObject(new Dictionary<string, object>()
                    {
                        {"topic_id", card.Topic.Id},
                        {"answer", card.Answer ?? false},
                        {"content", card.Content},
                        {"comment", card.Comment},
                    });
                    var content = new StringContent(jsonBody, Encoding.UTF8, "application/json");

                    var response = await client.PostAsync($"{ExamApp.BASE_URL}/users/{Id}/cards", content,
                        tokenSource.Token);

                    response.EnsureSuccessStatusCode();
                }
            }
        }


        public async Task UpdateCard(MyCard card, CancellationTokenSource tokenSource = null)
        {
            var cookieContainer = new CookieContainer();
            Cookies.ForEach(cookie => cookieContainer.Add(cookie));
            tokenSource = tokenSource ?? new CancellationTokenSource();
            using (var handler = new HttpClientHandler { CookieContainer = cookieContainer })
            {
                using (var client = new HttpClient(handler))
                {
                    var jsonBody = JsonConvert.SerializeObject(new Dictionary<string, object>()
                    {
                        {"topic_id", card.Topic.Id},
                        {"answer", card.Answer ?? false},
                        {"content", card.Content},
                        {"comment", card.Comment},
                    });
                    var content = new StringContent(jsonBody, Encoding.UTF8, "application/json");

                    var response = await client.PutAsync($"{ExamApp.BASE_URL}/users/{Id}/cards/{card.Id}", content,
                        tokenSource.Token);

                    response.EnsureSuccessStatusCode();
                }
            }
        }

    }


    public class ResultObj
    {
        [JsonProperty("result")] public string Result { get; set; }
    }


    public class MessageAndResult
    {
        public string Message { get; set; }
        public string Result { get; set; }
    }

    public class UserSettings
    {
        [JsonProperty("mix_cards")] public bool MixCards { get; set; }
        [JsonProperty("settings_id")] public int Id { get; set; }
        [JsonProperty("skip_feedback")] public bool SkipFeedback { get; set; }
    }
}