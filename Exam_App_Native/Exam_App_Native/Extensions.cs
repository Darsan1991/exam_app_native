﻿using System;
using System.Collections.Generic;
using Exam_App_Native.Model;
using Exam_App_Native.Model.ViewModel;

namespace Exam_App_Native
{
    public static class Extensions
    {
        public static void ForEach<T>(this IEnumerable<T> enumerable,Action<T> action)
        {
            foreach (var x1 in enumerable)
            {
                action?.Invoke(x1);
            }
        }

        public static void ForEach<T>(this IEnumerable<T> enumerable, Action<int,T> action)
        {
            var i = 0;
            foreach (var x1 in enumerable)
            {
                action?.Invoke(i,x1);
                i++;
            }
          
        }

        public static Dictionary<TKey, TVal> ToDictionary<T,TKey, TVal>(this IEnumerable<T> enumerable,Func<T,TKey> key,Func<T,TVal> val)
        {
            var dictionary = new Dictionary<TKey,TVal>();
            foreach (var x1 in enumerable)
            {
                dictionary.Add(key(x1),val(x1));
            }

            return dictionary;
        }

        public static TJ AllIntoOne<T, TJ>(this IEnumerable<T> enumerable, Func<T,TJ, TJ> selector)
        {
            var val = default(TJ);
            enumerable.ForEach(obj => val = selector.Invoke(obj,val));
            return val;
        }
       

    }


    public static class ModelExtensions
    {
        public static ICard<Exam> ToCard(this Exam exam)=>
        new Card<Exam>
        {
            Title = exam.JobTitle,
            SubTitle = exam.Institution,
            Footer = $"{exam.Company} - {exam.Year}"
        };

        public static ICard<Discipline> ToCard(this Discipline discipline)=>new Card<Discipline>
        {
            Title = discipline.Name,
            Footer = $"{discipline.CardCount} - cards"
        };

    }
}