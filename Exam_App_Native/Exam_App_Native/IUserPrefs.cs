﻿namespace Exam_App_Native
{
    public interface IUserPrefs
    {
        void SetString(string key, string value);
        string GetString(string key, string defValue="");

        void SetInt(string key, int value);
        int GetInt(string key, int defValue = 0);

        void SetFloat(string key, float value);
        float GetFloat(string key, float defValue =0f);

        void DeleteKey(string key);
    }
}