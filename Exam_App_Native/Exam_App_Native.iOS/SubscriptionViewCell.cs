﻿using Foundation;
using System;
using Exam_App_Native.Model;
using UIKit;
using CoreGraphics;

namespace Exam_App_Native.iOS
{
    public partial class SubscriptionViewCell : UITableViewCell,IHolder<object>
    {

        public SubscriptionViewCell (IntPtr handle) : base (handle)
        {
            
        }

        public object Item
        {
            get => (ViewModel) viewModel;
            set
            {
                if (value is ViewModel model)
                {
                    viewModel = model;
                }
            } }


        public ViewModel viewModel
        {
            get => _viewModel;
            set
            {
                _viewModel = value;
                contentLabel.Text = viewModel.Content;
                leftLabel.Text = viewModel.LeftTitle;
                rightLabel.Text = viewModel.RightTitle;
            }
        }

        private ViewModel _viewModel;


        public override void AwakeFromNib()
        {
            base.AwakeFromNib();
            actualContentView.Layer.MasksToBounds = false;
            actualContentView.Layer.ShadowColor = UIColor.DarkGray.CGColor;
            actualContentView.Layer.ShadowOffset = new CGSize(0, 2.5);
            actualContentView.Layer.ShadowRadius = 0.8f;
            actualContentView.Layer.ShadowOpacity = 0.2f;
        }



        public struct ViewModel
        {
            public string Content { get; set; }
            public string LeftTitle { get; set; }
            public string RightTitle { get; set; }
        }

    }
}