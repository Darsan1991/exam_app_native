﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Exam_App_Native.iOS.Adapter;
using Exam_App_Native.iOS.View;
using Exam_App_Native.Model.ViewModel;
using Foundation;
using UIKit;

namespace Exam_App_Native.iOS
{
    public partial class SimpleTableViewController : UIViewController
    {

        public ViewModel MViewModel { get; set; }

        protected ActivityIndicator activityIndicator;
        private Task<object> _activeTask;
        private CancellationTokenSource _actuveTokenSource;
        private Timer _searchBarTimer;

        private TableViewAdapter _tableViewAdapter;

        public UITableView TableView => tableView;

        public SimpleTableViewController (IntPtr handle) : base (handle)
        {

        }

        public override async void ViewDidLoad()
        {
            base.ViewDidLoad();
            MViewModel.Instance = this;

            if (NavigationController==null)
            {
                throw new InvalidOperationException("This View controller should be present with NavigationController");
            }
            
            NavigationItem.Title = MViewModel.Title;


            var activityHolderView = new UIView()
            {
                TranslatesAutoresizingMaskIntoConstraints = false,
                BackgroundColor = UIColor.Clear,
                UserInteractionEnabled = false
            };

            View.AddSubview(activityHolderView);

            activityHolderView.LeftAnchor.ConstraintEqualTo(tableView.LeftAnchor).Active = true;
            activityHolderView.RightAnchor.ConstraintEqualTo(tableView.RightAnchor).Active = true;
            activityHolderView.TopAnchor.ConstraintEqualTo(tableView.TopAnchor).Active = true;
            activityHolderView.BottomAnchor.ConstraintEqualTo(tableView.BottomAnchor).Active = true;

            activityIndicator = new ActivityIndicator(activityHolderView, 1f) { Animating = true };


            tableView.AddGestureRecognizer(new UITapGestureRecognizer((obj) => {
                View.EndEditing(true);
            })
            { CancelsTouchesInView = false });

            serchBar.Placeholder = NSBundle.MainBundle.LocalizedString(LocalizableWords.SEARCH, "Search").ToUpper();
            serchBar.TextChanged += (sender, args) =>
            {
                if (_searchBarTimer == null)
                {

                    _searchBarTimer = new System.Threading.Timer(
                        state => InvokeOnMainThread(action: () =>
                        {
                            args.SearchText = serchBar.Text;
                            MViewModel.OnSearchTextChanged?.Invoke(this,args);
                        })
                        , null
                        , 200
                        , Timeout.Infinite);


                }
                else
                {
                    _searchBarTimer.Change(200, Timeout.Infinite);
                }

            };

            serchBar.SearchButtonClicked += (sender, args) => MViewModel.OnSearchTextSubmit?.Invoke(this, args);



            tableView.TableFooterView = new UIView();

            if (MViewModel.Compeltion != null)
            {
                var compeltion = MViewModel.Compeltion;
                var buttonItem = new UIBarButtonItem()
                {
                    Title = compeltion.CompleteButtonTitle,
                };
                buttonItem.Clicked += (sender, args) =>
                {
                    compeltion.OnComplete?.Invoke(this);
                    NavigationController.PopViewController(true);
                };
                NavigationItem.RightBarButtonItem = buttonItem;
            }

            try
            {
                _tableViewAdapter = await MViewModel.TableViewAdapter;
                _tableViewAdapter.TableView = tableView;
                tableView.Delegate = _tableViewAdapter;
                tableView.DataSource = _tableViewAdapter;
               tableView.ReloadData();
            }
            catch (ExamAppException e)
            {
                IosExamApp.ShowToastAlert("Error!", e.Message, this,
                    () => NavigationController.PopViewController(true));
            }
            catch (Exception e)
            {
                IosExamApp.ShowToastAlert("Error!","Unknown Error!", this,
                    () => NavigationController.PopViewController(true));
            }

            activityIndicator.Animating = false;
        }

        public override void TouchesEnded(NSSet touches, UIEvent evt)
        {
            base.TouchesEnded(touches, evt);
            View.EndEditing(true);
        }

        public async void Update(Task<object> task, CancellationTokenSource tokenSource)
        {
            if (!MViewModel.TableViewAdapter?.IsCompleted ?? true)
            {
                tokenSource?.Cancel();
                return;
            }

            if (_activeTask != null && (!_activeTask.IsCompleted && !_activeTask.IsCanceled && !_activeTask.IsFaulted))
            {
                _actuveTokenSource?.Cancel();
            }

            _activeTask = task;
            _actuveTokenSource = tokenSource;

            activityIndicator.Animating = true;
            try
            {
                var obj = await task;
                MViewModel.OnUpdatated?.Invoke(this, obj);
            }
            catch (Exception e)
            {
                // ignored
            }

            activityIndicator.Animating = false;

        }

        public void ReloadData()
        {
            tableView.ReloadData();
        }

        public class ViewModel
        {
            public EventHandler<UISearchBarTextChangedEventArgs> OnSearchTextChanged { get; set; }
            public EventHandler<EventArgs> OnSearchTextSubmit { get; set; }
            public EventHandler<object> OnUpdatated { get; set; }
            public string Title { get; set; }
            public string SubTitle { get; set; }
            public Task<TableViewAdapter> TableViewAdapter { get; set; }
            public CancellationTokenSource TokenSource { get; set; }
            public CompletionGroup Compeltion{ get; set; }

            public SimpleTableViewController Instance { get; set; }

            public class CompletionGroup
            {
                public string CompleteButtonTitle { get; set; }
                public Action<SimpleTableViewController> OnComplete { get; set; }
            }
        }

    }
}