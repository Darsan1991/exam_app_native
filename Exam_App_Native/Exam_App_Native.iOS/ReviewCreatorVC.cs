﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Exam_App_Native.iOS.Adapter;
using Exam_App_Native.iOS.View;
using Exam_App_Native.Model;
using Exam_App_Native.Model.ViewModel;
using Foundation;
using UIKit;

namespace Exam_App_Native.iOS
{
    // ReSharper disable once InconsistentNaming
    public partial class ReviewCreatorVC : UIViewController
    {
        private ActivityIndicator _activityIndicator;
        private SimpleTableViewAdapter<IPartialSelectableItem<object>, PartialSelectableViewCell> _tableViewAdapter;

        public ViewModel MViewModel { get; set; }

        public ReviewCreatorVC (IntPtr handle) : base (handle)
        {
        }

        public override async void ViewDidLoad()
        {
            base.ViewDidLoad();

            if(NavigationController==null)
                throw new InvalidOperationException("This VC should be present inside the NavigationController");
            Title = MViewModel.Discipline.Name;
            var buttonItem = new UIBarButtonItem
            {
                Title = NSBundle.MainBundle.LocalizedString(LocalizableWords.START,"Start")
            };
            buttonItem.Clicked += (sender, args) =>
            {
                CheckAndStartTheReview();
            };
            NavigationItem.RightBarButtonItem = buttonItem;
           
            titleLabel.UserInteractionEnabled = true;
            titleLabel.AddGestureRecognizer(new UITapGestureRecognizer(ShowReviewNameChangeDialog));
            _activityIndicator = new ActivityIndicator(View,1f);

            _tableViewAdapter = new SimpleTableViewAdapter<IPartialSelectableItem<object>,PartialSelectableViewCell>("PartialSelectableViewCell");
            _tableViewAdapter.OnDequeueReuseCell += cell =>
            {
                cell.OnSelectableStateChanged -= CellOnSelectableStateChanged;
                cell.OnSelectableStateChanged +=CellOnSelectableStateChanged;
                cell.OnClickNextArrow -= CellOnNextArrowClicked;
                cell.OnClickNextArrow += CellOnNextArrowClicked;
            };
            tableView.Delegate = _tableViewAdapter;
            tableView.DataSource = _tableViewAdapter;

            _tableViewAdapter.OnItemClicked += item =>
            {
                var storyboard = UIStoryboard.FromName("Main",null);
                var controller = (SimpleTableViewController)storyboard.InstantiateViewController("SimpleTableViewController");

                Task<IEnumerable<TopicProgress>> topicTask = null;
                var subjectProgress = (SubjectProgress)item.Data;

                if (subjectProgress.TopicProgresses == null)
                {
                    topicTask = Task.Run(async () => MViewModel.Exam == null
                        ? await ExamApp.Instance.GetTopicProgresses(subjectProgress.SubjectId)
                        : await ExamApp.Instance.GetTopicProgresses(MViewModel.Exam.Id, subjectProgress.SubjectId));


                }
                else
                {
                    topicTask = Task.FromResult(subjectProgress.TopicProgresses);
                }

                SimpleTableViewAdapter<ISelectableItem<object>, ToggleViewCell> adapter = null;

                controller.MViewModel = new SimpleTableViewController.ViewModel
                {
                    Title = item.Title.Length > 15 ? item.Title.Substring(0, 15)+"..." : item.Title,
                    TableViewAdapter = Task.Run(async () =>
                    {
                        var topicProgresses = (await topicTask).ToList();
                  
                            topicProgresses.ForEach(progress =>
                            {
                                if(subjectProgress.State == SelectableState.All)
                                progress.Selected = true;
                                else if (subjectProgress.State == SelectableState.None)
                                {
                                    progress.Selected = false;
                                }
                            });
//                        ((SubjectProgress) item.Data).TopicProgresses = topicProgresses; 
                        adapter = (new SimpleTableViewAdapter<ISelectableItem<object>, ToggleViewCell>("ToggleViewCell", topicProgresses.Select(progress => new TopicProgressToObjectModel(progress))));
                        adapter.OnDequeueReuseCell +=(obj) => obj.SelectionStyle = UITableViewCellSelectionStyle.None;
                        return (TableViewAdapter)adapter;
                    }),
                    Compeltion = new SimpleTableViewController.ViewModel.CompletionGroup
                    {
                        CompleteButtonTitle = NSBundle.MainBundle.LocalizedString(LocalizableWords.SAVE,"Save"),
                        OnComplete = viewController =>
                        {
                            var itemData = (SubjectProgress)item.Data;
                            itemData.TopicProgresses =
                                adapter.Items.Select(selectableItem => (TopicProgress)selectableItem.Data).ToList();
                            tableView.ReloadData();
                            UpdateSelectedCardCount();
                        }
                    }
                };

                NavigationController.PushViewController(controller,true);
            };

            titleLabel.Text = MViewModel.Discipline.Name;
            _activityIndicator.Animating = true;
            try
            {
                var subjectProgresses = await MViewModel.SubjectProgressTask;
                _tableViewAdapter.Items = subjectProgresses.Select(progress => new SubjectProgressToObjectModel(progress));
                _tableViewAdapter.Items.ForEach((obj) => obj.State = SelectableState.All);
                tableView.ReloadData();
                UpdateSelectedCardCount();
            }
            catch (OperationCanceledException e)
            {

            }
            catch (ExamAppException e)
            {
                IosExamApp.ShowToastAlert("Error!", e.Message, this);


            }
            catch (Exception e)
            {
                IosExamApp.ShowToastAlert("Error!", "Something Went Wrong!", this);
            }

            _activityIndicator.Animating = false;
        }

        private void CellOnNextArrowClicked(object sender, EventArgs e)
        {
            var cell = (PartialSelectableViewCell)sender;
            var item = (IPartialSelectableItem<object>)cell.Item;
            var storyboard = UIStoryboard.FromName("Main", null);
            var controller = (SimpleTableViewController)storyboard.InstantiateViewController("SimpleTableViewController");

            Task<IEnumerable<TopicProgress>> topicTask = null;
            var subjectProgress = (SubjectProgress)item.Data;

            if (subjectProgress.TopicProgresses == null)
            {
                topicTask = Task.Run(async () => MViewModel.Exam == null
                    ? await ExamApp.Instance.GetTopicProgresses(subjectProgress.SubjectId)
                    : await ExamApp.Instance.GetTopicProgresses(MViewModel.Exam.Id, subjectProgress.SubjectId));


            }
            else
            {
                topicTask = Task.FromResult(subjectProgress.TopicProgresses);
            }

            SimpleTableViewAdapter<ISelectableItem<object>, ToggleViewCell> adapter = null;

            controller.MViewModel = new SimpleTableViewController.ViewModel
            {
                Title = item.Title.Length > 15 ? item.Title.Substring(0, 15) + "..." : item.Title,
                TableViewAdapter = Task.Run(async () =>
                {
                    var topicProgresses = (await topicTask).ToList();

                    topicProgresses.ForEach(progress =>
                    {
                        if (subjectProgress.State == SelectableState.All)
                            progress.Selected = true;
                        else if (subjectProgress.State == SelectableState.None)
                        {
                            progress.Selected = false;
                        }
                    });
                    //                        ((SubjectProgress) item.Data).TopicProgresses = topicProgresses; 
                    adapter = (new SimpleTableViewAdapter<ISelectableItem<object>, ToggleViewCell>("ToggleViewCell", topicProgresses.Select(progress => new TopicProgressToObjectModel(progress))));
                    adapter.OnDequeueReuseCell += (obj) => obj.SelectionStyle = UITableViewCellSelectionStyle.None;
                    return (TableViewAdapter)adapter;
                }),
                Compeltion = new SimpleTableViewController.ViewModel.CompletionGroup
                {
                    CompleteButtonTitle = NSBundle.MainBundle.LocalizedString(LocalizableWords.SAVE,"Save"),
                    OnComplete = viewController =>
                    {
                        var itemData = (SubjectProgress)item.Data;
                        itemData.TopicProgresses =
                            adapter.Items.Select(selectableItem => (TopicProgress)selectableItem.Data).ToList();
                        tableView.ReloadData();
                        UpdateSelectedCardCount();
                    }
                }
            };

            NavigationController.PushViewController(controller, true);
        }

        private void CheckAndStartTheReview()
        {
            if (GetSelectedCardCount() == 0)
            {
                IosExamApp.ShowToastAlert("Error!", "Please Select Some Cards.",this);
            }

            var selectableItems = _tableViewAdapter.Items.ToList();
            var subjects = selectableItems
                .Where(item => item.State == SelectableState.All).ToList();

            var topics = selectableItems.Where(item => item.State == SelectableState.Partial)
                             .Select(item => ((SubjectProgress)item.Data).TopicProgresses.Where(progress => progress.Selected))
                             .AllIntoOne<IEnumerable<TopicProgress>, List<TopicProgress>>((enumerable, list) =>
                             {
                                 if (list == null)
                                     list = new List<TopicProgress>();
                                 list.AddRange(enumerable);
                                 return list;
                             }) ?? new List<TopicProgress>();

            var title = titleLabel.Text;

            var viewModel = new QuizViewController.ViewModel
            {
                ReviewSetStartUpInfos = Task.Run(async ()=>
                {
                    var setStartUpInfo = await ExamApp.Instance
                        .CreateNewReviewSet(subjects.Select(item => ((SubjectProgress) item.Data).SubjectId)
                            // ReSharper disable once TooManyChainedReferences
                            , topics.Select(progresses => progresses.TopicId),title);

                    return (IEnumerable<ReviewSetStartUpInfo>)new[] {setStartUpInfo};
                })

            };

            var storyboard = UIStoryboard.FromName("Main",null);
            var controller = (QuizViewController)storyboard.InstantiateViewController("QuizViewController");
            controller.MViewModel = viewModel;

            PresentViewController(IosExamApp.CreateNavigationControllerWithTheme(controller),true, () =>
            {

                NavigationController.PopToRootViewController(false);
            });

        }


        private void ShowReviewNameChangeDialog()
        {
            var bundle = NSBundle.MainBundle;
            var controller = UIAlertController.Create(bundle.LocalizedString(LocalizableWords.CHANGE_USER_NAME, "Change User Name"), null, UIAlertControllerStyle.Alert);
            controller.AddTextField(field =>
            {
                field.Placeholder = bundle.LocalizedString(LocalizableWords.REVIEW_NAME, "Review name")+"...";
                field.Text = titleLabel.Text;
            });
            controller.AddAction(UIAlertAction.Create(bundle.LocalizedString(LocalizableWords.OK, bundle.LocalizedString(LocalizableWords.OK, "Ok")), UIAlertActionStyle.Default, async action =>
            {
                var text = controller.TextFields[0].Text;
                if (string.IsNullOrEmpty(text))
                {
                    IosExamApp.ShowToastAlert("Error!", "Review name cannot be Empty!", this);
                    return;
                }

                titleLabel.Text = text;
            }));
            controller.AddAction(UIAlertAction.Create(bundle.LocalizedString(LocalizableWords.CANCEL, "Cancel"), UIAlertActionStyle.Default, action => { }));
            PresentViewController(controller, true, null);
        }

        private void CellOnSelectableStateChanged(object sender, SelectableState selectableState)
        {
            UpdateSelectedCardCount();
        }


        public override void ViewDidDisappear(bool animated)
        {
            base.ViewDidDisappear(animated);
            MViewModel.TokenSource?.Cancel();
        }

        private void UpdateSelectedCardCount()
        {
            var selectedCount = GetSelectedCardCount();
            noOfCardsLabel.Text = selectedCount.ToString();
        }

        private int GetSelectedCardCount()
        {
            return _tableViewAdapter.Items.AllIntoOne<IPartialSelectableItem<object>, int>((item, count) =>
            {
                var subjectProgress = (SubjectProgress) item.Data;

                int val;

                switch (subjectProgress.State)
                {
                    case SelectableState.All:
                        val = subjectProgress.Total;
                        break;
                    case SelectableState.Partial:
                        val = subjectProgress.TopicProgresses?
                                  .Where(progress => progress.Selected)
                                  .Select(progress => progress.Total).Sum() ?? 0;
                        break;
                    case SelectableState.None:
                        val = 0;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }

                count += val;
                return count;
            });
        }


        public class ViewModel
        {
            public Discipline Discipline { get; set; }
            public Task<IEnumerable<SubjectProgress>> SubjectProgressTask { get; set; }
            public CancellationTokenSource TokenSource { get; set; }
            public Exam Exam { get; set; }
        }

        private class SubjectProgressToObjectModel:IPartialSelectableItem<object>
        {
            private readonly SubjectProgress _subjectProgress;
            public string Title
            {
                get => _subjectProgress.Title;
                set => _subjectProgress.Title = value;
            }
            public string SubTitle
            {
                get => _subjectProgress.SubTitle;
                set => _subjectProgress.SubTitle = value;
            }
            public object Data
            {
                get => _subjectProgress.Data;
                set => throw new NotImplementedException();
            }

            public SelectableState State
            {
                get => _subjectProgress.State;
                set => _subjectProgress.State = value;
            }

            public SubjectProgressToObjectModel(SubjectProgress subjectProgress)
            {
                _subjectProgress = subjectProgress;
            }
        }

        private class TopicProgressToObjectModel : ISelectableItem<object>
        {
            private readonly TopicProgress _topicProgress;
            public string Title
            {
                get => _topicProgress.Title;
                set => _topicProgress.Title = value;
            }
            public string SubTitle
            {
                get => _topicProgress.SubTitle;
                set => _topicProgress.SubTitle = value;
            }
            public object Data
            {
                get => _topicProgress;
                set => throw new NotImplementedException();
            }

            public bool Selected
            {
                get => _topicProgress.Selected;
                set => _topicProgress.Selected= value;
            }

            public TopicProgressToObjectModel(TopicProgress topicProgress)
            {
                _topicProgress = topicProgress;
            }
        }
    }
}