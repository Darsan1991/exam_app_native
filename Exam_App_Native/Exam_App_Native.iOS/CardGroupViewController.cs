﻿using Foundation;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CoreGraphics;
using Exam_App_Native.iOS.Adapter;
using Exam_App_Native.Model.ViewModel;
using ObjCRuntime;
using UIKit;
using System.Threading;
using Exam_App_Native.iOS.View;

namespace Exam_App_Native.iOS
{
    public partial class CardGroupViewController : UIViewController
    {
        public const float CARD_HEIGHT = 125;

        public event Action<ICard<object>> OnItemClicked; 

        public ViewModel MViewModel { get; set; }

        private Timer _searchBarTimer;
        private CardCollectionViewAdapter _collectionViewAdapter;
        private ActivityIndicator activityIndicator;
        private Task<object> _activeTask;
        private CancellationTokenSource _actuveTokenSource;

        public CardGroupViewController (IntPtr handle) : base (handle)
        {
        }

        public override async void ViewDidLoad()
        {
            base.ViewDidLoad();
            if (MViewModel.Cards == null)
            {
                throw new Exception("You have to set the view model Before call this function.");
            }

            if (NavigationItem == null)

            {
                throw new Exception("View Controller Must Inside the NavigationController.");
            }
            NavigationController.NavigationBar.TintColor = UIColor.White;
            NavigationItem.TitleView = titleView;
            titleLabel.Text = MViewModel.Title;
            if(MViewModel.SubTitle!=null)
            subTitleLabel.Text = MViewModel.SubTitle.Length < 25? MViewModel.SubTitle : MViewModel.SubTitle.Substring(0,24)+"...";
            subTitleLabel.Hidden = string.IsNullOrEmpty(MViewModel.SubTitle);



            var activityHolderView = new UIView()
            {
                TranslatesAutoresizingMaskIntoConstraints = false,
                BackgroundColor = UIColor.Clear,
                UserInteractionEnabled = false
            };

            View.AddSubview(activityHolderView);

            activityHolderView.LeftAnchor.ConstraintEqualTo(collectionView.LeftAnchor).Active = true;
            activityHolderView.RightAnchor.ConstraintEqualTo(collectionView.RightAnchor).Active = true;
            activityHolderView.TopAnchor.ConstraintEqualTo(collectionView.TopAnchor).Active = true;
            activityHolderView.BottomAnchor.ConstraintEqualTo(collectionView.BottomAnchor).Active = true;

            activityIndicator = new ActivityIndicator(activityHolderView, 1f) { Animating = true };


            collectionView.AddGestureRecognizer(new UITapGestureRecognizer((obj) =>
            {
                View.EndEditing(true);
            }){
                CancelsTouchesInView = false
            });
            
            collectionView.RegisterNibForCell(CardCollectionCell.Nib,nameof(CardCollectionCell));



            try
            {
                var cards = await MViewModel.Cards;
                _collectionViewAdapter = new CardCollectionViewAdapter(nameof(CardCollectionCell), cards);
                collectionView.Delegate = _collectionViewAdapter;
                collectionView.DataSource = _collectionViewAdapter;
                _collectionViewAdapter.OnItemClicked += CollectionViewAdapterOnItemClicked;
            }
            catch (Exception ex)
            {

            }
            activityIndicator.Animating = false;


            searchBar.Placeholder = NSBundle.MainBundle.LocalizedString(LocalizableWords.SEARCH, "Search").ToUpper();
            searchBar.TextChanged += (sender, args) =>
            {
                if (_searchBarTimer == null)
                {

                    _searchBarTimer = new System.Threading.Timer(
                        state => InvokeOnMainThread(action: () =>
                        {
                            args.SearchText = searchBar.Text;
                            MViewModel.OnSearchTextChanged?.Invoke(this, args);
                        })
                        , null
                        , 200
                        , Timeout.Infinite);


                }
                else
                {
                    _searchBarTimer.Change(200, Timeout.Infinite);
                }

            };
            searchBar.SearchButtonClicked += (sender, args) => MViewModel.OnSearchTextSubmit?.Invoke(this, args);

        }

        public override void ViewDidLayoutSubviews()
        {
            base.ViewDidLayoutSubviews();
            var colCount = 2;
            var arr = NSBundle.MainBundle.LoadNib("CardCollectionCell", null, null);
            var view = Runtime.GetNSObject<CardCollectionCell>(arr.ValueAt(0));

            
            var flowLayout = (UICollectionViewFlowLayout)collectionView.CollectionViewLayout;
            var cardWidth = CARD_HEIGHT * view.Frame.Width / view.Frame.Height ;
            
            flowLayout.ItemSize = new CGSize(cardWidth, CARD_HEIGHT);
            flowLayout.MinimumInteritemSpacing = (collectionView.Frame.Width - (colCount) * cardWidth)/(colCount-1+2);
            flowLayout.MinimumLineSpacing = flowLayout.MinimumInteritemSpacing;
            collectionView.ContentInset = new UIEdgeInsets(10, flowLayout.MinimumInteritemSpacing, 10, flowLayout.MinimumInteritemSpacing);

        }


        public void UpdateItems(IEnumerable<ICard<object>> items)
        {
            _collectionViewAdapter.Items = items;
            collectionView.ReloadData();
        }

        public async void Update(Task<object> task, CancellationTokenSource tokenSource)
        {
            if (!MViewModel.Cards?.IsCompleted ?? true)
            {
                tokenSource?.Cancel();
                return;
            }

            if (_activeTask != null && (!_activeTask.IsCompleted && !_activeTask.IsCanceled && !_activeTask.IsFaulted))
            {
                _actuveTokenSource?.Cancel();
            }

            _activeTask = task;
            _actuveTokenSource = tokenSource;

            activityIndicator.Animating = true;
            try
            {
                var obj = await task;
                MViewModel.OnUpdatated?.Invoke(this, obj);
            }
            catch (Exception e)
            {
                // ignored
            }

            activityIndicator.Animating = false;

        }


        private void CollectionViewAdapterOnItemClicked(ICard<object> card)
        {
            OnItemClicked?.Invoke(card);
        }

        public struct ViewModel
        {
            public EventHandler<UISearchBarTextChangedEventArgs> OnSearchTextChanged { get; set; }
            public EventHandler<EventArgs> OnSearchTextSubmit { get; set; }
            public EventHandler<object> OnUpdatated { get; set; }

            public Task<IEnumerable<ICard<object>>> Cards { get; set; }
            public string Title { get; set; }
            public string SubTitle { get; set; }
        }
    }
}