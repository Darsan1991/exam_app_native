﻿using System;
using CoreGraphics;
using Exam_App_Native.iOS.Adapter;
using Exam_App_Native.Model;
using Exam_App_Native.Model.ViewModel;
using Foundation;
using ObjCRuntime;
using UIKit;

namespace Exam_App_Native.iOS
{
    public partial class CardGroupViewCell : UITableViewCell, IHolder<object>
    {
        public event Action<CardGroupViewCell> OnDetailsClicked;
        public event Action<CardGroupViewCell, ICard<object>> OnChildItemClicked;

        public static readonly NSString Key = new NSString("CardGroupViewCell");
        public static readonly UINib Nib;

        public object Item
        {
            get => _item;
            set
            {
                if (!(value is IItemGroup<object, ICard<object>>))
                {
                    throw new Exception("The Object should be IItemGroup<object, ICard<object>>");
                }

                var groupItem = (IItemGroup<object, ICard<object>>) value;
                _collectionViewAdapter.Items = groupItem.Items;

                if (_viewLoaded)
                {
                    titleLabel.Text = groupItem.Title;
                    collectionView?.ReloadData();
                }

                _item = value;

            }
        }

        public bool InteractableDetails
        {
            get => _interactableDetails;
            set
            {
                if (_interactableDetails == value)
                    return;
                _interactableDetails = value;

                if (value)
                {
                    _collectionViewAdapter.OnItemClicked += CollectionViewAdapterOnItemClicked;
                }
                else
                {
                    _collectionViewAdapter.OnItemClicked -= CollectionViewAdapterOnItemClicked;
                }

                if (_viewLoaded)
                {
                    RefreshInteraction();
                }
            }
        }


        public IItemGroup<object, ICard<object>> GroupItem => (IItemGroup<object, ICard<object>>) Item;

        private bool _viewLoaded;
        private object _item;
        private readonly CardCollectionViewAdapter _collectionViewAdapter = new CardCollectionViewAdapter("CardCollectionViewCell");
        private bool _interactableDetails;


        static CardGroupViewCell()
        {
            Nib = UINib.FromName("CardGroupViewCell", NSBundle.MainBundle);
        }

        protected CardGroupViewCell(IntPtr handle) : base(handle)
        {

            InteractableDetails = true;

        }

        private void CollectionViewAdapterOnItemClicked(ICard<object> card)
        {
            OnChildItemClicked?.Invoke(this,card);
        }


        private void RefreshInteraction()
        {
            details.Enabled = InteractableDetails;
        }

        public override void AwakeFromNib()
        {
            base.AwakeFromNib();
            details.SetTitle(NSBundle.MainBundle.LocalizedString(LocalizableWords.DETAILS,"Details"),UIControlState.Normal);
            collectionView.RegisterNibForCell(CardCollectionCell.Nib, "CardCollectionViewCell");
            details.SetTitleColor(details.TitleColor(UIControlState.Normal),UIControlState.Disabled);
            collectionView.DataSource = _collectionViewAdapter;
            collectionView.Delegate = _collectionViewAdapter;
            collectionView.ContentInset = new UIEdgeInsets(0,15,0,15);
            titleLabel.Text = GroupItem?.Title;
            _viewLoaded = true;
            RefreshInteraction();
        }

        public override void LayoutSubviews()
        {
            base.LayoutSubviews();
            var arr = NSBundle.MainBundle.LoadNib("CardCollectionCell", null,null);
            var view = Runtime.GetNSObject<CardCollectionCell>(arr.ValueAt(0));
            var flowLayout = (UICollectionViewFlowLayout)collectionView.CollectionViewLayout;
            var cellHeight = collectionView.Frame.Height - 5f;
            flowLayout.ItemSize = new CGSize(cellHeight * view.Frame.Width/view.Frame.Height, cellHeight);
            flowLayout.MinimumInteritemSpacing = 20;
            flowLayout.MinimumLineSpacing = 20;
            Console.WriteLine("Height:"+Frame.Height + " Frame"+Frame);
           
        }

        public override void LayoutIfNeeded()
        {
            base.LayoutIfNeeded();
//            ((UICollectionViewFlowLayout)collectionView.CollectionViewLayout).ItemSize = new CGSize(Frame.Height - 40, Frame.Height);
        }

        partial void Details_TouchUpInside(UIButton sender)
        {
           OnDetailsClicked?.Invoke(this);
        }
    }
}
