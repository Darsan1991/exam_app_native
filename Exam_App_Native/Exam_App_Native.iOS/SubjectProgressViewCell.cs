﻿using Foundation;
using System;
using Exam_App_Native.Model;
using Exam_App_Native.Model.ViewModel;
using UIKit;

namespace Exam_App_Native.iOS
{
    public partial class SubjectProgressViewCell : UITableViewCell,IPartialSelectableHolder<object>
    {
        private SubjectProgress _item;
        public event EventHandler<SelectableState> OnSelectableStateChanged;


        public object Item
        {
            get => _item;
            set
            {
                _item = value as SubjectProgress;
                titleLabel.Text = _item?.Title;
                subtitleLabel.Text = $"{_item?.Resolved}/{_item?.Total}";

            }
        }

        public SelectableState State { get; set; }

        public SubjectProgress SubjectProgress => _item;

        public SubjectProgressViewCell (IntPtr handle) : base (handle)
        {
        }


    }
}