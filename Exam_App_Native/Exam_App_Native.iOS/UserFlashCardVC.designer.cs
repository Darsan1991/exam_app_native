// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace Exam_App_Native.iOS
{
    [Register ("UserFlashCardVC")]
    partial class UserFlashCardVC
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextView commentTextView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton disciplineBtn { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView falseRadioBtn { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView falseRadioBtnImg { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextView questionTextView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton subjectBtn { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton topicBtn { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView trueRadioBtn { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView trueRadioBtnImg { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (commentTextView != null) {
                commentTextView.Dispose ();
                commentTextView = null;
            }

            if (disciplineBtn != null) {
                disciplineBtn.Dispose ();
                disciplineBtn = null;
            }

            if (falseRadioBtn != null) {
                falseRadioBtn.Dispose ();
                falseRadioBtn = null;
            }

            if (falseRadioBtnImg != null) {
                falseRadioBtnImg.Dispose ();
                falseRadioBtnImg = null;
            }

            if (questionTextView != null) {
                questionTextView.Dispose ();
                questionTextView = null;
            }

            if (subjectBtn != null) {
                subjectBtn.Dispose ();
                subjectBtn = null;
            }

            if (topicBtn != null) {
                topicBtn.Dispose ();
                topicBtn = null;
            }

            if (trueRadioBtn != null) {
                trueRadioBtn.Dispose ();
                trueRadioBtn = null;
            }

            if (trueRadioBtnImg != null) {
                trueRadioBtnImg.Dispose ();
                trueRadioBtnImg = null;
            }
        }
    }
}