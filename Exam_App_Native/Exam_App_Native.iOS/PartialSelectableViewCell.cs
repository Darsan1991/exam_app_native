﻿using Foundation;
using System;
using System.IO;
using Exam_App_Native.Model;
using Exam_App_Native.Model.ViewModel;
using UIKit;
using Exam_App_Native.iOS.View;

namespace Exam_App_Native.iOS
{
    public partial class PartialSelectableViewCell : UITableViewCell, IPartialSelectableHolder<object>
    {
        public event EventHandler<SelectableState> OnSelectableStateChanged;
        public event EventHandler OnClickNextArrow;
        private SelectableState _state = SelectableState.None;

        public object Item
        {
            get => _item;
            set
            {
                
                _item = value as IPartialSelectableItem<object> ?? 
                        throw new InvalidDataException();
                if (IsViewLoaded)
                {
                    titleLabel.Text = _item.Title;
                    subtitleLabel.Text = _item.SubTitle;
                    State= _item.State;
                    
                }
            }
        }

        public SelectableState State
        {
            get => _state;
            set
            {

                if (IsViewLoaded)
                {
                    _partialToggle.State = value;
                }

                if (_item!=null && _item.State!=value)
                {
                    
                    _item.State = value;
                }

                _state = value;
                OnSelectableStateChanged?.Invoke(this,State);
            }
        }

        private PartialToggleSwitchView _partialToggle;
        private IPartialSelectableItem<object> _item;
        private bool IsViewLoaded { get; set; }

        public PartialSelectableViewCell(IntPtr handle) : base(handle)
        {
        }

        public override void AwakeFromNib()
        {
            base.AwakeFromNib();

            _partialToggle = new PartialToggleSwitchView(toggleImgView) {State = SelectableState.None};

            _partialToggle.ValueChanged += (sender, args) =>
            {
                State = _partialToggle.State;
            };
            IsViewLoaded = true;

            ContentView.AddGestureRecognizer(new UITapGestureRecognizer(()=>{
                State = State == SelectableState.None ? SelectableState.All : SelectableState.None;
            }));

            rightArrowView.UserInteractionEnabled = true;
            rightArrowView.AddGestureRecognizer(new UITapGestureRecognizer(() => {
                OnClickNextArrow?.Invoke(this,EventArgs.Empty);
            }));

            //Find Some better way
            if(Item!=null)
            Item = Item;
        }

        private class PartialToggleButton
        {
            private readonly UISwitch _swi;
            private SelectableState _state;

            public event EventHandler ValueChanged
            {
                add => _swi.ValueChanged += value;
                remove => _swi.ValueChanged -= value;
            }

            public bool On
            {
                get => _swi.On;
                set => _swi.On = value;
            }

            public SelectableState State
            {
                get => _state;
                set
                {
                    _swi.OnTintColor = value == SelectableState.Partial
                        ? new UIColor(78 / 255f, 185 / 255f, 222 / 255f, 1f)
                        : new UIColor(57f / 255, 139f / 255, 203f / 255, 1f);

                    if (_swi.On && value == SelectableState.None)
                    {
                        _swi.On = false;
                    }
                    else if (!_swi.On && value != SelectableState.None)
                    {
                        _swi.On = true;
                    }

                    _state = value;
                }
            }

            public PartialToggleButton(UISwitch swi)
            {
                _swi = swi;
            }
        }
    }
}