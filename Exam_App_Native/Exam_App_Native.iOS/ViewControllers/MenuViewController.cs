﻿using Foundation;
using System;
using System.Collections.Generic;
using CoreGraphics;
using Exam_App_Native.iOS.Model;
using Exam_App_Native.iOS.ViewControllers;
using Exam_App_Native.Model;
using UIKit;

namespace Exam_App_Native.iOS
{
    public partial class MenuViewController : UITableViewController
    {
        public event Action<IMenuItem> OnItemClicked;

        private readonly List<MenuTableViewCell.ViewModel> _menuViewModels = new List<MenuTableViewCell.ViewModel>
        {
            new MenuTableViewCell.ViewModel
            {
                MenuItem = new MenuItem {MenuType = MenuType.ACCOUNT},
                Name = NSBundle.MainBundle.LocalizedString(LocalizableWords.ACCOUNT,"Account").ToUpper(),
                Icon = UIImage.FromBundle("account_icon")
            },
            new MenuTableViewCell.ViewModel
            {
                Name = NSBundle.MainBundle.LocalizedString(LocalizableWords.TEACHERS,"Teachers").ToUpper(),
                MenuItem = new MenuItem {MenuType = MenuType.TEACHERS},
                Icon = UIImage.FromBundle("teachers_icon")
            },
            new MenuTableViewCell.ViewModel
            {
                MenuItem = new MenuItem {MenuType = MenuType.PLANS},
                Name = NSBundle.MainBundle.LocalizedString(LocalizableWords.PLANS,"Plans").ToUpper(),
                Icon = UIImage.FromBundle("plans_icon")
            },
            new MenuTableViewCell.ViewModel
            {
                MenuItem = new MenuItem {MenuType = MenuType.LOGOUT},
                Name = NSBundle.MainBundle.LocalizedString(LocalizableWords.LOGOUT,"Logout").ToUpper(),
                Icon = UIImage.FromBundle("signout")
            },
        };

        private UILabel _emailLabel;
        private UILabel _nameLabel;

        public MenuViewController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            TableView.ContentInset = new UIEdgeInsets(-20,0,0,0);
            TableView.TableFooterView = new UIView();
            var topView = TableView.TableHeaderView = new UIView(new CGRect(0,-20,TableView.Frame.Width,85))
            {
                BackgroundColor = UIColor.White
            };

            _emailLabel = new UILabel
            {
                TranslatesAutoresizingMaskIntoConstraints = false,
                TextColor = UIColor.Gray,
                Font = UIFont.SystemFontOfSize(10)
            };

            _nameLabel = new UILabel
            {
                TranslatesAutoresizingMaskIntoConstraints = false,
                TextColor = new UIColor(57f/255,139f/255,202f/255,1f),
                Font = UIFont.SystemFontOfSize(16)
            };
            topView.AddSubview(_nameLabel);
            topView.AddSubview(_emailLabel);

            _emailLabel.LeftAnchor.ConstraintEqualTo(topView.LeftAnchor, 7).Active=true;
            _emailLabel.BottomAnchor.ConstraintEqualTo(topView.BottomAnchor, -8).Active=true;

            _nameLabel.BottomAnchor.ConstraintEqualTo(_emailLabel.TopAnchor, -2).Active = true;
            _nameLabel.LeftAnchor.ConstraintEqualTo(topView.LeftAnchor, 7).Active = true;

            topView.Layer.ShadowOffset = new CGSize(0,3);
            topView.Layer.ShadowRadius = 3;
            topView.Layer.ShadowColor = UIColor.Gray.CGColor;
            topView.Layer.ShadowOpacity = 0.2f;

            var user = Auth.Instance.CurrentUser;
            user.OnUpdateData += CurrentUserOnUpdateData;
            
            CurrentUserOnUpdateData(user);
        }

        protected override void Dispose(bool disposing)
        {
            Auth.Instance.CurrentUser.OnUpdateData -= CurrentUserOnUpdateData;
            base.Dispose(disposing);

        }

        private void CurrentUserOnUpdateData(User user)
        {
            _emailLabel.Text = user.Email;
            _nameLabel.Text = user.Name;
        }

        public override void ViewDidUnload()
        {
            base.ViewDidUnload();
            Auth.Instance.CurrentUser.OnUpdateData -= CurrentUserOnUpdateData;
        }

        public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
        {
//            base.RowSelected(tableView, indexPath);
            OnItemClicked?.Invoke(_menuViewModels[indexPath.Row].MenuItem);
            tableView.CellAt(indexPath).Selected = false;
        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            var menuTableViewCell = (MenuTableViewCell) tableView.DequeueReusableCell("MenuTableViewCell");
            menuTableViewCell.viewModel = _menuViewModels[indexPath.Row];
            return menuTableViewCell;
        }

        public override nint RowsInSection(UITableView tableView, nint section)
        {
            switch (section)
            {
                case 0:
                    return _menuViewModels.Count;

                default:
                    return 0;
            }
        }
    }
}