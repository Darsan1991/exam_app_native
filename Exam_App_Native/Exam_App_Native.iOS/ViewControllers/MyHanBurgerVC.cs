﻿using Foundation;
using System;
using Exam_App_Native.iOS.Model;
using Exam_App_Native.iOS.ViewControllers;
using UIKit;

namespace Exam_App_Native.iOS
{
    public partial class MyHanBurgerVC : HandBurgerViewController<IMenuItem>
    {

        public static MyHanBurgerVC Instance { get; private set; }

        public MyHanBurgerVC (IntPtr handle) : base (handle)
        {
        }

        protected override IHandBurgerMenu<IMenuItem> HandBurgerMenu => menuContainer;
        protected override IHandBurgerContainer HandBurgerContainer => containerView;
        protected override void HandBurgerMenuOnOnItemClicked(IMenuItem menuItem)
        {
            switch(menuItem.MenuType)
            {
                case MenuType.ACCOUNT:
                    PresentControllerInsideNavigationController(CreateController("AccountTableViewController"));
                    break;

                case MenuType.PLANS:
                    PresentControllerInsideNavigationController(CreateController("SubscriptionPlanVC"));
                    break;
                case MenuType.TEACHERS:
                    PresentControllerInsideNavigationController(CreateController("TeachersViewController"));
                    break;

                case MenuType.LOGOUT:
                    IosExamApp.LogOut();
                    var storyboard = UIStoryboard.FromName("Main", null);
                    var controller = storyboard.InstantiateViewController("LogInViewController");
                    PresentViewController(controller, true, null);
                    break;
            }
            
            Close();
        }

        private UIViewController CreateController(string identifier)
        {
            var storyBoard = UIStoryboard.FromName("Main",null);
            return storyBoard.InstantiateViewController(identifier);
        }

        private void PresentControllerInsideNavigationController(UIViewController controller)
        {
            
            var navController = new UINavigationController(controller);
            navController.NavigationBar.BarTintColor = new UIColor(50f / 255, 116f / 255, 186f / 255, 1f);
            navController.NavigationBar.TintColor = UIColor.White;
            navController.NavigationBar.TitleTextAttributes = new UIStringAttributes
            {
                ForegroundColor = UIColor.White
            };
            var backBtn = new UIBarButtonItem
            {
                Title = NSBundle.MainBundle.LocalizedString(LocalizableWords.HOME,"Home")
            };
            backBtn.Clicked += (object sender, EventArgs e) => {
                navController.DismissViewController(true,null);
            };
            controller.NavigationItem.LeftBarButtonItem = backBtn;
            PresentViewController(navController,true,null);
        }



        public override void ViewDidLoad()
        {
            Instance = this;
            base.ViewDidLoad();
            
            
        }
    }
}