// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace Exam_App_Native.iOS
{
    [Register ("SubscriptionPlanVC")]
    partial class SubscriptionPlanVC
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        Exam_App_Native.iOS.ShadowButton buyNowBtn { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITableView tableView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.NSLayoutConstraint tableViewHeightConstraint { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (buyNowBtn != null) {
                buyNowBtn.Dispose ();
                buyNowBtn = null;
            }

            if (tableView != null) {
                tableView.Dispose ();
                tableView = null;
            }

            if (tableViewHeightConstraint != null) {
                tableViewHeightConstraint.Dispose ();
                tableViewHeightConstraint = null;
            }
        }
    }
}