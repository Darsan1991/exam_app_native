﻿using System;
using System.Linq;
using CoreGraphics;
using UIKit;

namespace Exam_App_Native.iOS.ViewControllers
{
    public abstract class HandBurgerViewController<T> : UIViewController
    {
        protected HandBurgerViewController(IntPtr handle) : base(handle)
        {
        }

        protected abstract IHandBurgerMenu<T> HandBurgerMenu { get; }
        protected abstract IHandBurgerContainer HandBurgerContainer { get; }

        public bool IsOpen { get; protected set; }

        public virtual CGRect ContainerFrame { get; } = UIApplication.SharedApplication.Windows.First().Frame;
        public virtual nfloat MenuWidth { get; } = UIApplication.SharedApplication.Windows.First().Frame.Width * 0.75f;
        public bool Animating { get; private set; }

        private readonly UIView coverView = new UIView()
        {
            UserInteractionEnabled = true,
            BackgroundColor = new UIColor(0, 0, 0, 0.2f)
        };

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            HandBurgerMenu.OnItemClicked += HandBurgerMenuOnOnItemClicked;

            HandBurgerMenu.View.Frame = new CGRect(-MenuWidth, 0, MenuWidth, View.Frame.Height);
            HandBurgerContainer.View.Frame = ContainerFrame;
            coverView.AddGestureRecognizer(new UITapGestureRecognizer(Close));
        }


        protected abstract void HandBurgerMenuOnOnItemClicked(T menuItem);


        public void Open()
        {
            if (IsOpen || Animating)
            {
                return;
            }
            Animating = true;


            HandBurgerContainer.View.AddSubview(coverView);
            coverView.Frame = HandBurgerContainer.View.Frame;
            coverView.BackgroundColor = UIColor.Clear;

            UIView.Animate(0.3f, () =>
            {
                HandBurgerMenu.View.Frame = new CGRect(0, 0, MenuWidth, View.Frame.Height);
                HandBurgerContainer.View.Frame = new CGRect(ContainerFrame.X + MenuWidth, ContainerFrame.Y,
                    ContainerFrame.Width, ContainerFrame.Height);
                coverView.BackgroundColor = new UIColor(0,0,0,0.3f);
            }, () => Animating = false);
            IsOpen = true;
        }

        public void Close()
        {
            if (!IsOpen || Animating)
            {
                return;
            }

            UIView.Animate(0.3f, () =>
            {
                coverView.BackgroundColor = UIColor.Clear;
                Animating = true;
                HandBurgerMenu.View.Frame = new CGRect(-MenuWidth, 0, MenuWidth, View.Frame.Height);
                HandBurgerContainer.View.Frame = ContainerFrame;
            }, completion: () =>
            {
                coverView.RemoveFromSuperview();
                Animating = false;
            });

            IsOpen = false;
        }

        public void Toggle()
        {
            if (IsOpen)
            {
                Close();
            }
            else
            {
                Open();
            }
        }
    }
}