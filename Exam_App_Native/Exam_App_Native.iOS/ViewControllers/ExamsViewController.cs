﻿using Foundation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using CoreGraphics;
using Exam_App_Native.iOS.Adapter;
using Exam_App_Native.iOS.View;
using Exam_App_Native.Model;
using Exam_App_Native.Model.ViewModel;
using UIKit;

// ReSharper disable once CheckNamespace
namespace Exam_App_Native.iOS
{
    public partial class ExamsViewController : UIViewController
    {
        private ActivityIndicator _searchBarActivityIndicator;
        private ActivityIndicator _activityIndicator;
        private CancellationTokenSource _tokenSource;
        private CardGroupTableViewAdapter _tableViewAdapter;
        private Timer _searchBarTimer;

        public ExamsViewController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {
            Title = NSBundle.MainBundle.LocalizedString(LocalizableWords.EXAM_CONTROLLER_TITLE,"Exams");
            _activityIndicator = new ActivityIndicator(View,1f,bgColor:View.BackgroundColor);
            _searchBarActivityIndicator = new ActivityIndicator(tableView,1f,bgColor: View.BackgroundColor);

            tableView.AddGestureRecognizer(new UITapGestureRecognizer((obj) => {
                View.EndEditing(true);
            }){CancelsTouchesInView = false});
            tableView.ContentInset = new UIEdgeInsets(10,0,0,0);
            tableView.RegisterNibForCellReuse(CardGroupViewCell.Nib, "CardGroupCell");
            _tableViewAdapter = new CardGroupTableViewAdapter("CardGroupCell");
            
            _tableViewAdapter.OnDequeueReuseCell += CardGroupTableViewOnDequeueReuseCell;

            searchBar.Placeholder = NSBundle.MainBundle.LocalizedString(LocalizableWords.SEARCH,"Search").ToUpper();
            searchBar.TextChanged += (sender, args) =>
            {
                if (_searchBarTimer == null)
                {

                    _searchBarTimer = new System.Threading.Timer(
                        state => InvokeOnMainThread(() => LoadOrUpdate(searchBar.Text, true))
                        , null
                        , 200
                        , Timeout.Infinite);


                }
                else
                {
                    _searchBarTimer.Change(200, Timeout.Infinite);
                }

            };

            tableView.DataSource = _tableViewAdapter;
            tableView.Delegate = _tableViewAdapter;
            tableView.ReloadData();
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);
            View.LayoutIfNeeded();
            LoadOrUpdate(searchLoad:false);
        }

        public override void ViewDidDisappear(bool animated)
        {
            base.ViewDidDisappear(animated);
            CancleLoadOrUpdate();
        }

        public override void TouchesEnded(NSSet touches, UIEvent evt)
        {
            base.TouchesEnded(touches, evt);
            View.EndEditing(true);
        }

        public async void LoadOrUpdate(string filter =null,bool searchLoad = true)
        {
            Console.WriteLine("Load Or Update:"+filter);
            if(_activityIndicator.Animating || _searchBarActivityIndicator.Animating)
            _tokenSource?.Cancel();
            var activityIndicator = searchLoad?_searchBarActivityIndicator:_activityIndicator;
            activityIndicator.Animating = true;
            _tokenSource = new CancellationTokenSource();
            try
            {
                var examGroups = await (string.IsNullOrEmpty(filter) || filter.Length<3?
                    ExamApp.Instance.GetExamGroups(_tokenSource) : ExamApp.Instance.SearchExamGroups(filter,_tokenSource));
                _tableViewAdapter.Items = examGroups.Select(group => new ItemGroup<object, ICard<object>>
                {
                    Data = group,
                    Items = group.Exams.Select(exam => new Card<object>
                    {
                        Title = exam.JobTitle,
                        SubTitle = exam.Institution,
                        Footer = $"{exam.Company} - {exam.Year}",
                        Data = exam
                    }),
                    Title = group.Name
                });
                tableView.ReloadData();
            }
            catch (Exception e)
            {

            }

            activityIndicator.Animating = false;
        }

        public void CancleLoadOrUpdate()
        {
            _tokenSource.Cancel();
        }

        private void CardGroupTableViewOnDequeueReuseCell(CardGroupViewCell cardGroupViewCell)
        {
            cardGroupViewCell.OnChildItemClicked -= CardGroupViewCellOnChildItemClicked;
            cardGroupViewCell.OnDetailsClicked -= CardGroupViewCellOnDetailsClicked;
            cardGroupViewCell.OnChildItemClicked += CardGroupViewCellOnChildItemClicked;
            cardGroupViewCell.OnDetailsClicked +=CardGroupViewCellOnDetailsClicked;
        }

        private void CardGroupViewCellOnDetailsClicked(CardGroupViewCell cardGroupViewCell)
        {
            var examGroup = (ExamGroup)cardGroupViewCell.GroupItem.Data;
            // ReSharper disable once ConvertToLocalFunction
            Func<string, CancellationTokenSource, Task<IEnumerable<ICard<object>>>> cardsTaskCreator =
                (query, tokenSource) =>
                {
                    tokenSource = tokenSource ?? new CancellationTokenSource();
                    return Task.Run(async () =>
                    {

                    var exams = await (string.IsNullOrEmpty(query) || query.Length < 3 ? ExamApp.Instance
                                       .GetExamsFromGroup((examGroup).Id)
                                : ExamApp.Instance.SearchExams(query, null));

                    return exams.Select(e => (ICard<object>)new Card<object>
                        {
                            Title = e.JobTitle,
                            SubTitle = e.Institution,
                            Footer = $"{e.Company} - {e.Year}",
                            Data = e
                        });

                    }, tokenSource.Token);
                };


            PresentCardGroupViewController(new CardGroupViewController.ViewModel
            {
                Title = cardGroupViewCell.GroupItem.Title,
                Cards = cardsTaskCreator("",new CancellationTokenSource()),
               OnSearchTextChanged = (sender,args)=>{
                    var tokenSource = new CancellationTokenSource();
                    ((CardGroupViewController)sender).Update(
                        Task.Run(async () => (await cardsTaskCreator(args.SearchText, tokenSource)) as object,
                            tokenSource.Token), tokenSource);
                },
                OnUpdatated = (sender,objects)=>{
                    ((CardGroupViewController)sender).UpdateItems((IEnumerable<ICard<object>>)objects);
                }
        }, OpenDisciplineSelection);
        }

        private void PresentCardGroupViewController(CardGroupViewController.ViewModel viewModel,Action<ICard<object>> onItemSelected)
        {
            var storyboard = UIStoryboard.FromName("Main", null);
            var controller = (CardGroupViewController)storyboard.InstantiateViewController("CardGroupViewController");
            controller.MViewModel = viewModel;
           
            if (onItemSelected != null)
            {
                controller.OnItemClicked += onItemSelected;
            }


            NavigationController.PushViewController(controller, true);
        }


        private void CardGroupViewCellOnChildItemClicked(CardGroupViewCell cardGroupViewCell, ICard<object> card)
        {
            OpenDisciplineSelection(card);

        }

        private void OpenDisciplineSelection(ICard<object> card)
        {

            var exam = (Exam)card.Data;
            // ReSharper disable once ConvertToLocalFunction
            Func<string, CancellationTokenSource, Task<IEnumerable<ICard<object>>>> cardsTaskCreator =
                (query, tokenSource) =>
                {
                    tokenSource = tokenSource ?? new CancellationTokenSource();
                    return Task.Run(async () =>
                    {

                    var disciplines = await (string.IsNullOrEmpty(query) || query.Length < 3
                           ? Task.FromResult(exam.Disciplines)
                           : ExamApp.Instance.SearchDisciplines(query, tokenSource));
                    return disciplines.Select(discipline => (ICard<object>)new Card<object>
                        {
                            Data = discipline,
                            Title = discipline.Name,
                            Footer = $"{discipline.CardCount} cards"
                        });

                    }, tokenSource.Token);
                };

            PresentCardGroupViewController(new CardGroupViewController.ViewModel
            {
                Title = "DISCIPLINES",
                SubTitle = card.Title,
                Cards = Task.FromResult(((Exam)card.Data).Disciplines.Select(discipline => (ICard<object>)new Card<object>
                {
                    Title = discipline.Name,
                    Data = discipline,
                    Footer = $"{discipline.CardCount} cards"
                })),
                OnSearchTextChanged = (sender,args)=>{
                    var tokenSource = new CancellationTokenSource();
                    ((CardGroupViewController)sender).Update(
                        Task.Run(async () => await cardsTaskCreator(args.SearchText, tokenSource) as object,
                            tokenSource.Token), tokenSource);
                },
                OnUpdatated = (sender,args)=>{
                    ((CardGroupViewController)sender).UpdateItems((IEnumerable<ICard<object>>)args);
                }
            }, c =>
            {
                var storyboard = UIStoryboard.FromName("Main", null);
                var controller = (ReviewCreatorVC)storyboard.InstantiateViewController("ReviewCreatorVC");

                //Set Task To Controller
                controller.MViewModel = new ReviewCreatorVC.ViewModel
                {
                    Discipline = (Discipline)c.Data,
                    SubjectProgressTask = ExamApp.Instance.GetSubjectProgressesForDiciplines(((Discipline)c.Data).Id)
                };

                NavigationController.PushViewController(controller, true);
            });
        }

        partial void HandBurgerBtn_Activated(UIBarButtonItem sender)
        {
            MyHanBurgerVC.Instance?.Toggle();
        }
    }
}