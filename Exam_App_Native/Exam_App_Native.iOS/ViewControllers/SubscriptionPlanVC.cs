﻿using Foundation;
using System;
using System.Collections.Generic;
using System.Linq;
using Exam_App_Native.iOS.Adapter;
using Exam_App_Native.iOS.View;
using UIKit;

namespace Exam_App_Native.iOS
{
    
    public partial class SubscriptionPlanVC : UIViewController,IUITableViewDataSource
    {
        private readonly List<SubscriptionViewCell.ViewModel> SUBSCRIPTION_CELL_MODELS = new List<SubscriptionViewCell.ViewModel>
        {
            new SubscriptionViewCell.ViewModel
            {
                LeftTitle = "20",
                RightTitle = NSBundle.MainBundle.LocalizedString(LocalizableWords.UNLIMITED,"Unlimited").ToUpper(),
                Content = NSBundle.MainBundle.LocalizedString(LocalizableWords.NO_OF_FLASH_CARDS_IN_EACH_DISCIPLINE,"Unlimited")
            },
            new SubscriptionViewCell.ViewModel
            {
                LeftTitle = "1",
                RightTitle = NSBundle.MainBundle.LocalizedString(LocalizableWords.UNLIMITED,"Unlimited").ToUpper(),
                Content = NSBundle.MainBundle.LocalizedString(LocalizableWords.NO_OF_EXAM,"Number of Exams")
            },
            new SubscriptionViewCell.ViewModel
            {
                LeftTitle = "100",
                RightTitle = NSBundle.MainBundle.LocalizedString(LocalizableWords.UNLIMITED,"Unlimited").ToUpper(),
                Content = NSBundle.MainBundle.LocalizedString(LocalizableWords.NO_OF_FLASH_CARDS_IN_EACH_EXAM,"Number of Exams")
            },
            new SubscriptionViewCell.ViewModel
            {
                LeftTitle = NSBundle.MainBundle.LocalizedString(LocalizableWords.UNLIMITED,"FREE").ToUpper(),
                RightTitle = "$19.99",
                Content = NSBundle.MainBundle.LocalizedString(LocalizableWords.PRICE,"Price")
            }
        };

        private ActivityIndicator _activityIndicator;

        public SubscriptionPlanVC (IntPtr handle) : base (handle)
        {
        }

        public override async void ViewDidLoad()
        {
            base.ViewDidLoad();
            _activityIndicator = new ActivityIndicator(View,1);
            Title = NSBundle.MainBundle.LocalizedString(LocalizableWords.SUBSCRIPTION_CONTROLLER_TITLE, "Subscription");
            //buyNowBtn.SetImage(UIImage.FromBundle("buy_btn"),UIControlState.Normal);
            buyNowBtn.ImageView.ContentMode = UIViewContentMode.ScaleAspectFit;
            buyNowBtn.ImageEdgeInsets = new UIEdgeInsets(8, 5, 8, 5);
            buyNowBtn.TouchUpInside += (sender, args) =>
            {
                using (var storyboard = UIStoryboard.FromName("Main",null))
                {
                    var controller = (PaymentViewController)storyboard.InstantiateViewController("PaymentViewController");
                    NavigationController.PushViewController(controller,true);
                }
            };

            _activityIndicator.Animating = true;
            try
            {
                var plan = await ExamApp.Instance.GetMonthlyPaymentPlanPrice();
                var models = SUBSCRIPTION_CELL_MODELS.ToList();
                var viewModel = models[3];
                viewModel.RightTitle = plan;
                models[3] = viewModel;
                tableView.DataSource = new SimpleTableViewAdapter<SubscriptionViewCell.ViewModel,SubscriptionViewCell>("SubscriptionViewCell",models);
                tableView.ReloadData();
            }
            catch (Exception e)
            {
                IosExamApp.ShowToastAlert("Error!", "Something went wrong!",this, () =>
                {
                    var ctrl = (UIViewController)NavigationController ?? this;
                    ctrl.DismissViewController(true,null);
                });
                
                return;
            }

            _activityIndicator.Animating = false;
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);
            //NavigationItem.BackBarButtonItem = new UIBarButtonItem
            //{
            //    Title = "Back"
            //};
        }

        public override void ViewDidLayoutSubviews()
        {
            base.ViewDidLayoutSubviews();
            tableViewHeightConstraint.Constant = tableView.ContentSize.Height;
        }

        public nint RowsInSection(UITableView tableView, nint section)
        {
            return SUBSCRIPTION_CELL_MODELS.Count;
        }

        public UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            var viewCell = (SubscriptionViewCell)tableView.DequeueReusableCell("SubscriptionViewCell");
            viewCell.viewModel = SUBSCRIPTION_CELL_MODELS[indexPath.Row];
            return viewCell;
        }
    }
}