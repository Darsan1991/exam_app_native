﻿using System;
using Foundation;
using UIKit;

namespace Exam_App_Native.iOS.ViewControllers
{
    public partial class MainTabBarViewController : UITabBarController
    {

        public static MainTabBarViewController Instance { get; private set; }

        public MainTabBarViewController(IntPtr pointer) : base(pointer)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            Instance = this;
            TabBar.SetValueForKey(new UIColor(194.0f / 255, 200.0f / 255, 207.0f / 255, 0.7f),new NSString("unselectedItemTintColor"));
            TabBar.SelectedImageTintColor = new UIColor(154f / 255, 163f / 255, 174f / 255, 1f);
        }
    }

    public partial class MainTabBarViewController
    {
        public void SetCurrentTab(TabButton btn, bool animate = true)
        {
            var i = GetPostionForTabButton(btn);
            SelectedIndex = i;
        }

        public void SetCurrentTab(int position, bool animate = true)
        {
            SelectedIndex = position;
        }

        public UIViewController GetTab(TabButton btn) => ViewControllers[GetPostionForTabButton(btn)];
        public UIViewController GetTab(int position) => ViewControllers[position];

        public int GetPostionForTabButton(TabButton btn)
        {
            switch (btn)
            {
                case TabButton.Exam:
                    return 0;
                case TabButton.Diciline:
                    return 1;
                case TabButton.MyCard:
                    return 2;
                case TabButton.Reviews:
                    return 3;
                case TabButton.Statics:
                    return 4;
                default:
                    throw new ArgumentOutOfRangeException(nameof(btn), btn, null);
            }
        }

        public enum TabButton
        {
            Exam, Diciline, MyCard, Reviews, Statics
        }
    }

    public interface IToggleMenu
    {
        event Action<IToggleMenu> OnToggleMenu;
    }

    public interface IControllerContainer
    {
        UIViewController containViewController { get; set; }
    }

    public interface IView
    {
        UIView View { get; }
    }

    public interface IHandBurgerContainer:IControllerContainer,IView
    {

    }

    public interface IHandBurgerMenu<out T>:IView
    {
        event Action<T> OnItemClicked;
    }
}