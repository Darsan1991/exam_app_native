﻿using Foundation;
using System;
using System.Collections.Generic;
using System.Linq;
using Exam_App_Native.iOS.Adapter;
using Exam_App_Native.Model;
using UIKit;

namespace Exam_App_Native.iOS
{
    public partial class DisciplinePerformancePageVC : UIPageViewController
    {
        private ViewModel _mViewModel;

        public bool ShowTrendLine
        {
            get => _showTrendLine;
            set
            {
                _disciplinesPerformanceVcs.ForEach(vc => vc.ShowTrendLine = value);
                _showTrendLine = value;
            }
        }

        public ViewModel MViewModel
        {
            get => _mViewModel;
            set
            {
                _disciplinesPerformanceVcs.Clear();
                _mViewModel = value;

                if (value == null)
                    return;

                using (var storyboard = UIStoryboard.FromName("Main", null))
                {
                    var controllers = _mViewModel.DisciplineTimeSeries.Select(series =>
                    {
                        // ReSharper disable once AccessToDisposedClosure
                        var controller =
                            (DisciplinesPerformanceVC) storyboard.InstantiateViewController("DisciplinesPerformanceVC");
                        controller.ShowTrendLine = ShowTrendLine;
                        controller.MViewModel = new DisciplinesPerformanceVC.ViewModel
                        {
                            KnownCardValues = series.KnownCards.ToDictionary(v =>
                                DisciplineTimeSeries.TimeAndValue.GetMonth(v.Time), v => v.Value),
                            TrendLineValues =
                                series.TrendSeries.ToDictionary(v => DisciplineTimeSeries.TimeAndValue.GetMonth(v.Time),
                                    v => v.Value),
                            Title = series.DisciplineName
                        };
                        return controller;
                    });
                    _disciplinesPerformanceVcs.AddRange(controllers);
                }

                if (IsViewLoaded)
                {
                    DataSource = null;
                    _dataSource = new SimplePageViewControllerDataSource
                    {
                        Controllers = _disciplinesPerformanceVcs
                    };
                    DataSource = _dataSource;
                    if (_disciplinesPerformanceVcs.Count > 0)
                        SetViewControllers(new UIViewController[] { _disciplinesPerformanceVcs.First() }, UIPageViewControllerNavigationDirection.Forward, false, null);
                  
                    
                }
            }
        }

        private readonly List<DisciplinesPerformanceVC> _disciplinesPerformanceVcs =
            new List<DisciplinesPerformanceVC>();

        private SimplePageViewControllerDataSource _dataSource;

        private bool _showTrendLine;

        public DisciplinePerformancePageVC(IntPtr handle) : base(handle)
        {

        }


        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            if (_disciplinesPerformanceVcs.Count <= 0)
            {
                var storyboard = UIStoryboard.FromName("Main", null);
                var controller =
                    (DisciplinesPerformanceVC)storyboard.InstantiateViewController("DisciplinesPerformanceVC");
                _disciplinesPerformanceVcs.Add(controller);
            }

            if (_disciplinesPerformanceVcs.Count > 0)
            {
                DataSource = new SimplePageViewControllerDataSource {Controllers = _disciplinesPerformanceVcs};
                SetViewControllers(new UIViewController[] { _disciplinesPerformanceVcs.First() }, UIPageViewControllerNavigationDirection.Forward, false, null);
            }
        }

        public class ViewModel
        {
            public IEnumerable<DisciplineTimeSeries> DisciplineTimeSeries { get; set; }
        }
    }

}