﻿using Foundation;
using System;
using System.Collections.Generic;
using System.Linq;
using Exam_App_Native.iOS.View;
using Exam_App_Native.Model;
using UIKit;

namespace Exam_App_Native.iOS
{
    public partial class AccountTableViewController : UITableViewController
    {
        private static readonly UIColor HEADER_TEXT_COLOR =
            new UIColor((nfloat) (84 / 255f), (nfloat) (95 / 255f), (nfloat) (106 / 255f), 1);

        private static readonly NSIndexPath SUBSCRIBED_INDEX_PATH = NSIndexPath.FromRowSection(1,2);
        private static readonly NSIndexPath NOT_SUBSCRIBED_INDEX_PATH = NSIndexPath.FromRowSection(0,2);


        private bool Subscribed
        {
            get => _subscribed;
            set
            {
                RemoveIndexFromHiddenPath(value?SUBSCRIBED_INDEX_PATH:NOT_SUBSCRIBED_INDEX_PATH);
                AddIndexToHiddenPath(value?NOT_SUBSCRIBED_INDEX_PATH:SUBSCRIBED_INDEX_PATH);
                _subscribed = value;
                TableView.ReloadData();
            }
        }

        private readonly List<NSIndexPath> _hiddenIndexPaths = new List<NSIndexPath>();
        private  bool _subscribed;
        private readonly Auth _auth = Auth.Instance;
        private ActivityIndicator _activityIndicator;
        private ToggleSwitchView _mixCardsToggle;
        private ToggleSwitchView _skipToggle;

        public AccountTableViewController (IntPtr handle) : base (handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            if(NavigationController==null)
                throw new InvalidOperationException("This View Controller Should push inside navigation controller");

            Title = NSBundle.MainBundle.LocalizedString(LocalizableWords.ACCOUNT_CONTROLLER_TITLE, "Account");

            var buttonItem = new UIBarButtonItem
            {
                Title = NSBundle.MainBundle.LocalizedString(LocalizableWords.LOGOUT,"LogOut"),
            };
            buttonItem.Clicked += (sender, args) =>
            {
                IosExamApp.LogOut();
                //                DismissViewController(false, null);
                var storyboard = UIStoryboard.FromName("Main",null);
                var controller = storyboard.InstantiateViewController("LogInViewController");

                PresentViewController(controller,true,null);

            };

            NavigationItem.RightBarButtonItem = buttonItem;

            _mixCardsToggle = new ToggleSwitchView(mixUserCardsImageView);
            _skipToggle = new ToggleSwitchView(skipPositiveImgView);

            nameLabel.AddGestureRecognizer(new UITapGestureRecognizer(ShowUsernameChangeDialog));
            _activityIndicator = new ActivityIndicator(View,0.7f);
            Subscribed = false;
            UpdateUserUI(_auth.CurrentUser);
            _auth.CurrentUser.OnUpdateData +=CurrentUserOnOnUpdateData;

            _mixCardsToggle.ValueChanged += async (sender, args) =>
            {
                _activityIndicator.Animating = true;
                try
                {
                    await _auth.CurrentUser.UpdateSettings(new UserSettings
                    {
                        MixCards = _mixCardsToggle.Checked,
                        SkipFeedback = _auth.CurrentUser.UserSettings.SkipFeedback
                    });
                }
                catch (Exception e)
                {
                    // ignored
                }

                _activityIndicator.Animating = false;
            };

            _skipToggle.ValueChanged += async (sender, args) =>
            {
                _activityIndicator.Animating = true;
                try
                {
                    await _auth.CurrentUser.UpdateSettings(new UserSettings
                    {
                        MixCards = _auth.CurrentUser.UserSettings.MixCards,
                        SkipFeedback = _skipToggle.Checked
                    });
                }
                catch (Exception e)
                {
                    // ignored
                }

                _activityIndicator.Animating = false;
            };


        }


        protected override void Dispose(bool disposing)
        {
            _auth.CurrentUser.OnUpdateData -= CurrentUserOnOnUpdateData;
            base.Dispose(disposing);
        }

        private void CurrentUserOnOnUpdateData(User user)
        {
            UpdateUserUI(user);
        }

        public override void WillDisplayHeaderView(UITableView tableView, UIView headerView, nint section)
        {
//            base.WillDisplayHeaderView(tableView, headerView, section);
            var header = (UITableViewHeaderFooterView) headerView;
            header.BackgroundView.BackgroundColor = UIColor.Clear;
            header.TextLabel.TextColor = HEADER_TEXT_COLOR;
        }

        public override string TitleForHeader(UITableView tableView, nint section)
        {
            switch (section)
            {
                case 0:
                    return NSBundle.MainBundle.LocalizedString(LocalizableWords.ACCOUNT_DETAILS, "Account Details");

                case 1:
                    return NSBundle.MainBundle.LocalizedString(LocalizableWords.SETTINGS, "Settings");

                case 2:
                    return NSBundle.MainBundle.LocalizedString(LocalizableWords.SUBSCRIPTION, "Subscription");

                default:
                    break;
            }

            return "";
        }

        public override nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
        {
            if (_hiddenIndexPaths.Any(path => path.Section == indexPath.Section && path.Row == indexPath.Row))
            {
                return 0;
            }

            return base.GetHeightForRow(tableView, indexPath);
        }

        // ReSharper disable once InconsistentNaming
        private void UpdateUserUI(User user)
        {
             if(!IsViewLoaded)
                 throw new NotImplementedException();

            nameLabel.Text = user.Name;
            emailLabel.Text = user.Email;
            if (_mixCardsToggle.Checked != user.UserSettings.MixCards)
            {
                _mixCardsToggle.Checked = user.UserSettings.MixCards;
            }

            if (_skipToggle.Checked != user.UserSettings.SkipFeedback)
            {
                _skipToggle.Checked = user.UserSettings.SkipFeedback;
            }

            cancelButton.Hidden = !user.Subscribed;
            Subscribed = !string.IsNullOrEmpty(user.ExpirationDate);
            dateLabel.Text = user.ExpirationDate;
            TableView.ReloadData();



        }

        private void AddIndexToHiddenPath(NSIndexPath path)
        {
            if (_hiddenIndexPaths.Any(indexPath => indexPath.Section == path.Section && indexPath.Row == path.Row))
            {
                return;
            }
            _hiddenIndexPaths.Add(path);
        }

        private void RemoveIndexFromHiddenPath(NSIndexPath path)
        {
            _hiddenIndexPaths.RemoveAll(indexPath => indexPath.Row == path.Row && indexPath.Section == path.Section);
        }

        async partial void CancelButton_TouchUpInside(UIButton sender)
        {
            _activityIndicator.Alpha = 0.3f;
            _activityIndicator.Animating = true;
            try
            {
                //Cancel the Subscription
                await _auth.CurrentUser.UnSubscribe();
            }
            catch (Exception e)
            {
                IosExamApp.ShowToastAlert("Error!", "Something went wrong!", this);
            }

            _activityIndicator.Animating = false;
        }

        partial void AdvancedBtn_TouchUpInside(ShadowButton sender)
        {
            var storyBoard = UIStoryboard.FromName("Main",null);
            var vc = storyBoard.InstantiateViewController("SubscriptionPlanVC");
            NavigationController.PushViewController(vc,true);
            vc.NavigationItem.LeftBarButtonItem = null;
        }



        private void ShowUsernameChangeDialog()
        {
            var bundle = NSBundle.MainBundle;
            var controller = UIAlertController.Create(bundle.LocalizedString(LocalizableWords.CHANGE_USER_NAME,"Change User Name"),null,UIAlertControllerStyle.Alert);
            controller.AddTextField(field =>
            {
                field.Placeholder = bundle.LocalizedString(LocalizableWords.USERNAME,"Username");
                field.Text = _auth.CurrentUser.Name;
            });
            controller.AddAction(UIAlertAction.Create(bundle.LocalizedString(LocalizableWords.OK,"Ok"),UIAlertActionStyle.Default,async action =>
            {
                var text = controller.TextFields[0].Text;
                if (string.IsNullOrEmpty(text))
                {
                    IosExamApp.ShowToastAlert("Error!", "Username cannot be Empty!", this);
                    return;
                }

                _activityIndicator.Animating = true;

                try
                {
                    await _auth.CurrentUser.ChangeUserName(text);
                }
                catch (Exception e)
                {
                    // ignored
                }

                _activityIndicator.Animating = false;
            }));
            controller.AddAction(UIAlertAction.Create(bundle.LocalizedString(LocalizableWords.CANCEL,"Cancel"),UIAlertActionStyle.Default,action => {}));
            PresentViewController(controller,true,null);
        }

        partial void ResetPasswordButton_TouchUpInside(ShadowButton sender)
        {
            var storyboard = UIStoryboard.FromName("Main",null);
            var controller = storyboard.InstantiateViewController("ResetPasswordVC");
            NavigationController.PushViewController(controller,true);
        }
    }
}