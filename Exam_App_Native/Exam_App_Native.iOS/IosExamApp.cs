﻿using System;
using Facebook.CoreKit;
using Facebook.LoginKit;
using UIKit;

namespace Exam_App_Native.iOS
{
    public class IosExamApp
    {
        public static void ShowToastAlert(string title, string message, UIViewController controller,Action onComplete=null)
        {
            var alertController = UIAlertController.Create(title,message,UIAlertControllerStyle.Alert);
            alertController.AddAction(UIAlertAction.Create("Ok",UIAlertActionStyle.Default, action =>
            {
                onComplete?.Invoke();
            }));
            controller.PresentViewController(alertController,true,null);
        }

        public static UINavigationController CreateNavigationControllerWithTheme(UIViewController rootViewController)
        {
            var navController = new UINavigationController(rootViewController);
            navController.NavigationBar.BarTintColor = new UIColor(50f / 255, 116f / 255, 186f / 255, 1f);
            navController.NavigationBar.TintColor = UIColor.White;
            navController.NavigationBar.TitleTextAttributes = new UIStringAttributes
            {
                ForegroundColor = UIColor.White
            };

            return navController;
        }

        public static void LogOut()
        {
            if (AccessToken.CurrentAccessToken != null)
            {
               (new  LoginManager()).LogOut();
            }
            Auth.Instance.LogOut();
        }
    }
}