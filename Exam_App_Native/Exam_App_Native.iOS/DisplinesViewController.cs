﻿using Foundation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Exam_App_Native.iOS.Adapter;
using Exam_App_Native.iOS.View;
using Exam_App_Native.Model;
using Exam_App_Native.Model.ViewModel;
using UIKit;
using System.Threading.Tasks;

namespace Exam_App_Native.iOS
{
    public partial class DisplinesViewController : UIViewController
    {
        private ActivityIndicator _activityIndicator;
        private CancellationTokenSource _tokenSource;
        private CardGroupTableViewAdapter _cardGroupTableViewAdapter;
        private ActivityIndicator _searchBarActivityIndicator;
        private Timer _searchBarTimer;

        public DisplinesViewController (IntPtr handle) : base (handle)
        {
        }

        public override void ViewDidLoad()
        {

            Title = NSBundle.MainBundle.LocalizedString(LocalizableWords.DISCIPLINE_CONTROLLER_TITLE, "Disciplines");
            _activityIndicator = new ActivityIndicator(View, 1f, bgColor: View.BackgroundColor);
            _searchBarActivityIndicator = new ActivityIndicator(tableView, 1f, bgColor: View.BackgroundColor);
            tableView.AddGestureRecognizer(new UITapGestureRecognizer((obj) => {
                View.EndEditing(true);
            })
            { CancelsTouchesInView = false });
            tableView.ContentInset = new UIEdgeInsets(10, 0, 0, 0);
            tableView.RegisterNibForCellReuse(CardGroupViewCell.Nib, "CardGroupCell");
            _cardGroupTableViewAdapter = new CardGroupTableViewAdapter("CardGroupCell");
//            _activityIndicator = new ActivityIndicator(View,1);
            _cardGroupTableViewAdapter.OnDequeueReuseCell += CardGroupTableViewOnDequeueReuseCell;

            searchBar.Placeholder = NSBundle.MainBundle.LocalizedString(LocalizableWords.SEARCH, "Search").ToUpper();
            searchBar.TextChanged += (sender, args) =>
            {
                if (_searchBarTimer == null)
                {

                    _searchBarTimer = new System.Threading.Timer(
                        state => InvokeOnMainThread(() => LoadOrUpdate(searchBar.Text, true))
                        , null
                        , 200
                        , Timeout.Infinite);


                }
                else
                {
                    _searchBarTimer.Change(200, Timeout.Infinite);
                }
               
            };

           


            tableView.DataSource = _cardGroupTableViewAdapter;
            tableView.Delegate = _cardGroupTableViewAdapter;
//            tableView.ReloadData();
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewDidAppear(animated);
            LoadOrUpdate();
        }

        public override void ViewDidDisappear(bool animated)
        {
            base.ViewDidDisappear(animated);
            CancelLoading();
        }

        public override void TouchesEnded(NSSet touches, UIEvent evt)
        {
            base.TouchesEnded(touches, evt);
            View.EndEditing(true);
        }

        private void CardGroupTableViewOnDequeueReuseCell(CardGroupViewCell cardGroupViewCell)
        {
            cardGroupViewCell.InteractableDetails = true;
            cardGroupViewCell.OnChildItemClicked -= CardGroupViewCellOnChildItemClicked;
            cardGroupViewCell.OnChildItemClicked += CardGroupViewCellOnChildItemClicked;
            cardGroupViewCell.OnDetailsClicked -= CardGroupViewCellOnDetailsClicked;
            cardGroupViewCell.OnDetailsClicked += CardGroupViewCellOnDetailsClicked;
        }

        private void CardGroupViewCellOnDetailsClicked(CardGroupViewCell cell)
        {

            // ReSharper disable once ConvertToLocalFunction
            Func<string, CancellationTokenSource, Task<IEnumerable<ICard<object>>>> cardsTaskCreator =
                (query, tokenSource) =>
                {
                    tokenSource = tokenSource ?? new CancellationTokenSource();
                    return Task.Run(async () =>
                    {

                    var disciplines = await (string.IsNullOrEmpty(query) || query.Length < 3 ? ExamApp.Instance.GetDesciplinesFromGroup(((IItemGroup<object,ICard<object>>)cell.Item).Title, tokenSource)
                                : ExamApp.Instance.SearchDisciplines(query, tokenSource));

                    return disciplines.Select(discipline => (ICard<object>)new Card<object>
                        {
                            Title = discipline.Name,
                            // ReSharper disable once TooManyChainedReferences
                            Footer = $"{discipline.CardCount} {NSBundle.MainBundle.LocalizedString(LocalizableWords.CARDS,"cards").ToLower()}",
                            Data = discipline
                        });

                    }, tokenSource.Token);
                };



            PresentCardGroupViewController(new CardGroupViewController.ViewModel
            {
                Title = cell.GroupItem.Title,
                Cards = cardsTaskCreator("",null),
                OnSearchTextChanged = (sender, args) =>
                {
                    var tokenSource = new CancellationTokenSource();
                    ((CardGroupViewController)sender).Update(
                        Task.Run(async () => (await cardsTaskCreator(args.SearchText, tokenSource)) as object,
                            tokenSource.Token), tokenSource);
                },
                OnUpdatated = (sender, objects) =>
                {
                    if (objects is IEnumerable<ICard<object>> items)
                    {
                        ((CardGroupViewController)sender).UpdateItems((IEnumerable<ICard<object>>)items);
                    }
                }

            }, (card)=>OpenReviewCreatorVC((Discipline)card.Data));
        }


        private void PresentCardGroupViewController(CardGroupViewController.ViewModel viewModel, Action<ICard<object>> onItemSelected)
        {
            var storyboard = UIStoryboard.FromName("Main", null);
            var controller = (CardGroupViewController)storyboard.InstantiateViewController("CardGroupViewController");
            controller.MViewModel = viewModel;

            if (onItemSelected != null)
            {
                controller.OnItemClicked += onItemSelected;
            }


            NavigationController.PushViewController(controller, true);
        }

        private void CardGroupViewCellOnChildItemClicked(CardGroupViewCell cardGroupViewCell, ICard<object> card)
        {
            OpenReviewCreatorVC((Exam_App_Native.Model.Discipline)card.Data);
        }

        private void OpenReviewCreatorVC(Discipline discipline)
        {
            var storyboard = UIStoryboard.FromName("Main", null);
            var controller = (ReviewCreatorVC)storyboard.InstantiateViewController("ReviewCreatorVC");

            controller.MViewModel = new ReviewCreatorVC.ViewModel
            {
                Discipline = discipline,
                SubjectProgressTask = ExamApp.Instance.GetSubjectProgressesForDiciplines(((discipline).Id))
            };

            NavigationController.PushViewController(controller, true);
        }

        private async void LoadOrUpdate(string filter=null,bool searchLoad=false)
        {
            if (_activityIndicator.Animating || _searchBarActivityIndicator.Animating)
                _tokenSource?.Cancel();
            var activityIndicator = searchLoad ? _searchBarActivityIndicator : _activityIndicator;

            activityIndicator.Animating = true;
            _tokenSource = new CancellationTokenSource();
            try
            {
                var examGroups = await (string.IsNullOrEmpty(filter) || filter.Length<3 ?
                    ExamApp.Instance.GetDesciplineGroups(_tokenSource) : ExamApp.Instance.SearchDisciplineGroups(filter,_tokenSource));
                _cardGroupTableViewAdapter.Items = examGroups.Select(group =>
                    new ItemGroup<object, ICard<object>>
                    {
                        Title = group.Name,
                        Items = group.Disciplines.Select(discipline => new Card<object>
                        {
                            Title = discipline.Name,
                            Footer = $"{discipline.CardCount} cards",
                            Data = discipline
                        })
                    });
                tableView.ReloadData();
            }
            catch (Exception e)
            {

            }

            activityIndicator.Animating = false;

        }

        public void CancelLoading()
        {
            _tokenSource.Cancel();
        }

        partial void HandBurgerBtn_Activated(UIBarButtonItem sender)
        {
            MyHanBurgerVC.Instance?.Toggle();
        }
    }
}