﻿using Foundation;
using System;
using Exam_App_Native.iOS.View;
using UIKit;

namespace Exam_App_Native.iOS
{
    // ReSharper disable once InconsistentNaming
    public partial class CodeConfirmationVC : UIViewController
    {
        private ActivityIndicator _activityIndicator;

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            _activityIndicator = new ActivityIndicator(View);
        }

        public CodeConfirmationVC (IntPtr handle) : base (handle)
        {
        }


        public override void TouchesEnded(NSSet touches, UIEvent evt)
        {
            base.TouchesEnded(touches, evt);
            View.EndEditing(true);
        }

       async partial void ConfirmButton_TouchUpInside(ShadowButton sender)
        {
            string error = null;
            if (string.IsNullOrEmpty(codeLabel.Text))
            {
                error = "Code Cannot be empty";

            }

            if (error != null)
            {
                IosExamApp.ShowToastAlert("Error!",error,this);
                return;
            }

            _activityIndicator.Animating = true;
            try
            {
                await Auth.Instance.CurrentUser.ConfirmEmail(codeLabel.Text);
                var storyboard = UIStoryboard.FromName("Main",null);
                var controller = storyboard.InstantiateViewController("MyHanBurgerVC");
                PresentViewController(controller,true,null);
            }
            catch (Exception e)
            {
               IosExamApp.ShowToastAlert("Error!",e.Message,this);
            }

            _activityIndicator.Animating = false;
        }

       async partial void ResendCodeButton_TouchUpInside(ShadowButton sender)
       {
           _activityIndicator.Animating = true;
            try
            {
                await Auth.Instance.CurrentUser.ResendToken();

            }
            catch (Exception e)
            {
                IosExamApp.ShowToastAlert("Error!",e.Message,this);
            }

           _activityIndicator.Animating = false;
       }
    }


}