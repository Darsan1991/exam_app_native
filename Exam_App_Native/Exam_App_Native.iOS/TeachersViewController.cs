﻿using Foundation;
using System;
using UIKit;
using Exam_App_Native.iOS.Adapter;
using Exam_App_Native.Model;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Exam_App_Native.iOS.View;

namespace Exam_App_Native.iOS
{
    public partial class TeachersViewController : UIViewController
    {

        public ViewModel MViewModel { get; set; }

        private ActivityIndicator _activityIndicator;



        public TeachersViewController (IntPtr handle) : base (handle)
        {
        }

        public override async void ViewDidLoad()
        {
            base.ViewDidLoad();

            NavigationItem.Title = $"{NSBundle.MainBundle.LocalizedString(LocalizableWords.TEACHERS,"Teachers")}";

            //This can be move to calling from
            var tokenSource = new CancellationTokenSource();
            MViewModel = new ViewModel
            {
                TeachersTask = ExamApp.Instance.GetAllTeachers(tokenSource),
                TokenSource = tokenSource
            };

            _activityIndicator = new ActivityIndicator(View,1f,layoutMode:ActivityIndicator.LayoutMode.Frame);
            var adapter = new SimpleTableViewAdapter<Teacher, TeacherViewCell>(nameof(TeacherViewCell));
            tableView.DataSource = adapter;
            tableView.Delegate = adapter;

            tableView.TableFooterView = new UIView();

            adapter.OnItemClicked += teacher =>
            {
                var storyboard = UIStoryboard.FromName("Main",null);
                var viewController = (TeacherDetailsVC)storyboard.InstantiateViewController("TeacherDetailsVC");
                viewController.MViewModel = new TeacherDetailsVC.ViewModel
                {
                    TeacherTask = Task.FromResult(teacher)
                };
                NavigationController.PushViewController(viewController,true);
            };

            _activityIndicator.Animating = true;
            try
            {
                var teachers = await MViewModel.TeachersTask;
                adapter.Items = teachers;
                tableView.ReloadData();
            }
            catch (OperationCanceledException)
            {

            }
            catch (ExamAppException e)
            {
                IosExamApp.ShowToastAlert("Error!",e.Message,this, () =>
                {
                    var controller = (NavigationController as UIViewController) ?? this;
                    controller.DismissViewController(true,null);
                });
            }
            catch (Exception e)
            {
                IosExamApp.ShowToastAlert("Error!", "Unkonow Error!", this, () =>
                {
                    var controller = (NavigationController as UIViewController) ?? this;
                    controller.DismissViewController(true, null);
                });
            }

            _activityIndicator.Animating = false;
        }

		public override void ViewDidLayoutSubviews()
		{
            base.ViewDidLayoutSubviews();
            _activityIndicator.RefreshLayout();
		}


		public override void ViewDidAppear(bool animated)
		{
            base.ViewDidAppear(animated);
            _activityIndicator.RefreshLayout();
		}

		public override void ViewDidDisappear(bool animated)
        {
            base.ViewDidDisappear(animated);
            if (!MViewModel.TeachersTask.IsCompleted && !MViewModel.TeachersTask.IsCanceled && !MViewModel.TeachersTask.IsFaulted)
            {
                MViewModel.TokenSource?.Cancel();
            }
        }

        public struct ViewModel
        {
            public Task<IEnumerable<Exam_App_Native.Model.Teacher>> TeachersTask { get; set; }
            public CancellationTokenSource TokenSource { get; set; }
        }
    }
}