﻿using Foundation;
using System;
using System.Linq;
using Exam_App_Native.iOS.ViewControllers;
using UIKit;

namespace Exam_App_Native.iOS
{
    public partial class HandBurgerContainer : UIView,IHandBurgerContainer
    {
        public HandBurgerContainer (IntPtr handle) : base (handle)
        {
           
        }

        public UIViewController containViewController
        {
            get => Subviews.Length > 0 ? (Subviews.First().NextResponder as UIViewController) : null;
            set
            {
                if (containViewController != null)
                {
                    Subviews[0].RemoveFromSuperview();
                    AddSubview(value.View);
                   
                }
            }
        }

        public UIView View => this;
    }
}