// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace Exam_App_Native.iOS
{
    [Register ("QuestionViewController")]
    partial class QuestionViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel completeIndicatorLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextView contentTxtView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        Exam_App_Native.iOS.ShadowButton correctButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIProgressView progressBar { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel subtitleLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel teacherLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel titleLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        Exam_App_Native.iOS.ShadowButton wrongButton { get; set; }

        [Action ("CorrectButton_TouchUpInside:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void CorrectButton_TouchUpInside (Exam_App_Native.iOS.ShadowButton sender);

        [Action ("WrongButton_TouchUpInside:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void WrongButton_TouchUpInside (Exam_App_Native.iOS.ShadowButton sender);

        void ReleaseDesignerOutlets ()
        {
            if (completeIndicatorLabel != null) {
                completeIndicatorLabel.Dispose ();
                completeIndicatorLabel = null;
            }

            if (contentTxtView != null) {
                contentTxtView.Dispose ();
                contentTxtView = null;
            }

            if (correctButton != null) {
                correctButton.Dispose ();
                correctButton = null;
            }

            if (progressBar != null) {
                progressBar.Dispose ();
                progressBar = null;
            }

            if (subtitleLabel != null) {
                subtitleLabel.Dispose ();
                subtitleLabel = null;
            }

            if (teacherLabel != null) {
                teacherLabel.Dispose ();
                teacherLabel = null;
            }

            if (titleLabel != null) {
                titleLabel.Dispose ();
                titleLabel = null;
            }

            if (wrongButton != null) {
                wrongButton.Dispose ();
                wrongButton = null;
            }
        }
    }
}