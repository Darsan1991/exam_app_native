﻿using Foundation;
using System;
using System.Security.Cryptography.X509Certificates;
using Exam_App_Native.iOS.Model;
using UIKit;

namespace Exam_App_Native.iOS
{
    public partial class MenuTableViewCell : UITableViewCell
    {

        public ViewModel viewModel
        {
            get => _viewModel;
            set
            {
                _viewModel = value;
                label.Text = value.Name;
                imgView.Image = value.Icon;
            }
        }

        

        private ViewModel _viewModel;


        public MenuTableViewCell (IntPtr handle) : base (handle)
        {
               
        }

        


        public struct ViewModel
        {
            public UIImage Icon { get; set; }
            public string Name { get; set; }
            public IMenuItem MenuItem { get; set; }
        }
    }
}