﻿using Foundation;
using System;
using System.Collections.Generic;
using System.Linq;
using CoreGraphics;
using UIKit;

namespace Exam_App_Native.iOS
{
    public partial class HorizontalProgressBar : NeverClearView
    {

        private readonly List<UIView> _childViews = new List<UIView>();
        private readonly List<ProgressTile> _progressTiles = new List<ProgressTile>();


        public IEnumerable<ProgressTile> ProgressTiles
        {
            get => _progressTiles;
            set
            {
                if (value == null)
                {
                    _childViews.ForEach(v => v.RemoveFromSuperview());
                    _childViews.Clear();
                    _progressTiles.Clear();
                    return;
                }
                
                _progressTiles.Clear();
                _progressTiles.AddRange(new List<ProgressTile>(value));

                while (_progressTiles.Count>_childViews.Count)
                {
                    CreateProgressChild();
                }

                while (_childViews.Count>_progressTiles.Count)
                {
                    RemoveProgressChild();
                }

                for (var i = 0; i < _childViews.Count; i++)
                {
                    _childViews[i].BackgroundColor = _progressTiles[i].Color;
                }

            }
        }

        public float[] Values { get; private set; }

        public float Total { get; set; } = 1f;

        public bool IsViewLoaded { get; set; }

        private void CreateProgressChild()
        {
            var view = new NeverClearView();
            view.TranslatesAutoresizingMaskIntoConstraints = false;
            AddSubview(view);
            
            view.TopAnchor.ConstraintEqualTo(TopAnchor).Active = true;
            view.BottomAnchor.ConstraintEqualTo(BottomAnchor, 1).Active = true;

            view.LeftAnchor
                    .ConstraintEqualTo(
                        _childViews.Count > 0 ? _childViews[_childViews.Count - 1].RightAnchor : LeftAnchor, 0).Active =
                true;

           
            view.WidthAnchor.ConstraintEqualTo(0).Active = true;
            view.Frame = new CGRect(0,0,Frame.Width,Frame.Height);
            _childViews.Add(view);
            LayoutIfNeeded();
        }

        private void RemoveProgressChild()
        {
            if (_childViews.Count <= 0) return;
            _childViews[_childViews.Count-1].RemoveFromSuperview();
            _childViews.RemoveAt(_childViews.Count-1);

        }

        

        public HorizontalProgressBar (IntPtr handle) : base (handle)
        {
        }

        public override void AwakeFromNib()
        {
            base.AwakeFromNib();
            IsViewLoaded = true;
            Layer.CornerRadius = Frame.Height / 2;
            ClipsToBounds = true;
            if (Values != null && Values.Length > 0)
            {
                UpdateValuesOfChilds(Values,true);
            }
        }

        public void SetValues(IEnumerable<float> values, bool animate = true)
        {
            var vals = values.ToArray();

            if (vals.Length != _progressTiles.Count || vals.Sum() > Total)
            {
                throw new Exception("Something vals are wrong");
            }

            Values = vals;

            if (!IsViewLoaded)
            {
                return;
            }

            UpdateValuesOfChilds(vals, false);

        }

        private void UpdateValuesOfChilds(float[] values, bool animate)
        {
            
            for (var i = 0; i < _childViews.Count; i++)
            {
                Console.WriteLine("Child Width:" + values[i] * Frame.Width / Total +"Child Height:"+_childViews[i].Frame.Height);
                _childViews[i].Constraints.First((c) => c.FirstAttribute == NSLayoutAttribute.Width).Constant =
                    values[i] * Frame.Width / Total;

            }

            if (animate)
            {
                UIView.Animate(1, LayoutIfNeeded);
            }
            else
            {
                LayoutIfNeeded();
            }
        }

        public struct ProgressTile
        {
            public UIColor Color { get; set; }
        }
    }

    public class NeverClearView : UIView
    {
        public override UIColor BackgroundColor
        {
            get => base.BackgroundColor;
            set
            {
                if (value.CGColor.Alpha == 0)
                    return;
                base.BackgroundColor = value;
            }
        }

        public NeverClearView(IntPtr ptr) : base(ptr)
        {

        }

        public NeverClearView()
        {
        }

        public NeverClearView(NSCoder coder) : base(coder)
        {
        }

        protected NeverClearView(NSObjectFlag t) : base(t)
        {
        }

  

        public NeverClearView(CGRect frame) : base(frame)
        {
        }
    }
}