﻿// WARNING
//
// This file has been generated automatically by Visual Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace Exam_App_Native.iOS
{
    [Register ("ReviewsViewController")]
    partial class ReviewsViewController
    {
        [Outlet]
        Exam_App_Native.iOS.ShadowButton allBtn { get; set; }

        [Outlet]
        UIKit.UILabel calender1Label { get; set; }

        [Outlet]
        UIKit.UILabel calender2Label { get; set; }

        [Outlet]
        UIKit.UILabel calender3Label { get; set; }

        [Outlet]
        UIKit.UILabel calender4Label { get; set; }

        [Outlet]
        UIKit.UILabel calender5Label { get; set; }

        [Outlet]
        UIKit.UILabel calender6Label { get; set; }

        [Outlet]
        UIKit.UILabel calender7Label { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIBarButtonItem handBurgerBtn { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView headerView { get; set; }

        [Outlet]
        Exam_App_Native.iOS.ProgressBar progressBar { get; set; }

        [Outlet]
        UIKit.UILabel question1Label { get; set; }

        [Outlet]
        UIKit.UILabel question2Label { get; set; }

        [Outlet]
        UIKit.UILabel question3Label { get; set; }

        [Outlet]
        UIKit.UILabel question4Label { get; set; }

        [Outlet]
        UIKit.UILabel question5Label { get; set; }

        [Outlet]
        UIKit.UILabel question6Label { get; set; }

        [Outlet]
        UIKit.UILabel question7Label { get; set; }

        [Outlet]
        UIKit.UIView sceheduledGroup { get; set; }

        [Outlet]
        Exam_App_Native.iOS.ShadowButton scheduledBtn { get; set; }

        [Outlet]
        UIKit.UILabel scheduledCountLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITableView tableView { get; set; }

        [Outlet]
        UIKit.UILabel titleLabel { get; set; }

        [Action ("AllBtn_Activated:")]
        partial void AllBtn_Activated (UIKit.UIButton sender);

        [Action ("Delete_Activated:")]
        partial void Delete_Activated (UIKit.UIButton sender);

        [Action ("HandBurgerBtn_Activated:")]
        partial void HandBurgerBtn_Activated (UIKit.UIBarButtonItem sender);

        [Action ("Scheduled_Activated:")]
        partial void Scheduled_Activated (UIKit.UIButton sender);
        
        void ReleaseDesignerOutlets ()
        {
            if (allBtn != null) {
                allBtn.Dispose ();
                allBtn = null;
            }

            if (calender1Label != null) {
                calender1Label.Dispose ();
                calender1Label = null;
            }

            if (calender2Label != null) {
                calender2Label.Dispose ();
                calender2Label = null;
            }

            if (calender3Label != null) {
                calender3Label.Dispose ();
                calender3Label = null;
            }

            if (calender4Label != null) {
                calender4Label.Dispose ();
                calender4Label = null;
            }

            if (calender5Label != null) {
                calender5Label.Dispose ();
                calender5Label = null;
            }

            if (calender6Label != null) {
                calender6Label.Dispose ();
                calender6Label = null;
            }

            if (calender7Label != null) {
                calender7Label.Dispose ();
                calender7Label = null;
            }

            if (handBurgerBtn != null) {
                handBurgerBtn.Dispose ();
                handBurgerBtn = null;
            }

            if (headerView != null) {
                headerView.Dispose ();
                headerView = null;
            }

            if (progressBar != null) {
                progressBar.Dispose ();
                progressBar = null;
            }

            if (question1Label != null) {
                question1Label.Dispose ();
                question1Label = null;
            }

            if (question2Label != null) {
                question2Label.Dispose ();
                question2Label = null;
            }

            if (question3Label != null) {
                question3Label.Dispose ();
                question3Label = null;
            }

            if (question4Label != null) {
                question4Label.Dispose ();
                question4Label = null;
            }

            if (question5Label != null) {
                question5Label.Dispose ();
                question5Label = null;
            }

            if (question6Label != null) {
                question6Label.Dispose ();
                question6Label = null;
            }

            if (question7Label != null) {
                question7Label.Dispose ();
                question7Label = null;
            }

            if (sceheduledGroup != null) {
                sceheduledGroup.Dispose ();
                sceheduledGroup = null;
            }

            if (scheduledBtn != null) {
                scheduledBtn.Dispose ();
                scheduledBtn = null;
            }

            if (scheduledCountLabel != null) {
                scheduledCountLabel.Dispose ();
                scheduledCountLabel = null;
            }

            if (tableView != null) {
                tableView.Dispose ();
                tableView = null;
            }

            if (titleLabel != null) {
                titleLabel.Dispose ();
                titleLabel = null;
            }
        }
    }
}
