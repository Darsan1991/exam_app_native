﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using UIKit;

namespace Exam_App_Native.iOS.General
{
    public class RemoteFileManager
    {
        public static readonly RemoteFileManager Instance = new RemoteFileManager();

        private readonly Dictionary<string, string> _imageUrlVsLocalUrl = new Dictionary<string, string>();

       
        public async Task<UIImage> DownloadImageAsync(string url, string suggestedName = null, CancellationTokenSource cancellationTokenSource = null)
        {
            var urlLowercased = url.ToLower();

            if (_imageUrlVsLocalUrl.ContainsKey(urlLowercased))
            {

                return UIImage.FromFile(_imageUrlVsLocalUrl[urlLowercased]);

            }

            var bytes = await ExamApp.Instance.GetFile(url);
            var localPath = "";
            if (_imageUrlVsLocalUrl.ContainsKey(urlLowercased))
                localPath = _imageUrlVsLocalUrl[urlLowercased];
            else
            {
                var docPath = System.Environment.GetFolderPath(Environment.SpecialFolder.Personal);
                var fileName = (suggestedName ?? "img_" + DateTime.Today.TimeOfDay.TotalMilliseconds) + ".png";
                localPath = System.IO.Path.Combine(docPath, fileName);
                var fs = new FileStream(localPath, FileMode.OpenOrCreate);
                if (cancellationTokenSource != null)
                    await fs.WriteAsync(bytes, 0, bytes.Length, cancellationTokenSource.Token);
                else
                {
                    await fs.WriteAsync(bytes, 0, bytes.Length);
                }
                fs.Close();
                if (_imageUrlVsLocalUrl.ContainsKey(urlLowercased))
                {
                    _imageUrlVsLocalUrl[urlLowercased] = localPath;
                }
                else
                {
                    _imageUrlVsLocalUrl.Add(urlLowercased, localPath);
                }
            }



            return UIImage.FromFile(localPath);

        }
    }
}