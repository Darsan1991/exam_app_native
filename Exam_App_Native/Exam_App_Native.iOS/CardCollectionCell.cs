﻿using System;
using CoreGraphics;
using Exam_App_Native.Model;
using Exam_App_Native.Model.ViewModel;
using Foundation;
using UIKit;

namespace Exam_App_Native.iOS
{
    public partial class CardCollectionCell : UICollectionViewCell,IHolder<object>
    {
        public static readonly NSString Key = new NSString("CardCollectionCell");
        public static readonly UINib Nib;

        public object Item
        {
            get => _item;
            set
            {
                if (!(value is ICard<object>))
                {
                    return;
                }

                var card = (ICard<object>)value;
                titleLabel.Text = card.Title;
                footerTitleLabel.Text = card.Footer;
                subtitleLabel.Text = card.SubTitle;

                subtitleLabel.Hidden = string.IsNullOrEmpty(card.SubTitle);

                _item = value;
            }
        }

        private object _item;

 

        static CardCollectionCell()
        {
            Nib = UINib.FromName("CardCollectionCell", NSBundle.MainBundle);
        }

        protected CardCollectionCell(IntPtr handle) : base(handle)
        {
            // Note: this .ctor should not contain any initialization logic.
        }

        public override void AwakeFromNib()
        {
            base.AwakeFromNib();

            ContentView.Layer.MasksToBounds = true;

            Layer.ShadowOffset = new CGSize(0.7, 0.7);
            Layer.ShadowColor = UIColor.Black.CGColor;
            Layer.ShadowRadius = 2f;
            Layer.ShadowOpacity = 0.3f;
            Layer.MasksToBounds = false;
        }
    }
}
