﻿using Foundation;
using System;
using System.Linq;
using Exam_App_Native.iOS.Model;
using Exam_App_Native.iOS.ViewControllers;
using UIKit;

namespace Exam_App_Native.iOS
{
    public partial class HandBurgerMenu : UIView,IHandBurgerMenu<IMenuItem>
    {
        public event Action<IMenuItem> OnItemClicked
        {
            add
            {
                if (menuViewController != null)
                {
                    menuViewController.OnItemClicked += value;
                }
            }
            remove
            {
                if (menuViewController != null)
                {
                    menuViewController.OnItemClicked -= value;
                }
            }
        }

        private MenuViewController menuViewController => Subviews.First()?.NextResponder as MenuViewController;

        public HandBurgerMenu (IntPtr handle) : base (handle)
        {
        }

        public UIView View => this;
    }
}