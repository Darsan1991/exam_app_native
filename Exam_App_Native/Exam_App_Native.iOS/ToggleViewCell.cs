﻿using Foundation;
using System;
using System.IO;
using Exam_App_Native.Model;
using Exam_App_Native.Model.ViewModel;
using UIKit;
using Exam_App_Native.iOS.View;

namespace Exam_App_Native.iOS
{
    public partial class ToggleViewCell : UITableViewCell,ISelectableHolder<object>
    {
        private readonly UIColor _selectedColor = new UIColor(0, 0, 1, 0.07f);
        private ISelectableItem<object> _item;

        public object Item
        {
            get => _item;
            set
            {
                if(!(value is ISelectableItem<object>))
                    throw new InvalidDataException();
                var selectableItem = (ISelectableItem<object>)value;

                if (IsViewLoaded)
                {
                    titleLabel.Text = selectableItem.Title;
                    subtitleLabel.Text = selectableItem.SubTitle;
                    SelectedItem = selectableItem.Selected;
                    
                }
                _item = selectableItem;
            }
        }

        public  bool SelectedItem
        {
            get => _toggleSwitch?.Checked ?? false;
            set
            {
                if (IsViewLoaded)
                {
                    _toggleSwitch.Checked = value;
                    if(HighlightBackground)
                    ContentView.BackgroundColor = value ? _selectedColor : UIColor.Clear;
                }

                if (_item!=null && _item.Selected!=value)
                {
                    _item.Selected = value;
                }
            }
        }

        public bool HighlightBackground{
            get{
                return _highlightBackgrozund;
            }
            set{
                if (IsViewLoaded)
                {
                    if (!value)
                        ContentView.BackgroundColor = UIColor.Clear;
                    else
                    {
                        ContentView.BackgroundColor = SelectedItem ? _selectedColor : UIColor.Clear;
                    }
                }

                _highlightBackgrozund = value;
            }
        }

        private bool IsViewLoaded { get; set; }
        private ToggleSwitchView _toggleSwitch;
        private bool _highlightBackgrozund;

        public ToggleViewCell (IntPtr handle) : base (handle)
        {
        }

        public override void AwakeFromNib()
        {
            base.AwakeFromNib();
            IsViewLoaded = true;

            _toggleSwitch = new ToggleSwitchView(toggleImgView);


            //Find Best way later
            if (Item != null)
                Item = Item;

            _toggleSwitch.ValueChanged += (sender, args) => { 
                SelectedItem = _toggleSwitch.Checked; };

            ContentView.AddGestureRecognizer(new UITapGestureRecognizer(()=>{
                _toggleSwitch.Checked = !_toggleSwitch.Checked; 
            }));
        }
    }
}