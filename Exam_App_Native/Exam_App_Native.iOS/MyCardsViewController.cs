﻿using Foundation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Exam_App_Native.iOS.Adapter;
using Exam_App_Native.iOS.View;
using Exam_App_Native.Model;
using Exam_App_Native.Model.ViewModel;
using UIKit;

namespace Exam_App_Native.iOS
{
    public partial class MyCardsViewController : UIViewController
    {

        private CancellationTokenSource _tokenSource;
        private Task<IEnumerable<MyDiscipline>> _task;
        private ActivityIndicator _activityIndicator;

        private UIBarButtonItem _newBtn;
        private readonly Auth _auth = Auth.Instance;
        private SimpleTableViewAdapter<IItem, ItemViewCell> _tableViewAdapter;
        private ActivityIndicator _searchBarActivityIndicator;
        private Timer _searchBarTimer;


        public MyCardsViewController (IntPtr handle) : base (handle)
        {
        }

        partial void HandBurgerBtn_Activated(UIBarButtonItem sender)
        {
            MyHanBurgerVC.Instance?.Toggle();
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            Title = NSBundle.MainBundle.LocalizedString(LocalizableWords.MY_CARDS_CONTROLLER_TITLE, "My Cards");
            if (NavigationController==null)
            {
                throw new InvalidOperationException("This ViewController Should be present in the NavigationController.");
            }
            _newBtn = new UIBarButtonItem
            {
                Title = NSBundle.MainBundle.LocalizedString(LocalizableWords.NEW,"New")
            };
            _newBtn.Clicked += (sender, args) =>
            {
                OpenUserFlashVC(null,(o, eventArgs) => LoadOrUpdate(searchBar.Text));
            };
            NavigationItem.RightBarButtonItem = _newBtn;
            _activityIndicator = new ActivityIndicator(View, 1f, bgColor: View.BackgroundColor);
            _searchBarActivityIndicator = new ActivityIndicator(tableView, 1f, bgColor: View.BackgroundColor);

            tableView.AddGestureRecognizer(new UITapGestureRecognizer((obj) => {
                View.EndEditing(true);
            })
            { CancelsTouchesInView = false });

            tableView.TableFooterView = new UIView();
            searchBar.Placeholder = NSBundle.MainBundle.LocalizedString(LocalizableWords.SEARCH, "Search").ToUpper();
            searchBar.TextChanged += (sender, args) =>
            {
                if (_searchBarTimer == null)
                {

                    _searchBarTimer = new Timer(
                        state => InvokeOnMainThread(() => LoadOrUpdate(searchBar.Text, true))
                        , null
                        , 200
                        , Timeout.Infinite);


                }
                else
                {
                    _searchBarTimer.Change(200, Timeout.Infinite);
                }

            };

            _tableViewAdapter = new SimpleTableViewAdapter<IItem,ItemViewCell>("ItemViewCell");
            tableView.DataSource = _tableViewAdapter;
            tableView.Delegate = _tableViewAdapter;
            _tableViewAdapter.OnItemClicked += item => { OpenMyCards((MyDiscipline) item.Data);};


        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);
            LoadOrUpdate(searchBar?.Text);
        }

        public override void ViewDidDisappear(bool animated)
        {
            base.ViewDidDisappear(animated);
            CancelLoadOrUpdate();
        }


        public override void TouchesEnded(NSSet touches, UIEvent evt)
        {
            base.TouchesEnded(touches, evt);
            View.EndEditing(true);
        }

        private void OpenMyCards(MyDiscipline discipline)
        {
            SimpleTableViewAdapter<MyCard, FlashCardViewCell> tableViewAdapter = null;
            var viewModel = new SimpleTableViewController.ViewModel
            {
                Title = discipline.Name + $" {NSBundle.MainBundle.LocalizedString(LocalizableWords.CARDS,"Cards")}"
            };

            // ReSharper disable once ConvertToLocalFunction
            Func<string, MyDiscipline, CancellationTokenSource, Task<IEnumerable<MyCard>>> loadCards =
                (query, myDiscipline, token) =>
                {
                    return Task.Run(async () =>
                    {
                        var cards = (string.IsNullOrEmpty(query) || query.Length < 3)
                            ? (await _auth.CurrentUser.GetMyCardsForDiscipline(discipline.Id, token)).ToList()
                            : (await _auth.CurrentUser.SearchMyCards(query,
                                new Discipline { Name = discipline.Name, Id = discipline.Id }, token)).ToList();
                        return cards.Select(card =>
                        {
                            if (myDiscipline != null)
                                card.Discipline = new Discipline
                                {
                                    Name = myDiscipline.Name,
                                    Id = myDiscipline.Id
                                };
                            return card;
                        });
                    }, token.Token);
                };

            //Setup Recycler Adapter
            viewModel.TableViewAdapter = Task.Run(async () =>
            {
                Console.WriteLine($"Auth:{_auth.CurrentUser}");
                var cards = await loadCards(null, discipline, new CancellationTokenSource());
                tableViewAdapter = new SimpleTableViewAdapter<MyCard, FlashCardViewCell>("FlashCardViewCell", cards);
                tableViewAdapter.OnItemClicked += card =>
                {
                   
                    OpenUserFlashVC(card, (sender, args) =>
                    {
                        try
                        {
                            viewModel.Instance.Update(Task.Run(async () =>
                            {
                                await args.Task;
                                var myCards = await loadCards(null, discipline, new CancellationTokenSource());
                                return (object)myCards;
                            }), null);

                        }
                        catch (Exception e)
                        {
                            // ignored
                        }
                    });

                };
                return (TableViewAdapter)tableViewAdapter;
            });

            //Update For Query Text Changed
            viewModel.OnSearchTextChanged = (sender, args) =>
            {
                var tokenSource = new CancellationTokenSource();
                ((SimpleTableViewController)sender).Update(Task.Run(async () =>
                {
                    var myCards = await loadCards(args.SearchText, discipline, tokenSource);
                    return (object)myCards;
                }, tokenSource.Token), tokenSource);
            };

            viewModel.OnUpdatated = (sender, o) =>
            {
                if (!(o is IEnumerable<MyCard> cards)) return;
                tableViewAdapter.Items = cards;
                ((SimpleTableViewController)sender).TableView.ReloadData();
            };

            using (var storyboard = UIStoryboard.FromName("Main",null))
            {
                var controller = (SimpleTableViewController)storyboard.InstantiateViewController("SimpleTableViewController");
                controller.MViewModel = viewModel;
                NavigationController.PushViewController(controller,true);
            }
        }

        // ReSharper disable once InconsistentNaming
        private void OpenUserFlashVC(MyCard card,EventHandler<UserFlashCardVC.CompleteEventArgs> onComplete=null)
        {
            using (var storyboard = UIStoryboard.FromName("Main", null))
            {
                var controller = (UserFlashCardVC)storyboard.InstantiateViewController("UserFlashCardVC");
                controller.MViewModel = new UserFlashCardVC.ViewModel
                {
                    MyCard = card,
                    OnComplete = (sender, args) => onComplete?.Invoke(sender,args)
                };
                NavigationController.PushViewController(controller, true);
            }
        }

        private async void LoadOrUpdate(string filter = null,bool searchLoad=false)
        {
            if (_activityIndicator.Animating || _searchBarActivityIndicator.Animating)
                _tokenSource?.Cancel();
            var activityIndicator = searchLoad ? _searchBarActivityIndicator : _activityIndicator;
            ShowLoading(activityIndicator);

            try
            {
                _tokenSource = new CancellationTokenSource();
                _task = string.IsNullOrEmpty(filter)
                    ? _auth.CurrentUser.GetMyCardsDisciplines(_tokenSource)
                    : _auth.CurrentUser.SearchMyCardsDisciplines(filter, _tokenSource);
                var disciplines = await _task;
                _tableViewAdapter.Items = disciplines.Select(discipline => new Item
                {
                    Title = discipline.Name,
                    SubTitle = discipline.Subjects.AllIntoOne<MyDiscipline.Subject, string>((subject, s) =>
                        s = (s == null ? "" : (s + ",")) + subject.Name),
                    Data = discipline
                });
                tableView.ReloadData();
            }

            catch (Exception e)
            {
                //                Toast.MakeText(Context,"Something went wrong with the connection",ToastLength.Short).Show();
            }

            HideLoading(activityIndicator);
        }

        private void HideLoading(ActivityIndicator activityIndicator=null)
        {
            activityIndicator = activityIndicator ??
                                (_activityIndicator.Animating ? _activityIndicator : _searchBarActivityIndicator);
            activityIndicator.Animating = false;
        }

        private static void ShowLoading(ActivityIndicator activityIndicator)
        {
            activityIndicator.Animating = true;
        }

        private void CancelLoadOrUpdate()
        {
            if (_activityIndicator.Animating || _searchBarActivityIndicator.Animating)
            {
                _tokenSource.Cancel();
            }
        }

       
    }
}