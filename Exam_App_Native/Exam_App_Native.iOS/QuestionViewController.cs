﻿using Foundation;
using System;
using System.Threading.Tasks;
using Exam_App_Native.iOS.View;
using Exam_App_Native.Model;
using UIKit;

namespace Exam_App_Native.iOS
{
    public partial class QuestionViewController : UIViewController
    {
        public event Action<bool, float> OnAnswered;

        public int TotalQuizCount { get; set; } = 1;

        public QuizCard QuizCard
        {
            get => _quizCard;
            set
            {
                _quizCard = value;
                if (IsViewLoaded)
                {
                    titleLabel.Text = _quizCard.Topic;
                    contentTxtView.Text = _quizCard.Content;
                    teacherLabel.Text = _quizCard.Teacher;
                    subtitleLabel.Text = _quizCard.Topic;
                }
            }
        }


        private QuizCard _quizCard;
        private int _currentQuizIndex = -1;
        private ActivityIndicator _activityIndicator;
        private DateTime _quizStartTime;
        private Task<QuizCard> _quizCardTask;


        public QuestionViewController (IntPtr handle) : base (handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            _activityIndicator = new ActivityIndicator(View, 1)
            {
                Animating = _quizCardTask != null &&
                            (!_quizCardTask.IsCompleted && !_quizCardTask.IsCanceled &&
                             !_quizCardTask.IsFaulted)
            };



            if (_quizCardTask != null && _quizCardTask.IsCompleted)
            {
                QuizCard = _quizCardTask.Result;
            }

        }

        partial void WrongButton_TouchUpInside(ShadowButton sender)
        {
            OnAnswered?.Invoke(false, DateTime.Now.Subtract(_quizStartTime).Seconds);
        }

        partial void CorrectButton_TouchUpInside(ShadowButton sender)
        {
            OnAnswered?.Invoke(true, DateTime.Now.Subtract(_quizStartTime).Seconds);
        }

        public void StartOrGoToNextQuiz(Task<QuizCard> task)
        {
            _quizCardTask = task;
            _currentQuizIndex++;

            if (IsViewLoaded)
            {
                UpdateProgress();
            }

            SetQuiz(task);

        }

        private void UpdateProgress()
        {
            completeIndicatorLabel.Text = $"{_currentQuizIndex} {NSBundle.MainBundle.LocalizedString(LocalizableWords.OF,"of").ToLower()} {TotalQuizCount}";
            progressBar.Progress = (_currentQuizIndex + 0f)/TotalQuizCount;
        }



        private async void SetQuiz(Task<QuizCard> task)
        {
            if (IsViewLoaded)
                _activityIndicator.Animating = true;
            try
            {
                QuizCard = await task;
                _quizStartTime = DateTime.Now;
            }
            catch (Exception e)
            {
                // ignored
            }

            if (IsViewLoaded)
                _activityIndicator.Animating = false;

        }
    }
}