// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace Exam_App_Native.iOS
{
    [Register ("ResetPasswordVC")]
    partial class ResetPasswordVC
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField emailLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        Exam_App_Native.iOS.ShadowButton resetButton { get; set; }

        [Action ("ResetButton_TouchUpInside:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void ResetButton_TouchUpInside (Exam_App_Native.iOS.ShadowButton sender);

        void ReleaseDesignerOutlets ()
        {
            if (emailLabel != null) {
                emailLabel.Dispose ();
                emailLabel = null;
            }

            if (resetButton != null) {
                resetButton.Dispose ();
                resetButton = null;
            }
        }
    }
}