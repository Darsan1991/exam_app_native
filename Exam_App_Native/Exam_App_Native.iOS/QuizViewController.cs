﻿using Foundation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreAnimation;
using CoreGraphics;
using Exam_App_Native.iOS.View;
using Exam_App_Native.iOS.ViewControllers;
using Exam_App_Native.Model;
using UIKit;

namespace Exam_App_Native.iOS
{
    public partial class QuizViewController : UIViewController
    {
        private QuestionViewController _questionViewController;
        private ResultViewController _resultViewController;

        private List<ReviewSetStartUpInfo> _startUpInfos;
        private ReviewCardInfo _currentReviewCardInfo;
        private ActivityIndicator _activityIndicator;
        private int _currentReviewIndex;


        public ViewModel MViewModel { get; set; }

        public QuizViewController (IntPtr handle) : base (handle)
        {
        }

        public override async void ViewDidLoad()
        {
            base.ViewDidLoad();

            _activityIndicator = new ActivityIndicator(View,1f);

            var storyboard = UIStoryboard.FromName("Main",null);
            _questionViewController = (QuestionViewController)storyboard.InstantiateViewController("QuestionViewController");
            _resultViewController = (ResultViewController)storyboard.InstantiateViewController("ResultViewController");

            questionContainer.AddSubview(_questionViewController.View);
            _questionViewController.DidMoveToParentViewController(this);

            resultContainer.AddSubview(_resultViewController.View);
            _resultViewController.DidMoveToParentViewController(this);

            _questionViewController.OnAnswered += QuizOnAnswered;
            _resultViewController.OnClickNextOrComplete += ResultVCOnClickNextOrComplete;

            if (NavigationController==null)
            {
                throw new InvalidOperationException("Quiz View Controller Should be Present inside the NavigationController!");
            }

            var buttonItem = new UIBarButtonItem
            {
                Title = NSBundle.MainBundle.LocalizedString(LocalizableWords.STOP,"Stop")
            };
            NavigationItem.RightBarButtonItem = buttonItem;
            NavigationItem.Title = NSBundle.MainBundle.LocalizedString(LocalizableWords.QUIZ_CONTROLLER_TITLE,"Quiz");
            buttonItem.Clicked += (sender, args) =>
            {
                CompleteTheQuiz();
            };

            _activityIndicator.Animating = true;
            try
            {
                _startUpInfos = (await MViewModel.ReviewSetStartUpInfos).ToList();
                _questionViewController.TotalQuizCount = _startUpInfos.Sum(info => info.CardCount);
                if (_startUpInfos.Count == 0)
                {
                    IosExamApp.ShowToastAlert("Error!", "No Review Sets found!",this, () =>
                    {
                        NavigationController.DismissViewController(true,null);
                    });
                  
                    return;
                }

                var startUpInfo = _startUpInfos[_currentReviewIndex];
                _currentReviewCardInfo = new ReviewCardInfo
                {
                    QuizCard = startUpInfo.FirstCard,
                    ReviewId = startUpInfo.ReviewId,
                    ReviewSetId = startUpInfo.ReviewSetId,

                };
                HideResultAndShowNext(_currentReviewCardInfo.QuizCard);
                
                _activityIndicator.Animating = false;
            }
            catch (Exception e)
            {
                IosExamApp.ShowToastAlert("Error!", "Something Went Wrong!",this, () =>
                {
                    DismissViewController(true,null);
                });
            }

        }

        public override void ViewDidLayoutSubviews()
        {
            base.ViewDidLayoutSubviews();
            _questionViewController.View.Frame = new CGRect(0,0,questionContainer.Frame.Width,questionContainer.Frame.Height);
            _resultViewController.View.Frame = new CGRect(0,0,resultContainer.Frame.Width,resultContainer.Frame.Height);
        }

        private void CompleteTheQuiz()
        {
            DismissViewController(true,null);
//            Show the result in the review vc
            MainTabBarViewController.Instance.SetCurrentTab(MainTabBarViewController.TabButton.Reviews);
            var controller = MainTabBarViewController.Instance.GetTab(MainTabBarViewController.TabButton.Reviews);



            var viewController = (controller is UINavigationController)?(controller as UINavigationController).ViewControllers[0]: controller;
            ((ReviewsViewController)viewController)
                .LoadOrUpdateReviews(MViewModel.ReviewSetId ?? _startUpInfos.First().ReviewSetId);
//            MainActivity.Instance.SetCurrentTab(MainActivity.TabButton.Reviews);
//            ((ReviewsFragment)MainActivity.Instance.GetTab(MainActivity.TabButton.Reviews))
//                .LoadOrUpdateReviews(_startUpInfos.ReviewSetId);
        }

        // ReSharper disable once InconsistentNaming
        private void ResultVCOnClickNextOrComplete()
        {
            if (_currentReviewCardInfo.QuizCard == null)
            {
                if (_currentReviewIndex == _startUpInfos.Count - 1)
                    CompleteTheQuiz();
                else
                {
                    var startUpInfo = _startUpInfos[++_currentReviewIndex];
                    _currentReviewCardInfo = new ReviewCardInfo
                    {
                        QuizCard = startUpInfo.FirstCard,
                        ReviewId = startUpInfo.ReviewId,
                        ReviewSetId = startUpInfo.ReviewSetId,

                    };
                    _questionViewController.TotalQuizCount = startUpInfo.CardCount;
                    HideResultAndShowNext(_currentReviewCardInfo.QuizCard);
                }
            }
            else
            {
                HideResultAndShowNext(_currentReviewCardInfo.QuizCard);
            }
        }

        private void QuizOnAnswered(bool answer, float totalSec)
        {
            var task = Task.Run(async () =>
            {
                var info = await ExamApp.Instance
                    .EvaluateCard(_currentReviewCardInfo.ReviewId
                        , _currentReviewCardInfo.QuizCard.Id, answer, (int)totalSec, _currentReviewCardInfo.HitReviewId,
                        _currentReviewCardInfo.MistakeReviewId);
                info.CurrentCard = _currentReviewCardInfo.QuizCard;
                info.IsReviewFinished = info.IsReviewFinished || info.NextCard == null;
                info.NextCard = info.IsReviewFinished ? null : info.NextCard;
                _currentReviewCardInfo.QuizCard = info.NextCard;
                _currentReviewCardInfo.HitReviewId = info.NewHitsReviewId;
                _currentReviewCardInfo.MistakeReviewId = info.NewMistakeReviewId;
                return info;
            });
            ShowResultWithAnimation(task);
        }

        private void ShowResultWithAnimation(Task<CardEvalResultInfo> result)
        {
            var frontTransform = CATransform3D.MakeTranslation(0, 0, View.Frame.Width / 2);
            resultContainer.Layer.Transform = CATransform3D.MakeRotation(new nfloat(Math.PI),0,1,0);
            resultContainer.Hidden = false;
            questionContainer.Layer.Transform = frontTransform;
            _resultViewController.SetResult(result);
            UIView.AnimateKeyframes(0.7,0,UIViewKeyframeAnimationOptions.CalculationModeCubic, () =>
            {
                UIView.AddKeyframeWithRelativeStartTime(0,0.6, () =>
                {
                    var layerTransform = questionContainer.Layer.Transform;
                    
                    questionContainer.Layer.Transform = layerTransform
                        .Rotate(new nfloat(Math.PI / 2), 0, 1, 0)
                        .Scale(new nfloat(0.9));

                  resultContainer.Layer.Transform = frontTransform
                      .Rotate(new nfloat(Math.PI * 3 / 2), 0, 1, 0)
                      .Scale(0.9f);

                });

                UIView.AddKeyframeWithRelativeStartTime(0.6, 0.4, () =>
                    {
                        resultContainer.Layer.Transform = frontTransform.Rotate(new nfloat(2 * Math.PI), 0, 1, 0);
                        questionContainer.Layer.Transform = CATransform3D.MakeRotation(new nfloat(Math.PI),0,1,0);
                    });

            }, finished =>
            {
                questionContainer.Layer.Transform = CATransform3D.Identity;
                questionContainer.Hidden = true;
            });
        }

        private void HideResultAndShowNext(QuizCard card)
        {
            questionContainer.Hidden = false;
            resultContainer.Hidden = true;
            _questionViewController.StartOrGoToNextQuiz(Task.FromResult(card));
        }




        public class ViewModel
        {
            public int? ReviewSetId { get; set; }
            public Task<IEnumerable<ReviewSetStartUpInfo>> ReviewSetStartUpInfos { get; set; }
        }


        private struct ReviewCardInfo
        {
            public QuizCard QuizCard { get; set; }
            public int ReviewId { get; set; }
            public int ReviewSetId { get; set; }
            public int? HitReviewId { get; set; }
            public int? MistakeReviewId { get; set; }
        }

    }
}