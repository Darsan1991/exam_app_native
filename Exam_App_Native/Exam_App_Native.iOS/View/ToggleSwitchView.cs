﻿using System;
using UIKit;

namespace Exam_App_Native.iOS.View
{
    public class ToggleSwitchView
    {
        public event EventHandler<bool> ValueChanged; 

        private static readonly UIImage CHECKED_IMAGE = UIImage.FromBundle("check_box_all");
        private static readonly UIImage UNCHECKED_IMAGE = UIImage.FromBundle("check_box_none");

        private readonly UIImageView _imageView;
        private bool? _checked;
        private bool? _enabled;
        private UIGestureRecognizer _recognizer;

        public bool Enabled
        {
            get => _enabled??false;
            set
            {
                if (_enabled==value)
                {
                    return;
                }

                _enabled = value;
                if(value)
                _imageView.AddGestureRecognizer(_recognizer);
                else
                {
                    _imageView.RemoveGestureRecognizer(_recognizer);
                }
            }
        }

        public bool Checked
        {
            get => _checked??false;
            set
            {
                if(_checked==value)
                    return;
                _imageView.Image = value ? CHECKED_IMAGE : UNCHECKED_IMAGE;
                _checked = value;
                ValueChanged?.Invoke(this,value);
            }
        }

        public ToggleSwitchView(UIImageView imageView)
        {
            _imageView = imageView;
            _imageView.UserInteractionEnabled = true;
            _recognizer = new UITapGestureRecognizer(() => { Checked = !Checked; });
            Enabled = true;
            Checked = false;
        }
    }
}