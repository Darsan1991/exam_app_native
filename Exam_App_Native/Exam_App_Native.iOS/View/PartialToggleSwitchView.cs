﻿using System;
using Exam_App_Native.Model.ViewModel;
using UIKit;

namespace Exam_App_Native.iOS.View
{
    public class PartialToggleSwitchView
    {
        public event EventHandler<SelectableState> ValueChanged;

        private static readonly UIImage CHECKED_IMAGE = UIImage.FromBundle("check_box_all");
        private static readonly UIImage UNCHECKED_IMAGE = UIImage.FromBundle("check_box_none");
        private static readonly UIImage PARTIAL_CHECKED_IMAGE = UIImage.FromBundle("check_box_partial");

        private readonly UIGestureRecognizer _recognizer;
        private readonly UIImageView _imageView;

        private bool? _enabled;
        private SelectableState? _state;

        public bool Enabled
        {
            get => _enabled ?? false;
            set
            {
                if (_enabled == value)
                {
                    return;
                }

                _enabled = value;
                if (value)
                    _imageView.AddGestureRecognizer(_recognizer);
                else
                {
                    _imageView.RemoveGestureRecognizer(_recognizer);
                }
            }
        }

        public SelectableState State
        {
            get => _state??SelectableState.None;
            set
            {
                if (_state==value)
                {
                    return;
                }

                UIImage image = null;
                switch (value)
                {
                    case SelectableState.All:
                        image = CHECKED_IMAGE;
                        break;
                    case SelectableState.Partial:
                        image = PARTIAL_CHECKED_IMAGE;
                        break;
                    case SelectableState.None:
                        image = UNCHECKED_IMAGE;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException(nameof(value), value, null);
                }

                _imageView.Image = image;
                _state = value;
                ValueChanged?.Invoke(this,State);
            }
        }

        public PartialToggleSwitchView(UIImageView imageView)
        {
            _imageView = imageView;
            _imageView.UserInteractionEnabled = true;
            _recognizer = new UITapGestureRecognizer(() =>
            {
                State = State == SelectableState.None ? SelectableState.All : SelectableState.None;
            });
            Enabled = true;
            State = SelectableState.None;
        }
    }
}