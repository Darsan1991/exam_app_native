﻿using System;
using System.Linq;
using CoreGraphics;
using UIKit;

namespace Exam_App_Native.iOS.View
{
    public class ActivityIndicator
    {
        public readonly UIView View;
        private readonly UIView _parent;
        private readonly UIActivityIndicatorView _uiActivityIndicatorView;
        private bool _animating;

        private readonly LayoutMode _layoutMode;

        public bool Animating
        {
            get => _animating;
            set
            {
                if (_animating==value)
                {
                    return;
                }

                if (value)
                {
                    _parentUserInteractionEnable = _parent.UserInteractionEnabled;
                    _parent.UserInteractionEnabled = false;
                    _parent.AddSubview(View);
                    View.Frame = new CGRect(0, 0, _parent.Frame.Width, _parent.Frame.Height);
                    if (_layoutMode == LayoutMode.Constraint)
                    {
                        View.TranslatesAutoresizingMaskIntoConstraints = false;
                        View.LeftAnchor.ConstraintEqualTo(_parent.LeftAnchor).Active = true;
                        View.RightAnchor.ConstraintEqualTo(_parent.RightAnchor).Active = true;
                        View.TopAnchor.ConstraintEqualTo(_parent.TopAnchor).Active = true;
                        View.BottomAnchor.ConstraintEqualTo(_parent.BottomAnchor).Active = true;
                        _parent.LayoutIfNeeded();
                    }
                    View.LayoutIfNeeded();
                    _uiActivityIndicatorView.StartAnimating();
                }
                else
                {
                    _parent.UserInteractionEnabled = _parentUserInteractionEnable;
                    View.RemoveFromSuperview();
                    _uiActivityIndicatorView.StopAnimating();
                }

                _animating = value;
            }
        }

        public nfloat Alpha
        {
            get => View.BackgroundColor.CGColor.Alpha;
            set
            {
                var cgColor = View.BackgroundColor.CGColor;
                cgColor = new CGColor(cgColor, value);
                View.BackgroundColor = new UIColor(cgColor);
            }
        }

        private bool _parentUserInteractionEnable;


        public ActivityIndicator(UIView parent=null,float alpha=0f,UIActivityIndicatorViewStyle style = UIActivityIndicatorViewStyle.WhiteLarge,LayoutMode layoutMode = LayoutMode.Constraint,UIColor bgColor = null)
        {
            _parent = parent ?? UIApplication.SharedApplication.Windows.First();
            _layoutMode = layoutMode;
            View = new UIView(_parent.Frame)
            {
                UserInteractionEnabled = true,
                BackgroundColor = bgColor ?? UIColor.White 
            };

            _uiActivityIndicatorView = new UIActivityIndicatorView(style)
            {
                TranslatesAutoresizingMaskIntoConstraints = false,
                Color = UIColor.DarkGray,
            };
            View.AddSubview(_uiActivityIndicatorView);

            _uiActivityIndicatorView.WidthAnchor.ConstraintEqualTo(_uiActivityIndicatorView.Frame.Width).Active = true;
            _uiActivityIndicatorView.HeightAnchor.ConstraintEqualTo(_uiActivityIndicatorView.Frame.Height).Active=true;
            _uiActivityIndicatorView.CenterXAnchor.ConstraintEqualTo(View.CenterXAnchor).Active = true;
            _uiActivityIndicatorView.CenterYAnchor.ConstraintEqualTo(View.CenterYAnchor).Active = true;
            Alpha = alpha;
        }

        public void RefreshLayout()
        {
            if (!Animating)
                return;
            switch (_layoutMode)
            {
                case LayoutMode.Frame:
                    View.Frame = new CGRect(0, 0, _parent.Frame.Width, _parent.Frame.Height);
                    View.LayoutIfNeeded();
                    break;
                case LayoutMode.Constraint:
                    View.LayoutIfNeeded();
                    break;
            }

        }
        public enum LayoutMode{
            Frame,Constraint
        }
    }
}