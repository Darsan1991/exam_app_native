﻿using Foundation;
using System;
using System.ComponentModel;
using CoreAnimation;
using CoreGraphics;
using UIKit;

namespace Exam_App_Native.iOS
{
    [DesignTimeVisible(true)]
    public partial class ShadowButton : UIButton,IComponent
    {
//        public override CALayer Layer
//        {
//            get
//            {
//                base.Layer.ShadowColor = UIColor.Gray.CGColor;
//                base.Layer.ShadowOffset = new CGSize(0, 2);
//                base.Layer.ShadowRadius = 2f;
//                base.Layer.ShadowOpacity = 0.4f;
//                
//                return base.Layer;
//            }
//        }

        [Export("ShadowRadius"), Browsable(true)]
        public nfloat ShadowRadius { get{ return Layer.ShadowRadius; }
            set{ Layer.ShadowRadius = value; }
        }

        [Export("ShadowOpacity"),Browsable(true)]
        public float ShadowOpacity
        {
            get => Layer.ShadowOpacity;
            set
            {
               
                Layer.ShadowOpacity = value;
            }
        }

        [Export("ShadowOffsetX"), Browsable(true)]
        public nfloat ShadowOffsetX
        {
            get =>Layer.ShadowOffset.Width;
            set
            {
                Layer.ShadowOffset = new CGSize(value, Layer.ShadowOffset.Height);
            }
        } 

        [Export("ShadowOffsetY"), Browsable(true)]
        public nfloat ShadowOffsetY
        {
            get => Layer.ShadowOffset.Height;
            set
            {
                Layer.ShadowOffset = new CGSize(Layer.ShadowOffset.Width,value);
            }
        }

        [Export("ShadowColor"),Browsable(true)]
        public UIColor ShadowColor
        {
            get => new UIColor(Layer.ShadowColor);
            set => Layer.ShadowColor = value.CGColor;
        }


        public ISite Site { get; set; }
        public event EventHandler Disposed;


//
        public ShadowButton (IntPtr handle) : base (handle)
        {
//            InitialSetupForView();
        }

        

//
//        public ShadowButton(UIButtonType type) : base(type)
//        {
//            InitialSetupForView();
//        }
//
//        public ShadowButton()
//        {
//            InitialSetupForView();
//        }
//
//        public ShadowButton(NSCoder coder) : base(coder)
//        {
//            InitialSetupForView();
//        }

        public override void AwakeFromNib()
        {
            base.AwakeFromNib();
//            InitialSetupForView();

        }

//        protected ShadowButton(NSObjectFlag t) : base(t)
//        {
//            InitialSetupForView();
//        }
//
//        public ShadowButton(CGRect frame) : base(frame)
//        {
//            InitialSetupForView();
//        }

        private void InitialSetupForView()
        {
            Layer.ShadowColor = UIColor.Gray.CGColor;
//            Layer.ShadowOffset = new CGSize(ShadowOffsetX, ShadowOffsetY);
//            Layer.ShadowRadius = 2f;
//            Layer.ShadowOpacity = 0.5f;
        }


    }
}