﻿using Foundation;
using System;
using Exam_App_Native.iOS.View;
using Facebook.CoreKit;
using Facebook.LoginKit;
using UIKit;

namespace Exam_App_Native.iOS
{
    public partial class LogInViewController : UIViewController
    {
        private ActivityIndicator _activityIndicator;

        public LogInViewController (IntPtr handle) : base (handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            _activityIndicator = new ActivityIndicator(View);

            facebookViewGroup.AddGestureRecognizer(new UITapGestureRecognizer((obj) => {
                FacebookLogin();
            }));
        }

        public override void TouchesEnded(NSSet touches, UIEvent evt)
        {
            base.TouchesEnded(touches, evt);
            View.EndEditing(true);
        }

       async partial void LogInButton_TouchUpInside(UIButton sender)
        {
            string error = null;

            if (!Exam_App_Native.Utils.IsValidEmail(emailLabel.Text))
            {
                error = "Email Not Valid!";
            }
            else if (passwordLabel.Text?.Length < 6)
            {
                error = "Password must have atleast 6 characters.";
            }

            if (error != null)
            {
                IosExamApp.ShowToastAlert("Error!",error,this);
                return;
            }

            _activityIndicator.Alpha =0.4f;
            _activityIndicator.Animating = true;

            try
            {
                var user = await Auth.Instance.LogIn(emailLabel.Text, passwordLabel.Text, true);
                OnLoginCompleted();
            }
            catch (NotConfirmedException e)
            {
                IosExamApp.ShowToastAlert("Error!","The User is already signup but not confirmed",this, () =>
                {
                    var storyboard = UIStoryboard.FromName("CodeConfirmationVC",null);
                    var controller = storyboard.InstantiateViewController("CodeConfirmationVC");
                    PresentViewController(controller,true,null);
                    DismissViewController(false,null);
                });

            }
            catch (Exception e)
            {
                IosExamApp.ShowToastAlert("Error!",e.Message,this);
            }

            _activityIndicator.Animating = false;
        }

        private void OnLoginCompleted()
        {
            var storyboard = UIStoryboard.FromName("Main", null);
            var controller = storyboard.InstantiateViewController("MyHanBurgerVC");
            PresentViewController(controller, true, null);
        }

        partial void SignUpButton_TouchUpInside(UIButton sender)
        {
            PerformSegue("SignUpViewController",null);
            //DismissViewController(false,null);
        }

        partial void FogotPasswordButton_TouchUpInside(UIButton sender)
        {
            var storyboard = UIStoryboard.FromName("Main",null);
            var viewController = storyboard.InstantiateViewController("ResetPasswordVC");
            PresentViewController(viewController,true,null);
        }

         void FacebookLogin()
        {
            _activityIndicator.Alpha = 0.4f;
            _activityIndicator.Animating = true;
            var loginManager = new LoginManager();
            loginManager.LogInWithReadPermissions(new[] { "public_profile", "email" }, this,async (result, error) =>
            {
                
                if (error != null)
                {
                    IosExamApp.ShowToastAlert("Error!", "Something went wrong!", this);
                    _activityIndicator.Animating = false;
                    return;
                }

                if (AccessToken.CurrentAccessToken!=null)
                {
                    var tokenString = AccessToken.CurrentAccessToken.TokenString;
                    await Auth.Instance.LogIn(tokenString);
                    OnLoginCompleted();
                }

                _activityIndicator.Animating = false;
            });
        }
    }
}