using Foundation;
using System;
using AVFoundation;
using CoreAnimation;
using CoreGraphics;
using UIKit;

namespace Exam_App_Native.iOS
{
    public partial class ProgressBar : UIView
    {
        private CAShapeLayer shapeLayer = new CAShapeLayer();
        private CAShapeLayer shapeBgLayer = new CAShapeLayer();

        public UILabel Label { get; } = new UILabel
        {
            Font = UIFont.PreferredTitle2.WithSize(20),
            TextColor = UIColor.Cyan,
        };

        public nfloat LineWidth
        {
            get => shapeLayer.LineWidth;
            set
            {
                shapeBgLayer.LineWidth = value;
                shapeLayer.LineWidth = value;
            }
        }

        public UIColor StokeBgColor
        {
            get=> new UIColor(shapeBgLayer.StrokeColor);
            set => shapeBgLayer.StrokeColor = value.CGColor;
        }

        public UIColor StokeColor
        {
            get => new UIColor(shapeLayer.StrokeColor);
            set => shapeLayer.StrokeColor = value.CGColor;
        }

        public float MaxProgress { get; set; } = 1f;
        public float Progress { get; private set; }

        public ProgressBar (IntPtr handle) : base (handle)
        {
            LineWidth = 6;
            StokeColor = new UIColor(71f / 255, 142f / 255, 203f / 255, 1f);
            StokeBgColor = new UIColor(235f/255,238f/255,239f/255,1f);
        }


        public override void AwakeFromNib()
        {
            base.AwakeFromNib();

            BackgroundColor = UIColor.Clear;

            AddSubview(Label);
            Label.TranslatesAutoresizingMaskIntoConstraints = false;
            Label.CenterXAnchor.ConstraintEqualTo(CenterXAnchor).Active = true;
            Label.CenterYAnchor.ConstraintEqualTo(CenterYAnchor).Active = true;
            Label.Text = "50%";
            Label.TextColor = StokeColor;//new UIColor(71f / 255, 142f / 255, 203f / 255, 1f);


            shapeBgLayer.Bounds = new CGRect(-Frame.Width / 2, -Frame.Height / 2, Frame.Width, Frame.Height);
           
            shapeBgLayer.Frame = new CGRect(0, 0, Frame.Width, Frame.Height);
            shapeBgLayer.Path = UIBezierPath//.FromOval(shapeLayer.Bounds).CGPath;
                .FromArc(new CGPoint(0, 0), (Frame.Width / 2 - LineWidth / 2), (nfloat)(-Math.PI / 2), (nfloat)(3 / 2f * Math.PI), true).CGPath;

            shapeBgLayer.LineCap = CAShapeLayer.CapRound;
            shapeBgLayer.StrokeEnd = 1f;
            shapeBgLayer.FillColor = UIColor.Clear.CGColor;
            Layer.AddSublayer(shapeBgLayer);


            shapeLayer.Bounds = new CGRect(-Frame.Width/2,-Frame.Height/2,Frame.Width,Frame.Height);
            shapeLayer.Frame = new CGRect(0,0,Frame.Width,Frame.Height);
            shapeLayer.Path = UIBezierPath//.FromOval(shapeLayer.Bounds).CGPath;
                .FromArc(new CGPoint(0,0), (Frame.Width/2-LineWidth/2), (nfloat)(-Math.PI / 2), (nfloat)(3/2f * Math.PI), true).CGPath;
            
            shapeLayer.LineCap = CAShapeLayer.CapRound;
            shapeLayer.StrokeEnd = 0.5f;
            shapeLayer.FillColor = UIColor.Clear.CGColor;
            Layer.AddSublayer(shapeLayer);
//            Layer.BackgroundColor = UIColor.Gray.CGColor;
//            SetNeedsDisplay();
        }

        public void SetProgress(float progress, bool animate = true)
        {
            //ignore the animation for now

            var progressRelative =  progress / MaxProgress;
            shapeLayer.StrokeEnd = progressRelative;
            Label.Text = (int) (progressRelative * 100) + "%";

        }

//        public override void LayoutSubviews()
//        {
//            base.LayoutSubviews();
//            shapeLayer.Bounds = Bounds;
//        }

//        public override void Draw(CGRect rect)
//        {
////            base.Draw(rect);
//            var bezierPath = UIBezierPath.FromOval(rect.Inset(5,5));
//            bezierPath.LineWidth = 10f;
//            UIColor.Cyan.SetStroke();
//            bezierPath.Stroke();
//           
//
//        }
    }
}