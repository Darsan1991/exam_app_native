﻿using Foundation;
using System;
using Exam_App_Native.iOS.View;
using Exam_App_Native.Model;
using UIKit;

namespace Exam_App_Native.iOS
{
    // ReSharper disable once InconsistentNaming
    public partial class ResetPasswordVC : UIViewController
    {
        private ActivityIndicator _activityIndicator;

        public ResetPasswordVC (IntPtr handle) : base (handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            _activityIndicator = new ActivityIndicator(View,0.4f);
        }

        public override void TouchesEnded(NSSet touches, UIEvent evt)
        {
            base.TouchesEnded(touches, evt);
            View.EndEditing(true);
        }

        async partial void ResetButton_TouchUpInside(ShadowButton sender)
        {
            string error = null;
            if (!Exam_App_Native.Utils.IsValidEmail(emailLabel.Text ?? ""))
            {
                error = "Email is not valid!";
            }

            if (error != null)
            {
                IosExamApp.ShowToastAlert("Error!",error,this);
                return;
            }

            _activityIndicator.Animating = true;
            try
            {
                await User.ResetPassword(emailLabel.Text);
                IosExamApp.ShowToastAlert("Success!","Check You Email for new password!",this, () =>
                {
                    DismissViewController(true,null);
                });
               
            }
            catch (Exception e)
            {
                IosExamApp.ShowToastAlert("Error!",e.Message,this);
            }

            _activityIndicator.Animating = false;
        }
    }
}