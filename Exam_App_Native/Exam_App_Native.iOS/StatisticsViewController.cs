﻿using Foundation;
using System;
using System.Linq;
using System.Threading.Tasks;
using CoreGraphics;
using Exam_App_Native.iOS.View;
using Exam_App_Native.Model;
using UIKit;

namespace Exam_App_Native.iOS
{
    public partial class StatisticsViewController : UIViewController
    {
        private ExamPerformancePageVC _examPageController;
        private DisciplinePerformancePageVC _disciplinePageController;
        private ActivityIndicator _activityIndicator;
        private ToggleSwitchView _trendLineToggle;

        private bool ShowTrendLine => _trendLineToggle?.Checked ?? false;

        private UIView _noStatsView;

        public StatisticsViewController (IntPtr handle) : base (handle)
        {
        }



        partial void HandBurgerBtn_Activated(UIBarButtonItem sender)
        {
            MyHanBurgerVC.Instance?.Toggle();
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            Title = NSBundle.MainBundle.LocalizedString(LocalizableWords.STATICS_CONTROLLER_TITLE, "Statics");

            _activityIndicator = new ActivityIndicator(View,1);
            _trendLineToggle = new ToggleSwitchView(trendLineToggleImgView);

            using (var storyboard = UIStoryboard.FromName("Main",null))
            {
                _examPageController = (ExamPerformancePageVC)storyboard.InstantiateViewController("ExamPerformancePageVC");
                _disciplinePageController = (DisciplinePerformancePageVC)storyboard.InstantiateViewController("DisciplinePerformancePageVC");
            }

            examPerformanceContainer.AddSubview(_examPageController.View);
            _examPageController.View.TranslatesAutoresizingMaskIntoConstraints = false;
            _examPageController.View.LeftAnchor.ConstraintEqualTo(examPerformanceContainer.LeftAnchor).Active = true;
            _examPageController.View.RightAnchor.ConstraintEqualTo(examPerformanceContainer.RightAnchor).Active = true;
            _examPageController.View.TopAnchor.ConstraintEqualTo(examPerformanceContainer.TopAnchor).Active = true;
            _examPageController.View.BottomAnchor.ConstraintEqualTo(examPerformanceContainer.BottomAnchor).Active = true;
            _examPageController.DidMoveToParentViewController(this);


            disciplinePerformanceContainer.AddSubview(_disciplinePageController.View);
            _disciplinePageController.View.Frame = new CGRect(0,0,disciplinePerformanceContainer.Frame.Width,disciplinePerformanceContainer.Frame.Height);
            _disciplinePageController.View.TranslatesAutoresizingMaskIntoConstraints = false;
            _disciplinePageController.View.LeftAnchor.ConstraintEqualTo(disciplinePerformanceContainer.LeftAnchor).Active = true;
            _disciplinePageController.View.RightAnchor.ConstraintEqualTo(disciplinePerformanceContainer.RightAnchor).Active = true;
            _disciplinePageController.View.TopAnchor.ConstraintEqualTo(disciplinePerformanceContainer.TopAnchor).Active = true;
            _disciplinePageController.View.BottomAnchor.ConstraintEqualTo(disciplinePerformanceContainer.BottomAnchor).Active = true;

            _disciplinePageController.DidMoveToParentViewController(this);

            _disciplinePageController.ShowTrendLine = ShowTrendLine;

            _trendLineToggle.ValueChanged += (sender, args) =>
                {
                    _disciplinePageController.ShowTrendLine = ShowTrendLine;
                };


            _noStatsView = new UIView()
            {
                TranslatesAutoresizingMaskIntoConstraints = false,
                BackgroundColor = UIColor.White
            };

            View.AddSubview(_noStatsView);

            _noStatsView.LeftAnchor.ConstraintEqualTo(View.LeftAnchor).Active = true;
            _noStatsView.RightAnchor.ConstraintEqualTo(View.RightAnchor).Active = true;
            _noStatsView.TopAnchor.ConstraintEqualTo(View.TopAnchor).Active = true;
            _noStatsView.BottomAnchor.ConstraintEqualTo(View.BottomAnchor).Active = true;

            var label = new UILabel()
            {
                TextColor = UIColor.Gray,
                Text = "No Statistics",
                Font = UIFont.SystemFontOfSize(12),
                TranslatesAutoresizingMaskIntoConstraints = false
            };

            _noStatsView.AddSubview(label);
            label.CenterXAnchor.ConstraintEqualTo(View.CenterXAnchor).Active = true;
            label.CenterYAnchor.ConstraintEqualTo(View.CenterYAnchor).Active = true;
        }

        public override async void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);
            _activityIndicator.Animating = true;
            try
            {
                await LoadDisciplineTimeSeries();
                await LoadExamTimeSeries();
            }
            catch (Exception e)
            {
                _disciplinePageController.MViewModel = null;
                _examPageController.MViewModel = null;

                IosExamApp.ShowToastAlert("Error!","Something went wrong!",this);
            }

            _noStatsView.Hidden = ((_disciplinePageController.MViewModel?.DisciplineTimeSeries.Any() ?? false) || (_examPageController.MViewModel?.ExamPerformances.Any() ?? false));

            _activityIndicator.Animating = false;
        }

        private async Task LoadDisciplineTimeSeries()
        {

                var disciplineTimeSeries = await Auth.Instance.CurrentUser.GetDisciplineTimeSeries();

                InvokeOnMainThread(() =>
                {
                    _disciplinePageController.MViewModel = new DisciplinePerformancePageVC.ViewModel
                    {
                        DisciplineTimeSeries = disciplineTimeSeries
                    };
                    
                });


        }

        private async Task LoadExamTimeSeries()
        {

                var examTimeSeries = await Auth.Instance.CurrentUser.GetExamPerformances();

                InvokeOnMainThread(() =>
                {
                    _examPageController.MViewModel = new ExamPerformancePageVC.ViewModel
                    {
                        ExamPerformances = examTimeSeries
                    };
                });


        }
    }

}