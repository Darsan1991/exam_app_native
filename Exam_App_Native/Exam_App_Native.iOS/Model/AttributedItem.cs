﻿using Foundation;

namespace Exam_App_Native.iOS.Model
{
    public class AttributedItem<T> : IAttributedItem<T>
    {
        public NSAttributedString Title { get; set; }
        public NSAttributedString SubTitle { get; set; }
        public T Data { get; set; }
    }


}