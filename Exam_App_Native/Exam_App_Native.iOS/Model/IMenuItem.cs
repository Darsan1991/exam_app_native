﻿namespace Exam_App_Native.iOS.Model
{
    public interface IMenuItem
    {
         MenuType MenuType { get; }
    }

    public class MenuItem : IMenuItem
    {
        public MenuType MenuType { get; set; }

        
    }

    public enum MenuType
    {
        ACCOUNT,TEACHERS,PLANS,LOGOUT
    }
}