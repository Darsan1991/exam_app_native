﻿using Foundation;

namespace Exam_App_Native.iOS.Model
{
    public interface IAttributedItem<T>
    {
        T Data { get; set; }
        NSAttributedString SubTitle { get; set; }
        NSAttributedString Title { get; set; }
    }
}