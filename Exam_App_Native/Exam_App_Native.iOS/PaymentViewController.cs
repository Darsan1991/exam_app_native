﻿using Foundation;
using System;
using System.Linq;
using Exam_App_Native.iOS.Adapter;
using Exam_App_Native.iOS.View;
using Exam_App_Native.Model;
using Exam_App_Native.Model.ViewModel;
using Ricardo.Stripe.iOS;
using UIKit;
using CoreGraphics;
using InputMask.Classes.View;

namespace Exam_App_Native.iOS
{
    public partial class PaymentViewController : UIViewController,IMaskedTextFieldDelegateListener
    {
        private const string PUBLISH_STRIPE_KEY = "pk_test_mJf4KS1A3f0694WniMD5kbX5";

        private  PaymentOptions _mPaymentOptions;
        private ActivityIndicator _activityIndicator;
        private ToggleSwitchView _autoRenewalSwitch;
        private SelectableTableViewAdapter<IItem, OrderOptionsViewCell> _adapter;

        private PaymentPlan SelectedPlan => _adapter.Selected?.Data as PaymentPlan;
        private Auth Auth => Auth.Instance;

        private PaymentOptions MPaymentOptions
        {
            get => _mPaymentOptions;
            set
            {
                _mPaymentOptions = value;
                if (IsViewLoaded)
                {
                    creditView.Hidden = value != PaymentOptions.CreditCard;
                    bankSlipView.Hidden = value != PaymentOptions.BankSlip;
                }
            }
        }

        private UITextField _currentOrLastActiveInputField;
        private NSObject _keyboardHideObserver;
        private NSObject _keyboardShowObserver;
        private nfloat _normalTopConstraintValue;
        private nfloat _normalBottomConstraintValue;

        private CGSize? _lastKeyboardSize;

        public PaymentViewController (IntPtr handle) : base (handle)
        {
        }

        public override async void ViewDidLoad()
        {
            Stripe.DefaultPublishableKey = PUBLISH_STRIPE_KEY;

            var bundle = NSBundle.MainBundle;

            NavigationItem.Title = bundle.LocalizedString(LocalizableWords.PAYMENT_CONTROLLER_TITLE,"Payment");

            if (NavigationController.NavigationBar.TopItem.BackBarButtonItem != null)
            {
                NavigationController.NavigationBar.TopItem.BackBarButtonItem.Title = NSBundle.MainBundle.LocalizedString(LocalizableWords.BACK,"Back");
            }

            _autoRenewalSwitch = new ToggleSwitchView(autoRenewalImg);
            bankSlipLabel.Text = "";
            tableView.ScrollEnabled = false;
            copyCodeBtn.TouchUpInside +=CopyCodeBtnOnTouchUpInside;
            generateBankSlipBtn.TouchUpInside += GenerateBankSlipBtnOnTouchUpInside;
            sendByEmailBtn.TouchUpInside +=SendByEmailBtnOnTouchUpInside;
            MPaymentOptions = PaymentOptions.CreditCard;
            segmentCtrl.ValueChanged+=SegmentCtrlOnValueChanged;
           
            segmentCtrl.SetTitle(bundle.LocalizedString(LocalizableWords.CREDIT_CARD_TEXT, "Credit Card"), 0);
            segmentCtrl.SetTitle(bundle.LocalizedString(LocalizableWords.BANK_SLIP_TEXT, "Bank Slip"), 1);
            _activityIndicator = new ActivityIndicator(View,1f);


            _activityIndicator.Alpha = 1f;
            _activityIndicator.Animating = true;
            try
            {
                var plans = await ExamApp.Instance.GetPaymentPlans();

                _adapter = new SelectableTableViewAdapter<IItem,OrderOptionsViewCell>("OrderOptionsViewCell", plans.Select(
                    plan => new Item
                    {
                        Title = plan.Name,
                        SubTitle = plan.Amount,
                        Data = plan
                    }));

                _adapter.Selected = _adapter.Items.First();
                tableView.DataSource = _adapter;
                tableView.TableFooterView = new UIView();
                tableView.Delegate = _adapter;
                tableView.ReloadData();
            }
            catch (Exception e)
            {
                IosExamApp.ShowToastAlert("Error!", "Some thing went wrong!",this, () =>
                    {
                        NavigationController.PopViewController(true);
                    });
                return;
            }
            _activityIndicator.Animating = false;

            _normalTopConstraintValue = topConstraint.Constant;
            _normalBottomConstraintValue = bottomConstraint.Constant;


            cardHolderNameET.EditingDidBegin +=(sender, e) => SetCurrentFocusedTextField((UITextField)sender);          
            cardNumberET.EditingDidBegin +=(sender, e) => SetCurrentFocusedTextField((UITextField)sender);          
            expiryDateET.EditingDidBegin +=(sender, e) => SetCurrentFocusedTextField((UITextField)sender);          
            ccvET.EditingDidBegin +=(sender, e) => SetCurrentFocusedTextField((UITextField)sender);          
            bankSlipCPFTF.EditingDidBegin +=(sender, e) => SetCurrentFocusedTextField((UITextField)sender);          
            creditCardCPFET.EditingDidBegin +=(sender, e) => SetCurrentFocusedTextField((UITextField)sender);

            cardNumberET.KeyboardType = UIKeyboardType.NumberPad;
            expiryDateET.KeyboardType = UIKeyboardType.NumberPad;
            bankSlipCPFTF.KeyboardType = UIKeyboardType.NumberPad;
            creditCardCPFET.KeyboardType = UIKeyboardType.NumberPad;
            ccvET.KeyboardType = UIKeyboardType.NumberPad;

            cardHolderNameET.Delegate = new TextFieldDelegate{MaxLetterCount = 50};
            cardNumberET.Delegate = new TextFieldDelegate { MaxLetterCount = 16 };
            var maskedDelegate = new MaskedTextFieldDelegate("[00]/[0000]");
            maskedDelegate.Listener = this;
            expiryDateET.Delegate = maskedDelegate;//new TextFieldDelegate { MaxLetterCount = 7 };
            ccvET.Delegate = new TextFieldDelegate { MaxLetterCount = 3 };
            creditCardCPFET.Delegate = new TextFieldDelegate { MaxLetterCount = 11 };
            bankSlipCPFTF.Delegate = new TextFieldDelegate { MaxLetterCount = 11 };

            _keyboardShowObserver = NSNotificationCenter.DefaultCenter.AddObserver(UIKeyboard.WillShowNotification,(NSNotification obj) => {
                var size = ((NSValue)obj.UserInfo[UIKeyboard.FrameEndUserInfoKey]).CGRectValue.Size;
                OnShowKeyBoard(size);
            });

           _keyboardHideObserver = NSNotificationCenter.DefaultCenter.AddObserver(UIKeyboard.WillHideNotification, (NSNotification obj) => {
                OnHideKeyBoard();
            });

        
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);
           
            tableViewHeightConstraint.Constant = tableView.ContentSize.Height;
        }

        public override void ViewDidLayoutSubviews()
        {
            base.ViewDidLayoutSubviews();
            tableViewHeightConstraint.Constant = tableView.ContentSize.Height;
        }

        protected override void Dispose(bool disposing)
        {
            NSNotificationCenter.DefaultCenter.RemoveObserver(_keyboardShowObserver);
            NSNotificationCenter.DefaultCenter.RemoveObserver(_keyboardHideObserver);
            base.Dispose(disposing);

        }

        private void SetCurrentFocusedTextField(UITextField field)
        {
            _currentOrLastActiveInputField = field;

            if(_lastKeyboardSize!=null)
            {
                OnShowKeyBoard((CGSize)_lastKeyboardSize);
            }
        }

        public override void TouchesEnded(NSSet touches, UIEvent evt)
        {
            base.TouchesEnded(touches, evt);
            View.EndEditing(true);
        }

        private void OnShowKeyBoard(CGSize size)
        {
            View.Layer.RemoveAllAnimations();
            _lastKeyboardSize = size;
            var lastMoved = bottomConstraint.Constant - _normalBottomConstraintValue;

            var point = _currentOrLastActiveInputField.ConvertPointToView(new CGPoint(_currentOrLastActiveInputField.Center.X+0,_currentOrLastActiveInputField.Center.Y +_currentOrLastActiveInputField.Bounds.Height/2),View);
            var moveVal = + (nfloat)Math.Max((point.Y + size.Height + lastMoved) - View.Bounds.Height, 0) + 50;
            bottomConstraint.Constant = _normalBottomConstraintValue + moveVal; 
            topConstraint.Constant = _normalTopConstraintValue - moveVal;

            UIView.Animate(0.5f,() => {
                View.LayoutIfNeeded();
            });
        }

        void OnHideKeyBoard()
        {
            bottomConstraint.Constant = _normalBottomConstraintValue ;
            topConstraint.Constant = _normalTopConstraintValue ;
            UIView.Animate(0.5f, () => {
                View.LayoutIfNeeded();
            });
        }

        private void SegmentCtrlOnValueChanged(object sender, EventArgs eventArgs)
        {
            MPaymentOptions = segmentCtrl.SelectedSegment == 0 ? PaymentOptions.CreditCard : PaymentOptions.BankSlip;
        }

        private async void SendByEmailBtnOnTouchUpInside(object sender, EventArgs eventArgs)
        {
            string error = null;

            if (string.IsNullOrWhiteSpace(bankSlipLabel.Text))
            {
                error = "Please Generate BankSlipCode before email";
            }

            if (error != null)
            {
                IosExamApp.ShowToastAlert("Error!",error,this);
                return;
            }

            _activityIndicator.Alpha = 0.3f;
            _activityIndicator.Animating = true;
            

            try
            {
                var messageAndResult = await Auth.CurrentUser.SendBankSlipCodeToEmail();
                IosExamApp.ShowToastAlert("Success!",messageAndResult.Message,this);
            }
            catch (Exception e)
            {
                IosExamApp.ShowToastAlert("Error!", "Something went wrong!", this);
            }

            _activityIndicator.Animating = false;


        }

        private async void GenerateBankSlipBtnOnTouchUpInside(object sender, EventArgs eventArgs)
        {

            string error = null;

            if (string.IsNullOrWhiteSpace(bankSlipCPFTF.Text))
            {
                error = "Some of the Field are Empty!";
            }

            if (error != null)
            {
                IosExamApp.ShowToastAlert("Error!",error,this);
                return;
            }

            _activityIndicator.Alpha = 0.3f;
            _activityIndicator.Animating = true;

            try
            {
                var bankSlip = await Auth.CurrentUser.GenerateBankSlip(SelectedPlan.Id, bankSlipCPFTF.Text);
                bankSlipLabel.Text = bankSlip.Code;
            }
            catch (Exception e)
            {
                IosExamApp.ShowToastAlert("Error!", "Something went wrong!", this);
            }

            _activityIndicator.Animating = false;
        }

        private void CopyCodeBtnOnTouchUpInside(object sender, EventArgs eventArgs)
        {
            string error = null;
            if (string.IsNullOrWhiteSpace(bankSlipLabel.Text))
            {
                error = "Please Generate BankSlip Code before Copy!.";
            }

            if (error != null)
            {
                IosExamApp.ShowToastAlert("Error!",error,this);
                return;
            }

            UIPasteboard.General.String = bankSlipLabel.Text;
            IosExamApp.ShowToastAlert("Success!","Copied to Clipboard!",this);
        }


        partial void CheckoutBtn_TouchUpInside(UIButton sender)
        {
            var splits = expiryDateET.Text.Split('/');

            string error = null;
            int year = 0;
            int month = 0;

            if (Auth.Instance.CurrentUser.Subscribed)
            {
                error = "Already Subscribed.Please UnSubscribed Current Before Order again!";
            }
            else if (string.IsNullOrWhiteSpace(cardHolderNameET.Text) || string.IsNullOrWhiteSpace(cardNumberET.Text) || string.IsNullOrWhiteSpace(ccvET.Text) || string.IsNullOrWhiteSpace(expiryDateET.Text) || string.IsNullOrWhiteSpace(creditCardCPFET.Text))
            {
                error = "Some of the field are Empty!";
            }
            else if (cardNumberET.Text.Length != 13 && cardNumberET.Text.Length != 16)
            {
                error = "The Card no is Wrong";
            }
            else if (!int.TryParse(splits[0], out month))
            {
                error = "The Month Cannot be Detected";
            }
            else if (!int.TryParse(splits[1], out year))
            {
                error = "The Year Cannot be Detected";
            }

            if (error != null)
            {
                IosExamApp.ShowToastAlert("Error!",error,this);
                return;
            }

            _activityIndicator.Alpha = 0.3f;
            _activityIndicator.Animating = true;

            STPAPIClient.SharedClient().CreateTokenWithCard(new STPCardParams
            {
                Number = cardNumberET.Text,
                ExpMonth = (nuint) month,
                ExpYear = (nuint) year,
                Cvc = ccvET.Text
            }, async (token, nsError) =>
            {
                if (nsError != null)
                {
                    IosExamApp.ShowToastAlert("Error!", "Something went wrong!", this);
                    _activityIndicator.Animating = false;
                    return;
                }

                try
                {
                    await Auth.Instance.CurrentUser.Subscribe(((PaymentPlan)_adapter.Selected.Data).Id, token.TokenId,
                        creditCardCPFET.Text, _autoRenewalSwitch.Checked);

                    IosExamApp.ShowToastAlert("Success","You have subscribed Successfully!",this,() => {
                        NavigationController.PopToRootViewController(true);
                    });

                }
                catch (Exception e)
                {
                    IosExamApp.ShowToastAlert("Error!", e.Message, this);
                }

                _activityIndicator.Animating = false;
            });

        }

        public void TextField(IUITextInput textField, bool complete, string value)
        {
            
        }

        [Export("textField:shouldChangeCharactersInRange:replacementString:")]
        public bool ShouldChangeCharacters(UITextField textField, Foundation.NSRange range, string replacementString)
        {
            return true;
        }

    }

    public partial class PaymentViewController
    {
        public enum PaymentOptions
        {
            CreditCard,BankSlip
        }
    }

    public partial class  PaymentViewController
    {
        
    }

    public class TextFieldDelegate:UITextFieldDelegate
    {

        public int MaxLetterCount
        {
            get;
            set;
        }

        public override bool ShouldChangeCharacters(UITextField textField, NSRange range, string replacementString)
        {
            return !(textField.Text.Length >= MaxLetterCount && range.Length == 0);
        }

    }
}