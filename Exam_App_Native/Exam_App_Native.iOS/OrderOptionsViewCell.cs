using Foundation;
using System;
using UIKit;
using Exam_App_Native.Model.ViewModel;
using Exam_App_Native.Model;

namespace Exam_App_Native.iOS
{
    public partial class OrderOptionsViewCell : UITableViewCell,ISelectableHolder<object>
    {
        private readonly UIColor _selectedColor = new UIColor(0, 0, 1, 0.07f);

        public object Item
        {
            get => _item; set
            {
                if (value is IItem item)
                {
                    _item = item;
                    if (IsViewLoaded)
                    {
                        titleLabel.Text = item.Title;
                        subtitleLabel.Text = item.SubTitle;
                    }

                }
            }
        }

        public bool SelectedItem
        {
            get => _selectedItem; set
            {
                _selectedItem = value;
                if (IsViewLoaded)
                {
                    radioImageView.Image = value ? UIImage.FromBundle("radio_btn_selected_1") : UIImage.FromBundle("radio_btn_unselected_1");
                    BackgroundColor = value ? _selectedColor : UIColor.Clear;
                }
            }
        }

        public bool IsViewLoaded { get; private set; }

        private bool _selectedItem;
        private IItem _item;

        public OrderOptionsViewCell (IntPtr handle) : base (handle)
        {
        }

        public override void AwakeFromNib()
        {
            base.AwakeFromNib();
            IsViewLoaded = true;


            //TODO:Try to solve in different way
            SelectedItem = SelectedItem;
            Item = Item;
        }
    }
}