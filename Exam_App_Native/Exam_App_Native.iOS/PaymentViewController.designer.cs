// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace Exam_App_Native.iOS
{
    [Register ("PaymentViewController")]
    partial class PaymentViewController
    {
        [Outlet]
        UIKit.NSLayoutConstraint bottomConstraint { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView autoRenewalImg { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView bandIV { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField bankSlipCPFTF { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel bankSlipLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView bankSlipView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField cardHolderNameET { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField cardNumberET { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField ccvET { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        Exam_App_Native.iOS.ShadowButton checkoutBtn { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        Exam_App_Native.iOS.ShadowButton copyCodeBtn { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField creditCardCPFET { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView creditView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField expiryDateET { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        Exam_App_Native.iOS.ShadowButton generateBankSlipBtn { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UISegmentedControl segmentCtrl { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        Exam_App_Native.iOS.ShadowButton sendByEmailBtn { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITableView tableView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.NSLayoutConstraint tableViewHeightConstraint { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.NSLayoutConstraint topConstraint { get; set; }


        [Action ("CheckoutBtn_TouchUpInside:")]
        partial void CheckoutBtn_TouchUpInside (UIKit.UIButton sender);

        void ReleaseDesignerOutlets ()
        {
            if (autoRenewalImg != null) {
                autoRenewalImg.Dispose ();
                autoRenewalImg = null;
            }

            if (bandIV != null) {
                bandIV.Dispose ();
                bandIV = null;
            }

            if (bankSlipCPFTF != null) {
                bankSlipCPFTF.Dispose ();
                bankSlipCPFTF = null;
            }

            if (bankSlipLabel != null) {
                bankSlipLabel.Dispose ();
                bankSlipLabel = null;
            }

            if (bankSlipView != null) {
                bankSlipView.Dispose ();
                bankSlipView = null;
            }

            if (bottomConstraint != null) {
                bottomConstraint.Dispose ();
                bottomConstraint = null;
            }

            if (cardHolderNameET != null) {
                cardHolderNameET.Dispose ();
                cardHolderNameET = null;
            }

            if (cardNumberET != null) {
                cardNumberET.Dispose ();
                cardNumberET = null;
            }

            if (ccvET != null) {
                ccvET.Dispose ();
                ccvET = null;
            }

            if (checkoutBtn != null) {
                checkoutBtn.Dispose ();
                checkoutBtn = null;
            }

            if (copyCodeBtn != null) {
                copyCodeBtn.Dispose ();
                copyCodeBtn = null;
            }

            if (creditCardCPFET != null) {
                creditCardCPFET.Dispose ();
                creditCardCPFET = null;
            }

            if (creditView != null) {
                creditView.Dispose ();
                creditView = null;
            }

            if (expiryDateET != null) {
                expiryDateET.Dispose ();
                expiryDateET = null;
            }

            if (generateBankSlipBtn != null) {
                generateBankSlipBtn.Dispose ();
                generateBankSlipBtn = null;
            }

            if (segmentCtrl != null) {
                segmentCtrl.Dispose ();
                segmentCtrl = null;
            }

            if (sendByEmailBtn != null) {
                sendByEmailBtn.Dispose ();
                sendByEmailBtn = null;
            }

            if (tableView != null) {
                tableView.Dispose ();
                tableView = null;
            }

            if (tableViewHeightConstraint != null) {
                tableViewHeightConstraint.Dispose ();
                tableViewHeightConstraint = null;
            }

            if (topConstraint != null) {
                topConstraint.Dispose ();
                topConstraint = null;
            }
        }
    }
}