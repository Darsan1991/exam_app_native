﻿using Foundation;
using System;
using System.Linq;
using System.Threading.Tasks;
using CoreText;
using Exam_App_Native.iOS.Adapter;
using Exam_App_Native.iOS.General;
using Exam_App_Native.iOS.Model;
using Exam_App_Native.iOS.View;
using Exam_App_Native.Model;
using Exam_App_Native.Model.ViewModel;
using UIKit;

namespace Exam_App_Native.iOS
{
    // ReSharper disable once InconsistentNaming
    public partial class TeacherDetailsVC : UIViewController
    {
        public ViewModel MViewModel { get; set; }

        private ActivityIndicator _imageActivityIndicator;
        private ActivityIndicator _activityIndicator;


        public TeacherDetailsVC (IntPtr handle) : base (handle)
        {

        }

        public override async void ViewDidLoad()
        {
            base.ViewDidLoad();


            if(NavigationController==null)
                throw new InvalidOperationException("This Should be present inside the Navigation Controller.");

            var adapter = new SimpleTableViewAdapter<IItem<object>,TeacherDetailsCardViewCell>("TeacherDetailsCardViewCell");
            tableView.DataSource = adapter;
            tableView.Delegate = adapter;
            tableView.TableFooterView = null;


            _imageActivityIndicator = new ActivityIndicator(imageView,0);
            _activityIndicator = new ActivityIndicator(View, 1f) {Animating = true};
            var bundle = NSBundle.MainBundle;

            adapter.OnBoforeBindCell += (cell, item) =>
            {
                var margin = !string.Equals(item.Title, bundle.LocalizedString(LocalizableWords.SUMMERY_TEXT, "Summary"), StringComparison.CurrentCultureIgnoreCase) ? 40 : 10;
                cell.TextViewLeadingConstraintConstant = margin;
            };

            _activityIndicator.Animating = true;
            try
            {
                var teacher = await MViewModel.TeacherTask;
                NavigationItem.Title = teacher.Name;
                adapter.Items = new[]
                {
                    //Summary
                    new Item<object>
                    {
                        Title = bundle.LocalizedString(LocalizableWords.SUMMERY_TEXT,"Summary"),
                        SubTitle = teacher.Summary + teacher.SocialMedia
                    },
//                    //Education
                    new Item<object>
                    {
                        Title = bundle.LocalizedString(LocalizableWords.EDUCATION_TEXT,"Education"),
                        SubTitle = ToBulletLines(
                            teacher.Educations.Select(education =>
                                    education.Year + " - " + education.Description + " - " + education.Institution)
                                .ToArray())
                    },
                    //Experience
                    new Item<object>
                    {
                        Title = bundle.LocalizedString(LocalizableWords.EXPERIENCE_TEXT,"Experience"),
                        SubTitle = ToBulletLines(
                            teacher.Experiences.Select(experience =>
                                    experience.Year + " - " + experience.Description + " - " + experience.Institution)
                                .ToArray())
                    }
                };
                tableView.ReloadData();
                
                _activityIndicator.Animating = false;

                tableViewHeightConstraint.Constant = tableView.ContentSize.Height;
                View.LayoutIfNeeded();

                _imageActivityIndicator.Animating = true;

                var image = await RemoteFileManager.Instance.DownloadImageAsync(teacher.ImageUrl, "photo_" + teacher.Name);
                imageView.Image = image;
                _imageActivityIndicator.Animating = false;


            }
            catch (Exception e)
            {

            }

           
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewDidAppear(animated);
            tableViewHeightConstraint.Constant = tableView.ContentSize.Height;
            View.LayoutIfNeeded();
        }


        private static string ToBulletLines(params string[] lines)
        {
            var str = "";
            for (var i = 0; i < lines.Length; i++)
            {
                str += "\u2022" + lines[i] + ((i < lines.Length - 1) ? "\n" : "");
            }

            return str;
        }


        public class ViewModel
        {
            public Task<Exam_App_Native.Model.Teacher> TeacherTask { get; set; }
        }

    }
}