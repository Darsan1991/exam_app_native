// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace Exam_App_Native.iOS
{
    [Register ("StatisticsViewController")]
    partial class StatisticsViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView disciplinePerformanceContainer { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView examPerformanceContainer { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIBarButtonItem handBurgerBtn { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView trendLineToggleImgView { get; set; }

        [Action ("HandBurgerBtn_Activated:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void HandBurgerBtn_Activated (UIKit.UIBarButtonItem sender);

        void ReleaseDesignerOutlets ()
        {
            if (disciplinePerformanceContainer != null) {
                disciplinePerformanceContainer.Dispose ();
                disciplinePerformanceContainer = null;
            }

            if (examPerformanceContainer != null) {
                examPerformanceContainer.Dispose ();
                examPerformanceContainer = null;
            }

            if (handBurgerBtn != null) {
                handBurgerBtn.Dispose ();
                handBurgerBtn = null;
            }

            if (trendLineToggleImgView != null) {
                trendLineToggleImgView.Dispose ();
                trendLineToggleImgView = null;
            }
        }
    }
}