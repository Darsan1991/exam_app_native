using System;
using System.Collections.Generic;
using System.Linq;
using UIKit;

namespace Exam_App_Native.iOS
{
    public partial class ProgressViewCell : UITableViewCell
    {
        public IProgressItem Item
        {
            get => _item;
            set
            {

                if (value == null)
                {
                    throw new Exception("Value Cannot be null");
                }

                if (progressBar.ProgressTiles.Count() != value.Values.Count())
                {
                    throw new Exception("Values and Progress bar count does't match");
                }

                if (IsViewLoaded)
                {
                    titleLabel.Text = value.Title;
                    progressBar.SetValues(value.Values.Select(f => 0f), false);
                    progressBar.SetValues(value.Values);
                }

                _item = value;
            }
        }

        private UIView _selectedBgView;

        private readonly UIColor _selectedColor = new UIColor(0,0,1,0.07f);
        private IProgressItem _item;

        private bool IsViewLoaded { get; set; }

        public ProgressViewCell (IntPtr handle) : base (handle)
        {
           
        }

        public override void AwakeFromNib()
        {
            base.AwakeFromNib();
            _selectedBgView = new UIView();
            SelectedBackgroundView = _selectedBgView;
            progressBar.ProgressTiles = new List<HorizontalProgressBar.ProgressTile>
            {
                new HorizontalProgressBar.ProgressTile
                {
                    Color = new UIColor(122f/255,212f/255,109f/255,1f)
                }, new HorizontalProgressBar.ProgressTile
                {
                    Color = new UIColor(237f/255,99f/255,76f/255,1f)
                },
            };

            IsViewLoaded = true;

            //TODO: Have to solve different way
            if(Item!=null)
            Item = Item;

        }

        public override void SetSelected(bool selected, bool animated)
        {
            base.SetSelected(selected, animated);
            _selectedBgView.BackgroundColor = Selected ? _selectedColor : UIColor.Clear;
        }

       

        public interface IProgressItem
        {
            string Title { get; set; }
            IEnumerable<float> Values { get; set; }
            object Data { get; set; }
        }

        public class ProgressItem : IProgressItem
        {
            public string Title { get; set; }
            public IEnumerable<float> Values { get; set; }
            public object Data { get; set; }
        }
    }
}