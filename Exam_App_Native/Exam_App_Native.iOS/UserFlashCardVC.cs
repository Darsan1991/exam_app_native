﻿using Foundation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Exam_App_Native.iOS.Adapter;
using Exam_App_Native.Model;
using Exam_App_Native.Model.ViewModel;
using UIKit;

namespace Exam_App_Native.iOS
{
    // ReSharper disable once InconsistentNaming
    public partial class UserFlashCardVC : UITableViewController
    {
        private static readonly UIColor TEXT_COLOR = UIColor.Black;

        public ViewModel MViewModel { get; set; }

        private CancellationTokenSource _tokenSource;
        private TileRadioGroup _tileRadioGroup;
        private MyCard _card;

        private MyCard ViewCard
        {
            get
            {
                var card = _card.Clone();
                card.Answer = _tileRadioGroup.Selected.Value;
                card.Content = string.Equals(((UITextViewAdapterWithHint)questionTextView.Delegate).Hint, questionTextView.Text, StringComparison.CurrentCultureIgnoreCase)?"": questionTextView.Text;
                card.Comment = string.Equals(((UITextViewAdapterWithHint)commentTextView.Delegate).Hint, commentTextView.Text, StringComparison.CurrentCultureIgnoreCase)?"":commentTextView.Text;
                return card;
            }
        }

        public UserFlashCardVC (IntPtr handle) : base (handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            if (NavigationController == null)
                throw new InvalidOperationException("This Window Should be Place in the NavigationController");

            NavigationItem.Title = $"{NSBundle.MainBundle.LocalizedString(LocalizableWords.USER_FLASH_CARD_TITLE,"User Flash Card")}";

            var item = new UIBarButtonItem
            {
                Title = NSBundle.MainBundle.LocalizedString(LocalizableWords.SAVE,"Save")
            };
            NavigationItem.RightBarButtonItem = item;
            item.Clicked += (sender, args) => CheckAndSave();

            TableView.AddGestureRecognizer(new UITapGestureRecognizer((obj) => View.EndEditing(true)){CancelsTouchesInView = false});

            disciplineBtn.TouchUpInside += (sender, args) => { OpenSelectionForDisciplines(_card.Discipline);};
            subjectBtn.TouchUpInside += (sender, args) => {OpenSelectionForSubjects(_card.Discipline,_card.Subject); };
            topicBtn.TouchUpInside += (sender, args) => { OpenSelectionForTopics(_card.Subject,_card.Topic);};

            var questionDelegate = new UITextViewAdapterWithHint
            {
                Hint = questionTextView.Text,
                HintColor = questionTextView.TextColor,
                NormalColor = TEXT_COLOR
            };

            questionTextView.Delegate = questionDelegate;

            var commentDelegate = new UITextViewAdapterWithHint
            {
                Hint = NSBundle.MainBundle.LocalizedString(LocalizableWords.ENTER_THE_COMMENT,"Enter the comment..."),
                HintColor = commentTextView.TextColor,
                NormalColor = TEXT_COLOR
            };
            commentTextView.Delegate = commentDelegate;

            _tileRadioGroup = new TileRadioGroup
            {
                RadioButtons = new[]
                {
                    new TileRadioButton
                    {
                        Background = trueRadioBtn,
                        RadioImageView = trueRadioBtnImg,
                        Value = true
                    },
                    new TileRadioButton
                    {
                        Background = falseRadioBtn,
                        RadioImageView = falseRadioBtnImg,
                        Value = false
                    },
                }
            };

            _card = MViewModel.MyCard ?? new MyCard();
            UpdateCard(_card);
        }

        private void OpenSelectionForDisciplines(Discipline selected)
        {
            var tokenSource = new CancellationTokenSource();

            // ReSharper disable once ConvertToLocalFunction
            Func<string, Task<object>> createTask = s =>
            {
                return Task.Run(async () =>
                {
                    //TODO:Will Change to search
                    return (object)(await ((string.IsNullOrEmpty(s) || s.Length<3)
                        ? ExamApp.Instance.GetAllDiscipline(tokenSource)
                        : ExamApp.Instance.SearchDisciplines(s,tokenSource))).Select(discipline =>
                        (IItem)new Item { Title = discipline.Name, Data = discipline });
                }, tokenSource.Token);
            };

            SelectableTableViewAdapter<IItem, RadioViewCell> adapter = null;

            var viewModel = new SimpleTableViewController.ViewModel
            {
                Title = "Subjects",
                TableViewAdapter = Task.Run(async () =>
                {
                    var result = await createTask(null);
                    adapter = new SelectableTableViewAdapter<IItem, RadioViewCell>("RadioViewCell", (IEnumerable<IItem>)result);
                    adapter.Selected = adapter.Items.FirstOrDefault(item => item.Data.Equals(selected));
                    
                    return (TableViewAdapter)adapter;
                }),
                Compeltion = new SimpleTableViewController.ViewModel.CompletionGroup
                {
                    CompleteButtonTitle = "Select",
                    OnComplete = controller =>
                    {
                        //Update the card
                        var myCard = ViewCard.Clone();
                        myCard.Discipline = ((SelectableTableViewAdapter<IItem, RadioViewCell>)controller.TableView.Delegate).Selected?.Data as Discipline;
                        myCard.Subject = null;
                        myCard.Topic = null;
                        UpdateCard(myCard);
                    }
                },
                OnSearchTextChanged = (sender, args) =>
                {
                    
                        ((SimpleTableViewController)sender).Update(createTask(args.SearchText), new CancellationTokenSource());
                },
                OnUpdatated = (sender, obj) =>
                {
                    var item = adapter.Selected;
                    adapter.Items = (IEnumerable<IItem>)obj;
                    adapter.Selected = item;
                    ((SimpleTableViewController)sender).ReloadData();
                }
            };
            using (var storyboard = UIStoryboard.FromName("Main", null))
            {
                var controller = (SimpleTableViewController)storyboard.InstantiateViewController("SimpleTableViewController");
                controller.MViewModel = viewModel;
                NavigationController.PushViewController(controller, true);
            }
        }


        private void OpenSelectionForSubjects(Discipline discipline, Subject selected)
        {
            var tokenSource = new CancellationTokenSource();

            // ReSharper disable once ConvertToLocalFunction
            Func<string, Task<object>> createTask = s =>
            {
                return Task.Run(async () =>
                {
                    //TODO:Will Change to search
                    return (object)(await (string.IsNullOrEmpty(s) || s.Length<3
                        ? ExamApp.Instance.GetSubjectsForDiscipline(discipline.Id, tokenSource)
                        : ExamApp.Instance.SearchSubjects(discipline.Id,s, tokenSource))).Select(subject =>
                        (IItem)new Item { Title = subject.Name, Data = subject });
                }, tokenSource.Token);
            };

            SelectableTableViewAdapter<IItem, RadioViewCell> adapter = null;

            var viewModel = new SimpleTableViewController.ViewModel
            {
                Title = "Subjects",
                TableViewAdapter = Task.Run(async () =>
                {
                    var result = await createTask(null);
                    adapter = new SelectableTableViewAdapter<IItem,RadioViewCell>("RadioViewCell",(IEnumerable<IItem>) result);
                    adapter.Selected = adapter.Items.FirstOrDefault(item => item.Data.Equals(selected));
                    return (TableViewAdapter)adapter;
                }),
                Compeltion = new SimpleTableViewController.ViewModel.CompletionGroup
                {
                    CompleteButtonTitle = "Select",
                    OnComplete = controller =>
                    {
                        //Update the card
                        var myCard = ViewCard.Clone();
                        myCard.Subject =((SelectableTableViewAdapter<IItem,RadioViewCell>)controller.TableView.Delegate).Selected?.Data as Subject;
                        myCard.Topic = null;
                        UpdateCard(myCard);
                    }
                },
                OnSearchTextChanged = (sender, args) =>
                {
                    
                        ((SimpleTableViewController)sender).Update(createTask(args.SearchText), new CancellationTokenSource());
                    
                },
                OnUpdatated = (sender, obj) =>
                {
                    var item = adapter.Selected;
                    adapter.Items = (IEnumerable<IItem>) obj;
                    adapter.Selected = item;
                    ((SimpleTableViewController)sender).ReloadData();
                }
            };
            using (var storyboard = UIStoryboard.FromName("Main",null))
            {
                var controller = (SimpleTableViewController)storyboard.InstantiateViewController("SimpleTableViewController");
                controller.MViewModel = viewModel;
                NavigationController.PushViewController(controller,true);
            }
        }

        private void OpenSelectionForTopics(Subject subject, Topic selected)
        {
            var tokenSource = new CancellationTokenSource();

            // ReSharper disable once ConvertToLocalFunction
            // ReSharper disable once ConvertToLocalFunction
            Func<string, Task<object>> createTask = s =>
            {
                return Task.Run(async () =>
                {
                    //TODO:Will Change to search
                    return (object)(await (string.IsNullOrEmpty(s)
                        ? ExamApp.Instance.GetTopicsForSubject(subject.Id, tokenSource)
                        : ExamApp.Instance.SearchTopics(subject.Id,s, tokenSource))).Select(topic =>
                        (IItem)new Item { Title = topic.Name, Data = topic });
                }, tokenSource.Token);
            };

            SelectableTableViewAdapter<IItem, RadioViewCell> adapter = null;

            var viewModel = new SimpleTableViewController.ViewModel
            {
                Title = "Subjects",
                TableViewAdapter = Task.Run(async () =>
                {
                    var result = await createTask(null);
                    adapter = new SelectableTableViewAdapter<IItem, RadioViewCell>("RadioViewCell", (IEnumerable<IItem>)result);
                    adapter.Selected = adapter.Items.FirstOrDefault(item => item.Data.Equals(selected));
                    return (TableViewAdapter)adapter;
                }),
                Compeltion = new SimpleTableViewController.ViewModel.CompletionGroup
                {
                    CompleteButtonTitle = "Select",
                    OnComplete = controller =>
                    {
                        //Update the card
                        var myCard = ViewCard.Clone();
                        myCard.Topic = ((SelectableTableViewAdapter<IItem, RadioViewCell>)controller.TableView.Delegate).Selected?.Data as Topic;
                        UpdateCard(myCard);
                    }
                },
                OnSearchTextChanged = (sender, args) =>
                {
                    
                        ((SimpleTableViewController)sender).Update(createTask(args.SearchText), new CancellationTokenSource());
                },
                OnUpdatated = (sender, obj) =>
                {
                    var item = adapter.Selected;
                    adapter.Items = (IEnumerable<IItem>)obj;
                    adapter.Selected = item;
                    ((SimpleTableViewController)sender).ReloadData();
                }
            };
            using (var storyboard = UIStoryboard.FromName("Main", null))
            {
                var controller = (SimpleTableViewController)storyboard.InstantiateViewController("SimpleTableViewController");
                controller.MViewModel = viewModel;
                NavigationController.PushViewController(controller, true);
            }
        }


        private void CheckAndSave()
        {
            var card = ViewCard;

            string error = null;

            if (card.Discipline == null || card.Subject == null
                                        || card.Topic == null
                                        || card.Answer == null
                                        || string.IsNullOrEmpty(card.Content)
                                        || string.IsNullOrEmpty(card.Comment))
            {
                error = "Some Of the Fields are Empty!";
            }

            if (error != null)
            {
                ShowError(error);
                return;
            }

            _tokenSource = new CancellationTokenSource();
            var user = Auth.Instance.CurrentUser;
            MViewModel.OnComplete?.Invoke(this, new CompleteEventArgs
            {
                Task = (card.Id == null ? user.CreateNewCard(card) : user.UpdateCard(card))
            });

            NavigationController.PopViewController(true);
        }

        private void ShowError(string message, Action onComplete = null)
        {
            IosExamApp.ShowToastAlert("Error!",message,this,onComplete);
        }

        private void UpdateCard(MyCard card)
        {
            var selectStr = NSBundle.MainBundle.LocalizedString(LocalizableWords.SELECT, "Select");
            _card = card;
            _card.Answer = _card.Answer ?? false;
            _tileRadioGroup.Selected =
                _tileRadioGroup.RadioButtons.First(button => button.Value == _card.Answer);
            disciplineBtn.SetTitle(_card.Discipline?.Name ?? selectStr,UIControlState.Normal);
            subjectBtn.SetTitle(_card.Subject?.Name ?? selectStr,UIControlState.Normal);
            topicBtn.SetTitle(_card.Topic?.Name ?? selectStr,UIControlState.Normal);
            questionTextView.Text = _card.Content;

            //TODO:Try to solve different way
            ((UITextViewAdapterWithHint)questionTextView.Delegate).SettedText(questionTextView);
            commentTextView.Text = _card.Comment;
            ((UITextViewAdapterWithHint)commentTextView.Delegate).SettedText(commentTextView);
        }

    }

    // ReSharper disable once InconsistentNaming
    public partial class UserFlashCardVC
    {
        public class ViewModel
        {
            public MyCard MyCard { get; set; }
            public EventHandler<CompleteEventArgs> OnComplete{ get; set; }
        }


        public class CompleteEventArgs
        {
            public Task Task { get; set; }
            public CancellationTokenSource TokenSource { get; set; }
        }
    }

    // ReSharper disable once InconsistentNaming
    public partial class UserFlashCardVC
    {
        private class TileRadioGroup
        {
            public event EventHandler<TileRadioButton> OnSelectionChanged;
            private readonly List<TileRadioButton> _radioButtons = new List<TileRadioButton>();
            private TileRadioButton _selected;

            public IEnumerable<TileRadioButton> RadioButtons
            {
                get => _radioButtons;
                set
                {
                    if (value == null)
                    {
                        throw new Exception("Null Value not Accepted Or Inplemented");
                    }

                    _radioButtons.Clear();
                    _radioButtons.AddRange(value);
                    _radioButtons.ForEach(action: button =>
                    {
                        button.Selected = false;
                        button.Clicked += RadioButtonClicked;
                    });
                }
            }

            private void RadioButtonClicked(object sender, bool b)
            {
                Selected = (TileRadioButton)sender;
            }

            public TileRadioButton Selected
            {
                get => _selected;
                set
                {
                    if (_selected == value)
                    {
                        return;
                    }

                    var radioButton = _radioButtons.FirstOrDefault(button => button.Selected);
                    if (radioButton != null)
                    {
                        radioButton.Selected = false;
                    }

                    value.Selected = true;
                    _selected = value;
                    OnSelectionChanged?.Invoke(this, _selected);
                }
            }
        }

        private class TileRadioButton
        {
            private UIView _background;
            private bool _selected;
            public event EventHandler<bool> Clicked;
            
            private readonly UIGestureRecognizer _recognizer;

            public TileRadioButton()
            {
                _recognizer = new UITapGestureRecognizer(() => Clicked?.Invoke(this,Value));
            }

            public UIView Background
            {
                get => _background;
                set
                {
                    if (Equals(_background, value))
                    {
                        return;
                    }

                    _background?.RemoveGestureRecognizer(_recognizer);
                    _background = value;
                    _background?.AddGestureRecognizer(_recognizer);
                }
            }

            public bool Selected
            {
                get => _selected;
                set
                {
                    //TODO:Set the Images Later
                    RadioImageView.Image = UIImage.FromBundle(value ? "radio_btn_selected_1": "radio_btn_unselected_1");
                    _selected = value;
                }
            }

            public UIImageView RadioImageView { get; set; }
            public bool Value { get; set; }
        }
    }


}