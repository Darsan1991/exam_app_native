﻿using System.Security.Policy;

namespace Exam_App_Native.iOS
{
    public static class LocalizableWords
    {
        public const string OK = "ok_text";
        public const string STOP = "stop_text";
        public const string DETAILS = "details_text";
        public const string ACCOUNT = "account_text";
        public const string TEACHERS = "teachers_text";
        public const string PLANS = "plans_text";
        public const string LOGOUT = "logout_text";
        public const string START = "start_text";
        public const string CARDS = "cards_text";
        public const string YES = "yes_text";
        public const string NO = "no_text";
        public const string ARE_YOU_SURE_WANT_TO_DELETE_REVIEW = "are_you_sure_want_to_delete_review_text";
        public const string NO_OF_CARDS_YOU_KNOW = "no_of_cards_you_know_text";
        public const string NEW = "new_text";
        public const string ANSWER = "answer_text";
        public const string TRUE = "true_text";
        public const string FALSE = "false_text";
        public const string TOPIC = "topic_text";
        public const string SAVE = "save_text";
        public const string ENTER_THE_QUESTION = "enter_the_question_text";
        public const string ENTER_THE_COMMENT = "enter_the_comment_text";
        public const string SELECT = "select_text";
        public const string ACCOUNT_DETAILS = "acccount_details_text";
        public const string SETTINGS = "settings_text";
        public const string SUBSCRIPTION = "subcription_text";
        public const string NO_OF_FLASH_CARDS_IN_EACH_DISCIPLINE = "no_of_flash_cards_in_each_discipline_text";
        public const string NO_OF_EXAM = "no_of_exam_text";
        public const string NO_OF_FLASH_CARDS_IN_EACH_EXAM = "no_of_flash_cards_in_each_exam_text";
        public const string PRICE = "price_text";
        public const string FREE = "free_text";
        public const string UNLIMITED = "unlimited_text";
        public const string OF = "of_text";
        public const string DELETE_REVIEW = "delete_review_text";
        public const string TREND_LINE = "trend_line_text" ;
        public const string CHANGE_USER_NAME = "change_user_name_text";
        public const string USERNAME = "username_text";
        public const string CANCEL = "cancel_text";
        public const string REVIEW_NAME = "review_name_text";

        public const string SUMMERY_TEXT = "summery_text";
        public const string EDUCATION_TEXT = "education_text";
        public const string EXPERIENCE_TEXT = "experience_text";
        public const string CREDIT_CARD_TEXT =  "credit_card_text";
        public const string BANK_SLIP_TEXT = "bank_slip_text";
        public const string HOME = "home_text";
        public const string SCHEDULE = "schedule_text";
        public const string ALL = "all_text";
        public const string SEARCH = "search_text";
        public const string BACK = "back_text";



        public const string JAN = "jan_text";
        public const string FEB = "feb_text";
        public const string MAR = "mar_text";
        public const string API = "api_text";
        public const string MAY = "may_text";
        public const string JUN = "jun_text";
        public const string JUL = "jul_text";
        public const string AUG = "aug_text";
        public const string SEP = "sep_text";
        public const string OCT = "oct_text";
        public const string NOV = "nov_text";
        public const string DEC = "dec_text";

        public const string QUIZ_CONTROLLER_TITLE = "quiz_controller_title";
        public const string ACCOUNT_CONTROLLER_TITLE = "account_controller_title";
        public const string TEACHERS_CONTROLLER_TITLE = "teachers_controller_title";
        public const string SUBSCRIPTION_CONTROLLER_TITLE = "subscription_controller_title";
        public const string USER_FLASH_CARD_TITLE = "user_flash_card_tile";
       
        public const string PAYMENT_CONTROLLER_TITLE =  "payment_controller_title";
        public const string EXAM_CONTROLLER_TITLE = "exam_controller_title";
        public const string DISCIPLINE_CONTROLLER_TITLE = "discipline_controller_title";
        public const string MY_CARDS_CONTROLLER_TITLE = "my_cards_controller_title";
        public const string REVIEW_CONTROLLER_TITLE = "review_controller_title";
        public const string STATICS_CONTROLLER_TITLE = "statics_controller_title";


    }
}