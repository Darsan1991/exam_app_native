﻿using Foundation;
using System;
using System.Threading;
using UIKit;

namespace Exam_App_Native.iOS
{
    public partial class SplashViewController : UIViewController
    {
        public SplashViewController (IntPtr handle) : base (handle)
        {
        }

        public override async void ViewDidLoad()
        {
            base.ViewDidLoad();

            try
            {
                await Auth.Instance.TryAutoLogIn();
                var storyboard = UIStoryboard.FromName("Main",null);
                var controller = storyboard.InstantiateViewController("MyHanBurgerVC");
                PresentViewController(controller, true, null);
            }
            catch (Exception e)
            {

                NSTimer.CreateScheduledTimer(1, false, nsTimer =>
                {
                    var storyboard = UIStoryboard.FromName("Main", null);
                    var controller = storyboard.InstantiateViewController("LogInViewController");
                    PresentViewController(controller, true, null);
                });
                
            }
        }
    }
}