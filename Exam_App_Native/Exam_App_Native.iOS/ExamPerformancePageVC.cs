﻿using Foundation;
using System;
using System.Collections.Generic;
using System.Linq;
using Exam_App_Native.iOS.Adapter;
using Exam_App_Native.Model;
using UIKit;

namespace Exam_App_Native.iOS
{
    public partial class ExamPerformancePageVC : UIPageViewController
    {
        public ViewModel MViewModel
        {
            get => _mViewModel;
            set
            {

                _mViewModel = value;
                _examPerformanceVcs.Clear();

                if (value == null)
                    return;

                var storyboard = UIStoryboard.FromName("Main", null);
                
                    var controllers = _mViewModel.ExamPerformances.Select(performance =>
                    {
                        // ReSharper disable once AccessToDisposedClosure
                        var controller =
                            (ExamPerformanceVC)storyboard.InstantiateViewController("ExamPerformanceVC");
                       
                        controller.MViewModel = new ExamPerformanceVC.ViewModel
                        {
                            ExamPerformance = performance
                        };
                        return controller;
                    });
                    _examPerformanceVcs.AddRange(controllers);
                

                if (IsViewLoaded)
                {
                    DataSource = new SimplePageViewControllerDataSource
                    {
                        Controllers = _examPerformanceVcs
                    };

                    if (_examPerformanceVcs.Count > 0)
                    {
                        SetViewControllers(new UIViewController[] { _examPerformanceVcs.First() }, UIPageViewControllerNavigationDirection.Forward, false, null);
                    }
                }

            }
        }

        private readonly List<ExamPerformanceVC> _examPerformanceVcs = new List<ExamPerformanceVC>();
        private ViewModel _mViewModel;

        public ExamPerformancePageVC (IntPtr handle) : base (handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            if (_examPerformanceVcs.Count == 0)
            {
                using (var storyboard = UIStoryboard.FromName("Main",null))
                {
                    // ReSharper disable once AccessToDisposedClosure
                    var controller =
                        (ExamPerformanceVC)storyboard.InstantiateViewController("ExamPerformanceVC");
                    _examPerformanceVcs.Add(controller);
                }

            }

            if (_examPerformanceVcs.Count>0)
            {
                DataSource = new SimplePageViewControllerDataSource
                {
                    Controllers = _examPerformanceVcs
                };
                SetViewControllers(new UIViewController[]{_examPerformanceVcs.First()},UIPageViewControllerNavigationDirection.Forward,false,null);
            }

        }

        public class ViewModel
        {
            public IEnumerable<ExamPerformance> ExamPerformances { get; set; }
        }
    }
}