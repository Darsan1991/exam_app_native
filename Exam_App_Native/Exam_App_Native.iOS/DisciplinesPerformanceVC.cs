﻿using Foundation;
using System;
using System.Linq;
using System.Collections.Generic;
using iOSCharts;
using UIKit;

namespace Exam_App_Native.iOS
{
    public partial class DisciplinesPerformanceVC : UIViewController
    {

        public bool ShowTrendLine
        {
            get => _showTrendLine;
            set
            {
                if (value == _showTrendLine)
                    return;

                if (_trendLineDataSet != null)
                {
                    if (value && !_lineData.ContainsWithDataSet(_trendLineDataSet))
                        _lineData.AddDataSet(_trendLineDataSet);
                    else if (_lineData.ContainsWithDataSet(_trendLineDataSet))
                        _lineData.RemoveDataSet(_trendLineDataSet);
                    

                    if (IsViewLoaded)
                    {
                        
//                        _lineChart.Invalidate();
                                                _linerChart.NotifyDataSetChanged();
                    }

                }

                _showTrendLine = value;
            }
        }

        public ViewModel MViewModel
        {
            get => _mViewModel;
            set
            {

                _knownCardLineDataSet =
                    new LineChartDataSet(
                        value.KnownCardValues.Select(pair => new ChartDataEntry(pair.Key, pair.Value)).ToArray(),
                        $"{NSBundle.MainBundle.LocalizedString(LocalizableWords.NO_OF_CARDS_YOU_KNOW,"No of cards you know")}(%)")
                    {
                        Mode = LineChartMode.CubicBezier,
                        ValueFont = UIFont.SystemFontOfSize(0),
                        Colors = new[]
                        {
                            UIColor.FromRGB(80f / 255, 120f / 255, 215f / 255)
                        }
                    };

                _trendLineDataSet = new LineChartDataSet(value.TrendLineValues.Select(pair => new ChartDataEntry(pair.Key, pair.Value)).ToArray(),NSBundle.MainBundle.LocalizedString(LocalizableWords.TREND_LINE,"Trend line"))
                {
                    Colors = new[]
                    {
                        UIColor.FromRGB(80f / 255, 120f / 255, 215f / 255)
                    },
                    ValueFont = UIFont.SystemFontOfSize(0)
                };


                _lineData.ClearValues();
                _lineData.AddDataSet(_knownCardLineDataSet);

                if (ShowTrendLine)
                    _lineData.AddDataSet(_trendLineDataSet);

                if (IsViewLoaded)
                {
                    _linerChart.NotifyDataSetChanged();
                    _titleLabel.Text = value.Title;
                    _linerChart.LeftAxis.AxisMinValue = Math.Min(_trendLineDataSet.YMin, _knownCardLineDataSet.YMin);

                }

                _mViewModel = value;
            }
        }

        private UILabel _titleLabel;
        private LineChartView _linerChart;

        private readonly LineChartData _lineData = new LineChartData();
        private ViewModel _mViewModel;
        private LineChartDataSet _knownCardLineDataSet;
        private LineChartDataSet _trendLineDataSet;
        private bool _showTrendLine;

        public DisciplinesPerformanceVC (IntPtr handle) : base (handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            InitView();
            _linerChart.Data = _lineData;
            if(_trendLineDataSet!=null && _knownCardLineDataSet!=null)
            _linerChart.LeftAxis.AxisMinValue = Math.Min(_trendLineDataSet.YMin, _knownCardLineDataSet.YMin);
            _titleLabel.Text = MViewModel?.Title;
        }

        void InitView()
        {
            _titleLabel = new UILabel
            {
                Text = "Hello",
                Font = UIFont.SystemFontOfSize(13),
                TranslatesAutoresizingMaskIntoConstraints = false
            };
            View.AddSubview(_titleLabel);
            _titleLabel.CenterXAnchor.ConstraintEqualTo(View.CenterXAnchor).Active = true;
            _titleLabel.TopAnchor.ConstraintEqualTo((View.TopAnchor)).Active = true;

            _linerChart = new LineChartView{ TranslatesAutoresizingMaskIntoConstraints = false };
            View.AddSubview(_linerChart);
            _linerChart.LeftAnchor.ConstraintEqualTo(View.LeftAnchor, 10).Active = true;
            _linerChart.RightAnchor.ConstraintEqualTo(View.RightAnchor, -10).Active = true;
            _linerChart.TopAnchor.ConstraintEqualTo(_titleLabel.BottomAnchor, 5).Active = true;
            _linerChart.BottomAnchor.ConstraintEqualTo(View.BottomAnchor).Active = true;


            _linerChart.DrawGridBackgroundEnabled = false;
            _linerChart.HighlightPerTapEnabled = false;
            _linerChart.HighlightPerDragEnabled = false;
            //            _linerChart.Description = null;
            _linerChart.RightAxis.Enabled = false;
            _linerChart.DescriptionText = "";

            _linerChart.XAxis.AxisMinimum = 0;
            _linerChart.XAxis.AxisMaximum = 11;
            _linerChart.XAxis.SetLabelCount(12, true);
            _linerChart.XAxis.ValueFormatter = new MonthAxisValueFormatter(0);
           

        }


        public class ViewModel
        {
            public Dictionary<int, float> KnownCardValues { get; set; }
            public Dictionary<int, float> TrendLineValues { get; set; }
            public string Title { get; set; }
        }

    }


    public partial class DisciplinesPerformanceVC
    {
        public class MonthAxisValueFormatter : ChartDefaultAxisValueFormatter
        {
            public MonthAxisValueFormatter(int digits) : base(digits)
            {
            }

            // ReSharper disable once MethodTooLong
            public override string Axis(double value, ChartAxisBase axis)
            {
                var month = (int)value;
                var bundle = NSBundle.MainBundle;
                switch (month)
                {
                    case 0:
                        return bundle.LocalizedString(LocalizableWords.JAN,"Jan");
                    case 1:
                        return bundle.LocalizedString(LocalizableWords.FEB, "Feb");
                    case 2:
                        return bundle.LocalizedString(LocalizableWords.MAR, "Mar");
                    case 3:
                        return bundle.LocalizedString(LocalizableWords.API, "Api");
                    case 4:
                        return bundle.LocalizedString(LocalizableWords.MAY, "May");
                    case 5:
                        return bundle.LocalizedString(LocalizableWords.JUN, "Jun");
                    case 6:
                        return bundle.LocalizedString(LocalizableWords.JUL, "Jul");
                    case 7:
                        return bundle.LocalizedString(LocalizableWords.AUG, "Aug");
                    case 8:
                        return bundle.LocalizedString(LocalizableWords.SEP, "Sep");
                    case 9:
                        return bundle.LocalizedString(LocalizableWords.OCT, "Oct");
                    case 10:
                        return bundle.LocalizedString(LocalizableWords.NOV, "Nov");
                    case 11:
                        return bundle.LocalizedString(LocalizableWords.DEC, "Dec");
                }

                return "";
            }
        }
    }
}