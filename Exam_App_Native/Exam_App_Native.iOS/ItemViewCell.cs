﻿using Foundation;
using System;
using Exam_App_Native.Model;
using Exam_App_Native.Model.ViewModel;
using UIKit;

namespace Exam_App_Native.iOS
{
    public partial class ItemViewCell : UITableViewCell,IHolder<object>
    {

        public object Item
        {
            get => ActualItem;
            set
            {
                if (value is IItem item)
                {
                    if (IsViewLoaded)
                    {
                        titleLabel.Text = item.Title;
                        detailsLabel.Text = item.SubTitle;
                    }
                    ActualItem = item;
                }
            }
        }

        public IItem ActualItem { get; set; }
        public bool IsViewLoaded { get; private set; }

        public ItemViewCell (IntPtr handle) : base (handle)
        {
        }

        public override void AwakeFromNib()
        {
            base.AwakeFromNib();
            IsViewLoaded = true;

            //TODO:Try to solve different way
            if (Item != null)
                Item = Item;
        }
    }
}