// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace Exam_App_Native.iOS
{
    [Register ("SubscriptionViewCell")]
    partial class SubscriptionViewCell
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView actualContentView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel contentLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel leftLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel rightLabel { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (actualContentView != null) {
                actualContentView.Dispose ();
                actualContentView = null;
            }

            if (contentLabel != null) {
                contentLabel.Dispose ();
                contentLabel = null;
            }

            if (leftLabel != null) {
                leftLabel.Dispose ();
                leftLabel = null;
            }

            if (rightLabel != null) {
                rightLabel.Dispose ();
                rightLabel = null;
            }
        }
    }
}