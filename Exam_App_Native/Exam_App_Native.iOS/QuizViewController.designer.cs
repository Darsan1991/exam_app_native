// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace Exam_App_Native.iOS
{
    [Register ("QuizViewController")]
    partial class QuizViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView questionContainer { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView resultContainer { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (questionContainer != null) {
                questionContainer.Dispose ();
                questionContainer = null;
            }

            if (resultContainer != null) {
                resultContainer.Dispose ();
                resultContainer = null;
            }
        }
    }
}