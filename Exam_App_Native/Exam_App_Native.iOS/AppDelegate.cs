﻿using Facebook.CoreKit;
using Foundation;
using Ricardo.Stripe.iOS;
using UIKit;

namespace Exam_App_Native.iOS
{
    // The UIApplicationDelegate for the application. This class is responsible for launching the
    // User Interface of the application, as well as listening (and optionally responding) to application events from iOS.
    [Register("AppDelegate")]
    public class AppDelegate : UIApplicationDelegate
    {
        private const string PUBLISH_STRIPE_KEY = "pk_test_mJf4KS1A3f0694WniMD5kbX5";

        // class-level declarations

        public override UIWindow Window
        {
            get;
            set;
        }

        public override bool FinishedLaunching(UIApplication application, NSDictionary launchOptions)
        {
            // Override point for customization after application launch.
            // If not required for your application you can safely delete this method
            ExamApp.Instance.SetUserPrefs(new UserPrefs());
            Stripe.DefaultPublishableKey = PUBLISH_STRIPE_KEY;
            ApplicationDelegate.SharedInstance.FinishedLaunching(application, launchOptions);
            return true;
        }

        public override bool OpenUrl(UIApplication application, NSUrl url, string sourceApplication, NSObject annotation)
        {
            return ApplicationDelegate.SharedInstance.OpenUrl(application, url, sourceApplication, annotation);
            
        }

        public override void OnResignActivation(UIApplication application)
        {
            // Invoked when the application is about to move from active to inactive state.
            // This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) 
            // or when the user quits the application and it begins the transition to the background state.
            // Games should use this method to pause the game.
        }

        public override void DidEnterBackground(UIApplication application)
        {
            // Use this method to release shared resources, save user data, invalidate timers and store the application state.
            // If your application supports background exection this method is called instead of WillTerminate when the user quits.
        }

        public override void WillEnterForeground(UIApplication application)
        {
            // Called as part of the transiton from background to active state.
            // Here you can undo many of the changes made on entering the background.
        }

        public override void OnActivated(UIApplication application)
        {
            // Restart any tasks that were paused (or not yet started) while the application was inactive. 
            // If the application was previously in the background, optionally refresh the user interface.
        }

        public override void WillTerminate(UIApplication application)
        {
            // Called when the application is about to terminate. Save data, if needed. See also DidEnterBackground.
        }
    }

    public class UserPrefs : IUserPrefs
    {
        private readonly NSUserDefaults _userDefaults = NSUserDefaults.StandardUserDefaults;

        public void SetString(string key, string value)
        {
            
            _userDefaults.SetString(value, key);
        }

        public string GetString(string key, string defValue = "") => 
            _userDefaults.StringForKey(key);

        public void SetInt(string key, int value)
        {
            _userDefaults.SetInt(value,key);        }

        public int GetInt(string key, int defValue = 0)
        {
            return (int)_userDefaults.IntForKey(key);
        }

        public void SetFloat(string key, float value)
        {
            _userDefaults.SetFloat(value,key);

        }

        public float GetFloat(string key, float defValue = 0) => _userDefaults.FloatForKey(key);

        public void DeleteKey(string key)
        {
            _userDefaults.RemoveObject(key);
            _userDefaults.Synchronize();
        }
    }
}

