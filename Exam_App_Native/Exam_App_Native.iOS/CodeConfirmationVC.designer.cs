// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace Exam_App_Native.iOS
{
    [Register ("CodeConfirmationVC")]
    partial class CodeConfirmationVC
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField codeLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        Exam_App_Native.iOS.ShadowButton confirmButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        Exam_App_Native.iOS.ShadowButton resendCodeButton { get; set; }

        [Action ("ConfirmButton_TouchUpInside:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void ConfirmButton_TouchUpInside (Exam_App_Native.iOS.ShadowButton sender);

        [Action ("ResendCodeButton_TouchUpInside:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void ResendCodeButton_TouchUpInside (Exam_App_Native.iOS.ShadowButton sender);

        void ReleaseDesignerOutlets ()
        {
            if (codeLabel != null) {
                codeLabel.Dispose ();
                codeLabel = null;
            }

            if (confirmButton != null) {
                confirmButton.Dispose ();
                confirmButton = null;
            }

            if (resendCodeButton != null) {
                resendCodeButton.Dispose ();
                resendCodeButton = null;
            }
        }
    }
}