﻿using Foundation;
using System;
using System.Collections.Generic;
using System.Linq;
using Exam_App_Native.Model;
using iOSCharts;
using UIKit;

namespace Exam_App_Native.iOS
{
    public partial class ExamPerformanceVC : UIViewController
    {
        private static readonly UIColor[] COLORS = new[]
        {
            UIColor.Red, UIColor.Blue, UIColor.Brown, UIColor.DarkGray, UIColor.Purple, UIColor.Orange, UIColor.Cyan,
            UIColor.Purple, UIColor.LightGray, UIColor.Magenta,UIColor.Green,UIColor.Yellow
        };


        private ViewModel _mViewModel;
        private readonly RadarChartData _radarData = new RadarChartData();
        private List<string> _webAxises = new List<string>();
        private UILabel _titleLabel;
        private UILabel _subTitleLabel;
        private RadarChartView _radarChart;

        public ViewModel MViewModel
        {
            get => _mViewModel;
            set
            {
                _radarData.ClearValues();
                _webAxises.Clear();

                var group = value.ExamPerformance.PerformanceResults.GroupBy(result => result.Month).ToList();
                var axises = group.SelectMany(results => results.Select(result => result.DisciplineName.ToLower())).Distinct();

                _webAxises.AddRange(axises);

                group.ForEach((index, results) =>
                {
                    var list = results.ToList();
                    var entries = new RadarChartDataEntry[list.Count];

                    foreach (var result in results)
                    {
                        var i = _webAxises.IndexOf(result.DisciplineName.ToLower());
                        entries[i] = new RadarChartDataEntry(result.Score);
                    }

                    for (var i = 0; i < entries.Length; i++)
                    {
                        if (entries[i] == null)
                        {
                            entries[i] = new RadarChartDataEntry(0);
                        }
                    }

                    _radarData.AddDataSet(new RadarChartDataSet(entries.Select(entry => (ChartDataEntry)entry).ToArray(), results.Key)
                    {
                        LineWidth = 2,
                        ValueFont = UIFont.SystemFontOfSize(0),
                        Colors = new []{COLORS[index]}
                    });



                });

                _valueFormatter.AxisTitles = _webAxises;

                if (IsViewLoaded)
                {
                    _titleLabel.Text = value.ExamPerformance.JobTitle;
                    _subTitleLabel.Text =
                        $"{value.ExamPerformance.Institution}/{value.ExamPerformance.YearAndCompany}";
                    _radarChart.XAxis.SetLabelCount(_valueFormatter.AxisTitles.Count(), true);
                    _radarChart.NotifyDataSetChanged();

                }


                _mViewModel = value;
            }
        }

        private readonly ValueFormatter _valueFormatter = new ValueFormatter(0);

        public ExamPerformanceVC (IntPtr handle) : base (handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            InitView();
            _radarChart.Data = _radarData;
            _titleLabel.Text = MViewModel?.ExamPerformance.JobTitle;
            _radarChart.XAxis.XOffset = 0;
            _radarChart.XAxis.YOffset = 0;

            _radarChart.XAxis.ValueFormatter = _valueFormatter;
            _radarChart.XAxis.SetLabelCount(_webAxises.Count, true);

            _radarChart.YAxis.AxisMaxValue = 100;
            _radarChart.YAxis.AxisMinValue = 0;
            _radarChart.YAxis.SetLabelCount(3, true);
            _radarChart.WebAlpha = (int)(0.3f * 255);
            _radarChart.DragDecelerationEnabled = false;
            _radarChart.RotationEnabled = false;



            //_radarChart.XAxis.DrawLabelsEnabled = true;
            _subTitleLabel.Text =
                $"{MViewModel?.ExamPerformance.Institution}/{MViewModel?.ExamPerformance.YearAndCompany}";
        }


        void InitView()
        {
            _titleLabel = new UILabel
            {
                Text = "Hello",
                Font = UIFont.SystemFontOfSize(13),
                TranslatesAutoresizingMaskIntoConstraints = false
            };

            View.AddSubview(_titleLabel);
            _titleLabel.LeftAnchor.ConstraintEqualTo((View.LeftAnchor)).Active = true;
            _titleLabel.TopAnchor.ConstraintEqualTo((View.TopAnchor)).Active = true;

            _subTitleLabel = new UILabel
            {
                Text = "Hello",
                Font = UIFont.SystemFontOfSize(10),
                TranslatesAutoresizingMaskIntoConstraints = false
            };
            View.AddSubview(_subTitleLabel);
            _subTitleLabel.LeftAnchor.ConstraintEqualTo((View.LeftAnchor)).Active = true;
            _subTitleLabel.TopAnchor.ConstraintEqualTo((_titleLabel.BottomAnchor),5).Active = true;

            _radarChart = new RadarChartView
            {
                TranslatesAutoresizingMaskIntoConstraints = false,
            };
            
            View.AddSubview(_radarChart);
            _radarChart.LeftAnchor.ConstraintEqualTo(View.LeftAnchor, 10).Active = true;
            _radarChart.RightAnchor.ConstraintEqualTo(View.RightAnchor, 10).Active = true;
            _radarChart.TopAnchor.ConstraintEqualTo(_subTitleLabel.BottomAnchor, 3).Active  = true;
            _radarChart.BottomAnchor.ConstraintEqualTo(View.BottomAnchor).Active = true;

            _radarChart.DescriptionText = "";
            _radarChart.YAxis.AxisMinimum = 0;
            _radarChart.YAxis.SetLabelCount(3, true);
            _radarChart.WebAlpha = 0.3f;
            _radarChart.DragDecelerationEnabled = false;
            _radarChart.RotationEnabled = false;
            _radarChart.WebColor = UIColor.Black;
        }

        private class ValueFormatter:ChartDefaultAxisValueFormatter{

            private const int MAX_LENGTH_AXIS_TITLE = 10;
            public IEnumerable<string> AxisTitles{
                get{
                    return _axisTitleList;
                }
                set{
                    _axisTitleList.Clear();

                    if (value == null)
                        return;

                    _axisTitleList.AddRange(value);
                }
            }

            private readonly List<string> _axisTitleList = new List<string>();

            public ValueFormatter(int digts):base(digts)
            {

            }

            public override string Axis(double value, ChartAxisBase axis)
            {
                var month = (int)value;

                if(_axisTitleList.Count == 0)
                {
                    return "";
                }
                else{
                    var str = _axisTitleList[month % _axisTitleList.Count];
                    return str.Length > MAX_LENGTH_AXIS_TITLE ? str.Substring(0, MAX_LENGTH_AXIS_TITLE-3) + "..." : str;
                }

            }

        }

        public class ViewModel
        {
            public ExamPerformance ExamPerformance { get; set; }
        }
    }
}