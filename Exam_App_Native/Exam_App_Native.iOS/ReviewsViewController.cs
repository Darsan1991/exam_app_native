﻿using Foundation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Exam_App_Native.iOS.View;
using Exam_App_Native.Model;
using UIKit;

namespace Exam_App_Native.iOS
{
    public partial class ReviewsViewController : UIViewController,IUITableViewDataSource
    {
        private ActivityIndicator _activityIndicator;
        private CancellationTokenSource _tokenSource;

        private readonly List<ReviewSet> _reviewSets = new List<ReviewSet>();
        private TableViewDelegate _tableViewDelegate;
        private ReviewSet _selectedItem;
        private UIView _noReviewsView;

        private readonly List<CalenderAndQuestionText> _calenderAndQuestionTexts = new List<CalenderAndQuestionText>();

        private ReviewSet SelectedItem
        {
            get => _selectedItem;
            set
            {
                if (value == null)
                    throw new NotImplementedException();

                if (IsViewLoaded)
                {
                    var schedules = value.Schedules.ToList();
                    for (var i = 0; i < _calenderAndQuestionTexts.Count; i++)
                    {
                        if (schedules.Count > i)
                        {
                            var text = _calenderAndQuestionTexts[i];

                            text.CalenderLabel.Text = schedules[i].Period.ToString();
                            text.QuestionLabel.Text = schedules[i].CardCount.ToString();
                            text.CalenderLabel.Hidden = false;
                            text.QuestionLabel.Hidden = false;
                        }
                        else
                        {
                            var text = _calenderAndQuestionTexts[i];
                            text.CalenderLabel.Hidden = true;
                            text.QuestionLabel.Hidden = true;
                        }
                    }

                    titleLabel.Text = value.Name;
                    sceheduledGroup.Hidden = value.Badge == 0;
                    scheduledBtn.Enabled = value.Badge != 0;
                    scheduledCountLabel.Text = value.Badge.ToString();
                    progressBar.SetProgress(value.Score/100f);
                }

                _selectedItem = value;
            }
        }

        public ReviewsViewController (IntPtr handle) : base (handle)
        {
        }

        // ReSharper disable once MethodTooLong
        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            Title = NSBundle.MainBundle.LocalizedString(LocalizableWords.REVIEW_CONTROLLER_TITLE, "Reviews");
            scheduledBtn.SetTitle(NSBundle.MainBundle.LocalizedString(LocalizableWords.SCHEDULE,"Schedule").ToUpper(),UIControlState.Normal);
            allBtn.SetTitle(NSBundle.MainBundle.LocalizedString(LocalizableWords.ALL, "All").ToUpper(), UIControlState.Normal);
            tableView.DataSource = this;
            _tableViewDelegate = new TableViewDelegate(headerView);
            tableView.Delegate = _tableViewDelegate;
            _activityIndicator = new ActivityIndicator(View,1f);

            _tableViewDelegate.OnSelected += path => { SelectedItem = _reviewSets[path.Row]; };
            tableView.TableFooterView = new UIView();

            _calenderAndQuestionTexts.Add(new CalenderAndQuestionText
            {
                CalenderLabel = calender1Label,
                QuestionLabel = question1Label
            });

            _calenderAndQuestionTexts.Add(new CalenderAndQuestionText
            {
                CalenderLabel = calender2Label,
                QuestionLabel = question2Label
            });

            _calenderAndQuestionTexts.Add(new CalenderAndQuestionText
            {
                CalenderLabel = calender3Label,
                QuestionLabel = question3Label
            });

            _calenderAndQuestionTexts.Add(new CalenderAndQuestionText
            {
                CalenderLabel = calender4Label,
                QuestionLabel = question4Label
            });

            _calenderAndQuestionTexts.Add(new CalenderAndQuestionText
            {
                CalenderLabel = calender5Label,
                QuestionLabel = question5Label
            });

            _calenderAndQuestionTexts.Add(new CalenderAndQuestionText
            {
                CalenderLabel = calender6Label,
                QuestionLabel = question6Label
            });

            _calenderAndQuestionTexts.Add(new CalenderAndQuestionText
            {
                CalenderLabel = calender7Label,
                QuestionLabel = question7Label
            });

            _noReviewsView = new UIView()
            {
                TranslatesAutoresizingMaskIntoConstraints = false,
                BackgroundColor = UIColor.White
            };

            View.AddSubview(_noReviewsView);

            _noReviewsView.LeftAnchor.ConstraintEqualTo(View.LeftAnchor).Active = true;
            _noReviewsView.RightAnchor.ConstraintEqualTo(View.RightAnchor).Active = true;
            _noReviewsView.TopAnchor.ConstraintEqualTo(View.TopAnchor).Active = true;
            _noReviewsView.BottomAnchor.ConstraintEqualTo(View.BottomAnchor).Active = true;

            var label = new UILabel()
            {
                TextColor = UIColor.Gray,
                Text = "No Reviews",
                Font = UIFont.SystemFontOfSize(12),
                TranslatesAutoresizingMaskIntoConstraints = false
            };

            _noReviewsView.AddSubview(label);
            label.CenterXAnchor.ConstraintEqualTo(View.CenterXAnchor).Active = true;
            label.CenterYAnchor.ConstraintEqualTo(View.CenterYAnchor).Active = true;

        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);
            LoadOrUpdateReviews();
        }


        partial void HandBurgerBtn_Activated(UIBarButtonItem sender)
        {
            MyHanBurgerVC.Instance?.Toggle();
        }

        partial void AllBtn_Activated(UIButton sender)
        {
            OpenQuizViewController(new QuizViewController.ViewModel
            {
                ReviewSetStartUpInfos = Task.Run(async ()=>
                {
                    var startup = await ExamApp.Instance.RestartAllReviewSet(SelectedItem.Id);
                    return (IEnumerable<ReviewSetStartUpInfo>)new[] {startup};
                })
            });

        }

        partial void Scheduled_Activated(UIButton sender)
        {
            OpenQuizViewController(new QuizViewController.ViewModel
            {
                ReviewSetStartUpInfos = ExamApp.Instance.RetakeReviewSet(SelectedItem.Id),
                ReviewSetId = SelectedItem.Id
            });
        }

        private void OpenQuizViewController(QuizViewController.ViewModel viewModel)
        {
            var storyboard = UIStoryboard.FromName("Main", null);
            var controller = (QuizViewController)storyboard.InstantiateViewController("QuizViewController");
            controller.MViewModel = viewModel;

            PresentViewController(IosExamApp.CreateNavigationControllerWithTheme(controller), true, null);
        }

        partial void Delete_Activated(UIButton sender)
        {
            if (SelectedItem == null)
            {
                IosExamApp.ShowToastAlert("Info","Please Select the Item before Delete",this);
                return;
            }
            ShowAlertAndDeleteItem(SelectedItem);
        }


        public nint RowsInSection(UITableView tableView, nint section)
        {
            return section == 0 ? _reviewSets.Count : 0;
        }

        public UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            var cell = (ProgressViewCell)tableView.DequeueReusableCell("ProgressViewCell");
            var set = _reviewSets[indexPath.Row];
            cell.Item = new ProgressViewCell.ProgressItem
            {
                Title = set.Name,
                Values = new[]
                {
                    set.Resolved.Total==0?0:(float) set.Resolved.Hits / set.Resolved.Total,  set.Resolved.Total==0?0:(float) set.Resolved.Mistakes / set.Resolved.Total
                },
                Data = set

            };
            //cell.BackgroundColor = UIColor.Blue;
            return cell;
        }

        private void ShowAlertAndDeleteItem(ReviewSet reviewSet)
        {
            var controller = UIAlertController.Create(NSBundle.MainBundle.LocalizedString(LocalizableWords.DELETE_REVIEW,"Delete Review"),NSBundle.MainBundle.LocalizedString(LocalizableWords.ARE_YOU_SURE_WANT_TO_DELETE_REVIEW,"Are you sure want to delete the review?"),UIAlertControllerStyle.Alert);
            controller.AddAction(UIAlertAction.Create(NSBundle.MainBundle.LocalizedString(LocalizableWords.NO, "No"), UIAlertActionStyle.Default, action => { }));
            controller.AddAction(UIAlertAction.Create(NSBundle.MainBundle.LocalizedString(LocalizableWords.YES, "Yes"), UIAlertActionStyle.Default,async action =>
            {
                _activityIndicator.Alpha = 0.3f;
                _activityIndicator.Animating = true;
                try
                {
                    _tokenSource = _tokenSource ?? new CancellationTokenSource();
                    await Auth.Instance.CurrentUser.DeleteReviewSet(reviewSet.Id, _tokenSource);
                    var index = _reviewSets.IndexOf(reviewSet);
                    _reviewSets.RemoveAt(index);
                    tableView.BeginUpdates();
                    tableView.DeleteRows(new[] { NSIndexPath.FromItemSection(index, 0) }, UITableViewRowAnimation.Automatic);
                    tableView.EndUpdates();
                    _selectedItem = null;
                    if(!_reviewSets.Any())
                    {
                        _noReviewsView.Hidden = false;
                    }
                }
                catch (ExamAppException e)
                {
                    IosExamApp.ShowToastAlert("Error!", e.Message, this);
                }
                catch (OperationCanceledException e)
                {

                }
                catch (Exception e)
                {
                    IosExamApp.ShowToastAlert("Error!", "Unknown Error Occurs!", this);
                }

                _activityIndicator.Animating = false;
            }));
            PresentViewController(controller,true,null);
        }

        public async void LoadOrUpdateReviews(int? selReviewId = null)
        {
            var v = View;
            if (_activityIndicator.Animating)
            {
                return;
            }

            _activityIndicator.Alpha = 1;
            _activityIndicator.Animating = true;
         
            try
            {
                _tokenSource = new CancellationTokenSource();
                var reviewSets = (await Auth.Instance.CurrentUser.GetReviewSets()).ToList();
//                _emptyNotifactionGroup.Visibility = reviewSets.Any() ? ViewStates.Gone : ViewStates.Visible;
                _reviewSets.Clear();

                _reviewSets.AddRange(reviewSets);

//                var recyclerAdapterItems = _recyclerAdapter.Items.ToList();
                if (_reviewSets.Any())
                {
                    var indexPath = NSIndexPath.FromRowSection(selReviewId != null ? 
                        _reviewSets.FindIndex(set => set.Id == selReviewId) : 0,0);
                    
                    tableView.ReloadData();
                    tableView.SelectRow(indexPath
                        ,false,UITableViewScrollPosition.Middle);
                    _tableViewDelegate.RowSelected(tableView,indexPath);

                    _noReviewsView.Hidden = true;
                }
                else{

                    _noReviewsView.Hidden = false;

                
                }

            }
            catch (Exception e)
            {

            }

            _activityIndicator.Animating = false;

        }

       

        

        private class TableViewDelegate:UITableViewDelegate
        {
            public event Action<NSIndexPath> OnSelected; 
            private readonly UIView _headerView;

           

            public override UIView GetViewForHeader(UITableView tableView, nint section)
            {
                _headerView.LayoutIfNeeded();
                return _headerView;
            }

            public override nfloat GetHeightForHeader(UITableView tableView, nint section)
            {
                return  _headerView.Frame.Height;
            }

            public TableViewDelegate(UIView headerView)
            {
                this._headerView = headerView;
            }

            public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
            {
//                base.RowSelected(tableView, indexPath);
                OnSelected?.Invoke(indexPath);
            }
        }


        private struct CalenderAndQuestionText
        {
            public UILabel CalenderLabel{ get; set; }
            public UILabel QuestionLabel{ get; set; }
        }
    }


}