﻿using Foundation;
using System;
using Exam_App_Native.iOS.View;
using UIKit;

namespace Exam_App_Native.iOS
{
    public partial class SignUpViewController : UIViewController
    {
        private ActivityIndicator _activityIndicator;

        public SignUpViewController (IntPtr handle) : base (handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            _activityIndicator = new ActivityIndicator(View);
        }

        public override void TouchesEnded(NSSet touches, UIEvent evt)
        {
            base.TouchesEnded(touches, evt);
            View.EndEditing(true);
        }

        async partial void SignUpButton_TouchUpInside(ShadowButton sender)
        {
            string error = null;
            if (string.IsNullOrEmpty(nameLabel.Text) 
                || string.IsNullOrEmpty(emailLabel.Text) 
                || string.IsNullOrEmpty(passwordLabel.Text))
            {
                error = "Some of the Fields are Empty!";
            }
            else if (!Utils.IsValidEmail(emailLabel.Text))
            {
                error = "Email is not valid!";
            }
            else if (passwordLabel.Text.Length < 8)
            {
                error = "Password Length Must be atleast 8";
            }

            if (error != null)
            {
                 IosExamApp.ShowToastAlert("Error!",error,this);
                return;
            }

            _activityIndicator.Alpha = 0.4f;
            _activityIndicator.Animating = true;
            try
            {
                await Auth.Instance.Regiser(nameLabel.Text, emailLabel.Text, passwordLabel.Text);
                PerformSegue("CodeConfirmationVC",null);
                //DismissViewController(false,null);
            }
            catch (NotConfirmedException e)
            {
                IosExamApp.ShowToastAlert("Error!"
                    ,"This Email Already Registed!.Please Confirm the Code"
                    ,this);
                
            }
            catch (Exception e)
            {
                IosExamApp.ShowToastAlert("Error!", e.Message, this);
            }

            _activityIndicator.Animating = false;
        }


    }
}