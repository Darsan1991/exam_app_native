﻿using System.Collections.Generic;
using System.Linq;
using UIKit;

namespace Exam_App_Native.iOS.Adapter
{
    public class SimplePageViewControllerDataSource : UIPageViewControllerDataSource
    {
        private readonly List<UIViewController> _controllers = new List<UIViewController>();

        public IEnumerable<UIViewController> Controllers
        {
            get => _controllers;
            set
            {
                _controllers.Clear();
                _controllers.AddRange(value);
            }
        }


        public override UIViewController GetPreviousViewController(UIPageViewController pageViewController,
            UIViewController referenceViewController)
        {
            var controller = pageViewController.ViewControllers.FirstOrDefault();

            if (controller == null)
                return null;

            var index = _controllers.IndexOf(controller);

            return index > 0 ? _controllers[index - 1] : null;
        }

        public override UIViewController GetNextViewController(UIPageViewController pageViewController,
            UIViewController referenceViewController)
        {
            var controller = pageViewController.ViewControllers.FirstOrDefault();

            if (controller == null)
                return null;

            var index = _controllers.IndexOf(controller);

            return index < _controllers.Count-1 ? _controllers[index + 1] : null;
        }
    }
}