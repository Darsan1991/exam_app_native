﻿using System;
using System.Collections.Generic;
using System.Linq;
using Exam_App_Native.Model;
using Foundation;
using UIKit;

namespace Exam_App_Native.iOS.Adapter
{
    public class SimpleTableViewAdapter<T, H>:TableViewAdapter where H:UITableViewCell,IHolder<object>
    {
        public event Action<H> OnDequeueReuseCell;
        public event Action<H,T> OnBoforeBindCell;
        public event Action<T> OnItemClicked; 

        public virtual IEnumerable<T> Items
        {
            get => itemList;
            set
            {
                itemList.Clear();
                if (value != null)
                {
                    itemList.AddRange(value);
                }

                ApplyFilter(Filter);
              
            }
        }


        public virtual Predicate<T> Filter
        {
            private get { return _filter; }
            set
            {
                _filter = value;
                ApplyFilter(value);

            }
        }


        protected readonly string tableViewCellIdentifier;
        protected readonly List<T> itemList = new List<T>();
        protected readonly List<T> activeItemList = new List<T>();
        private Predicate<T> _filter;

        public SimpleTableViewAdapter(string tableViewCellIdentifier,IEnumerable<T> items = null)
        {
            this.tableViewCellIdentifier = tableViewCellIdentifier;
            if (items != null)
            {
                // ReSharper disable once VirtualMemberCallInConstructor
                Items = items.ToList();
            }
        }

        public override nint RowsInSection(UITableView tableView, nint section)
        {
            TableView = TableView ?? tableView;
            return itemList.Count;
        }

        public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
        {
            OnItemClicked?.Invoke(itemList[indexPath.Row]);
            tableView.CellAt(indexPath).Selected = false;
//            base.RowSelected(tableView, indexPath);
        }


        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            var holder = (H)tableView.DequeueReusableCell(tableViewCellIdentifier);
            OnDequeueReuseCell?.Invoke(holder);
            OnBoforeBindCell?.Invoke(holder, itemList[indexPath.Row]);
            holder.Item = itemList[indexPath.Row];

            return holder;
        }

        protected virtual void ApplyFilter(Predicate<T> value)
        {
            activeItemList.Clear();
            activeItemList.AddRange(_filter == null ? itemList : itemList.FindAll(value));
            TableView?.ReloadData();

        }


        public void RemoveItem(T item)
        {
            itemList.Remove(item);
            var index = activeItemList.FindIndex(obj => Equals(item, obj));
            if (index != -1)
            {
                activeItemList.RemoveAt(index);
                TableView?.BeginUpdates();
                TableView?.DeleteRows(new []{NSIndexPath.FromItemSection(index,0)},UITableViewRowAnimation.Automatic);
                TableView?.EndUpdates();
            }
        }

        public int GetIndexForItem(T item) => activeItemList.IndexOf(item);
    }

    public class SelectableTableViewAdapter<T, TH> : SimpleTableViewAdapter<T, TH> where TH : UITableViewCell, ISelectableHolder<object>
    {
        public event Action<T> OnSelectionChanged;

        public T Selected
        {
            get
            {
                if (_selected == null)
                {
                    return _selected;
                }

                return activeItemList.Contains(_selected) ? _selected : default(T);
            }
            set
            {
                if (Equals(_selected, value))
                {
                    return;
                }
                if (_selected != null)
                {
                    _itemVsSelectedDict[_selected] = false;
                    var cell = TableView?.CellAt(NSIndexPath.FromItemSection(GetIndexForItem(_selected),0)) as ISelectableHolder<object>;
                    if (cell!=null)
                    {
                        cell.SelectedItem = false;
                    }
                }
                _selected = value;
                if (_selected != null)
                {
                    _itemVsSelectedDict[_selected] = true;
                    var cell = TableView?.CellAt(NSIndexPath.FromItemSection(GetIndexForItem(_selected), 0)) as ISelectableHolder<object>;
                    if (cell != null)
                    {
                        cell.SelectedItem = true;
                    }
                }

                OnSelectionChanged?.Invoke(Selected);
            }
        }

        public override IEnumerable<T> Items
        {
            get => base.Items;
            set
            {
                foreach (var item in value)
                {
                    if (!_itemVsSelectedDict.ContainsKey(item))
                    {
                        _itemVsSelectedDict.Add(item, false);
                    }
                }
                base.Items = value;
                //TODO: Try to change later
                Selected = _selected;

            }
        }

        private T _selected;
        private readonly Dictionary<T, bool> _itemVsSelectedDict = new Dictionary<T, bool>();


        public SelectableTableViewAdapter(string tableViewCellIdentifier, IEnumerable<T> items = null) : base(tableViewCellIdentifier, items)
        {
            OnItemClicked += obj => { Selected = obj; };
        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            var cell = base.GetCell(tableView, indexPath);
            ((ISelectableHolder<object>)cell).SelectedItem = _itemVsSelectedDict.ContainsKey(activeItemList[indexPath.Row]) 
                                                             && _itemVsSelectedDict[activeItemList[indexPath.Row]];
            return cell;
        }

        protected override void ApplyFilter(Predicate<T> value)
        {
            var selected = Selected;
            base.ApplyFilter(value);
            if (!Equals(selected, Selected))
            {
                OnSelectionChanged?.Invoke(Selected);
            }
        }
    }
}