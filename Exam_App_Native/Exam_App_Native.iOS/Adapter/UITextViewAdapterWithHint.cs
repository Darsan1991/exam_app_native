﻿using System;
using UIKit;

namespace Exam_App_Native.iOS.Adapter
{
    // ReSharper disable once InconsistentNaming
    public class UITextViewAdapterWithHint : UITextViewDelegate
    {


        public string Hint { get; set; }
        public UIColor NormalColor { get; set; }
        public UIColor HintColor { get; set; }



        public override void EditingStarted(UITextView textView)
        {
            if (string.Equals(textView.Text, Hint, StringComparison.CurrentCultureIgnoreCase))
            {
                textView.Text = "";
            }
            UpdateColor(textView);

            textView.BecomeFirstResponder();
        }

        private void UpdateColor(UITextView textView)
        {
            textView.TextColor = !string.Equals(textView.Text, Hint, StringComparison.CurrentCultureIgnoreCase) ? NormalColor : HintColor;
        }

        public override void EditingEnded(UITextView textView)
        {

            if (string.IsNullOrEmpty(textView.Text))
            {

                textView.Text = Hint;
            }
            UpdateColor(textView);

            textView.ResignFirstResponder();
        }

        // ReSharper disable once IdentifierTypo
        public void SettedText(UITextView textView)
        {
            if (string.IsNullOrEmpty(textView.Text))
            {

                textView.Text = Hint;
            }
            UpdateColor(textView);
        }
    }
}