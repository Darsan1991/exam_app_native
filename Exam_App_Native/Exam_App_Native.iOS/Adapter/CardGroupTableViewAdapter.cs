﻿using System.Collections.Generic;
using Exam_App_Native.Model.ViewModel;

namespace Exam_App_Native.iOS.Adapter
{
    public class CardGroupTableViewAdapter:SimpleTableViewAdapter<IItemGroup<object, ICard<object>>, CardGroupViewCell>
    {
        public CardGroupTableViewAdapter(string tableViewCellIdentifier, IEnumerable<IItemGroup<object, ICard<object>>> items = null) : base(tableViewCellIdentifier, items)
        {

        }
    }

    public class CardCollectionViewAdapter : SimpleCollectionViewAdapter<ICard<object>, CardCollectionCell>
    {
        public CardCollectionViewAdapter(string tableViewCellIdentifier, IEnumerable<ICard<object>> items = null) : base(tableViewCellIdentifier, items)
        {

        }
    }
}