﻿using System;
using Foundation;
using UIKit;

namespace Exam_App_Native.iOS.Adapter
{
    public abstract class TableViewAdapter : UITableViewDelegate, IUITableViewDataSource
    {
        public abstract nint RowsInSection(UITableView tableView, nint section);
        public abstract UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath);
        public UITableView TableView { get; set; }
    }
}