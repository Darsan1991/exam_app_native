﻿using System;
using System.Collections.Generic;
using Exam_App_Native.Model;
using Foundation;
using UIKit;

namespace Exam_App_Native.iOS.Adapter
{
    public class SimpleCollectionViewAdapter<T, H> : UICollectionViewDelegate,IUICollectionViewDataSource where H : UICollectionViewCell, IHolder<object>
    {
        public event Action<H> OnDequeueReuseCell;
        public event Action<T> OnItemClicked;

        public IEnumerable<T> Items
        {
            get => _itemList;
            set
            {
                _itemList.Clear();
                if (value != null)
                {
                    _itemList.AddRange(value);
                }
            }
        }

        private readonly string _collectionViewCellIdentifier;
        private readonly List<T> _itemList = new List<T>();

        public SimpleCollectionViewAdapter(string collectionViewCellIdentifier, IEnumerable<T> items = null)
        {
            _collectionViewCellIdentifier = collectionViewCellIdentifier;
            _itemList.AddRange(items ?? new List<T>());
        }


        public nint GetItemsCount(UICollectionView collectionView, nint section) => _itemList.Count;

        public UICollectionViewCell GetCell(UICollectionView collectionView, NSIndexPath indexPath)
        {
            var viewCell = (H) collectionView.DequeueReusableCell(_collectionViewCellIdentifier,indexPath);
            OnDequeueReuseCell?.Invoke(viewCell);
            viewCell.Item = _itemList[indexPath.Row];
            return viewCell;
        }

        public override void ItemSelected(UICollectionView collectionView, NSIndexPath indexPath)
        {
//            base.ItemSelected(collectionView, indexPath);
            OnItemClicked?.Invoke(_itemList[indexPath.Row]);
        }
    }
}