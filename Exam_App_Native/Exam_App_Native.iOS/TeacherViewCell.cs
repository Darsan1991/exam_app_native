﻿using Foundation;
using System;
using System.Threading;
using Exam_App_Native.iOS.General;
using Exam_App_Native.iOS.View;
using Exam_App_Native.Model;
using UIKit;

namespace Exam_App_Native.iOS
{
    public partial class TeacherViewCell : UITableViewCell,IHolder<object>
    {
        private Teacher _teacher;
        private ActivityIndicator _activityIndicator;
        private CancellationTokenSource _tokenSource;

        public Teacher Teacher
        {
            get => _teacher;
            set
            {
                //TODO:Have to check the id instead of the object
                if (_teacher==value)
                {
                    return;
                }

                imageView.ClipsToBounds = true;
                nameLabel.Text = value.Name;
                jobLabel.Text = value.JobTitle;
                subjectLabel.Text = value.Disciplines;
                LoadAndSetImage(value.ImageUrl, value.Name);
                _teacher = value;
            }
        }

        public object Item { get => Teacher; set => Teacher = value as Teacher; }


        public bool LoadingImage
        {
            get => _activityIndicator.Animating;
            private set =>
                _activityIndicator.Animating = value;
        }

        public TeacherViewCell (IntPtr handle) : base (handle)
        {
        }

        public override void AwakeFromNib()
        {
            base.AwakeFromNib();
            imageView.Layer.CornerRadius = imageView.Frame.Width / 2;
             _activityIndicator = new ActivityIndicator(imageView,0f,style: UIActivityIndicatorViewStyle.White);
        }

        private async void LoadAndSetImage(string url, string suggName)
        {
            LoadingImage = true;
            try
            {
                _tokenSource = new CancellationTokenSource();
                var image = await RemoteFileManager.Instance
                    .DownloadImageAsync(url, $"teacher{suggName}",_tokenSource);
                LoadingImage = false;
                imageView.Image = image;

            }
            catch (Exception e)
            {
                
            }

        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            _tokenSource.Cancel();
        }
    }
}