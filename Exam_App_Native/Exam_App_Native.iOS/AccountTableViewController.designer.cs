// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace Exam_App_Native.iOS
{
    [Register ("AccountTableViewController")]
    partial class AccountTableViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        Exam_App_Native.iOS.ShadowButton advancedBtn { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton cancelButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel dateLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel emailLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIBarButtonItem homeBtn { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView mixUserCardsImageView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel nameLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        Exam_App_Native.iOS.ShadowButton resetPasswordButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView skipPositiveImgView { get; set; }

        [Action ("AdvancedBtn_TouchUpInside:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void AdvancedBtn_TouchUpInside (Exam_App_Native.iOS.ShadowButton sender);

        [Action ("CancelButton_TouchUpInside:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void CancelButton_TouchUpInside (UIKit.UIButton sender);

        [Action ("ResetPasswordButton_TouchUpInside:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void ResetPasswordButton_TouchUpInside (Exam_App_Native.iOS.ShadowButton sender);

        void ReleaseDesignerOutlets ()
        {
            if (advancedBtn != null) {
                advancedBtn.Dispose ();
                advancedBtn = null;
            }

            if (cancelButton != null) {
                cancelButton.Dispose ();
                cancelButton = null;
            }

            if (dateLabel != null) {
                dateLabel.Dispose ();
                dateLabel = null;
            }

            if (emailLabel != null) {
                emailLabel.Dispose ();
                emailLabel = null;
            }

            if (homeBtn != null) {
                homeBtn.Dispose ();
                homeBtn = null;
            }

            if (mixUserCardsImageView != null) {
                mixUserCardsImageView.Dispose ();
                mixUserCardsImageView = null;
            }

            if (nameLabel != null) {
                nameLabel.Dispose ();
                nameLabel = null;
            }

            if (resetPasswordButton != null) {
                resetPasswordButton.Dispose ();
                resetPasswordButton = null;
            }

            if (skipPositiveImgView != null) {
                skipPositiveImgView.Dispose ();
                skipPositiveImgView = null;
            }
        }
    }
}