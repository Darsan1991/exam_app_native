﻿using Foundation;
using System;
using Exam_App_Native.Model;
using Exam_App_Native.Model.ViewModel;
using UIKit;
using CoreGraphics;

namespace Exam_App_Native.iOS
{
    public partial class TeacherDetailsCardViewCell : UITableViewCell, IHolder<object>
    {
        private float _textViewLeadingConstraintConstant;
        private object _item;

        public TeacherDetailsCardViewCell(IntPtr handle) : base(handle)
        {
        }

        public object Item
        {
            get => _item;
            set
            {
                ItemContainer = value as IItem<object> ?? throw new Exception("This Item type should be IItem<object>");
                _item = value;
            }
        }

        public IItem<object> ItemContainer
        {
            get => _item as IItem<object>;
            private set
            {
                if (_item == value || value == null)
                {
                    return;
                }

                titleLabel.Text = value.Title;
                contentTextView.Text = value.SubTitle;

                _item = value;
            }
        }

        public float TextViewLeadingConstraintConstant
        {
            get => _textViewLeadingConstraintConstant;
            set
            {
                if (IsViewLoaded)
                {
                    textViewLeadingConstraint.Constant = value;
                }

                _textViewLeadingConstraintConstant = value;
            }
        }

        public bool IsViewLoaded { get; set; }

        public override void AwakeFromNib()
        {
            base.AwakeFromNib();
            contentTextView.Editable = false;
            contentTextView.Selectable = false;
            contentTextView.DataDetectorTypes = UIDataDetectorType.All;
            IsViewLoaded = true;

            actualContainerView.Layer.MasksToBounds = false;
            actualContainerView.Layer.ShadowOffset = new CGSize(0, 1.5);
            actualContainerView.Layer.ShadowRadius = 0.8f;
            actualContainerView.Layer.ShadowOpacity = 0.2f;

            textViewLeadingConstraint.Constant = TextViewLeadingConstraintConstant;
        }
    }
}