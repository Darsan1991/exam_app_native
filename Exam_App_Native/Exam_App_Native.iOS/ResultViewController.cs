﻿using Foundation;
using System;
using System.Threading.Tasks;
using Exam_App_Native.iOS.View;
using Exam_App_Native.Model;
using UIKit;

namespace Exam_App_Native.iOS
{
    public partial class ResultViewController : UIViewController
    {
        public event Action OnClickNextOrComplete;

        private static readonly UIColor CORRECT_COLOR = UIColor.FromRGB(123f/255,212f/255,110f/255);
        private static readonly UIColor WRONG_COLOR = UIColor.FromRGB(239f / 255, 67f / 255, 58f / 255); 

        private ActivityIndicator _activityIndicator;
        private Task<CardEvalResultInfo> _task;
        private ActionButton _currentActionButton;
        private bool _answredCorrectly;

        private ActionButton CurrentActionButton
        {
            get => _currentActionButton;
            set
            {
                if (IsViewLoaded)
                {
                    UpdateButtonUserIntForAction(value, AnswredCorrectly);
                    
                }
                _currentActionButton = value;
            }
        }

        private bool AnswredCorrectly
        {
            get => _answredCorrectly;

            set
            {
                if (IsViewLoaded)
                {
                    UpdateButtonUserIntForAction(CurrentActionButton, value);
                    imageView.Image = UIImage.FromBundle(value ? "right_img" : "wrong_img");
                }
                _answredCorrectly = value;
            }
        }

        public CardEvalResultInfo CardEvalResultInfo => _task.IsCompleted ? _task.Result : null;




        public ResultViewController (IntPtr handle) : base (handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            _activityIndicator = new ActivityIndicator(View, 1f)
            {
                Animating = (_task != null && (!_task.IsCompleted && !_task.IsCanceled && !_task.IsFaulted))
            };


            UpdateButtonUserIntForAction(CurrentActionButton, AnswredCorrectly);

            if (_task != null && _task.IsCompleted)
            {
                UpdateTheUiForResult(_task.Result);
            }

            nextBtn.TouchUpInside += (sender, args) => OnClickNextOrComplete?.Invoke();
        }


        public async void SetResult(Task<CardEvalResultInfo> task)
        {
            if (IsViewLoaded)
                _activityIndicator.Animating = true;
            _task = task;
            try
            {
                var resultInfo = await task;
                UpdateTheUiForResult(resultInfo);
            }
            catch (Exception e)
            {
                // ignored
            }

            if (IsViewLoaded)
                _activityIndicator.Animating = false;
        }

        private void UpdateTheUiForResult(CardEvalResultInfo info)
        {
            titleLabel.Text = info.CurrentCard.Subject;
            subtitleLabel.Text = info.CurrentCard.Topic;
            teacherLabel.Text = info.CurrentCard.Teacher;

            contentTextView.Text = info.Comment;
            feedbackLabel.Text = info.Feedback;
            AnswredCorrectly = info.AnswredCorrectly;
            CurrentActionButton = info.IsReviewFinished ? ActionButton.Complete : ActionButton.Next;
        }


        private void UpdateButtonUserIntForAction(ActionButton actionButton, bool answredCorrectly)
        {
            nextBtn.SetTitle(actionButton != ActionButton.Complete ? "NEXT" : "COMPLETE",UIControlState.Normal);
            nextBtn.BackgroundColor = answredCorrectly?CORRECT_COLOR:WRONG_COLOR;
        }

        private enum ActionButton
        {
            Next, Complete
        }
    }
}