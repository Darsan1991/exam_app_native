﻿using Foundation;
using System;
using CoreText;
using Exam_App_Native.Model;
using UIKit;

namespace Exam_App_Native.iOS
{
    public partial class FlashCardViewCell : UITableViewCell,IHolder<object>
    {
        public object Item
        {
            get => MyCard;
            set
            {
                if (value is MyCard card)
                {

                    if (IsViewLoaded)
                    {
                        titleLabel.Text = card.Content;
                        var bundle = NSBundle.MainBundle;
                        var attributedString = new NSMutableAttributedString($"{bundle.LocalizedString(LocalizableWords.ANSWER,"Answer")}:");
                        attributedString.Append(new NSAttributedString((card.Answer ?? false) ?bundle.LocalizedString(LocalizableWords.TRUE,"True") :bundle.LocalizedString(LocalizableWords.FALSE,"False"), new UIStringAttributes
                        {
                            ForegroundColor = (card.Answer ?? false) ? UIColor.Blue : UIColor.Red
                        }));
                        attributedString.Append(new NSAttributedString($",  {bundle.LocalizedString(LocalizableWords.TOPIC,"Topic")}:", new UIStringAttributes
                        {
                            ForegroundColor = UIColor.Gray
                        }));
                        attributedString.Append(new NSAttributedString(card.Topic.Name, new UIStringAttributes
                        {
                            ForegroundColor = UIColor.DarkGray
                        }));
                        detailsLabel.AttributedText = attributedString;
                    }
                    MyCard = card;
                }
            }
        }

        public MyCard MyCard { get; set; }
        public bool IsViewLoaded { get;private set; }

        public FlashCardViewCell (IntPtr handle) : base (handle)
        {
        }

        public override void AwakeFromNib()
        {
            base.AwakeFromNib();
            IsViewLoaded = true;
            //TODO:Try to solve in different way
            if (Item!=null)
            {
                Item = Item;
            }
        }
    }
}